/* Copyright (C) 2016, 2017, 2020, 2024 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>

struct fv_app_sq_midpoint_t
{
  int num_squares;
  int num_args;
  mpz_t args[3];
  void (*dump_func) (mpz_t *, FILE *);
};

static void
dump_binary_num (mpz_t *i, FILE *out)
{
  mpz_out_raw (out, *i);
}

static void
dump_num (mpz_t *i, FILE *out)
{
  char *buf = malloc (mpz_sizeinbase (*i, 10) + 2);
  mpz_get_str (buf, 10, *i);
  fprintf (out, "%s", buf);
  fprintf (out, "\n");
  free (buf);
}

static void
calc_midpoint (struct fv_app_sq_midpoint_t *app, mpz_t *root, mpz_t *num, FILE *out)
{
  mpz_t lo, hi, hiroot, res;
  mpz_inits (lo, hi, hiroot, res, NULL);

  mpz_set (lo, *num);
  mpz_set (hi, lo);
  mpz_set (hiroot, *root);

  for (int i = 0; i < app->num_squares; i++)
    {
      mpz_add (hi, hi, hiroot);
      mpz_add (hi, hi, hiroot);
      mpz_add_ui (hi, hi, 1);
      mpz_add_ui (hiroot, hiroot, 1);
    }

  mpz_sub (res, hi, lo);
  mpz_cdiv_q_ui (res, res, 2);
  mpz_add (res, res, lo);

  app->dump_func (&res, out);
  mpz_clears (lo, hi, hiroot, res, NULL);
}

static int
sq_midpoint (struct fv_app_sq_midpoint_t *app, mpz_t start, mpz_t finish, mpz_t incr, FILE *out)
{
  mpz_t i, root, lastroot;
  mpz_inits (i, root, lastroot, NULL);
  mpz_set (i, start);
  mpz_sqrt (root, i);
  if (mpz_cmp_ui (root, 0) == 0)
    mpz_add_ui (root, root, 1);
  mpz_mul (i, root, root);
  mpz_sqrt (lastroot, finish);

  calc_midpoint (app, &root, &i, out);

  while (mpz_cmp (root, lastroot) < 0)
    {
      for (int j = 0; mpz_cmp_ui (incr, j) > 0; j++)
        {
          mpz_add (i, i, root);
          mpz_add (i, i, root);
          mpz_add_ui (i, i, 1);
          mpz_add_ui (root, root, 1);
        }
      calc_midpoint (app, &root, &i, out);
    }
  mpz_clears (i, root, lastroot, NULL);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_sq_midpoint_t *app = (struct fv_app_sq_midpoint_t *) state->input;
  switch (key)
    {
    case 'n':
      app->num_squares = atoi (arg);
      break;
    case 'o':
      app->dump_func = dump_binary_num;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 3)
        argp_error (state, "too many arguments");
      else
        {
          mpz_set_str (app->args[app->num_args], arg, 10);
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      app->dump_func = dump_num;
      for (int i = 0; i < 3; i++)
        mpz_init (app->args[i]);
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_NO_ARGS:
      argp_error (state, "missing argument.");
      break;
    }
  return 0;
}

int
fituvalu_sq_midpoint (struct fv_app_sq_midpoint_t *app, FILE *out)
{
  mpz_t one;
  mpz_init (one);
  mpz_set_ui (one, 1);

  int ret = 0;

  switch (app->num_args)
    {
    case 1:
      ret = sq_midpoint (app, one, app->args[0], one, out);
      break;
    case 2:
      ret = sq_midpoint (app, app->args[0], app->args[1], one, out);
      break;
    case 3:
      ret = sq_midpoint (app, app->args[0], app->args[2], app->args[1], out);
      break;
    }

  mpz_clear (one);
  return ret;
}

static struct argp_option
options[] =
{
  { "num-squares", 'n', "NUM", 0, "Distance between squares to take midpoint of (default 2)"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "LAST\nFIRST LAST\nFIRST INCREMENT LAST",
  "Compute the mid-point between a sequence of perfect squares.\vIf FIRST or INCREMENT is omitted, it defaults to 1.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_sq_midpoint_t app;
  memset (&app, 0, sizeof (app));
  app.num_squares = 2;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_sq_midpoint (&app, stdout);

}
