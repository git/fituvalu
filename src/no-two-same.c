/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <stdbool.h>
#include "magicsquareutil.h"

struct fv_app_no_two_same_t
{
  int num_columns;
  void (*display_numbers) (mpz_t *s, int, FILE *out);
  int (*read_numbers) (FILE *, mpz_t *, int, char **, size_t *);
};

static bool
no_two_same (mpz_t *a, int size)
{
  bool found = false;
  for (int i = 0; i < size; i++)
    {
      for (int j = 0; j < i; j++)
        {
          if (i == j)
            continue;
          if (mpz_cmp (a[i], a[j]) == 0)
            {
              found = true;
              break;
            }
        }
    }
  return !found;
}

int
fituvalu_no_two_same (struct fv_app_no_two_same_t *app, FILE *in, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[app->num_columns];

  for (int i = 0; i < app->num_columns; i++)
    mpz_init (a[i]);

  while (1)
    {
      read = app->read_numbers (in, a, app->num_columns, &line, &len);
      if (read == -1)
        break;
      if (no_two_same (a, app->num_columns))
        app->display_numbers (a, app->num_columns, out);
    }

  for (int i = 0; i < app->num_columns; i++)
    mpz_clear (a[i]);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_no_two_same_t *app = (struct fv_app_no_two_same_t *) state->input;
  switch (key)
    {
    case 'n':
      app->num_columns = atoi (arg);
      break;
    case 'i':
      app->read_numbers = binary_read_numbers_from_stream;
      break;
    case 'o':
      app->display_numbers = disp_binary_record;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "num-columns", 'n', "NUM", 0, "A record has NUM columns (default 3)"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept three numbers separated by commas and only show them if no two are the same.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_no_two_same_t app;
  memset (&app, 0, sizeof (app));
  app.display_numbers = disp_record;
  app.read_numbers = read_numbers_from_stream;
  app.num_columns = 3;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_no_two_same (&app, stdin, stdout);
}
