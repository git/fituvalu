/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <argp.h>
#include <gmp.h>
#include <string.h>
#include "magicsquareutil.h"
enum type_square_mode_enum_t
{
  MODE_SHOW_TYPE,
  MODE_FILTER,
  MODE_TOTAL,
};

struct filter
{
  int square;
  int type;
  int subtype;
};

struct fv_app_type_square_t
{
  enum type_square_mode_enum_t mode;
  int num_filters;
  struct filter *filter;
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int show_subtype;
};
/*
 how to generate the lookup tables:
 the doit.sh script:
#!/bin/bash
while IFS='' read -r line || [[ -n "$line" ]]; do
  tmpfile=`mktemp`
  echo $line | ./rotate-square > $tmpfile
  type=""
  while IFS='' read -r rline || [[ -n "$rline" ]]; do
    type=`grep -n "^$rline$" $2 | cut -f1 -d:`
    if [ "x$type" != "x" ]; then
      break
    fi
  done < "$tmpfile"
  echo -n "[0b"
  echo -n "$line" | sed -e 's/[, ]//g'
  echo "] = $type,"
  rm $tmpfile
done < "$1"

then run:
echo "1,0,0,0,0,0,0,0,0" | ./permute-square -S | sort | uniq > 1
echo "1,0,0,0,0,0,0,0,0" | ./permute-square -S | sort | uniq | ./uniq-squares > 1.uniq
./doit.sh 1 1.uniq
echo "1,1,0,0,0,0,0,0,0" | ./permute-square -S | sort | uniq > 2
echo "1,1,0,0,0,0,0,0,0" | ./permute-square -S | sort | uniq | ./uniq-squares > 2.uniq
./doit.sh 2 2.uniq
echo "1,1,1,0,0,0,0,0,0" | ./permute-square -S | sort | uniq > 3
echo "1,1,1,0,0,0,0,0,0" | ./permute-square -S | sort | uniq | ./uniq-squares > 3.uniq
./doit.sh 3 3.uniq
and so on.

*/

static int
count_set_bits (int i)
{
  return __builtin_popcount (i);
}

static void
show_type (int num_squares, int type, FILE *out)
{
  type = map_from_bremner (num_squares, type);
  for (int i = 0; i < 512; i++)
    {
      if (count_set_bits (i) == num_squares && get_type_index (i) == type)
        {
          int num = i;
          int mask = 512;
          int s[3][3];
          memset (s, 0, sizeof (s));
          int row = 0;
          for (int j = 0; j < 9; j++)
            {
              mask = mask >> 1;
              s[row][j % 3] = num & mask ? 1 : 0;
              if (j % 3 == 2)
                row++;
            }
          fprintf (out, "  +---+---+---+\n");
          fprintf (out, "  | %c | %c | %c |\n", s[0][0] ? 'X' : ' ',
                   s[0][1] ? 'X' : ' ', s[0][2] ? 'X' : ' ');
          fprintf (out, "  +---+---+---+\n");
          fprintf (out, "  | %c | %c | %c |\n", s[1][0] ? 'X' : ' ',
                   s[1][1] ? 'X' : ' ', s[1][2] ? 'X' : ' ');
          fprintf (out, "  +---+---+---+\n");
          fprintf (out, "  | %c | %c | %c |\n", s[2][0] ? 'X' : ' ',
                   s[2][1] ? 'X' : ' ', s[2][2] ? 'X' : ' ');
          fprintf (out, "  +---+---+---+\n");

          break;
        }
    }
}

static int
parse_type (struct argp_state *state, char *arg, int *squares, int *type, int *subtype)
{
  int got_subtype = 0;
  int retval = sscanf (arg, "%d:%d:%d", squares, type, subtype);
  if (retval == 3)
    {
      if (*squares != 6)
        argp_error (state, "subtype only makes sense when NUM_SQUARES is 6.");
      got_subtype = 1;
    }
  else if (retval != 2)
    argp_error (state, "bad format in -f. expecting \"NUM_SQUARES:TYPE\".");

  if (*squares < 1 || *squares > 9)
    argp_error (state, "number of squares must be between 1 and 9.");
  else if (*type < 1 || *type > 23)
    argp_error (state, "type must be between 1 and 23.");
  if (got_subtype)
    {
      switch (*type)
        {
        case 1:
        case 2:
        case 4:
        case 5:
        case 6:
        case 12:
        case 13:
        case 14:
        case 16:
          if (*subtype < 1 || *subtype > 8)
            argp_error (state, "sub type must be between 1 and 8.");
          break;
        case 3:
          if (*subtype < 1 || *subtype > 4)
            argp_error (state, "sub type must be between 1 and 4.");
          break;
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 15:
          if (*subtype < 1 || *subtype > 16)
            argp_error (state, "sub type must be between 1 and 16.");
          break;
        }
    }
  return 1;
}

static void
display_subtype (int type, int subtype, FILE *out)
{
  int num = 0;
  SubTypeArrayType* lookup = get_subtypes (type, &num);
  if (!lookup)
    return;
  int cfg[9][2];
  memcpy (cfg, lookup[subtype - 1], sizeof (cfg));
  int idx[9];
  memset (idx, 0, sizeof (idx));
  int count = 1;
  for (int i = 0; i < 9; i++)
    {
      int row = cfg[i][0];
      int col = cfg[i][1];

      idx[(row*3) + col] = count;
      count++;
    }

  fprintf (out, "  +---+---+---+\n");
  fprintf (out, "  | %d | %d | %d |\n", idx[0], idx[1], idx[2]);
  fprintf (out, "  +---+---+---+\n");
  fprintf (out, "  | %d | %d | %d |\n", idx[3], idx[4], idx[5]);
  fprintf (out, "  +---+---+---+\n");
  fprintf (out, "  | %d | %d | %d |\n", idx[6], idx[7], idx[8]);
  fprintf (out, "  +---+---+---+\n");
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_type_square_t *app = (struct fv_app_type_square_t *) state->input;
  int square = 0, type = 0, subtype = 0;
  switch (key)
    {
    case 's':
      app->show_subtype = 1;
      break;
    case 't':
      app->mode = MODE_TOTAL;
      break;
    case 288:
      if (parse_type (state, arg, &square, &type, &subtype))
          {
            if (subtype)
              {
                FILE *fp = fopen ("/tmp/w1", "w");
                if (fp)
                  {
                    show_type (square, type, fp);
                    fclose (fp);
                  }
                fp = fopen ("/tmp/w2", "w");
                if (fp)
                  {
                    display_subtype (type, subtype, fp);
                    fclose (fp);
                  }
                system ("paste /tmp/w1 /tmp/w2");
                remove ("/tmp/w1");
                remove ("/tmp/w2");
              }
            else
              show_type (square, type, stdout);
            exit (1);
          }
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case 'f':
      if (parse_type (state, arg, &square, &type, &subtype))
        {
          app->mode = MODE_FILTER;
          app->filter =
            realloc (app->filter,
                     sizeof (struct filter) * (app->num_filters + 1));
          app->filter[app->num_filters].square = square;
          app->filter[app->num_filters].type = type;
          app->filter[app->num_filters].subtype = subtype;
          app->num_filters++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static int
filter_type_square (struct fv_app_type_square_t *app, FILE *in, FILE *out)
{
  int i, j;
  mpz_t a[3][3];
  ssize_t read;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  char* line = NULL;
  size_t len = 0;
  while (!feof (in))
    {
      read = app->read_square (in, &a, &line, &len);
      if (read == -1)
        break;

      int signature = 0;
      int num_squares = 0;
      int type = square_get_type (&a, &num_squares);
      int subtype = 0;
      if (num_squares == 6)
        subtype = square_get_subtype (&a, num_squares, type);
      for (int i = 0; i < app->num_filters; i++)
        {
          if (app->filter[i].square == num_squares &&
              app->filter[i].type == type)
            {
              if (app->filter[i].subtype == 0)
                app->display_square (a, out);
              else if (app->filter[i].subtype == subtype)
                app->display_square (a, out);
              break;
            }
        }
    }

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
  if (line)
    free (line);
  return 0;
}

static int
show_type_square (struct fv_app_type_square_t *app,  FILE *in, FILE *out)
{
  int i, j;
  mpz_t a[3][3];
  ssize_t read;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  char* line = NULL;
  size_t len = 0;
  while (!feof (in))
    {
      read = app->read_square (in, &a, &line, &len);
      if (read == -1)
        break;

      int num_squares = 0;
      int type = square_get_type (&a, &num_squares);
      if (app->show_subtype)
        {
          int subtype = square_get_subtype (&a, num_squares, type);
          fprintf (out, "%d:%d:%d\n", num_squares, type, subtype);
        }
      else
        fprintf (out, "%d:%d\n", num_squares, type);

    }
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
  if (line)
    free (line);
  return 0;
}

static void
dump_total (int count[512], FILE *out)
{
  int total[10][24];
  memset (total, 0, sizeof (total));
  for (int i = 0; i < 512; i++)
    {
      if (count[i])
        {
          int num_squares = count_set_bits (i);
          int type = get_type_from_signature (i, num_squares);
          total[num_squares][type] += count[i];
        }
    }

  int width = 0;
  char buf[64];
  unsigned long long tot = 0;
  for (int i = 0; i < 9; i++)
    {
      for (int j = 0; j < 24; j++)
        {
          if (total[i][j])
            {
              tot += total[i][j];
              snprintf (buf, sizeof (buf), "%d", total[i][j]);
              if (strlen (buf) > width)
                width = strlen (buf);
            }
        }
    }
  if (width <= 0)
    return;

  snprintf (buf, sizeof (buf), "%llu", tot);
  if (strlen (buf) > width)
    width = strlen (buf);

  snprintf (buf, sizeof (buf), " %%%dd", width);
  for (int i = 0; i < 9; i++)
    {
      for (int j = 0; j < 24; j++)
        {
          if (total[i][j])
            {
              fprintf (out, buf, total[i][j]);
              fprintf (out, " %d:%d\n", i, j);
            }
        }
    }
  fprintf (out, " %-llu total\n", tot);
}

static void
dump_subtype_total (int count[512][2], FILE *out)
{
  int total[10][24][2];
  memset (total, 0, sizeof (total));
  for (int i = 0; i < 512; i++)
    {
      for (int j = 0; j < 2; j++)
        {
          if (count[i][j])
            {
              int num_squares = count_set_bits (i);
              int type = get_type_from_signature (i, num_squares);
              total[num_squares][type][j] += count[i][j];
            }
        }
    }

  int width = 0;
  char buf[64];
  unsigned long long tot = 0;
  for (int i = 0; i < 9; i++)
    {
      for (int j = 0; j < 24; j++)
        {
          for (int k = 0; k < 2; k++)
            {
              if (total[i][j][k])
                {
                  tot += total[i][j][k];
                  snprintf (buf, sizeof (buf), "%d", total[i][j][k]);
                  if (strlen (buf) > width)
                    width = strlen (buf);
                }
            }
        }
    }
  if (width <= 0)
    return;

  snprintf (buf, sizeof (buf), "%llu", tot);
  if (strlen (buf) > width)
    width = strlen (buf);

  snprintf (buf, sizeof (buf), " %%%dd", width);
  for (int i = 0; i < 9; i++)
    {
      for (int j = 0; j < 24; j++)
        {
          for (int k = 0; k < 2; k++)
            {
              if (total[i][j][k])
                {
                  fprintf (out, buf, total[i][j][k]);
                  fprintf (out, " %d:%d:%d\n", i, j, k + 1);
                }
            }
        }
    }
  fprintf (out, " %-llu total\n", tot);
}

static int
total_type_square (struct fv_app_type_square_t *app,  FILE *in, FILE *out)
{
  int count[512];
  int subcount[512][2];
  int i, j;
  mpz_t a[3][3];
  ssize_t read;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  memset (count, 0, sizeof (count));
  memset (subcount, 0, sizeof (subcount));
  char* line = NULL;
  size_t len = 0;
  while (!feof (in))
    {
      read = app->read_square (in, &a, &line, &len);
      if (read == -1)
        break;

      int signature = 0;
      int num_squares = 0;
      for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
          {
            signature <<= 1;
            if (mpz_perfect_square_p (a[i][j]))
              {
                num_squares++;
                signature |=  0x1;
              }
          }
      count[signature]++;
      if (app->show_subtype)
        {
          int num_squares = 0;
          int type = square_get_type (&a, &num_squares);
          if (num_squares == 6)
            {
              int subt = square_get_subtype (&a, num_squares, type);
              subcount[signature][subt-1]++;
            }
        }
    }
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
  if (line)
    free (line);

  int checker = 0;
  for (i = 0; i < 512; i++)
    for (j = 0; j < 2; j++)
      if (subcount[i][j])
        checker = 1;

  if (app->show_subtype && checker)
    dump_subtype_total (subcount, out);
  else
    dump_total (count, out);
  return 0;
}

int
fituvalu_type_square (struct fv_app_type_square_t *app,  FILE *in, FILE *out)
{
  switch (app->mode)
    {
    case MODE_SHOW_TYPE:
      return show_type_square (app, in, out);
    case MODE_FILTER:
      return filter_type_square (app, in, out);
    case MODE_TOTAL:
      return total_type_square (app, in, out);
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "filter", 'f', "NUM_SQUARES:TYPE", 0, "Show the magic squares that have this number of perfect squares, and type"},
  { "show", 288, "NUM_SQUARES:TYPE", 0, "Don't process anything, just show a picture of TYPE"},
  { "total", 't', 0, 0, "Produce a grand total"},
  { "subtype", 's', 0, 0, "Also show the subtype"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares from the standard input, and determine which configuration it is.\vThe nine values must be separated by commas and terminated by a newline. --out-binary is only used with --filter.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_type_square_t app;
  memset (&app, 0, sizeof (app));
  app.read_square = read_square_from_stream;
  app.display_square = display_square_record;
  app.mode = MODE_SHOW_TYPE;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_type_square (&app, stdin, stdout);
}
