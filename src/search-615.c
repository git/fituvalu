/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_615_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  double median;
  double max;
  double percent;
  int mult;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int threesq;
  int subtype;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t i, iroot, j, twoiroot, limit, sum;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_615_type_1 (struct fv_app_search_615_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  //    +---+---+---+
  //    |   |   | B |
  //    +---+---+---+
  //    | A |   | D |
  //    +---+---+---+
  //    | E | C |   |
  //    +---+---+---+

  mpz_set ((*s)[2][1], w->hi);
  mpz_set ((*s)[1][0], w->lo);

  mpz_set ((*s)[1][2], q->i);
  mpz_set ((*s)[2][0], q->j);

  mpz_sub ((*s)[0][1], (*s)[2][0], w->distance);

  if (mpz_cmp ((*s)[2][1], (*s)[0][1]) > 0)
    {
      mpz_sub (q->sum, (*s)[2][1], (*s)[0][1]);
      mpz_cdiv_q_ui (q->sum, q->sum, 2);
      mpz_add ((*s)[1][1], (*s)[0][1], q->sum);
    }
  else
    {
      mpz_sub (q->sum, (*s)[0][1], (*s)[2][1]);
      mpz_cdiv_q_ui (q->sum, q->sum, 2);
      mpz_add ((*s)[1][1], (*s)[2][1], q->sum);
    }

  mpz_mul_ui (q->sum, (*s)[1][1], 3);

  mpz_sub ((*s)[0][0], q->sum, (*s)[0][1]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[0][2]);

  mpz_sub ((*s)[2][2], q->sum, (*s)[2][1]);
  mpz_sub ((*s)[2][2], (*s)[2][2], (*s)[2][0]);

  //check for dups
  int dup = 0;
  if (mpz_cmp ((*s)[2][1], (*s)[1][2]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0)
    dup = 1;
  else
    {
      mpz_sub (q->sum, q->sum, (*s)[0][0]);
      mpz_sub (q->sum, q->sum, (*s)[1][1]);
      mpz_sub (q->sum, q->sum, (*s)[2][2]);
    }
  if (!dup && mpz_cmp_ui (q->sum, 0) == 0)
    {
      if (mpz_perfect_square_p ((*s)[0][0]))
        {
          pthread_mutex_lock (&display_lock);
          app->display_square (*s, app->out);
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);
        }
    }
}

static void
generate_615_type_2 (struct fv_app_search_615_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  //    +---+---+---+
  //    |   |   | B |
  //    +---+---+---+
  //    | C |   | E |
  //    +---+---+---+
  //    | D | A |   |
  //    +---+---+---+

  mpz_set ((*s)[2][1], w->lo);
  mpz_set ((*s)[1][0], w->hi);

  mpz_set ((*s)[1][2], q->j);
  mpz_set ((*s)[2][0], q->i);

  mpz_add ((*s)[0][1], (*s)[2][0], w->distance);

  if (mpz_cmp ((*s)[2][1], (*s)[0][1]) > 0)
    {
      mpz_sub (q->sum, (*s)[2][1], (*s)[0][1]);
      mpz_cdiv_q_ui (q->sum, q->sum, 2);
      mpz_add ((*s)[1][1], (*s)[0][1], q->sum);
    }
  else
    {
      mpz_sub (q->sum, (*s)[0][1], (*s)[2][1]);
      mpz_cdiv_q_ui (q->sum, q->sum, 2);
      mpz_add ((*s)[1][1], (*s)[2][1], q->sum);
    }

  mpz_mul_ui (q->sum, (*s)[1][1], 3);

  mpz_sub ((*s)[0][0], q->sum, (*s)[0][1]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[0][2]);

  mpz_sub ((*s)[2][2], q->sum, (*s)[2][1]);
  mpz_sub ((*s)[2][2], (*s)[2][2], (*s)[2][0]);

  //check for dups
  int dup = 0;
  if (mpz_cmp ((*s)[2][1], (*s)[1][2]) == 0 ||
      mpz_cmp ((*s)[1][2], (*s)[0][2]) == 0)
    dup = 1;
  else
    {
      mpz_sub (q->sum, q->sum, (*s)[0][0]);
      mpz_sub (q->sum, q->sum, (*s)[1][1]);
      mpz_sub (q->sum, q->sum, (*s)[2][2]);
    }
  if (!dup && mpz_cmp_ui (q->sum, 0) == 0)
    {
      if (mpz_perfect_square_p ((*s)[0][0]))
        {
          pthread_mutex_lock (&display_lock);
          app->display_square (*s, app->out);
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);
        }
    }
}

static void
generate_615_type_1_and_2 (struct fv_app_search_615_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_mul_ui (q->limit, w->hi, app->mult);

  mpz_sqrt (q->iroot, w->distance);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  while (1)
    {
      mpz_sub (q->j, q->i, w->distance);
      if (mpz_perfect_square_p (q->j))
        {
          if (app->subtype <= 1)
            generate_615_type_1 (app, w, q, s);
          if (app->subtype == 2 || app->subtype == 0)
            generate_615_type_2 (app, w, q, s);
        }

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
handle_progression (struct fv_app_search_615_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  generate_615_type_1_and_2 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_615_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[0][2], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[0][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[0][2], w->distance);
          mpz_add (w->hi, (*s)[0][2], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }
      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_615_t *app =
    (struct fv_app_search_615_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.sum, q.twoiroot, q.limit, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[0][2], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[0][2], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[0][2], 1) > 0)
        generate_progressions (app, &w, &q, &s);
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (q.i, q.iroot, q.j, q.sum, q.twoiroot, q.limit, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_615_t *app =
    (struct fv_app_search_615_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.sum, q.twoiroot, q.limit, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[0][2], w.lo);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (q.i, q.iroot, q.j, q.sum, q.twoiroot, q.limit, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_615 (struct fv_app_search_615_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *end = NULL;
  struct fv_app_search_615_t *app = (struct fv_app_search_615_t *) state->input;
  switch (key)
    {
    case 's':
      app->subtype = atoi (arg);
      if (app->subtype <= 0 || app->subtype > 2)
        argp_error (state, "invalid subtype");
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'm':
      app->percent = strtod (arg, &end);
      if (end == NULL || *end != '\0' || app->percent <= 0)
        argp_error (state, "invalid argument `%s' to option --multiply", arg);
      app->percent /= 100.0;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      app->mult = ceil (app->max * app->percent);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "multiply", 'm', "PERC", 0, "Find a starting point for E by multiplying C by a value between 1 and 9755.2353 expressed as a percent (1-100)"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "subtype", 's', "NUM", 0, "Only produce subtype 1 or 2 instead of both"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:15 given a FILE containing top right values.\vWhen FILE is not provided, it is read from the standard input.  The default value for PERC is 0.0015895998183739555.  Magic squares of type 6:15 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  | X |   | X |   | Z |   | B |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating E\n"
"  | X |   | X |   | C |   | D |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below E.\n"
"  | X | X |   |   | E | A |   |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n"
"                  +---+---+---+\n"
"                  | Z |   | B |\n"
"                  +---+---+---+  (starts iterating F upwards)\n"
"                  | A |   | F |\n"
"                  +---+---+---+\n"
"                  | E | C |   |\n"
"                  +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_615_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.median = 4.4007;
  app.max = 830529.7879;
  app.percent = (app.median * 3.0) / app.max;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_615 (&app);
}
/*

the point of this is to identify information that can help us get more 6:15s.  we want to be able to predict 'center' values (the B values) for 6:15 magic squares.

  +---+---+---+   +---+---+---+
  | X |   | X |   | Z |   | B |
  +---+---+---+   +---+---+---+
  | X |   | X |   | A |   | F |
  +---+---+---+   +---+---+---+
  | X | X |   |   | E | C |   |
  +---+---+---+   +---+---+---+
                  +---+---+---+
                  | Z |   | B |
                  +---+---+---+
                  | C |   | D |
                  +---+---+---+
                  | E | A |   |
                  +---+---+---+

we identified which B values lead to magic squares with 6 perfect squares in the 6:15 configuration, and then here we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:15 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 1986 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

84, 52, 68, 231 / 46524 = 0.004965
24, 60, 120, 231 / 46518 = 0.004966
84, 4, 116, 231 / 46520 = 0.004966
84, 64, 56, 234 / 46528 = 0.005029
12, 72, 120, 239 / 46519 = 0.005138
84, 16, 104, 244 / 46520 = 0.005245
0, 84, 120, 182 / 31024 = 0.005866
84, 0, 120, 182 / 31024 = 0.005866
84, 120, 0, 182 / 31024 = 0.005866
16, 68, 120, 290 / 46521 = 0.006234

well we're around two thirds of a percent, and again we have the ever-present 16, 68, 120 which is the best.

second run is depth of 2, and breadth of 2000.

1716, 1956, 15 / 1731 = 0.008666
696, 1344, 27 / 3113 = 0.008673
1648, 1616, 17 / 1951 = 0.008713
1716, 1548, 17 / 1950 = 0.008718
1716, 1344, 19 / 2082 = 0.009126
532, 1436, 30 / 3229 = 0.009291
1920, 1140, 20 / 2083 = 0.009602
1516, 1436, 21 / 2154 = 0.009749
1308, 1956, 21 / 1953 = 0.010753
1104, 1548, 26 / 2402 = 0.010824

hmm the pecentage is about 2 times higher.  down from 290 hits to 26 though.
the best pair seems to be "1104, 1548".

and just for kicks, let's try a depth of 1 and a breadth of 100000.

88332, 2 / 38 = 0.052632
91505, 2 / 37 = 0.054054
91626, 2 / 37 = 0.054054
92802, 2 / 37 = 0.054054
92896, 2 / 37 = 0.054054
93776, 2 / 36 = 0.055556
97165, 2 / 35 = 0.057143

i'm sure we're just in la-la guessing land here.
but maybe 97165 is worth trying.

let's say the minimum count has to be 10 or higher.

1020, 11 / 3108 = 0.003539
762, 15 / 4154 = 0.003611
720, 16 / 4399 = 0.003637
1164, 10 / 2723 = 0.003672
372, 33 / 8516 = 0.003875
1034, 12 / 3064 = 0.003916
960, 13 / 3300 = 0.003939
924, 14 / 3433 = 0.004078
1428, 10 / 2220 = 0.004505
2068, 10 / 1534 = 0.006519

2068 is the best i guess.  i see half of 2068 too.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

4, 2, 0, 1986 / 1053758 = 0.001885
4, 1, 1, 1986 / 1580637 = 0.001256
4, 0, 2, 1986 / 1053758 = 0.001885
3, 1, 2, 1986 / 1580637 = 0.001256
2, 2, 2, 1986 / 1580637 = 0.001256
2, 2, 0, 1986 / 1580637 = 0.001256
2, 1, 1, 1986 / 2370955 = 0.000838
2, 0, 2, 1986 / 1580637 = 0.001256
2, 0, 0, 1986 / 1580637 = 0.001256
1, 3, 2, 1986 / 1580637 = 0.001256
1, 2, 0, 1986 / 2107516 = 0.000942
1, 1, 2, 1986 / 2370956 = 0.000838
1, 1, 1, 1986 / 3161274 = 0.000628
1, 1, 0, 1986 / 3161274 = 0.000628
1, 0, 2, 1986 / 2107516 = 0.000942
1, 0, 1, 1986 / 3161274 = 0.000628
1, 0, 0, 1986 / 3161274 = 0.000628
0, 4, 2, 1986 / 1053758 = 0.001885
0, 2, 2, 1986 / 1580637 = 0.001256
0, 2, 0, 1986 / 1580637 = 0.001256
0, 1, 2, 1986 / 2107516 = 0.000942
0, 1, 1, 1986 / 3161274 = 0.000628
0, 1, 0, 1986 / 3161274 = 0.000628
0, 0, 2, 1986 / 1580637 = 0.001256
0, 0, 1, 1986 / 3161274 = 0.000628
4, 2, 6, 1686 / 790322 = 0.002133

we can go up by "4, 2" and get them all.

results:

so looking forward for more sixes, pick one of "97165", "93776", or "92896".  Try "2068", and "1104, 1548", and "16, 68, 120".

and we can recalculate them all quicker by using the "4,8" pattern.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 169 in sq-seq-finder.

72, 100, 32, 235 / 46519 = 0.005052
88, 120, 104, 154 / 30420 = 0.005062
72, 102, 30, 238 / 46520 = 0.005116
72, 52, 80, 239 / 46521 = 0.005137
72, 120, 12, 239 / 46519 = 0.005138
72, 64, 68, 242 / 46524 = 0.005202
72, 16, 116, 252 / 46518 = 0.005417
0, 52, 104, 223 / 40556 = 0.005499
52, 0, 104, 223 / 40556 = 0.005499
52, 104, 0, 223 / 40556 = 0.005499

well this is weirdo-land.
the best tuple is "72, 16, 116".  there's a frequent sum of 204.

no arithmetic progressions in non-zero tuples.

we did a little worse with a different starting point in terms of percentage.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

1504, 1448, 21 / 2155 = 0.009745
1704, 1764, 18 / 1833 = 0.009820
1500, 1832, 19 / 1913 = 0.009932
1908, 1152, 21 / 2084 = 0.010077
888, 1152, 32 / 3122 = 0.010250
520, 1352, 35 / 3398 = 0.010300
1500, 1968, 19 / 1838 = 0.010337
684, 1356, 35 / 3118 = 0.011225
1534, 650, 33 / 2909 = 0.011344
1092, 1560, 30 / 2405 = 0.012474

best of the bunch is "1092, 1560".
we actually did a tiny bit better than earlier too.

okay let's do depth=1, and breadth is 100000

90093, 3 / 39 = 0.076923
91572, 3 / 38 = 0.078947
91632, 3 / 38 = 0.078947
92048, 3 / 38 = 0.078947
97164, 3 / 36 = 0.083333
97840, 3 / 36 = 0.083333

8 percent is in the good category.
does it pan out? probably not.

let's do depth=1, breadth is 100000, min hits of 10

1404, 15 / 2264 = 0.006625
936, 25 / 3394 = 0.007366
1848, 13 / 1723 = 0.007545
1716, 15 / 1853 = 0.008095
1456, 18 / 2185 = 0.008238
1872, 15 / 1701 = 0.008818
2184, 13 / 1460 = 0.008904
2574, 12 / 1236 = 0.009709
2808, 12 / 1136 = 0.010563
4368, 11 / 735 = 0.014966

4368 is  best.  no real standouts.

so overall, looking forward for more sixes, pick one of "97165", "93776", or "92896", and then one of "97840", "97164", or "92048".  Try "1104, 1568", "16, 68, 120", "1092, 1560", and "72, 16, 116".  and throw in "2068" and "4368".

this one makes me think there's a large round single repeater somewhere.

okay, let's do the first run again but sort on hits to see how it shakes out.

4, 2, 0, 1986 / 1053755 = 0.001885
4, 1, 1, 1986 / 1580632 = 0.001256
4, 0, 2, 1986 / 1053755 = 0.001885
3, 1, 2, 1986 / 1580632 = 0.001256
2, 2, 2, 1986 / 1580632 = 0.001256
2, 2, 0, 1986 / 1580632 = 0.001256
2, 1, 1, 1986 / 2370947 = 0.000838
2, 0, 2, 1986 / 1580632 = 0.001256
2, 0, 0, 1986 / 1580632 = 0.001256
1, 3, 2, 1986 / 1580632 = 0.001256
1, 2, 0, 1986 / 2107509 = 0.000942
1, 1, 2, 1986 / 2370948 = 0.000838
1, 1, 1, 1986 / 3161263 = 0.000628
1, 1, 0, 1986 / 3161263 = 0.000628
1, 0, 2, 1986 / 2107509 = 0.000942
1, 0, 1, 1986 / 3161263 = 0.000628
1, 0, 0, 1986 / 3161263 = 0.000628
0, 4, 2, 1986 / 1053755 = 0.001885
0, 2, 2, 1986 / 1580632 = 0.001256
0, 2, 0, 1986 / 1580632 = 0.001256
0, 1, 2, 1986 / 2107509 = 0.000942
0, 1, 1, 1986 / 3161263 = 0.000628
0, 1, 0, 1986 / 3161263 = 0.000628
0, 0, 2, 1986 / 1580632 = 0.001256
0, 0, 1, 1986 / 3161263 = 0.000628
4, 2, 6, 1686 / 790320 = 0.002133
4, 2, 2, 1683 / 1185476 = 0.001420

no better than before. gotta go up by "4,2" to get them all.

this concludes our analysis of anticipating B values in 6:15 magic squares.

in order to get high value 6:15s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

also we could consider running this test again but including all D values as B values.

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="97165" 1 10000000000000000000000000000 | ./search-615
sq-seq --pattern="93776" 1 10000000000000000000000000000 | ./search-615
sq-seq --pattern="92896" 1 10000000000000000000000000000 | ./search-615
sq-seq --pattern="1104,1568" 1 10000000000000000000000000000 | ./search-615
sq-seq --pattern="16,68,120" 1 10000000000000000000000000000 | ./search-615
sq-seq --pattern="2068" 1 10000000000000000000000000000 | ./search-615

sq-seq --pattern="97840" 169 10000000000000000000000000000 | ./search-615
sq-seq --pattern="97164" 169 10000000000000000000000000000 | ./search-615
sq-seq --pattern="92048" 169 10000000000000000000000000000 | ./search-615
sq-seq --pattern="1092,1560" 169 10000000000000000000000000000 | ./search-615
sq-seq --pattern="72,16,116" 169 10000000000000000000000000000 | ./search-615
sq-seq --pattern="4368" 169 10000000000000000000000000000 | ./search-615
*/
