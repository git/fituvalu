/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <argp.h>
#include <gmp.h>
#include <string.h>
#include "magicsquareutil.h"

struct fv_app_rotate_square_t
{
  int one;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  int six;
  int congruum;
};


static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_rotate_square_t *app = (struct fv_app_rotate_square_t *) state->input;
  switch (key)
    {
    case 'c':
      app->congruum = 1;
      break;
    case '6':
      app->six = 1;
      break;
    case '1':
      app->one = 1;
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    }
  return 0;
}

static int
compare_square (mpz_t (*l)[3][3], mpz_t (*r)[3][3])
{
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      {
        int ret = mpz_cmp ((*l)[i][j], (*r)[i][j]);
        if (ret)
          return ret;
      }
  return 0;
}

static void
normalize_square (mpz_t (*a)[3][3])
{
  mpz_t squares[12][3][3];
  int i = 0, j, k, l;
  for (; i < 12; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
        mpz_init (squares[i][j][k]);

  i = 0;
  for (l = 0; l < 4; l++, i++)
    {
      rotate_square (a);
      for (j = 0; j < 3; j++)
        for (k = 0; k < 3; k++)
          mpz_set (squares[i][j][k], (*a)[j][k]);
    }

  flip_square_horizontally (a);
  for (l = 0; l < 4; l++, i++)
    {
      rotate_square (a);
      for (j = 0; j < 3; j++)
        for (k = 0; k < 3; k++)
          mpz_set (squares[i][j][k], (*a)[j][k]);
    }

  flip_square_vertically (a);
  for (l = 0; l < 4; l++, i++)
    {
      rotate_square (a);
      for (j = 0; j < 3; j++)
        for (k = 0; k < 3; k++)
          mpz_set (squares[i][j][k], (*a)[j][k]);
    }

  int low = 0;
  for (i = 1; i < 12; i++)
    {
      if (compare_square (&squares[i], &squares[low]) < 0)
        low = i;
    }

  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      mpz_set ((*a)[j][k], squares[low][j][k]);

  for (i = 0; i < 12; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
        mpz_clear (squares[i][j][k]);
}

static void
rotate_congruum (mpz_t (*a)[3][3])
{
  /*
   * a congruum is the difference between perfect squares in a three
   * square progression, and this isn't quite that because we don't care
   * if they're perfect squares or not.
   */
  mpz_t c[2];
  mpz_inits (c[0], c[1], NULL);
  mpz_sub (c[0], (*a)[1][1], (*a)[0][0]);
  mpz_abs (c[0], c[0]);
  mpz_sub (c[0], (*a)[1][1], (*a)[0][2]);
  mpz_abs (c[1], c[1]);

  if (mpz_cmp (c[0], c[1]) < 0)
    {
      // if the lowest congrua is already in the primary diagonal
      // check to see if the lowest value is in the top left
      if (mpz_cmp ((*a)[0][0], (*a)[2][2]) > 0)
        {
          flip_square_vertically (a);
          flip_square_horizontally (a);
        }
    }
  else
    {
      // the lowest congrua is on the alternate diagonal so we need to
      // flip it horizontally once for certain
      // but then it might need to be flipped again so that the lowest value
      // is in the top left corner
      int flip = 0;
      if (mpz_cmp ((*a)[0][2], (*a)[2][0]) > 0)
        flip = 1;
      flip_square_horizontally (a);
      if (flip)
        {
          flip_square_vertically (a);
          flip_square_horizontally (a);
        }
    }
  mpz_clears (c[0], c[1], NULL);
}

int
fituvalu_rotate_square (struct fv_app_rotate_square_t *app, FILE *in, FILE *out)
{
  int i, j, k;
  mpz_t a[3][3];
  mpz_t result[3][3];
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
        mpz_init (a[i][j]);
        mpz_init (result[i][j]);
      }

  while (1)
    {
      char *line = NULL;
      size_t len = 0;
      ssize_t read = app->read_square (in, &a, &line, &len);
      free (line);
      if (read == -1)
        return 0;

      for (j = 0; j < 3; j++)
        for (k = 0; k < 3; k++)
          mpz_set (result[j][k], a[j][k]);
      if (app->one)
        {
          normalize_square (&result);
          app->display_square (result, out);
        }
      else if (app->congruum)
        {
          rotate_congruum (&result);
          app->display_square (result, out);
        }
      else if (app->six)
        {
          int num;
          int type = square_get_type (&result, &num);
          if (num == 6)
            {
              rotate_six (&result, type);
              app->display_square (result, out);
            }
        }
      else
        {
          for (j = 0; j < 4; j++)
            {
              rotate_square (&result);
              app->display_square (result, out);
            }
          flip_square_horizontally (&result);
          for (j = 0; j < 4; j++)
            {
              rotate_square (&result);
              app->display_square (result, out);
            }
          flip_square_vertically (&result);
          for (j = 0; j < 4; j++)
            {
              rotate_square (&result);
              app->display_square (result, out);
            }
        }
    }
          
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clears (a[i][j], result[i][j], NULL);
  return 0;
}

static struct argp_option
options[] =
{
  { "one", '1', 0, 0, "Show the square with the smallest values in order" },
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "six", '6', 0, 0, "Rotate squares with six perfect squares to their standard orientation"},
  { "congruum", 'c', 0, 0, "Rotate squares so that the lowest congrua goes from top left to bottom right"},
    { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Rotate a 3x3 magic square given on the standard input, and show all the ways the square can be turned and flipped, and still be the same square.\vThe nine values must be separated by a comma and terminated by a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_rotate_square_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.read_square = read_square_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_rotate_square (&app, stdin, stdout);
}
