/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct fv_app_expand_square_t
{
  int num_args;
  mpq_t multiplier;
  mpz_t max_tries;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
};

static void
multiply_square (struct fv_app_expand_square_t *app, mpq_t (*a)[3][3])
{
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpq_mul ((*a)[i][j], (*a)[i][j], app->multiplier);
}

int
fituvalu_expand_square (struct fv_app_expand_square_t *app, FILE *stream, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t s[3][3];
  mpz_t j, diff;
  mpq_t a[3][3];

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpq_init (a[i][j]);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  mpz_inits (j, diff, NULL);

  read = app->read_square (stream, &s, &line, &len);
  if (read != -1)
    {
      app->display_square (s, out);
      while (1)
        {
          for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
              mpq_set_z (a[i][j], s[i][j]);
          for (mpz_set_ui (j, 0); mpz_cmp (j, app->max_tries) < 0;
               mpz_add_ui (j, j, 1))
            {
              multiply_square (app, &a);
              for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                  mpz_set_q (s[i][j], a[i][j]);

              //mpz_sub (diff, ar[1], ar[0]);
              //mpz_add (ar[2], ar[1], diff);
              app->display_square (s, out);
            }
          read = app->read_square (stream, &s, &line, &len);
          if (read == -1)
            break;
        }
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpq_clear (a[i][j]);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (j, diff, NULL);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_expand_square_t *app = (struct fv_app_expand_square_t *) state->input;
  switch (key)
    {
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else if (app->num_args == 1)
        mpz_set_str (app->max_tries, arg, 10);
      else if (app->num_args == 0)
        mpq_set_str (app->multiplier, arg, 10);
      app->num_args++;
      break;
    case ARGP_KEY_INIT:
      mpq_init (app->multiplier);
      mpz_init (app->max_tries);
      mpz_set_ui (app->max_tries, 1);
      break;
    case ARGP_KEY_NO_ARGS:
      argp_error (state, "missing argument");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "MUL [TRIES]",
  "Accept nine numbers from the standard input, and multiply them by MUL which can be a rational number.\vThe values must be separated by a comma and terminated by a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_expand_square_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.read_square = read_square_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_expand_square (&app, stdin, stdout);
}
