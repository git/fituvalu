/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct histogram_rec_t
{
  int category;
  int count;
};

struct fv_app_filter_progression_t
{
  void (*display_tuple) (mpz_t s[3], FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
  int magnitude_histogram;
  int fraction_histogram;
  struct histogram_rec_t *histogram_recs;
  int num_histogram_recs;
};

static int
check_3sq (struct fv_app_filter_progression_t *app, mpz_t (*a)[3])
{
  mpz_t diff1, diff2;
  mpz_inits (diff1, diff2, NULL);
  mpz_sub (diff1, (*a)[1], (*a)[0]);
  mpz_sub (diff2, (*a)[2], (*a)[1]);
  int ret = mpz_cmp (diff1, diff2);
  mpz_clears (diff1, diff2, NULL);
  if (ret != 0)
    return 0;
  for (int i = 0; i < 3; i++)
    if (!mpz_perfect_square_p ((*a)[i]))
      return 0;
  return 1;
}

static void
calculate_magnitude_histogram (struct fv_app_filter_progression_t *app, mpz_t (*a)[3])
{
  int magnitude = mpz_sizeinbase ((*a)[1], 10);
  int found = 0;
  for (int i = 0; i < app->num_histogram_recs; i++)
    {
      if (app->histogram_recs[i].category == magnitude)
        {
          found = 1;
          app->histogram_recs[i].count++;
          break;
        }
    }
  if (!found)
    {
      app->histogram_recs = realloc (app->histogram_recs, sizeof (struct histogram_rec_t) *
                                     (app->num_histogram_recs + 1));
      app->histogram_recs[app->num_histogram_recs].category= magnitude;
      app->histogram_recs[app->num_histogram_recs].count = 1;
      app->num_histogram_recs++;
    }
}

static void
calculate_fraction_histogram (struct fv_app_filter_progression_t *app, mpz_t (*a)[3])
{
  mpf_t n, d, r;
  mpf_inits (n, d, r, NULL);
  mpf_set_z (n, (*a)[0]);
  mpf_set_z (d, (*a)[1]);
  mpf_div (r, n, d);
  int percentile = ceil (mpf_get_d (r) * 100.0);
  int found = 0;
  for (int i = 0; i < app->num_histogram_recs; i++)
    {
      if (app->histogram_recs[i].category == percentile)
        {
          found = 1;
          app->histogram_recs[i].count++;
          break;
        }
    }
  if (!found)
    {
      app->histogram_recs = realloc (app->histogram_recs, sizeof (struct histogram_rec_t) *
                                     (app->num_histogram_recs + 1));
      app->histogram_recs[app->num_histogram_recs].category = percentile;
      app->histogram_recs[app->num_histogram_recs].count = 1;
      app->num_histogram_recs++;
    }
  mpf_clears (n, d, r, NULL);
}

static void
display_magnitude_histogram (struct fv_app_filter_progression_t *app, FILE *out)
{
  int max_magnitude = 0;
  for (int i = 0; i < app->num_histogram_recs; i++)
    if (app->histogram_recs[i].category > max_magnitude)
      max_magnitude = app->histogram_recs[i].category;
  if (max_magnitude == 0)
    return;

  for (int i = 1; i <= max_magnitude; i++)
    {
      int found = 0;
      int idx = 0;
      for (int j = 0; j < app->num_histogram_recs; j++)
        {
          if (app->histogram_recs[j].category == i)
            {
              found = 1;
              idx = j;
            }
        }
      if (found)
        fprintf (out, "10^%d, %d\n", i, app->histogram_recs[idx].count);
      else
        fprintf (out, "10^%d, %d\n", i, 0);
    }
}

static void
display_fraction_histogram (struct fv_app_filter_progression_t *app, FILE *out)
{
  for (int i = 1; i <= 100; i++)
    {
      int found = 0;
      int idx = 0;
      for (int j = 0; j < app->num_histogram_recs; j++)
        {
          if (app->histogram_recs[j].category == i)
            {
              found = 1;
              idx = j;
            }
        }
      if (found)
        fprintf (out, "%03.2f, %d\n", i/100.0, app->histogram_recs[idx].count);
      else
        fprintf (out, "%03.2f, %d\n", i/100.0, 0);
    }
}

int
fituvalu_filter_progression (struct fv_app_filter_progression_t *app, FILE *in, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3];

  for (int i = 0; i < 3; i++)
    mpz_init (a[i]);

  while (1)
    {
      read = app->read_tuple (in, a, &line, &len);
      if (read == -1)
        break;
      if (app->magnitude_histogram)
        {
          if (check_3sq (app, &a))
            calculate_magnitude_histogram (app, &a);
        }
      else if (app->fraction_histogram)
        {
          if (check_3sq (app, &a))
            calculate_fraction_histogram (app, &a);
        }
      else
        {
          if (check_3sq (app, &a))
            app->display_tuple (a, out);
        }
    }

  if (app->magnitude_histogram)
    display_magnitude_histogram (app, out);
  else if (app->fraction_histogram)
    display_fraction_histogram (app, out);
  for (int i = 0; i < 3; i++)
    mpz_clear (a[i]);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_filter_progression_t *app = (struct fv_app_filter_progression_t *) state->input;
  switch (key)
    {
    case 'h':
      app->fraction_histogram = 1;
      break;
    case 'H':
      app->magnitude_histogram = 1;
      break;
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_tuple = display_binary_three_record;
      break;
    case ARGP_KEY_FINI:
      if (app->fraction_histogram && app->magnitude_histogram)
        argp_error (state, "can't do both -h and -H simultaneously");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "fraction-histogram", 'h', 0, 0, "Show a histogram of fractional differences"},
  { "magnitude-histogram", 'H', 0, 0, "Show a histogram of center number magnitudes"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept three numbers from the standard input, and display them if they are comprised of squared integers, are in ascending order, and are an arithmetic progression.\vThe three values must be separated by a comma and terminated by a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_filter_progression_t app;
  memset (&app, 0, sizeof (app));
  app.display_tuple = display_three_record;
  app.read_tuple = read_three_numbers_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_filter_progression (&app, stdin, stdout);
}
