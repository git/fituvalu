/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <unistd.h>
#include "magicsquareutil.h"
#include "linecount.h"
int linecount;

struct fv_app_2sq_search_616_t
{
  FILE *infile;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_pair) (FILE *, mpz_t *, char **, size_t *);
  void (*display_pair) (mpz_t *one, mpz_t *two, FILE *out);
  int show_pairs;
  int positive;
};

struct rec
{
  mpz_t ap[2];
};

static void
create_square (struct fv_app_2sq_search_616_t *app, struct rec *rec, mpz_t *b, mpz_t *c, mpz_t *middle, mpz_t diff, FILE *out)
{
  mpz_t s[3][3];
  mpz_t sum, p;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  mpz_set (s[2][0], rec->ap[0]);
  mpz_set (s[1][1], *middle);
  mpz_set (s[0][2], rec->ap[1]);
  mpz_set (s[0][0], b[0]);
  mpz_set (s[2][1], b[1]);
  mpz_set (s[0][1], c[0]);
  mpz_set (s[2][2], c[1]);
  mpz_sub (s[1][2], s[0][0], diff);
  mpz_add (s[1][0], s[2][2], diff);

  app->display_square (s, out);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
}

static int
search_for_diff (struct fv_app_2sq_search_616_t *app, mpz_t match, mpz_t *b, mpz_t *c, struct rec *rec, mpz_t *middle, mpz_t *sum, FILE *out)
{
  int found = 0;
  mpz_t i, iroot, j, jroot, diff, limiter, targetsum;
  mpz_inits (i, iroot, j, jroot, diff, limiter, targetsum, NULL);
  mpz_set_ui (i, 4);
  mpz_set_ui (iroot, 2);
  mpz_mul_ui (limiter, match, 2);
  while (1)
    {
      if (mpz_cmp (i, match) > 0)
        break;

      mpz_add (diff, match, i);
      //char *buf = malloc (mpz_sizeinbase (diff, 10) + 2);
      //mpz_get_str (buf, 10, diff);
      //fprintf (stdout, "%s\n", buf);
      //free (buf);
      if (mpz_perfect_square_p (diff))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], diff);
          mpz_set_ui (j, 4);
          mpz_set_ui (jroot, 2);
                  
          mpz_sub (targetsum, *sum, b[0]);
          mpz_sub (targetsum, targetsum, *middle);

          if (mpz_perfect_square_p (targetsum))
            {
              while (1)
                {
                  mpz_add (diff, match, j);
                  if (mpz_cmp (j, limiter) > 0)
                    break;
                  if (mpz_perfect_square_p (diff))
                    {
                      mpz_set (c[0], j);
                      mpz_set (c[1], diff);

                      mpz_add (targetsum, *middle, b[0]);
                      mpz_add (targetsum, targetsum, c[1]);
                      if (mpz_cmp (targetsum, *sum) == 0)
                        {
                          if (mpz_cmp (c[0], b[0]) != 0 &&
                              mpz_cmp (c[1], b[1]) != 0 &&
                              mpz_cmp (c[0], rec->ap[0]) != 0 &&
                              mpz_cmp (c[1], rec->ap[1]) != 0 &&
                              mpz_cmp (b[0], rec->ap[0]) != 0 &&
                              mpz_cmp (b[1], rec->ap[1]) != 0)
                            {
                              create_square (app, rec, b, c, middle, match, out);
                            }
                        }
                    }
                  mpz_add (j, j, jroot);
                  mpz_add (j, j, jroot);
                  mpz_add_ui (j, j, 1);
                  mpz_add_ui (jroot, jroot, 1);
                }
            }
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, jroot, diff, limiter, targetsum, NULL);
  return found;
}

static int
pair_search2 (struct fv_app_2sq_search_616_t *app, struct rec *rec, mpz_t *b, mpz_t *c, mpz_t match, mpz_t *middle, mpz_t *sum, FILE *out)
{
  if (mpz_even_p (rec->ap[1]))
    return 0;
  int found = 0;
  mpz_t i, iroot, j, jroot, diff, limiter, targetsum;
  mpz_inits (i, iroot, j, jroot, diff, limiter, targetsum, NULL);
  mpz_set (i, match);
  mpz_sqrt (iroot, i);
  mpz_mul (i, iroot, iroot);

  mpz_sub (match, rec->ap[1], rec->ap[0]);
  mpz_cdiv_q_ui (match, match, 2);
  mpz_add (*middle, rec->ap[0], match);
  mpz_add (*sum, rec->ap[0], rec->ap[1]);
  mpz_add (*sum, *sum, *middle);
  while (1)
    {
      if (mpz_cmp (i, rec->ap[0]) > 0)
        break;
      mpz_add (diff, match, i);
      //char *buf = malloc (mpz_sizeinbase (diff, 10) + 2);
      //mpz_get_str (buf, 10, diff);
      //fprintf (stdout, "%s\n", buf);
      //free (buf);
      mpz_add (limiter, rec->ap[1], rec->ap[1]);
      if (mpz_perfect_square_p (diff))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], diff);
          mpz_set (j, rec->ap[2]);
          mpz_sqrt (jroot, j);
          mpz_mul (j, jroot, jroot);
                  
          mpz_sub (targetsum, *sum, b[0]);
          mpz_sub (targetsum, targetsum, *middle);

          if (mpz_perfect_square_p (targetsum))
            {
              while (1)
                {
                  mpz_add (diff, match, j);
                  if (mpz_cmp (j, limiter) > 0)
                    break;
                  if (mpz_perfect_square_p (diff))
                    {
                      mpz_set (c[0], j);
                      mpz_set (c[1], diff);

                      mpz_add (targetsum, *middle, b[0]);
                      mpz_add (targetsum, targetsum, c[1]);
                      if (mpz_cmp (targetsum, *sum) == 0)
                        {
                          if (mpz_cmp (c[0], b[0]) != 0 &&
                              mpz_cmp (c[1], b[1]) != 0 &&
                              mpz_cmp (c[0], rec->ap[0]) != 0 &&
                              mpz_cmp (c[1], rec->ap[1]) != 0 &&
                              mpz_cmp (b[0], rec->ap[0]) != 0 &&
                              mpz_cmp (b[1], rec->ap[1]) != 0)
                            {
                              create_square (app, rec, b, c, middle, match, out);
                            }
                        }
                    }
                  mpz_add (j, j, jroot);
                  mpz_add (j, j, jroot);
                  mpz_add_ui (j, j, 1);
                  mpz_add_ui (jroot, jroot, 1);
                }
            }
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, jroot, diff, limiter, targetsum, NULL);
  return found;
}

static int
pair_search (struct fv_app_2sq_search_616_t *app, struct rec *rec, mpz_t *b, mpz_t *c, mpz_t *diff, mpz_t *middle, mpz_t *sum, FILE *out)
{
  if (mpz_even_p (rec->ap[1]))
    return 0;
  mpz_sub (*diff, rec->ap[1], rec->ap[0]);
  mpz_cdiv_q_ui (*diff, *diff, 2);
  mpz_add (*middle, rec->ap[0], *diff);
  mpz_add (*sum, rec->ap[0], rec->ap[1]);
  mpz_add (*sum, *sum, *middle);
  search_for_diff (app, *diff, b, c, rec, middle, sum, out);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_2sq_search_616_t *app = (struct fv_app_2sq_search_616_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'p':
      app->show_pairs = 1;
      break;
    case 'P':
      app->positive = 1;
      break;
    case 'i':
      app->read_pair = binary_read_two_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      app->display_pair = display_binary_two_record;
      break;
    case ARGP_KEY_ARG:
      if (app->infile)
        argp_error (state, "too many arguments");
      app->infile = fopen (arg, "r");
      if (!app->infile)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      app->infile = stdin;
      break;
    }
  return 0;
}

int
fituvalu_2sq_search_616 (struct fv_app_2sq_search_616_t *app, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  struct rec rec;
  mpz_inits (rec.ap[0], rec.ap[1], NULL);
  mpz_t b[2], c[2];

  for (int i = 0; i < 2; i++)
    {
      mpz_init (b[i]);
      mpz_init (c[i]);
    }

  mpz_t diff, middle, sum;
  mpz_inits (diff, middle, sum, NULL);
  while (1)
    {
      read = app->read_pair (app->infile, rec.ap, &line, &len);
      if (read == -1)
        break;
      update_linecount (&linecount);
      if (app->positive)
        pair_search2 (app, &rec, b, c, diff, &middle, &sum, out);
      else
        pair_search (app, &rec, b, c, &diff, &middle, &sum, out);
    }
  if (line)
    free (line);

  mpz_clears (rec.ap[0], rec.ap[1], NULL);
  mpz_clears (diff, middle, sum, NULL);

  for (int i = 0; i < 2; i++)
    {
      mpz_clear (b[i]);
      mpz_clear (c[i]);
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "pairs", 'p', 0, 0, "Show the pair instead of the square"},
  { "positive", 'P', 0, 0, "Only find squares with all positive numbers"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Try to make a 3x3 magic square with 6 or more perfect squares by giving it a series of 2 square progressions in FILE (X1 and X2).  The program iterates over pairs of squares to find two squares that have the correct difference.\vEach square is laid out like:\n\
  +------+------+------+\n\
  |  Y1  |  Z1  |  X2  |\n\
  +------+------+------+\n\
  |      |      |      |\n\
  +------+------+------+\n\
  |  X1  |  Y2  |  Z2  |\n\
  +------+------+------+\nThe 63 in the name comes from the fact we're trying to make a square of the form 6:3.  By default this program generates squares with a single negative value.  Sometimes near-magic squares are produced.",
  0
};

int
main (int argc, char **argv)
{
  init_linecount (&linecount);
  struct fv_app_2sq_search_616_t app;
  memset (&app, 0, sizeof (app));

  app.display_square = display_square_record;
  app.read_pair = read_two_numbers_from_stream;
  app.display_pair = display_two_record;
  app.infile = stdin;

  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_2sq_search_616 (&app, stdout);
}
