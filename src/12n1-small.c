#include <stdio.h>
#include <stdlib.h>
#include <argp.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "magicsquareutil.h"

struct fv_app_twelven1_t
{
  FILE *out;
  unsigned long long num;
  int in_binary;
  int out_binary;
  int start;
  int num_args;
  int filter;
  FILE *infile;
  int progress;
  unsigned long long stop;
  int even;
  int odd;
  int twelve;
  int square;
};

static int
fituvalu_twelven1 (struct fv_app_twelven1_t *app, long long i, FILE *out)
{
  unsigned long long j;
  if (app->start)
    {
      i--;
      i /= app->twelve;
      if (i % 2 == 1)
        i++;
    }
  else
    {
      if (i > 2)
        {
          if (i % 2 == 1)
            i--;
        }
      else
        i = 2;
    }
  if (app->even && i % 2 == 1)
    i++;
  if (app->odd && i % 2 == 0)
    i--;
  while (1)
    {
      j = (i * app->twelve) + 1;
      int mod10 = j % 10;
      if (mod10 != 3 && mod10 != 7)
        {
          int show = 1;
          if (app->square)
            show = small_is_square (j);
          if (app->out_binary)
            fwrite (&j, sizeof (j), 1, out);
          else
            fprintf (out, "%llu\n", j);
          fflush (out);
        }
      i+=2;
      if (app->stop && j > app->stop)
        break;
    }
}

static int
fituvalu_twelven1_filter (struct fv_app_twelven1_t *app, FILE *in, FILE *out)
{
  ssize_t read;
  char *line = NULL;
  size_t len = 0;
  char *end = NULL;
  unsigned long long int i;
  int num_lines = 0;
  const int max_num_lines = 100000;
          
  char fname[32];
  snprintf (fname, sizeof (fname), "/tmp/%s.%d",
            "12n1-small", getpid ());

  while (1)
    {
      unsigned long long int ii;
      if (app->in_binary)
        read = fread (&ii, sizeof (ii), 1, in);
      else
        read = fv_getline (&line, &len, in);
      if (read == -1)
        break;
      else if (!app->in_binary)
        ii = strtoull (line, &end, 10);
      i = ii;
      if (!app->in_binary)
        {
          if (end == NULL || *end != '\n')
            continue;
        }
      if (app->progress)
        {
          num_lines++;
          if (num_lines == max_num_lines)
            {
              FILE *fp = fopen (fname, "a");
              if (fp)
                {
                  fprintf (fp, "%lu, %s", time (NULL), line);
                  fclose (fp);
                }
              num_lines = 0;
            }
        }
      if ((i - 1) % app->twelve == 0)
        {
          int digit = i % 10;
          int digitisthreeorseven = digit == 3 || digit == 7;
          int even = 0;
          //if (app->even)
            {
              even = (i - 1) / app->twelve % 2 == 0;
            }
          int show = 0;
          if (app->even && even)
            show = 1;
          else if (!app->even && !even)
            show = 1;
          if (app->odd && !even)
            show = 1;
          if (app->square)
            show = small_is_square (i);

          if (digitisthreeorseven)
            show = 0;
          if (show)
            {
              if (app->out_binary)
                fwrite (&i, sizeof (i), 1, out);
              else
                {
                  if (app->in_binary)
                    fprintf (out, "%llu\n", i);
                  else
                    fprintf (out, "%s", line);
                }
              fflush (out);
            }
        }

    }
  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_twelven1_t *app = (struct fv_app_twelven1_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case '2':
      app->square = 1;
      break;
    case 'n':
        {
          app->twelve = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0' || app->twelve <= 1)
            argp_error (state, "invalid argument `%s' for -n", arg);
        }
      break;
    case 'e':
      app->even = 1;
      break;
    case 'O':
      app->odd = 1;
      break;
    case 'S':
        {
          app->stop = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0' || app->stop <= 0)
            argp_error (state, "invalid argument `%s' for --stop", arg);
        }
      break;
    case 'p':
      app->progress = 1;
      break;
    case 'f':
      app->filter = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->out_binary = 1;
      break;
    case 's':
      app->start = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else if (app->num_args == 0)
        {
          char *end = NULL;
          app->num = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0' || app->num <= 0)
            argp_error (state, "invalid value `%s' for NUM", arg);
          app->num_args++;
        }
      break;
    case ARGP_KEY_FINI:
      if (app->filter)
        {
          if (!app->even && !app->odd)
            {
              app->even = 1;
              app->odd = 1;
            }
        }
      else
        {
          if (!app->even && !app->odd)
            app->even = 1;
        }
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "out-binary", 'o', 0, 0, "Output raw unsigned long longs instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text (with -f)"},
  { "start", 's', 0, 0, "Use NUM-1/12 as a starting point"},
  { "filter", 'f', 0, 0, "Read in numbers and only show the ones that are 12n+1 and not ending in 3 or 7"},
  { "stop", 'S', "NUM", 0, "When we get to NUM stop"},
  { "progress", 'p', 0, OPTION_HIDDEN, "Show progress information in /tmp"},
  { "even", 'e', 0, 0, "Same but the n in 12n+1 is even"},
  { "odd", 'O', 0, 0, "Same but the n in 12n+1 is odd"},
  { "square", '2', 0, 0, "Only show numbers that are square"},
  { NULL, 'n', "NUM", OPTION_HIDDEN, "Use NUM instead of 12"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "NUM",
  "Count upwards from NUM such that the set of numbers is NUM * 12 + 1, and the numbers don't end in 3 or 7.\vThis program creates suitable input for filter-composite-small.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_twelven1_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.infile = stdin;
  app.twelve = 12;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (app.filter)
    return fituvalu_twelven1_filter (&app, app.infile, app.out);
  else
    return fituvalu_twelven1 (&app, app.num, app.out);
}
