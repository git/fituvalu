/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_61_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  int threesq;
  int quick;
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_61_type_1 (struct fv_app_search_61_t *app, unsigned long long lo, unsigned long long hi, unsigned long long distance, unsigned long long sum, unsigned long long sum_minus_middle, long long int (*s)[3][3])
{
  unsigned long long i, iroot, j, distance2, limit, twoiroot;

  distance2 = distance * 2;

  limit = sum;

  iroot = sqrtl (distance2);
  iroot++;
  i = iroot * iroot;

// +---+---+---+
// |   |   | C |
// +---+---+---+
// |   | B | D |
// +---+---+---+
// | A | E |   |
// +---+---+---+

  (*s)[0][2] = hi;
  (*s)[2][0] = lo;

  while (1)
    {
      j = i - distance2;

      (*s)[2][1] = i;
      (*s)[1][2] = j;

      (*s)[0][0] = (*s)[1][2] + distance;

      (*s)[0][1] = sum_minus_middle - (*s)[2][1];
      (*s)[1][0] = sum_minus_middle - (*s)[1][2];
      (*s)[2][2] = sum_minus_middle - (*s)[0][0];

        {
          int dup = 0;
          //check for dups
          if ((*s)[1][1] == (*s)[1][2] ||
              (*s)[1][1] == (*s)[2][2])
            dup = 1;
          if (!dup &&
              small_is_square (j) &&
              small_is_square ((*s)[2][2]))
            {
              pthread_mutex_lock (&display_lock);
              fprintf (app->out,
                       "%llu, %lld, %llu, "
                       "%llu, %llu, %llu, "
                       "%llu, %llu, %llu, \n",
                       (*s)[0][0], (*s)[0][1], (*s)[0][2],
                       (*s)[1][0], (*s)[1][1], (*s)[1][2],
                       (*s)[2][0], (*s)[2][1], (*s)[2][2]);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }

        }
      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
      if (i > limit)
        break;
    }
}

static int
square_is_twelvenminusone (unsigned long long i)
{
  return (i - 1) % 12 == 0;
}

static int
detect (unsigned long long int num)
{
  //we return 1 if the next number in the sequence is 4 squares ahead
  unsigned long long int root = sqrtl(num);

  unsigned long long int p1root = root + 2;
  unsigned long long int p2root = root + 4;
  unsigned long long int p1 = p1root * p1root;
  unsigned long long int p2 = p2root * p2root;

  int checkp1 = square_is_twelvenminusone (p1);
  int checkp2 = square_is_twelvenminusone (p2);

  if (checkp1 && !checkp2)
    return 0;
  else if (!checkp1 && checkp2)
    return 1;
  //can't get here
  return 0;
}

static void
quick_generate_61_type_1 (struct fv_app_search_61_t *app, unsigned long long lo, unsigned long long hi, unsigned long long distance, unsigned long long sum, unsigned long long sum_minus_middle, long long int (*s)[3][3])
{
  unsigned long long i, iroot, j, distance2, limit, twoiroot;

  distance2 = distance * 2;

  limit = sum;

  iroot = sqrtl (distance2);
  if (iroot % 2 == 0)
    {
      iroot++;
      i = iroot * iroot;
    }
  else
    i = iroot * iroot;

// +---+---+---+
// |   |   | C |
// +---+---+---+
// |   | B | D |
// +---+---+---+
// | A | E |   |
// +---+---+---+

  (*s)[0][2] = hi;
  (*s)[2][0] = lo;

  while (1)
    {
      j = i - distance2;

      (*s)[2][1] = i;
      (*s)[1][2] = j;

      (*s)[0][0] = (*s)[1][2] + distance;

      (*s)[0][1] = sum_minus_middle - (*s)[2][1];
      (*s)[1][0] = sum_minus_middle - (*s)[1][2];
      (*s)[2][2] = sum_minus_middle - (*s)[0][0];

        {
          int dup = 0;
          //check for dups
          if ((*s)[1][1] == (*s)[1][2] ||
              (*s)[1][1] == (*s)[2][2])
            dup = 1;
          if (!dup &&
              small_is_square (j) &&
              small_is_square ((*s)[2][2]))
            {
              pthread_mutex_lock (&display_lock);
              fprintf (app->out,
                       "%llu, %lld, %llu, "
                       "%llu, %llu, %llu, "
                       "%llu, %llu, %llu, \n",
                       (*s)[0][0], (*s)[0][1], (*s)[0][2],
                       (*s)[1][0], (*s)[1][1], (*s)[1][2],
                       (*s)[2][0], (*s)[2][1], (*s)[2][2]);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }

        }

      //advance i by 2 squares
      unsigned long long int fourroot = iroot * 4;
      i += fourroot;
      i+=4;
      iroot+=2;

      if (i > limit)
        break;
    }
}

static unsigned long long int
gcd (unsigned long long int n1, unsigned long long int n2)
{
  while (n1 != n2)
    {
      if(n1 > n2)
        n1 -= n2;
      else
        n2 -= n1;
    }
  return n1;
}
static void
handle_progression (struct fv_app_search_61_t *app, unsigned long long lo, unsigned long long hi, unsigned long long int distance, unsigned long long sum, unsigned long long sum_minus_middle, long long (*s)[3][3])
{
  unsigned long long int num = gcd (lo, hi);
  if (num % 2 == 0)
    return;
  if (app->quick)
    quick_generate_61_type_1 (app, lo, hi, distance, sum, sum_minus_middle, s);
  else
    generate_61_type_1 (app, lo, hi, distance, sum, sum_minus_middle, s);
}

static void
generate_progressions (struct fv_app_search_61_t *app, unsigned long long n, long long (*s)[3][3])
{
  unsigned long long i, iroot, diff, limit, nn, mn, twomn, lo, hi, sum, sum_minus_middle, twoiroot;

  sum = n * 3;
  sum_minus_middle = sum - n;
  limit = n / 2;
  i = 1;
  iroot = 1;
  while (1)
    {
      if (i > limit)
        break;

      diff = n - i;

      if (small_is_square (diff))
        {
          nn = sqrtl (diff);
          mn = iroot * nn;
          twomn = mn * 2;
          lo = n - twomn;
          hi = n + twomn;
      
          if (twomn > 0)
            handle_progression (app, lo, hi, twomn, sum, sum_minus_middle, s);
        }

      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
  return;
}

static void
quick_generate_progressions (struct fv_app_search_61_t *app, unsigned long long n, long long (*s)[3][3])
{
  unsigned long long i, iroot, diff, limit, nn, mn, twomn, lo, hi, sum, sum_minus_middle, twoiroot;

  sum = n * 3;
  sum_minus_middle = sum - n;
  limit = n / 2;
  i = n;
  iroot = sqrtl (n);
  if (iroot % 2 == 0)
    {
      iroot++;
      i = iroot * iroot;
    }
  int up_by_four = detect (i);
  while (1)
    {
      unsigned long long int distance = i - n;
      if (distance > n)
        break;
      unsigned long long int j = n - distance;

      if (small_is_square (j))
        {
          lo = j;
          hi = i;
      
          if (distance > 0)
            handle_progression (app, lo, hi, distance, sum, sum_minus_middle, s);
        }

      if (up_by_four)
        {
          //advance i by 4 squares
          unsigned long long int eightroot = iroot * 8;
          i += eightroot;
          i+=16;
          iroot+=4;
        }
      else
        {
          //advance i by 2 squares
          unsigned long long int fourroot = iroot * 4;
          i += fourroot;
          i+=4;
          iroot+=2;
        }

      up_by_four = !up_by_four;

    }
  return;
}


static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_61_t *app =
    (struct fv_app_search_61_t *) param->data;

  long long int s[3][3];

  unsigned long long int a[3];
  unsigned long long int b[3];

  unsigned long long distance, sum, sum_minus_middle;

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          if (fread (a, sizeof (unsigned long long), 3, app->infile) != 3)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          size_t read = read_ull_numbers (app->infile, a, 3, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      b[0] = a[0];
      b[1] = a[1];
      b[2] = a[2];
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on 3sq
        {
          s[1][1] = b[1];
          distance = b[1] - b[0];
          sum = b[1] * 3;
          sum_minus_middle = sum - b[1];
          if (distance > 0)
            handle_progression (app, b[0], b[2],
                                distance, sum, sum_minus_middle, &s);
        }
    }


  if (line)
    free (line);
  return NULL;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_61_t *app =
    (struct fv_app_search_61_t *) param->data;

  long long s[3][3];

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          size_t read = fread (&n, sizeof (unsigned long long), 1, app->infile);
          if (read != 1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          num = strtoull (line, &end, 10);
        }
      n = num;

      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
        {
          s[1][1] = n;
          if (app->quick)
            quick_generate_progressions (app, n, &s);
          else
            generate_progressions (app, n, &s);
        }
    }

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_61 (struct fv_app_search_61_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_61_t *app = (struct fv_app_search_61_t *) state->input;
  switch (key)
    {
    case 'q':
      app->quick = 1;
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "quick", 'q', 0, 0, "Skip when A, C, D, or F aren't congruent to 1 or 5 mod 6 then squared"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:1 given a FILE containing center values.\vWhen FILE is not provided, it is read from the standard input.  This program is limited to 64-bit integers.  Magic squares of type 6:1 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   |   | X |   |   |   | C |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating F\n"
"  |   | X | X |   |   | B | D |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of 2*(B-A) below F.\n"
"  | X | X | X |   | A | F | Z |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_61_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_61 (&app);
}
//seq 1000 | ./filter-prime -p | sed -e 's/$/^6/'
//every square found here is 12n+1 and 4n+1
//which is to say each square is pythagorean
//
//center values are numbers congruent to 1 or 5 mod 6 then squared.
//https://oeis.org/A007310
