/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_65_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int threesq;
  int quick_def;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, sum, sum_minus_middle, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t i, j, iroot, twoiroot, limit;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_65_type_1 (struct fv_app_search_65_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_sqrt (q->iroot, w->distance);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  //iterate from distance to sum upwards, checking downwards for another square
  //we end at sum because we want at most two negative numbers
  //three negatives means we can't find a seven
  while (1)
    {
      mpz_sub (q->j, q->i, w->distance);

      mpz_set ((*s)[1][0], w->lo);
      mpz_set ((*s)[1][2], w->hi);

      mpz_set ((*s)[2][0], q->i);
      mpz_set ((*s)[2][2], q->j);

      mpz_sub ((*s)[2][1], w->sum, (*s)[2][0]);
      mpz_sub ((*s)[2][1], (*s)[2][1], (*s)[2][2]);

      mpz_sub ((*s)[0][1], w->sum_minus_middle, (*s)[2][1]);
      mpz_sub ((*s)[0][0], w->sum_minus_middle, (*s)[2][2]);
      mpz_sub ((*s)[0][2], w->sum_minus_middle, (*s)[2][0]);

      int dup = 0;
      if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
          mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0)
        dup = 1;

      if (!dup)
        {
          if (mpz_perfect_square_p ((*s)[2][1]) && mpz_perfect_square_p (q->j))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }

      if (mpz_cmp (q->i, w->sum) > 0)
        break;

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
    }
}

static void
quick_generate_65_type_1 (struct fv_app_search_65_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_sqrt (q->iroot, w->distance);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  mpz_set (q->limit, w->sum_minus_middle);
  while (1)
    {
      mpz_sub (q->j, q->i, w->distance);

      mpz_set ((*s)[1][0], w->lo);
      mpz_set ((*s)[1][2], w->hi);

      mpz_set ((*s)[2][0], q->i);
      mpz_set ((*s)[2][2], q->j);

      mpz_sub ((*s)[2][1], w->sum, (*s)[2][0]);
      mpz_sub ((*s)[2][1], (*s)[2][1], (*s)[2][2]);

      mpz_sub ((*s)[0][1], w->sum_minus_middle, (*s)[2][1]);
      mpz_sub ((*s)[0][0], w->sum_minus_middle, (*s)[2][2]);
      mpz_sub ((*s)[0][2], w->sum_minus_middle, (*s)[2][0]);

      int dup = 0;
      if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
          mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0)
        dup = 1;

      if (!dup)
        {
          if (mpz_perfect_square_p ((*s)[2][1]) && mpz_perfect_square_p (q->j))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }

      if (mpz_cmp (q->i, q->limit) > 0)
        break;

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
    }
}

static void
handle_progression (struct fv_app_search_65_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  if (app->quick_def)
    quick_generate_65_type_1 (app, w, q, s);
  else
    generate_65_type_1 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_65_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_mul_ui (w->sum, (*s)[1][1], 3);
  mpz_mul_ui (w->sum_minus_middle, (*s)[1][1], 2);
  mpz_cdiv_q_ui (w->limit, (*s)[1][1], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);

  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[1][1], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[1][1], w->distance);
          mpz_add (w->hi, (*s)[1][1], w->distance);
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }
      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_65_t *app =
    (struct fv_app_search_65_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.j, q.iroot, q.twoiroot, q.limit, NULL);

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[1][1], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[1][1], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[1][1], 1) > 0)
        generate_progressions (app, &w, &q, &s);
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  
  mpz_clears (q.i, q.j, q.iroot, q.twoiroot, q.limit, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_65_t *app =
    (struct fv_app_search_65_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.j, q.iroot, q.twoiroot, q.limit, NULL);

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[1][1], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[1][1], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[1][1], w.lo);
      mpz_mul_ui (w.sum, s[1][1], 3);
      mpz_mul_ui (w.sum_minus_middle, s[1][1], 2);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);


  mpz_clears (q.i, q.j, q.iroot, q.twoiroot, q.limit, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_65 (struct fv_app_search_65_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *end = NULL;
  struct fv_app_search_65_t *app = (struct fv_app_search_65_t *) state->input;
  switch (key)
    {
    case 'q':
      app->quick_def = 1;
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "quick-def", 'q', 0, OPTION_HIDDEN, "Do an abridged search"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:5 given a FILE containing center values.\vWhen FILE is not provided, it is read from the standard input.  Center values must be perfect squares.  This program generates magic squares with at most two numbers being negative.  Magic squares of type 6:5 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   |   |   |   |   |   |   |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating E\n"
"  | X | X | X |   | A | B | C |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below E.\n"
"  | X | X | X |   | E | Z | D |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_65_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_65 (&app);
}
/*

the point of this is to identify information that can help us get more 6:5s.  we want to be able to predict 'center' values (the B values) for 6:5 magic squares.

  +---+---+---+   +---+---+---+
  |   |   |   |   |   |   |   |
  +---+---+---+   +---+---+---+
  | X | X | X |   | A | B | C |
  +---+---+---+   +---+---+---+
  | X | X | X |   | E | Z | D |
  +---+---+---+   +---+---+---+

we identified which B values lead to magic squares with 6 perfect squares in the 6:5 configuration, and then here we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:5 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 3438 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

110, 58, 84, 270 / 37640 = 0.007173
110, 82, 60, 273 / 37651 = 0.007251
86, 108, 58, 280 / 37654 = 0.007436
110, 84, 58, 284 / 37648 = 0.007544
86, 72, 94, 284 / 37642 = 0.007545
110, 48, 94, 288 / 37638 = 0.007652
74, 94, 84, 293 / 37642 = 0.007784
74, 118, 60, 296 / 37656 = 0.007861
74, 120, 58, 307 / 37651 = 0.008154
74, 84, 94, 311 / 37646 = 0.008261

well we're not quite at a percent.  all of the sums add up to 252, so perhaps that's a better breadth to use than 121. e.g. 74+84+94=252.

second run is depth of 2, and breadth of 2000.
1082, 1942, 25 / 2105 = 0.011876
1874, 1990, 20 / 1651 = 0.012114
1960, 1148, 25 / 2050 = 0.012195
782, 1654, 32 / 2616 = 0.012232
1072, 552, 49 / 3926 = 0.012481
1596, 1008, 31 / 2448 = 0.012663
1884, 552, 34 / 2620 = 0.012977
840, 1596, 35 / 2620 = 0.013359
1072, 1364, 38 / 2623 = 0.014487
1368, 1740, 33 / 2061 = 0.016012


hmm the pecentage is about 2 times higher.  down from 311 hits to 33 though.
there's a worry that we're just trying to predict somewhat random numbers here, and that any of the top 5 are just as good.  there's a frequent sum of 2436 here. (1072+1364)

and just for kicks, let's try a depth of 1 and a breadth of 100000.

97872, 2 / 35 = 0.057143
64700, 3 / 52 = 0.057692
65100, 3 / 52 = 0.057692
66192, 3 / 51 = 0.058824
99856, 2 / 34 = 0.058824
99904, 2 / 34 = 0.058824
68916, 3 / 49 = 0.061224
74028, 3 / 46 = 0.065217


i'm sure we're just in la-la guessing land here.
but maybe 74028 is worth trying.

let's say the minimum count has to be 10 or higher.
1860, 14 / 1711 = 0.008182
1848, 15 / 1722 = 0.008711
2608, 11 / 1222 = 0.009002
1092, 27 / 2909 = 0.009282
2100, 15 / 1518 = 0.009881
1932, 17 / 1649 = 0.010309
1736, 19 / 1834 = 0.010360
2604, 14 / 1226 = 0.011419
3864, 11 / 828 = 0.013285
5208, 11 / 618 = 0.017799


well 5208 is nearly 2 percent.  interesting.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

2, 2, 2, 3438 / 1579512 = 0.002177
2, 2, 0, 3438 / 1579512 = 0.002177
2, 1, 1, 3438 / 2369268 = 0.001451
2, 0, 2, 3438 / 1579512 = 0.002177
2, 0, 0, 3438 / 1579512 = 0.002177
1, 1, 2, 3438 / 2369268 = 0.001451
1, 1, 1, 3438 / 3159024 = 0.001088
1, 1, 0, 3438 / 3159024 = 0.001088
1, 0, 1, 3438 / 3159024 = 0.001088
1, 0, 0, 3438 / 3159024 = 0.001088
0, 2, 2, 3438 / 1579512 = 0.002177
0, 2, 0, 3438 / 1579512 = 0.002177
0, 1, 1, 3438 / 3159024 = 0.001088
0, 1, 0, 3438 / 3159024 = 0.001088
0, 0, 2, 3438 / 1579512 = 0.002177
0, 0, 1, 3438 / 3159024 = 0.001088
4, 2, 2, 2755 / 1184637 = 0.002326


pretty stinky.  gotta go up by 2s to get them all.  nothing better.

results:

so looking forward for more sixes, pick one of "99904", "68916", or "74028".  Try "5208", and "1368, 1740", and "74, 84, 94".

we can't recalculate any quicker and get them all.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 841 in sq-seq-finder.
0, 116, 116, 194 / 27275 = 0.007113
116, 0, 0, 194 / 27275 = 0.007113
116, 0, 116, 194 / 27275 = 0.007113
116, 116, 0, 194 / 27275 = 0.007113
116, 116, 116, 194 / 27275 = 0.007113
82, 84, 86, 268 / 37645 = 0.007119
116, 120, 112, 198 / 27277 = 0.007259
46, 94, 112, 277 / 37645 = 0.007358
46, 118, 88, 280 / 37658 = 0.007435
46, 120, 86, 291 / 37652 = 0.007729

pretty tight distribution here. lots of zeroes and 116s. 252 is a popular sum (46+118+88)
i keep seeing arithmetic progressions.  here we have 82 84 86.  is this a thing?

well the best of the bunch is "46, 120, 86" but it has our maximum breadth so let's go with the next best of 46, 118, 88.

well we didn't do any better with a different starting point.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

232, 580, 115 / 7835 = 0.014678
1508, 1740, 29 / 1973 = 0.014698
1856, 1392, 29 / 1967 = 0.014743
1508, 928, 41 / 2629 = 0.015595
1566, 1682, 31 / 1973 = 0.015712
754, 1682, 45 / 2628 = 0.017123
1044, 580, 68 / 3939 = 0.017263
1856, 580, 47 / 2632 = 0.017857
812, 1624, 48 / 2630 = 0.018251
1044, 1392, 51 / 2631 = 0.019384

well we did much better here, but we only matched 51.  i'd say it's worth a shot to try "1044, 1392".
a sum of 2436 (1044+1392) comes up again and again.
nearly at 2 percent here.  very nice but it's no grand pattern.

okay let's do depth=1, and breadth is 100000

71282, 4 / 49 = 0.081633
96222, 3 / 36 = 0.083333
98331, 3 / 36 = 0.083333
99260, 3 / 35 = 0.085714
99652, 3 / 35 = 0.085714
99874, 3 / 35 = 0.085714
77084, 4 / 45 = 0.088889
91350, 4 / 39 = 0.102564

well 10 percent is certainly high,  but does it pan out in the next 39?

let's do depth=1, breadth is 100000, min hits of 10

3770, 13 / 851 = 0.015276
4988, 10 / 644 = 0.015528
5278, 10 / 609 = 0.016420
3248, 17 / 989 = 0.017189
4060, 14 / 793 = 0.017654
6188, 10 / 521 = 0.019194
6496, 10 / 497 = 0.020121
7308, 12 / 445 = 0.026966
6300, 14 / 516 = 0.027132
10556, 10 / 310 = 0.032258

well we got to 3 percent. 6300 is a nice round number.

so overall, looking forward for more sixes, pick one of "99904", "68916", or "74028", and then one of "99874", "77084", or "91350".  Try "1368, 1740", "74, 84, 94", "1044, 1392", and "46, 118, 88".

okay, let's do the first run again but sort on hits to see how it shakes out.

2, 2, 2, 3438 / 1579499 = 0.002177
2, 2, 0, 3438 / 1579499 = 0.002177
2, 1, 1, 3438 / 2369248 = 0.001451
2, 0, 2, 3438 / 1579499 = 0.002177
2, 0, 0, 3438 / 1579499 = 0.002177
1, 1, 2, 3438 / 2369248 = 0.001451
1, 1, 1, 3438 / 3158997 = 0.001088
1, 1, 0, 3438 / 3158997 = 0.001088
1, 0, 1, 3438 / 3158997 = 0.001088
1, 0, 0, 3438 / 3158997 = 0.001088
0, 2, 2, 3438 / 1579499 = 0.002177
0, 2, 0, 3438 / 1579499 = 0.002177
0, 1, 1, 3438 / 3158997 = 0.001088
0, 1, 0, 3438 / 3158997 = 0.001088
0, 0, 2, 3438 / 1579499 = 0.002177
0, 0, 1, 3438 / 3158997 = 0.001088
2, 2, 4, 2755 / 1184628 = 0.002326


no better than before. gotta go up by 2 squares at a time to get them all.

this concludes our analysis of anticipating B values in 6:5 magic squares.

in order to get high value 6:5s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

also we could consider running this test again but including all D values as B values.

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="99904" 1 10000000000000000000000000000 | ./search-65
sq-seq --pattern="68916" 1 10000000000000000000000000000 | ./search-65
sq-seq --pattern="74028" 1 10000000000000000000000000000 | ./search-65
sq-seq --pattern="74,84,94" 1 10000000000000000000000000000 | ./search-65
sq-seq --pattern="1368,1740" 1 10000000000000000000000000000 | ./search-65
sq-seq --pattern="5208" 1 10000000000000000000000000000 | ./search-65

sq-seq --pattern="99874" 841 10000000000000000000000000000 | ./search-65
sq-seq --pattern="77084" 841 10000000000000000000000000000 | ./search-65
sq-seq --pattern="91350" 841 10000000000000000000000000000 | ./search-65
sq-seq --pattern="1044,1392" 841 10000000000000000000000000000 | ./search-65
sq-seq --pattern="10556" 841 10000000000000000000000000000 | ./search-65
*/
