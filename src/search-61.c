/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_61_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int threesq;
  int quick_abc;
  int quick_def;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, sum, sum_minus_middle, twoiroot;
};
struct mpz_inner_worker_t
{
  mpz_t i, iroot, j, distance2, twoiroot, limit;
};
static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_61_type_1 (struct fv_app_search_61_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_mul_ui (q->distance2, w->distance, 2);
  mpz_set (q->limit, w->sum);

  mpz_sqrt (q->iroot, q->distance2);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

// +---+---+---+
// |   |   | C |
// +---+---+---+
// |   | B | E |
// +---+---+---+
// | A | D |   |
// +---+---+---+

  // XXX XXX XXX 0,0 is at least 1/2 of B

  mpz_set ((*s)[0][2], w->hi);
  mpz_set ((*s)[2][0], w->lo);

  while (1)
    {
      mpz_sub (q->j, q->i, q->distance2);

      mpz_set ((*s)[2][1], q->i);
      mpz_set ((*s)[1][2], q->j);

      mpz_add ((*s)[0][0], (*s)[1][2], w->distance);

      mpz_sub ((*s)[0][1], w->sum_minus_middle, (*s)[2][1]);
      mpz_sub ((*s)[1][0], w->sum_minus_middle, (*s)[1][2]);
      mpz_sub ((*s)[2][2], w->sum_minus_middle, (*s)[0][0]);

        {
          int dup = 0;
          //check for dups
          if (mpz_cmp ((*s)[1][1], (*s)[1][2]) == 0 ||
              mpz_cmp ((*s)[1][1], (*s)[2][2]) == 0)
            dup = 1;
          if (!dup &&
              mpz_perfect_square_p (q->j) &&
              mpz_perfect_square_p ((*s)[2][2]))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }

        }
      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
quick_generate_61_type_1 (struct fv_app_search_61_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_mul_ui (q->distance2, w->distance, 2);
  mpz_set (q->limit, w->sum);

  mpz_mul_ui (q->i, (*s)[1][1], 2);
  mpz_sqrt (q->iroot, q->i);
  mpz_mul (q->i, q->iroot, q->iroot);


// +---+---+---+
// |   |   | C |
// +---+---+---+
// |   | B | E |
// +---+---+---+
// | A | D |   |
// +---+---+---+

  // XXX XXX XXX 0,0 is at least 1/2 of B

  mpz_set ((*s)[0][2], w->hi);
  mpz_set ((*s)[2][0], w->lo);

  while (1)
    {
      mpz_sub (q->j, q->i, q->distance2);

      mpz_set ((*s)[2][1], q->i);
      mpz_set ((*s)[1][2], q->j);

      mpz_add ((*s)[0][0], (*s)[1][2], w->distance);

      mpz_sub ((*s)[0][1], w->sum_minus_middle, (*s)[2][1]);
      mpz_sub ((*s)[1][0], w->sum_minus_middle, (*s)[1][2]);
      mpz_sub ((*s)[2][2], w->sum_minus_middle, (*s)[0][0]);

        {
          int dup = 0;
          //check for dups
          if (mpz_cmp ((*s)[1][1], (*s)[1][2]) == 0 ||
              mpz_cmp ((*s)[1][1], (*s)[2][2]) == 0)
            dup = 1;
          if (!dup &&
              mpz_perfect_square_p (q->j) &&
              mpz_perfect_square_p ((*s)[2][2]))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
handle_progression (struct fv_app_search_61_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  if (app->quick_def)
    quick_generate_61_type_1 (app, w, q, s);
  else
    generate_61_type_1 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_61_t *app, mpz_t (*s)[3][3], struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q)
{
  mpz_mul_ui (w->sum, (*s)[1][1], 3);
  mpz_mul_ui (w->sum_minus_middle, (*s)[1][1], 2);
  mpz_cdiv_q_ui (w->limit, (*s)[1][1], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[1][1], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[1][1], w->distance);
          mpz_add (w->hi, (*s)[1][1], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void
quick_generate_progressions (struct fv_app_search_61_t *app, mpz_t (*s)[3][3], struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q)
{
  mpz_mul_ui (w->sum, (*s)[1][1], 3);
  mpz_mul_ui (w->sum_minus_middle, (*s)[1][1], 2);
  mpz_cdiv_q_ui (w->limit, (*s)[1][1], 3);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[1][1], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[1][1], w->distance);
          mpz_add (w->hi, (*s)[1][1], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_61_t *app =
    (struct fv_app_search_61_t *) param->data;

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);

  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.distance2, q.twoiroot, q.limit, NULL);

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[1][1], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[1][1], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[1][1], 1) > 0)
        {
          if (app->quick_abc)
            quick_generate_progressions (app, &s, &w, &q);
          else
            generate_progressions (app, &s, &w, &q);
        }
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  mpz_clears (q.i, q.iroot, q.j, q.distance2, q.twoiroot, q.limit, NULL);

  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_61_t *app =
    (struct fv_app_search_61_t *) param->data;

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);

  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.distance2, q.twoiroot, q.limit, NULL);

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[1][1], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[1][1], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[1][1], w.lo);
      mpz_mul_ui (w.sum, s[1][1], 3);
      mpz_mul_ui (w.sum_minus_middle, s[1][1], 2);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  mpz_clears (q.i, q.iroot, q.j, q.distance2, q.twoiroot, q.limit, NULL);

  if (line)
    free (line);
  return NULL;
}


int
fituvalu_search_61 (struct fv_app_search_61_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_61_t *app = (struct fv_app_search_61_t *) state->input;
  switch (key)
    {
    case 'Q':
      app->quick_abc = 1;
      break;
    case 'q':
      app->quick_def = 1;
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "quick-def", 'q', 0, OPTION_HIDDEN, "Do an abridged search"},
  { "quick-abc", 'Q', 0, OPTION_HIDDEN, "Do an abridged search"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:1 given a FILE containing center values.\vWhen FILE is not provided, it is read from the standard input.  Magic squares of type 6:1 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   |   | X |   |   |   | C |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating F\n"
"  |   | X | X |   |   | B | D |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of 2*(B-A) below F.\n"
"  | X | X | X |   | A | F | Z |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_61_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_61 (&app);
}
/*

the point of this is to identify information that can help us get more 6:1s.  we want to be able to predict 'center' values (the B values) for 6:1 magic squares.

  +---+---+---+   +---+---+---+
  |   |   | X |   |   |   | C |
  +---+---+---+   +---+---+---+
  |   | X | X |   |   | B | D |
  +---+---+---+   +---+---+---+
  | X | X | X |   | A | F | Z |
  +---+---+---+   +---+---+---+


we identified which B values lead to magic squares with 6 perfect squares in the 6:1 configuration, and then we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:1 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 263 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

84, 100, 20, 49 / 46067 = 0.001064
24, 100, 76, 51 / 46989 = 0.001085
84, 16, 104, 50 / 46067 = 0.001085
84, 48, 72, 50 / 46067 = 0.001085
84, 52, 68, 51 / 46067 = 0.001107
84, 68, 120, 39 / 34551 = 0.001129
84, 120, 0, 40 / 30712 = 0.001302
16, 68, 120, 68 / 46068 = 0.001476

well there's a 120, so maybe we need a greater breadth.
incidentally it's the lowest percentage among all magic square configurations.  is this a sign of something?

second run is depth of 2, and breadth of 2000.

1784, 936, 8 / 2304 = 0.003472
424, 1276, 13 / 3688 = 0.003525
424, 1616, 11 / 3073 = 0.003580
1444, 1616, 10 / 2049 = 0.004880

hmm the pecentage is 3 times higher.  down from 68 hits to 10 though.
there's a worry that we're just trying to predict somewhat random numbers here, and that any of the top 10 are just as good.


and here's the ones that start with 16
16, 1548, 6 / 4006 = 0.001498
16, 936, 10 / 6582 = 0.001519
16, 1916, 5 / 3244 = 0.001541
16, 1480, 7 / 4189 = 0.001671
16, 1832, 6 / 3393 = 0.001768
16, 1904, 6 / 3264 = 0.001838
16, 1988, 6 / 3128 = 0.001918
16, 1888, 7 / 3292 = 0.002126
so, way behind.  nothing to see here.

and just for kicks, let's try a depth of 1 and a breadth of 100000.

57172, 2 / 57 = 0.035088
63004, 2 / 52 = 0.038462
85452, 2 / 39 = 0.051282

i'm sure we're just in la-la guessing land here.
but maybe 85452 is worth trying.

let's say the minimum count has to be 10 or higher.
112, 13 / 27971 = 0.000465
108, 14 / 29006 = 0.000483
76, 20 / 41219 = 0.000485
12, 127 / 261043 = 0.000487
28, 58 / 111877 = 0.000518
156, 11 / 20083 = 0.000548
176, 11 / 17800 = 0.000618
168, 12 / 18647 = 0.000644
228, 11 / 13740 = 0.000801
84, 30 / 37292 = 0.000804

nothing to see here.  maybe the 12 is sorta interesting wrt 4 and 2 seen below.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

0, 0, 4, 263 / 783126 = 0.000336
0, 0, 2, 263 / 1566252 = 0.000168
0, 0, 1, 263 / 3132504 = 0.000084
4, 8, 4, 203 / 587345 = 0.000346
4, 4, 8, 202 / 587345 = 0.000344
4, 8, 12, 201 / 391564 = 0.000513
4, 12, 8, 192 / 391564 = 0.000490

iterating over every 4th square gets us all of them.


results:

so looking forward for more sixes, try "85452", "16, 1888", and disregard the "16, 68, 120" because it happens to have our max breadth.
in recalculating quicker, we skip forward by 4 squares starting from 1 and still get all of them.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 15625 in sq-seq-finder.
95, 105, 100, 50 / 31327 = 0.001596
97, 103, 100, 50 / 31327 = 0.001596
98, 102, 100, 50 / 31327 = 0.001596
99, 101, 100, 50 / 31327 = 0.001596
108, 92, 100, 51 / 31327 = 0.001628
96, 104, 100, 53 / 31327 = 0.001692
104, 96, 100, 54 / 31327 = 0.001724
92, 108, 100, 56 / 31328 = 0.001788
80, 120, 100, 60 / 31327 = 0.001915
120, 80, 100, 63 / 31327 = 0.002011

well lots of 120s, and no zeroes.  and it's amazing how much the starting value changes our results.

i dunno, "120, 80, 100" actually looks like something.
the sums are common.
120+80+100 = 300
95+105+100 = 300

starting at 1 doesn't have this property, which makes me think this is identifying actual structure in the sequence of matching squares.  interesting.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

300, 1740, 14 / 3074 = 0.004554
0, 1800, 8 / 1744 = 0.004587
1760, 1840, 8 / 1744 = 0.004587
1800, 0, 8 / 1744 = 0.004587
1800, 1800, 8 / 1744 = 0.004587
900, 1800, 11 / 2325 = 0.004731
300, 1400, 18 / 3689 = 0.004879
600, 1500, 15 / 2986 = 0.005023
1700, 1800, 10 / 1793 = 0.005577
1320, 1740, 14 / 2051 = 0.006826

well we have zeroes.  i bet we'd have 2000s if our breadth was 2001.

notice that we can't seem to ever get to 1 percent.

1800 is a thing.  isn't it strange that 1700,1800 does so well.

okay let's do depth=1, and breadth is 100000

98140, 2 / 34 = 0.058824
98272, 2 / 34 = 0.058824
98330, 2 / 34 = 0.058824
99250, 2 / 34 = 0.058824
99360, 2 / 34 = 0.058824
99505, 2 / 34 = 0.058824
99608, 2 / 34 = 0.058824
68100, 3 / 49 = 0.061224
71228, 3 / 47 = 0.063830
77480, 3 / 44 = 0.068182

six percent! and if any of these were giant even numbers i'd mark it as interesting.

let's do depth=1, breadth is 100000, min hits of 10

450, 14 / 6965 = 0.002010
300, 25 / 10446 = 0.002393
600, 13 / 5223 = 0.002489
700, 12 / 4477 = 0.002680
500, 17 / 6267 = 0.002713
900, 14 / 3487 = 0.004015

pretty interesting here.
900, and 450 and lots of even numbers.

so overall, looking forward with a start value of 15625 i'd try "900", "1800", "1700,1800", "1320,1740", "77480", and "120, 80, 100".

okay, let's do the first run again but sort on hits to see how it shakes out.

0, 0, 4, 263 / 783096 = 0.000336
0, 0, 2, 263 / 1566191 = 0.000168
0, 0, 1, 263 / 3132381 = 0.000084
0, 8, 4, 263 / 522064 = 0.000504
8, 4, 12, 207 / 391549 = 0.000529

lots of sums of 12 going on (not shown).
skipping forward by 4s still works fine with this new start value.
but notice we can go forward 8 and then 4 and still hit them all.

this concludes our analysis of anticipating B values in 6:1 magic squares.

in order to get high value 6:1s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="57172" 1 10000000000000000000000000000 | ./search-61
sq-seq --pattern="63004" 1 10000000000000000000000000000 | ./search-61
sq-seq --pattern="85452" 1 10000000000000000000000000000 | ./search-61
sq-seq --pattern="16,1888" 1 10000000000000000000000000000 | ./search-61
sq-seq --pattern="16,68,120" 1 10000000000000000000000000000 | ./search-61
sq-seq --pattern="84" 1 10000000000000000000000000000 | ./search-61

sq-seq --pattern="77480" 15625 10000000000000000000000000000 | ./search-61
sq-seq --pattern="68100" 15625 10000000000000000000000000000 | ./search-61
sq-seq --pattern="71228" 15625 10000000000000000000000000000 | ./search-61
sq-seq --pattern="1700,1800" 15625 10000000000000000000000000000 | ./search-61
sq-seq --pattern="1320,1740" 15625 10000000000000000000000000000 | ./search-61
sq-seq --pattern="120, 80, 100" 15625 10000000000000000000000000000 | ./search-61
sq-seq --pattern="1800" 15625 10000000000000000000000000000 | ./search-61



cross-comparison results

there are 8 categories of favoured patterns

d1b100000s1
d2b2000s1
d3b121s1
d1b100000m10s1
d1b100000sf
d2b2000sf
d3b121sf
d1b100000m10sf

s1 = start value of 1
sf = start of the first B value in our collection of magic squares
m10 = minimum hits of 10
d1 = depth of 1
b2000 = breadth of 2000


and there are 15 different configurations of sixes

        d1b100000s1         d1b100000sf
 6:1    85452, 0.051282     77480, 0.068182
 6:2    99705, 0.058824     96824, 0.083333
 6:4    81092, 0.048780     91586, 0.078947
 6:5    74028, 0.065217     91350, 0.102564
 6:6    99456, 0.058824     70005, 0.061224
 6:7    99984, 0.058824     77740, 0.088889
 6:8    93902, 0.055556     89790, 0.076923
 6:9    85459, 0.051282     96264, 0.083333
6:10    99564, 0.058824     98456, 0.085714
6:11    92292, 0.054054     93840, 0.105263
6:12    78044, 0.046512     82450, 0.071429
6:13    99288, 0.085714     87164, 0.097561
6:14    99984, 0.058824     77740, 0.088889
6:15    97165, 0.057143     97840, 0.083333
6:16    88620, 0.052632     90944, 0.078947

here we can see a link between 6:7 and 6:14 -- the stream of B values created by patterns s1 99984, and sf 77740 make 6:7 and 6:14 magic squares.  are they the same values? if yes, we can double our effectiveness with a single run.
incidentally, we've known there's a relationship between 6:7 and 6:14 magic squares because we always have half of the number of 6:7s that we do of 6:14s.
so this means there's probably a way to write a program that generates both 6:7 and 6:14s in an efficient manner (if their B values are shared.)

all of the sf values are better than the s1 values.  maybe if we pick a larger start value (say the first square over 1 million) will the percentage go up?) 

also the range of sf values is about twice as wide.  why should that be the case?

the best of the bunch is 6:11 sf, which happens to be 6th among all configurations wrt the number of magic squares found -- so it's not like we're just matching the one with the most squares.  but hey it's very likely just a fluke.  there's no way that the pattern really holds.  the next best sf is 6:5 which again is not a great source of magic squares at all (it is middling.)
and even if this pattern works, we still have to go to the trouble of calculating the three square progression, and then iterating over all of the search space.
is it a fool's game to increase the breadth such that it intersects with one of our high 6:11s? probably.

        d1b100000m10s1      d1b100000m10sf
 6:1       84, 0.000804       900, 0.004015
 6:2     2088, 0.006570      3948, 0.013547
 6:4      418, 0.001456       636, 0.002013
 6:5     5208, 0.017799     10556, 0.032258
 6:6      408, 0.001809       834, 0.002642
 6:7     2148, 0.006775      6600, 0.026477
 6:8      384, 0.001465      2952, 0.009320
 6:9     1026, 0.003249      4340, 0.013624
6:10     1260, 0.003990       600, 0.003997
6:11      876, 0.002772      9520, 0.037901
6:12      396, 0.001378      1428, 0.007654
6:13     5880, 0.021818      6300, 0.023346
6:14     2148, 0.006775      6600, 0.026477
6:15     2068, 0.006519      4368, 0.014966
6:16      630, 0.002192       840, 0.003980

well the 6:1 s1 value is much lower than the rest.  is this a function of having so few 6:1s?

we can again see the tie between 6:7 and 6:14.

and again each sf outperforms its associated s1.

6:11 and 6:5s win again in terms of percentage -- so it's not a flash in the pan, anyway.

        d2b2000s1                d2b2000sf
 6:1    1444, 1616, 0.004880     1320, 1740, 0.006826
 6:2    1368, 1740, 0.009268     1332, 1776, 0.013625
 6:4    1642, 1334, 0.004701     1932, 1020, 0.007449
 6:5    1368, 1740, 0.016012     1044, 1392, 0.019384
 6:6    1780, 1964, 0.005322     1560, 1770, 0.005793
 6:7    1884, 1886, 0.027075      400, 1300, 0.028022
 6:8    1512, 1514, 0.011021      984, 1476, 0.012871
 6:9    1546, 1310, 0.013459     1512, 1344, 0.015681
6:10    1546, 1310, 0.009932     1522, 1334, 0.009932
6:11     628, 1888, 0.015085     1360,  680, 0.021127
6:12    1546, 1310, 0.005858     1428, 1428, 0.007654
6:13    1800, 1140, 0.016957      812, 1624, 0.018209
6:14    1884, 1886, 0.027075      400, 1300, 0.028022
6:15    1104, 1548, 0.010824     1092, 1560, 0.012474
6:16    1546, 1310, 0.005856     1512, 1344, 0.006303

        d3b121s1                    d3b121s1f
 6:1     16,  68, 120, 0.001476     120,  80, 100, 0.002011
 6:2    120, 105, 111, 0.005206     111, 112, 113, 0.005135
 6:4     16,  68, 120, 0.001765      94, 116,  36, 0.001610
 6:5     74,  84,  94, 0.008261      46, 120,  86, 0.007729
 6:6     90,  90,  72, 0.002050     106,  26, 102, 0.002102
 6:7     16,  68, 120, 0.009175     100,  80, 120, 0.011556
 6:8     16,  68, 120, 0.003762     110,  54,  82, 0.003624
 6:9     16,  68, 120, 0.004742      56, 112,   0, 0.005217
6:10     84,  34,  86, 0.003802      60,  34, 110, 0.003479
6:11     16,  68, 120, 0.005623      76,  60,  68, 0.005752
6:12     16,  68, 120, 0.002108     102,  68,  34, 0.002473
6:13    112,  56,   0, 0.009654     116, 120, 112, 0.008276
6:14     16,  68, 120, 0.009175     100,  80, 120, 0.011556
6:15     16,  68, 120, 0.006234      52, 104,   0, 0.005499
6:16     16,  68, 120, 0.002303      56, 112,   0, 0.002605

*/
