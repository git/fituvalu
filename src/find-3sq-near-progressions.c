/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct fv_app_find_3sq_near_progressions_t
{
  int middles;
  int starts;
  int ends;
  FILE *infile;
  void (*display_record) (mpz_t *, FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_find_3sq_near_progressions_t *app = (struct fv_app_find_3sq_near_progressions_t *) state->input;
  switch (key)
    {
    case 'm':
      app->middles = 0;
      break;
    case 's':
      app->starts = 0;
      break;
    case 'e':
      app->ends = 0;
      break;
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_record = display_binary_three_record;
      break;
    case ARGP_KEY_ARG:
      app->infile = fopen (arg, "r");
      if (!app->infile)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static void
show_near_progressions (struct fv_app_find_3sq_near_progressions_t *app,
                        mpz_t *vec, FILE *out)
{
  int i;
  mpz_t progression[3], diff, n;
  for (i = 0; i < 3; i++)
    mpz_init (progression[i]);
  mpz_inits (diff, n, NULL);
  mpz_sub (diff, vec[1], vec[0]);
  if (app->ends)
    {
      mpz_set (progression[0], vec[1]);
      mpz_set (progression[1], vec[2]);
      mpz_add (progression[2], progression[1], diff);
      app->display_record (progression, out);
    }
  if (app->starts)
    {
      mpz_set (progression[1], vec[0]);
      mpz_set (progression[2], vec[1]);
      mpz_sub (progression[0], progression[1], diff);
      app->display_record (progression, out);
    }
  if (mpz_even_p (diff) && app->middles)
    {
      mpz_cdiv_q_ui (n, diff, 2);
      mpz_set (progression[0], vec[0]);
      mpz_add (progression[1], progression[0], n);
      mpz_set (progression[2], vec[1]);
      app->display_record (progression, out);
      mpz_set (progression[0], vec[1]);
      mpz_add (progression[1], progression[0], n);
      mpz_set (progression[2], vec[2]);
      app->display_record (progression, out);
    }
  for (i = 0; i < 3; i++)
    mpz_clear (progression[i]);
  mpz_clears (diff, n, NULL);
}

int
fituvalu_find_3sq_near_progressions (struct fv_app_find_3sq_near_progressions_t *app, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t vec[3];

  int i;
  for (i = 0; i < 3; i++)
    mpz_init (vec[i]);

  while (1)
    {
      read = app->read_tuple (app->infile, vec, &line, &len);
      if (read == -1)
        break;
      show_near_progressions (app, vec, out);
    }

  for (i = 0; i < 3; i++)
    mpz_clear (vec[i]);

  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "middle", 'm', 0, 0, "Only show when the middle number isn't a square"},
  { "start", 's', 0, 0, "Only show when the starting number isn't a square"},
  { "end", 'e', 0, 0, "Only show when the ending number isn't a square"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text", 1},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text", 1},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Generate arithmetic progressions containing two squares and one non-square from three square progressions given in FILE.\vIf FILE is `-' or is unspecified it is read from the standard input.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_find_3sq_near_progressions_t app;
  memset (&app, 0, sizeof (app));
  app.display_record = display_three_record;
  app.read_tuple = read_three_numbers_from_stream;
  app.infile = stdin;
  app.middles = 1;
  app.starts = 1;
  app.ends = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_find_3sq_near_progressions (&app, stdout);
}
