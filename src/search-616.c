/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_616_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  double median;
  double max;
  double percent;
  int mult;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int threesq;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t i, iroot, j, distance2, sum_minus_middle, twoiroot, limit;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_616_type_1 (struct fv_app_search_616_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_mul_ui (q->distance2, w->distance, 2);

  mpz_mul_ui (q->limit, w->hi, app->mult);

  mpz_sqrt (q->iroot, q->distance2);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  mpz_set ((*s)[1][0], w->hi);
  mpz_set ((*s)[0][1], w->lo);

  //+---+---+---+
  //|   | A | D |
  //+---+---+---+
  //| C |   |   |
  //+---+---+---+
  //| E |   | B |
  //+---+---+---+

  while (1)
    {
      mpz_set ((*s)[0][2], q->i);

      mpz_sub (q->j, q->i, q->distance2);

      if (mpz_perfect_square_p (q->j))
        {
          mpz_set ((*s)[2][0], q->j);

          mpz_add ((*s)[1][1], (*s)[2][0], w->distance);
          mpz_mul_ui (q->sum_minus_middle, (*s)[1][1], 2);

          mpz_sub ((*s)[0][0], q->sum_minus_middle, (*s)[2][2]);
          mpz_sub ((*s)[1][2], q->sum_minus_middle, (*s)[1][0]);
          mpz_sub ((*s)[2][1], q->sum_minus_middle, (*s)[0][1]);
            {
              //check for dups
              int dup = 0;
              if (mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0 ||
                  mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0)
                dup = 1;
              if (!dup)
                {
                  if (mpz_perfect_square_p ((*s)[0][0]))
                    {
                      pthread_mutex_lock (&display_lock);
                      app->display_square (*s, app->out);
                      fflush (app->out);
                      pthread_mutex_unlock (&display_lock);
                    }
                }
            }
        }


      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
handle_progression (struct fv_app_search_616_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  generate_616_type_1 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_616_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[2][2], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[2][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[2][2], w->distance);
          mpz_add (w->hi, (*s)[2][2], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }
      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_616_t *app =
    (struct fv_app_search_616_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.distance2, q.sum_minus_middle, q.twoiroot, q.limit, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[2][2], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[2][2], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[2][2], 1) > 0)
        generate_progressions (app, &w, &q, &s);
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (q.i, q.iroot, q.j, q.distance2, q.sum_minus_middle, q.twoiroot, q.limit, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_616_t *app =
    (struct fv_app_search_616_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.distance2, q.sum_minus_middle, q.twoiroot, q.limit, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[2][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[2][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[2][2], w.lo);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (q.i, q.iroot, q.j, q.distance2, q.sum_minus_middle, q.twoiroot, q.limit, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_616 (struct fv_app_search_616_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_616_t *app = (struct fv_app_search_616_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case '3':
      app->threesq = 1;
      break;
    case 'm':
      app->percent = strtod (arg, &end);
      if (end == NULL || *end != '\0' || app->percent <= 0)
        argp_error (state, "invalid argument `%s' to option --multiply", arg);
      app->percent /= 100.0;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      app->mult = ceil (app->max * app->percent);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "multiply", 'm', "PERC", 0, "Find a starting point for F by multiplying C by a value between 1 and 9755.2353 expressed as a percent (1-100)"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:16 given a FILE containing bottom right values.\vWhen FILE is not provided, it is read from the standard input.  The default value for PERC is 0.07361202787133798.  Magic squares of type 6:16 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  | X | X | X |   | Z | A | F |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating F\n"
"  | X |   |   |   | C |   |   |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of 2*(B-A) below F.\n"
"  | X |   | X |   | D |   | B |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_616_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.median = 3.5335;
  app.max = 14400.4999;
  app.percent = (app.median * 3.0) / app.max;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_616 (&app);
}
/*

the point of this is to identify information that can help us get more 6:16s.  we want to be able to predict 'center' values (the B values) for 6:16 magic squares.

  +---+---+---+   +---+---+---+
  | X | X | X |   | Z | A | F |
  +---+---+---+   +---+---+---+
  | X |   |   |   | C |   |   |
  +---+---+---+   +---+---+---+
  | X |   | X |   | D |   | B |
  +---+---+---+   +---+---+---+

we identified which B values lead to magic squares with 6 perfect squares in the 6:16 configuration, and then here we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:16 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 747 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

6, 0, 78, 145 / 75224 = 0.001928
6, 78, 0, 145 / 75224 = 0.001928
16, 102, 86, 91 / 46465 = 0.001958
90, 112, 50, 74 / 37616 = 0.001967
0, 90, 78, 75 / 37616 = 0.001994
90, 0, 78, 75 / 37616 = 0.001994
90, 78, 0, 75 / 37616 = 0.001994
6, 84, 78, 117 / 56420 = 0.002074
34, 56, 78, 122 / 56420 = 0.002162
16, 68, 120, 107 / 46464 = 0.002303

well that's a poor percentage, and again we have the ever-present 16, 68, 120 which is the best.

second run is depth of 2, and breadth of 2000.

1852, 1718, 8 / 1774 = 0.004510
594, 834, 20 / 4434 = 0.004511
1098, 1842, 10 / 2156 = 0.004638
622, 1870, 12 / 2537 = 0.004730
1546, 1786, 9 / 1901 = 0.004734
1852, 1208, 10 / 2068 = 0.004836
424, 1004, 22 / 4433 = 0.004963
628, 800, 25 / 4434 = 0.005638
118, 1310, 25 / 4431 = 0.005642
1546, 1310, 13 / 2220 = 0.005856

hmm the pecentage is about 2 times higher.  down from 107  hits to 13 though.
the best pair seems to be "1546, 1310".  we have seen this pair before.

and just for kicks, let's try a depth of 1 and a breadth of 100000.

56692, 2 / 58 = 0.034483
62289, 2 / 53 = 0.037736
64333, 2 / 52 = 0.038462
65772, 2 / 51 = 0.039216
80332, 2 / 42 = 0.047619
88620, 2 / 38 = 0.052632

i'm sure we're just in la-la guessing land here.
but maybe 88620 is worth trying.

let's say the minimum count has to be 10 or higher.

168, 24 / 18810 = 0.001276
336, 12 / 9406 = 0.001276
210, 20 / 15048 = 0.001329
328, 13 / 9634 = 0.001349
84, 52 / 37614 = 0.001382
252, 18 / 12543 = 0.001435
492, 11 / 6423 = 0.001713
396, 14 / 7982 = 0.001754
420, 15 / 7526 = 0.001993
630, 11 / 5019 = 0.002192

630 is the best i guess.  84 is a bit of a standout.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

4, 2, 0, 747 / 1053059 = 0.000709
4, 1, 1, 747 / 1579588 = 0.000473
4, 0, 2, 747 / 1053059 = 0.000709
3, 1, 2, 747 / 1579589 = 0.000473
2, 2, 2, 747 / 1579589 = 0.000473
2, 2, 0, 747 / 1579589 = 0.000473
2, 1, 1, 747 / 2369383 = 0.000315
2, 0, 2, 747 / 1579589 = 0.000473
2, 0, 0, 747 / 1579589 = 0.000473
1, 3, 2, 747 / 1579589 = 0.000473
1, 2, 0, 747 / 2106119 = 0.000355
1, 1, 2, 747 / 2369384 = 0.000315
1, 1, 1, 747 / 3159178 = 0.000236
1, 1, 0, 747 / 3159178 = 0.000236
1, 0, 2, 747 / 2106119 = 0.000355
1, 0, 1, 747 / 3159178 = 0.000236
1, 0, 0, 747 / 3159178 = 0.000236
0, 4, 2, 747 / 1053059 = 0.000709
0, 2, 2, 747 / 1579589 = 0.000473
0, 2, 0, 747 / 1579589 = 0.000473
0, 1, 2, 747 / 2106119 = 0.000355
0, 1, 1, 747 / 3159178 = 0.000236
0, 1, 0, 747 / 3159178 = 0.000236
0, 0, 2, 747 / 1579589 = 0.000473
0, 0, 1, 747 / 3159178 = 0.000236
2, 2, 4, 621 / 1184692 = 0.000524

we can go up by "4, 2" and get them all.

results:

so looking forward for more sixes, pick one of "88620", "80332", or "65772".  Try "630", and "1546, 1310", and "16, 68, 120".

and we can recalculate them all quicker by using the "4,8" pattern.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 1225 in sq-seq-finder.

56, 78, 118, 83 / 37618 = 0.002206
18, 38, 112, 126 / 56424 = 0.002233
50, 6, 112, 126 / 56421 = 0.002233
56, 84, 112, 85 / 37620 = 0.002259
56, 112, 84, 88 / 37617 = 0.002339
114, 110, 28, 90 / 37616 = 0.002393
56, 84, 28, 140 / 56422 = 0.002481
0, 56, 112, 98 / 37619 = 0.002605
56, 0, 112, 98 / 37619 = 0.002605
56, 112, 0, 98 / 37619 = 0.002605

lots of zeroes and 112s hanging around.
the best tuple is "56, 84, 28".  there's a frequent sum of 168 and 252.
(e.g. 114+112+84)

no arithmetic progressions in non-zero tuples.

we did a little better with a different starting point in terms of percentage.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

1778, 1918, 9 / 1713 = 0.005254
1938, 1758, 9 / 1713 = 0.005254
1482, 1962, 10 / 1842 = 0.005429
1226, 1972, 11 / 1982 = 0.005550
1512, 1820, 11 / 1903 = 0.005780
1764, 1932, 10 / 1714 = 0.005834
1922, 1774, 10 / 1714 = 0.005834
1902, 1794, 10 / 1713 = 0.005838
1512, 1680, 12 / 1990 = 0.006030
1512, 1344, 14 / 2221 = 0.006303

best of the bunch is "1512, 1344".
we actually did a tiny bit better than earlier too.

okay let's do depth=1, and breadth is 100000

99916, 2 / 34 = 0.058824
99932, 2 / 34 = 0.058824
99992, 2 / 34 = 0.058824
69300, 3 / 49 = 0.061224
70728, 3 / 48 = 0.062500
87754, 3 / 39 = 0.076923
90944, 3 / 38 = 0.078947

8 percent is in the good category.
does it pan out? probably not.

let's do depth=1, breadth is 100000, min hits of 10

168, 47 / 18814 = 0.002498
420, 19 / 7531 = 0.002523
252, 32 / 12546 = 0.002551
616, 14 / 5134 = 0.002727
504, 18 / 6276 = 0.002868
336, 27 / 9409 = 0.002870
672, 15 / 4706 = 0.003187
924, 12 / 3423 = 0.003506
1232, 10 / 2570 = 0.003891
840, 15 / 3769 = 0.003980

840 is  best.  168 is a standout.

so overall, looking forward for more sixes, pick one of "88620", "80332", or "65772", and then one of "70728", "87754", or "90944".  Try "1546, 1310", "16, 68, 120", "1512, 1344 ", and "56, 84, 28".  and throw in "630" and "840".

this one makes me think there's a large round single repeater somewhere.

okay, let's do the first run again but sort on hits to see how it shakes out.

2, 4, 0, 747 / 1053049 = 0.000709
2, 3, 1, 747 / 1579573 = 0.000473
2, 2, 2, 747 / 1579573 = 0.000473
2, 2, 0, 747 / 1579573 = 0.000473
2, 1, 3, 747 / 1579573 = 0.000473
2, 1, 1, 747 / 2369359 = 0.000315
2, 1, 0, 747 / 2106097 = 0.000355
2, 0, 4, 747 / 1053049 = 0.000709
2, 0, 2, 747 / 1579573 = 0.000473
2, 0, 1, 747 / 2106097 = 0.000355
2, 0, 0, 747 / 1579573 = 0.000473
1, 1, 4, 747 / 1579573 = 0.000473
1, 1, 2, 747 / 2369359 = 0.000315
1, 1, 1, 747 / 3159145 = 0.000236
1, 1, 0, 747 / 3159145 = 0.000236
1, 0, 1, 747 / 3159145 = 0.000236
1, 0, 0, 747 / 3159145 = 0.000236
0, 2, 4, 747 / 1053049 = 0.000709
0, 2, 2, 747 / 1579573 = 0.000473
0, 2, 1, 747 / 2106097 = 0.000355
0, 2, 0, 747 / 1579573 = 0.000473
0, 1, 1, 747 / 3159145 = 0.000236
0, 1, 0, 747 / 3159145 = 0.000236
0, 0, 2, 747 / 1579573 = 0.000473
0, 0, 1, 747 / 3159145 = 0.000236
2, 4, 2, 621 / 1184680 = 0.000524

no better than before. gotta go up by "2,4" to get them all.

this concludes our analysis of anticipating B values in 6:16 magic squares.

in order to get high value 6:16s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

also we could consider running this test again but including all D values as B values.

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="88620" 1 10000000000000000000000000000 | ./search-616
sq-seq --pattern="80332" 1 10000000000000000000000000000 | ./search-616
sq-seq --pattern="65772" 1 10000000000000000000000000000 | ./search-616
sq-seq --pattern="1546,1310" 1 10000000000000000000000000000 | ./search-616
sq-seq --pattern="16,68,120" 1 10000000000000000000000000000 | ./search-616
sq-seq --pattern="630" 1 10000000000000000000000000000 | ./search-616

sq-seq --pattern="70728" 7225 10000000000000000000000000000 | ./search-616
sq-seq --pattern="87754" 7225 10000000000000000000000000000 | ./search-616
sq-seq --pattern="90944" 7225 10000000000000000000000000000 | ./search-616
sq-seq --pattern="1512,1344" 7225 10000000000000000000000000000 | ./search-616
sq-seq --pattern="56,84,28" 7225 10000000000000000000000000000 | ./search-616
sq-seq --pattern="840" 7225 10000000000000000000000000000 | ./search-616
*/
