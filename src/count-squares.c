/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct fv_app_count_squares_t
{
  int fourn1;
  int twelven1;
  int only_check;
  int gt;
  int num_columns;
  int *only_show;
  int num_only_show;
  int check[3][3];
  int (*read_numbers)(FILE *, mpz_t *, int, char **, size_t *);
  void (*disp_record) (mpz_t *vec, int size, FILE *out);
};

static int
check_square (struct fv_app_count_squares_t *app, mpz_t num)
{
  if (app->fourn1)
    {
      if (!mpz_perfect_square_p (num))
        return 0;
      mpz_t r;
      mpz_inits (r, NULL);
      mpz_sub_ui (r, num, 1);
      int ret = mpz_divisible_ui_p (r, 4);
      mpz_clears (r, NULL);
      return ret != 0;
    }
  else if (app->twelven1)
    {
      if (!mpz_perfect_square_p (num))
        return 0;
      mpz_t r;
      mpz_inits (r, NULL);
      mpz_sub_ui (r, num, 1);
      int ret = mpz_divisible_ui_p (r, 12);
      mpz_clears (r, NULL);
      return ret != 0;
    }
  else
    {
      if (!mpz_perfect_square_p (num))
        return 0;
    }
  return 1;
}

int
fituvalu_display_square_count (struct fv_app_count_squares_t *app, FILE *stream)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[app->num_columns];

  int i;
  for (i = 0; i < app->num_columns; i++)
    mpz_init (a[i]);

  while (1)
    {
      read = app->read_numbers (stream, a, app->num_columns, &line, &len);
      if (read == -1)
        break;
      int count = 0;
      if (app->num_columns == 9)
        {
          for (i = 0; i < app->num_columns; i++)
            {
              int y = i / 3;
              int x = i % 3;
              if (app->check[y][x])
                {
                  if (check_square (app, a[i]))
                    count++;
                }
            }
        }
      else
        {
          for (i = 0; i < app->num_columns; i++)
            {
              if (check_square (app, a[i]))
                count++;
            }
        }
      if (app->num_only_show)
        {
          for (int i = 0; i < app->num_only_show; i++)
            {
              if (app->only_show[i] == count)
                {
                  app->disp_record (a, app->num_columns, stdout);
                  break;
                }
              else if (app->gt && count >= app->only_show[i])
                {
                  app->disp_record (a, app->num_columns, stdout);
                  break;
                }
            }
        }
      else
        {
          fprintf (stdout, "%d\n", count);
          fflush (stdout);
        }
    }

  for (i = 0; i < app->num_columns; i++)
    mpz_clear (a[i]);

  if (line)
    free (line);
  return 0;
}

static int
get_coord (char *s, int *y, int *x)
{
  int retval = sscanf (s, "%d,%d", y, x);
  if (retval != 2)
    return 0;
  if (*y < 0 || *y > 2)
    return 0;
  if (*x < 0 || *x > 2)
    return 0;
  return 1;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  int y = 0, x = 0;
  struct fv_app_count_squares_t *app = (struct fv_app_count_squares_t *) state->input;
  switch (key)
    {
    case 'p':
      app->fourn1 = 1;
      break;
    case 't':
      app->twelven1 = 1;
      break;
    case 'c':
        {
          if (app->only_check == 0)
            memset (app->check, 0, sizeof (app->check));
          app->only_check = 1;
          if (get_coord (arg, &y, &x))
            app->check[y][x] = 1;
          else
            argp_error (state, "%s is an invalid cell coordinate", arg);
        }
      break;
    case 'g':
      app->gt = 1;
      break;
    case 'n':
      app->num_columns = atoi (arg);
      break;
    case 'i':
      app->read_numbers = binary_read_numbers_from_stream;
      break;
    case 'o':
      app->disp_record = disp_binary_record;
      break;
    case 'f':
        {
          int num = atoi (arg);
          if (num >= 0 && num <= 9)
            {
              app->only_show =
                realloc (app->only_show, (app->num_only_show + 1) * sizeof (int));
              app->only_show[app->num_only_show] = atoi (arg);
              app->num_only_show++;
            }
          else
            argp_error (state, "%s is an invalid count for -f", arg);
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->only_check && app->num_columns != 9)
        argp_error (state, "can't use option -c with -n");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "check", 'c', "X,Y", 0, "Check cell X,Y instead of all cells for squares" },
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "filter", 'f', "NUM-SQUARES", 0, "Instead of showing the count, show the magic square if it has NUM-SQUARES perfect squares"},
  { "num-columns", 'n', "COLS", 0, "Instead of reading in 9 numbers read in this many"},
  { "gt", 'g', 0, 0, "Output magic squares with NUM-SQUARES or more"},
  { "fourn1", 'p', 0, 0, "Only count squares that are 4n+1 (pythagorean)"},
  { "twelven1", 't', 0, 0, "Only count squares that are 12n+1"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 9 numbers from the standard input and count the perfect squares.\vThe nine values must be separated by a comma and terminated by a newline.  Whereas other programs with the \"-squares\" suffix refers to magic squares, in this program it refers to perfect squares.  We are counting the squared integers.  Option --out-binary is only used with --filter.  Option --check cannot be used with --num-columns.  For argument X,Y a value of 0,0 is the top left corner, and 2,1 is the middle on the bottom row.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_count_squares_t app;
  memset (&app, 0, sizeof (app));
  app.num_columns = 9;
  app.read_numbers = read_numbers_from_stream;
  app.disp_record = disp_record;
  //by default we check all cells.
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      app.check[i][j] = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_display_square_count (&app, stdin);
}
