/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

int max_recs = 1000;
//int max_recs = 3;
struct fv_app_search_66_t
{
  int in_binary;
  int num_args;
  int threads;
  int from_stdin;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int threesq;
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}


struct prec
{
  mpz_t lo;
  mpz_t hi;
};

//http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_66_t *app, struct prec *recs, int num_recs, mpz_t sum, mpz_t (*s)[3][3]);

void printCombination (int arr[], int n, int r, struct fv_app_search_66_t *app, struct prec *recs, int num_recs, mpz_t sum, mpz_t (*s)[3][3])
{
  int data[r];
  combinationUtil (arr, data, 0, n-1, 0, r, app, recs, num_recs, sum, s);
}

static void
handle_progressions (struct fv_app_search_66_t *app, mpz_t n, struct prec *recs, int num_recs, int by, int *arr)
{
  mpz_t sum, s[3][3];
  mpz_init (sum);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  mpz_mul_ui (sum, n, 3);
  mpz_set (s[1][1], n);
  printCombination (arr, num_recs, by, app, recs, num_recs, sum, &s);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clear (sum);
}

static void
generate_progressions (struct fv_app_search_66_t *app, mpz_t n, struct prec **recs, int *num_recs)
{
  mpz_t i, iroot, diff, limit, mm, nn, mn, twomn, twoiroot;
  mpz_inits (i, iroot, diff, limit, mm, nn, mn, twomn, twoiroot, NULL);

  mpz_cdiv_q_ui (limit, n, 2);
  mpz_set_ui (i, 1);
  mpz_set_ui (iroot, 1);

  while (1)
    {
      if (mpz_cmp (i, limit) > 0)
        break;

      mpz_sub (diff, n, i);

      if (mpz_perfect_square_p (diff))
        {
          mpz_set (mm, iroot);
          mpz_sqrt (nn, diff);
          mpz_mul (mn, mm, nn);
          mpz_mul_ui (twomn, mn, 2);
          if (*num_recs >= max_recs)
            *recs = realloc (*recs, sizeof (struct prec) * ((*num_recs) + 1));
          mpz_init_set ((*recs)[*num_recs].lo, n);
          mpz_sub ((*recs)[*num_recs].lo, (*recs)[*num_recs].lo, twomn);
          mpz_init_set ((*recs)[*num_recs].hi, n);
          mpz_add ((*recs)[*num_recs].hi, (*recs)[*num_recs].hi, twomn);
          (*num_recs)++;
        }

      mpz_mul_ui (twoiroot, iroot, 2);
      mpz_add (i, i, twoiroot);
      mpz_incr (i);
      mpz_incr (iroot);
    }
  mpz_clears (i, iroot, diff, limit, mm, nn, mn, twomn, twoiroot, NULL);
  return;
}

static void
generate_66_square_type_a (struct fv_app_search_66_t *app, mpz_t sum, mpz_t (*s)[3][3], struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  mpz_t distance, twodistance;
  mpz_inits (distance, twodistance, NULL);
  if (mpz_cmp (one->lo, two->hi) < 0)
    {
      mpz_sub (twodistance, two->hi, one->lo);
      mpz_cdiv_q_ui (distance, twodistance, 2);
      mpz_add ((*s)[2][2], one->lo, distance);
    }
  else
    {
      mpz_sub (twodistance, one->lo, two->hi);
      mpz_cdiv_q_ui (distance, twodistance, 2);
      mpz_add ((*s)[2][2], two->hi, distance);
    }
 
  if (mpz_perfect_square_p ((*s)[2][2]))
    {
      mpz_set ((*s)[0][1], one->lo);
      mpz_set ((*s)[2][1], one->hi);
      mpz_set ((*s)[1][2], two->lo);
      mpz_set ((*s)[1][0], two->hi);

      mpz_sub ((*s)[0][0], sum, (*s)[1][1]);
      mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][2]);

      mpz_sub ((*s)[0][2], sum, (*s)[2][2]);
      mpz_sub ((*s)[0][2], (*s)[0][2], two->lo);

      mpz_sub ((*s)[2][0], sum, (*s)[2][2]);
      mpz_sub ((*s)[2][0], (*s)[2][0], one->hi);

      if (app->threads > 1)
        pthread_mutex_lock (&display_lock);
      app->display_square (*s, app->out);
      fflush (app->out);
      if (app->threads > 1)
        pthread_mutex_unlock (&display_lock);
    }
  mpz_clears (distance, twodistance, NULL);
}

static void
generate_66_square_type_b (struct fv_app_search_66_t *app, mpz_t sum, mpz_t (*s)[3][3], struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  mpz_t distance, twodistance;
  mpz_inits (distance, twodistance, NULL);
  if (mpz_cmp (one->lo, two->lo) < 0)
    {
      mpz_sub (twodistance, two->lo, one->lo);
      mpz_cdiv_q_ui (distance, twodistance, 2);
      mpz_add ((*s)[2][2], one->lo, distance);
    }
  else
    {
      mpz_sub (twodistance, one->lo, two->lo);
      mpz_cdiv_q_ui (distance, twodistance, 2);
      mpz_add ((*s)[2][2], two->lo, distance);
    }
 
  if (mpz_perfect_square_p ((*s)[2][2]))
    {
      mpz_set ((*s)[0][1], one->lo);
      mpz_set ((*s)[2][1], one->hi);
      mpz_set ((*s)[1][0], two->lo);
      mpz_set ((*s)[1][2], two->hi);

      mpz_sub ((*s)[0][0], sum, (*s)[1][1]);
      mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][2]);

      mpz_sub ((*s)[0][2], sum, (*s)[2][2]);
      mpz_sub ((*s)[0][2], (*s)[0][2], two->hi);

      mpz_sub ((*s)[2][0], sum, (*s)[2][2]);
      mpz_sub ((*s)[2][0], (*s)[2][0], one->hi);

      if (app->threads > 1)
        pthread_mutex_lock (&display_lock);
      app->display_square (*s, app->out);
      fflush (app->out);
      if (app->threads > 1)
        pthread_mutex_unlock (&display_lock);
    }
  mpz_clears (distance, twodistance, NULL);
}

static void
generate_66_square_type_c (struct fv_app_search_66_t *app, mpz_t sum, mpz_t (*s)[3][3], struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  mpz_t distance, twodistance;
  mpz_inits (distance, twodistance, NULL);
  if (mpz_cmp (one->hi, two->hi) < 0)
    {
      mpz_sub (twodistance, two->hi, one->hi);
      mpz_cdiv_q_ui (distance, twodistance, 2);
      mpz_add ((*s)[2][2], one->hi, distance);
    }
  else
    {
      mpz_sub (twodistance, one->hi, two->hi);
      mpz_cdiv_q_ui (distance, twodistance, 2);
      mpz_add ((*s)[2][2], two->hi, distance);
    }
 
  if (mpz_perfect_square_p ((*s)[2][2]))
    {
      mpz_set ((*s)[2][1], one->lo);
      mpz_set ((*s)[0][1], one->hi);
      mpz_set ((*s)[1][2], two->lo);
      mpz_set ((*s)[1][0], two->hi);

      mpz_sub ((*s)[0][0], sum, (*s)[1][1]);
      mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][2]);

      mpz_sub ((*s)[0][2], sum, (*s)[2][2]);
      mpz_sub ((*s)[0][2], (*s)[0][2], two->lo);

      mpz_sub ((*s)[2][0], sum, (*s)[2][2]);
      mpz_sub ((*s)[2][0], (*s)[2][0], one->lo);

      if (app->threads > 1)
        pthread_mutex_lock (&display_lock);
      app->display_square (*s, app->out);
      fflush (app->out);
      if (app->threads > 1)
        pthread_mutex_unlock (&display_lock);
    }
  mpz_clears (distance, twodistance, NULL);
}

static void
generate_66_square_type_d (struct fv_app_search_66_t *app, mpz_t sum, mpz_t (*s)[3][3], struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  mpz_t distance, twodistance;
  mpz_inits (distance, twodistance, NULL);
  if (mpz_cmp (one->hi, two->lo) < 0)
    {
      mpz_sub (twodistance, two->lo, one->hi);
      mpz_cdiv_q_ui (distance, twodistance, 2);
      mpz_add ((*s)[2][2], one->hi, distance);
    }
  else
    {
      mpz_sub (twodistance, one->hi, two->lo);
      mpz_cdiv_q_ui (distance, twodistance, 2);
      mpz_add ((*s)[2][2], two->lo, distance);
    }
 
  if (mpz_perfect_square_p ((*s)[2][2]))
    {
      mpz_set ((*s)[2][1], one->lo);
      mpz_set ((*s)[0][1], one->hi);
      mpz_set ((*s)[1][0], two->lo);
      mpz_set ((*s)[1][2], two->hi);

      mpz_sub ((*s)[0][0], sum, (*s)[1][1]);
      mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][2]);

      mpz_sub ((*s)[0][2], sum, (*s)[2][2]);
      mpz_sub ((*s)[0][2], (*s)[0][2], two->hi);

      mpz_sub ((*s)[2][0], sum, (*s)[2][2]);
      mpz_sub ((*s)[2][0], (*s)[2][0], one->lo);

      if (app->threads > 1)
        pthread_mutex_lock (&display_lock);
      app->display_square (*s, app->out);
      fflush (app->out);
      if (app->threads > 1)
        pthread_mutex_unlock (&display_lock);
    }
  mpz_clears (distance, twodistance, NULL);
}

static void
generate_67_square (struct fv_app_search_66_t *app, mpz_t sum, mpz_t (*s)[3][3], struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  generate_66_square_type_a (app, sum, s, one, two, recs, num_recs);
  generate_66_square_type_b (app, sum, s, one, two, recs, num_recs);
  generate_66_square_type_c (app, sum, s, one, two, recs, num_recs);
  generate_66_square_type_d (app, sum, s, one, two, recs, num_recs);
}

void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_66_t *app, struct prec *recs, int num_recs, mpz_t sum, mpz_t (*s)[3][3])
{
  if (index == r)
    {
      generate_67_square (app, sum, s, &recs[data[0]], &recs[data[1]], recs, num_recs);
      return;
    }

  for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
      data[index] = arr[i];
      combinationUtil (arr, data, i + 1, end, index + 1, r, app, recs, num_recs, sum, s);
    }
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_66_t *app =
    (struct fv_app_search_66_t *) param->data;

  int *arr = malloc (max_recs * sizeof (int));
  for (int j = 0; j < max_recs; j++)
    arr[j] = j;

  mpz_inits (n, num, NULL);
  struct prec *recs = malloc (sizeof (struct prec) * max_recs);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (n, app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (num, line, 10);
        }
      mpz_set (n, num);
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
        {
          int num_recs = 0;
          generate_progressions (app, n, &recs, &num_recs);
          if (num_recs > max_recs)
            {
              // we won't be hitting this with max_recs = 1000
              // but hey, no arbitrary limits.
              arr = realloc (arr, sizeof (int) * (num_recs));
              for (int i = max_recs; i < num_recs; i++)
                arr[i] = i;
            }
          if (num_recs >= 2)
            handle_progressions (app, n, recs, num_recs, 2, arr);
          for (int i = 0; i < num_recs; i++)
            mpz_clears (recs[i].lo, recs[i].hi, NULL);
          num_recs = 0;
        }
    }
  mpz_clears (n, num, NULL);

  free (arr);
  free (recs);
  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_66_t *app =
    (struct fv_app_search_66_t *) param->data;

  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3];
  mpz_t n;

  for (int i = 0; i < 3; i++)
    mpz_init (a[i]);
  mpz_init (n);

  int *arr = malloc (max_recs * sizeof (int));
  for (int j = 0; j < max_recs; j++)
    arr[j] = j;

  struct prec *recs = malloc (sizeof (struct prec) * max_recs);
  int num_recs = 0;
  while (1)
    {
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);
      read = app->read_tuple (app->infile, a, &line, &len);
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);
      if (read == -1)
        break;

      if (mpz_cmp (a[1], n) == 0)
        {
          if (num_recs >= max_recs)
            recs = realloc (recs, sizeof (struct prec) * (num_recs + 1));
          mpz_init_set (recs[num_recs].lo, a[0]);
          mpz_init_set (recs[num_recs].hi, a[2]);
          num_recs++;
          if (num_recs > max_recs)
            {
              // we won't be hitting this with max_recs = 1000
              // but hey, no arbitrary limits.
              arr = realloc (arr, sizeof (int) * (num_recs));
              for (int i = max_recs; i < num_recs; i++)
                arr[i] = i;
              max_recs = num_recs;
            }
        }
      else
        {
          if (num_recs >= 2)
            handle_progressions (app, n, recs, num_recs, 2, arr);
          for (int i = 0; i < num_recs; i++)
            mpz_clears (recs[i].lo, recs[i].hi, NULL);
          num_recs = 1;
          mpz_set (n, a[1]);
          mpz_init_set (recs[0].lo, a[0]);
          mpz_init_set (recs[0].hi, a[2]);
        }

    }

  if (num_recs >= 2)
    handle_progressions (app, n, recs, num_recs, 2, arr);

  for (int i = 0; i < num_recs; i++)
    mpz_clears (recs[i].lo, recs[i].hi, NULL);

  for (int i = 0; i < 3; i++)
    mpz_clear (a[i]);

  mpz_clear (n);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_66 (struct fv_app_search_66_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_66_t *app = (struct fv_app_search_66_t *) state->input;
  switch (key)
    {
    case '3':
      app->threesq = 1;
      break;
    case 'i':
      app->in_binary = 1;
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->threesq)
        app->threads = 1;
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in near progressions instead of center values"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:6 given a FILE containing center values.\vWhen FILE is not provided, it is read from the standard input.  Center values must be perfect squares.  Magic squares of type 6:6 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   | X |   |   |   | A |   |\n"
"  +---+---+---+   +---+---+---+\n"
"  | X | X | X |   | D | N | C |  A,N,B and C,N,D are three square\n"
"  +---+---+---+   +---+---+---+  progressions.  Z is a square that shakes\n"
"  |   | X | X |   |   | B | Z |  out.\n"
"  +---+---+---+   +---+---+---+\n"
"This program checks every combination of 2 three square progressions.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_66_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.display_square = display_square_record;
  app.read_tuple = read_three_numbers_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_66 (&app);
}
