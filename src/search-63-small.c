/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

int max_recs = 1000;

struct fv_app_search_63_t
{
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  int in_binary;
  int twos;
  int quick;
  int progress;
};

struct fv_app_log_t lg;

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}


struct prec
{
  unsigned long long int lo;
  unsigned long long int hi;
};

static int
square_is_twelvenminusone (unsigned long long i)
{
  return (i - 1) % 12 == 0;
}

static int
detect (unsigned long long int num, int *advance)
{
  //we return 1 if the next number in the sequence is 4 squares ahead
  unsigned long long int root = sqrtl(num);

  unsigned long long int p1root = root + 2;
  unsigned long long int p2root = root + 4;
  unsigned long long int p1 = p1root * p1root;
  unsigned long long int p2 = p2root * p2root;

  int checkp1 = square_is_twelvenminusone (p1);
  int checkp2 = square_is_twelvenminusone (p2);

  if (checkp1 && !checkp2)
    return 0;
  else if (!checkp1 && checkp2)
    return 1;
  else if (checkp1 && checkp2)
    *advance = 1;
  return 0;
}
static void
quick_generate_progressions (struct fv_app_search_63_t *app, unsigned long long int n, struct prec **recs, int *num_recs)
{
  unsigned long long i, iroot;

  iroot = sqrtl (n);
  iroot++;
  if (iroot % 2 == 0)
    iroot++;
  i = iroot * iroot;
  int advance = 0;
  int up_by_four = detect (i, &advance);
  if (advance)
    {
      iroot += 4;
      i = iroot * iroot;
      up_by_four = detect (i, &advance);
    }
  while (1)
    {
      unsigned long long int distance = i - n;
      if (distance > n)
        break;
      unsigned long long int j = n - distance;

      if (small_is_square (j))
        {
          if (*num_recs >= max_recs)
            *recs = realloc (*recs, sizeof (struct prec) * ((*num_recs) + 1));
          (*recs)[*num_recs].lo = j;
          (*recs)[*num_recs].hi = i;
          (*num_recs)++;
        }

      if (up_by_four)
        {
          //advance i by 4 squares
          unsigned long long int eightroot = iroot * 8;
          i += eightroot;
          i+=16;
          iroot+=4;
        }
      else
        {
          //advance i by 2 squares
          unsigned long long int fourroot = iroot * 4;
          i += fourroot;
          i+=4;
          iroot+=2;
        }

      up_by_four = !up_by_four;

    }
  return;
}
static void
generate_progressions (struct fv_app_search_63_t *app, unsigned long long int n, struct prec **recs, int *num_recs)
{
  unsigned long long int i, iroot = 1, diff, limit, twoiroot;
  limit = n / 2;
  i = 1;
  while (1)
    {
      if (i > limit)
        break;

      diff = n - i;
      long double diffroot = sqrtl (diff);
      if (ceill (diffroot) == diffroot)
      //if ((long long)diffroot * (long long)diffroot == diff)
        {
          unsigned long long int mn, twomn, mm, nn;
          mm = iroot;
          nn = (long long) diffroot;
          mn = mm * nn;
          twomn = mn * 2;
          if (*num_recs >= max_recs)
            *recs = realloc (*recs, sizeof (struct prec) * ((*num_recs) + 1));
          (*recs)[*num_recs].lo = n - twomn;
          (*recs)[*num_recs].hi = n + twomn;
          (*num_recs)++;
        }
      if (i == 1)
        {
          i = 4;
          iroot = 2;
          continue;
        }
      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
  return;
}

static int
search_square (long long int s, struct prec *recs, int num_recs)
{
  for (int i = 0; i < num_recs; i++)
    {
      if (s == recs[i].lo)
        return 1;
      if (s == recs[i].hi)
        return 1;
    }
  return 0;
}

static void
generate_63_square2 (struct fv_app_search_63_t *app, unsigned long long center, unsigned long long sum, struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  long long int top = sum - one->lo;
  top = top - two->hi;

  // whip through our known squares before doing a more expensive check
  if (search_square (top, recs, num_recs) || small_is_square (top))
    {
      long long int bottom = sum - two->lo;
      bottom = bottom - one->hi;

      if (search_square (bottom, recs, num_recs) || small_is_square (bottom))
        {
          long long int left = sum - one->lo;
          left = left - two->lo;

          long long int right = sum - two->hi;
          right = right - one->hi;

          pthread_mutex_lock (&display_lock);
          fprintf (app->out,
                   "%lld, %lld, %lld, "
                   "%lld, %lld, %lld, "
                   "%lld, %lld, %lld, \n",
                   one->lo, left, two->lo,
                   top, center, bottom,
                   two->hi, right, one->hi);
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);
        }
    }
}

static void
generate_63_square (struct fv_app_search_63_t *app, unsigned long long center, unsigned long long sum, struct prec *one, struct prec *two, struct prec *three)
{
  //try to make a 6:3 magic square from three progressions.
  //one->lo, center, one->hi
  //two->lo, center, two->hi
  //three->lo, center, three->hi

  if (one->lo > two->lo && three->hi > two->lo)
    {
      unsigned long long diag1 = one->lo + center;
      diag1 = diag1 + one->hi;
      unsigned long long row1 = one->lo + two->lo;
      row1 = row1 + three->hi;
      if (diag1 == row1)
        {
          long long int left = sum - one->lo;
          left = left - three->lo;

          long long int right = sum - three->hi;
          right = right - one->hi;

          pthread_mutex_lock (&display_lock);
          fprintf (app->out,
                   "%lld, %lld, %lld, "
                   "%lld, %lld, %lld, "
                   "%lld, %lld, %lld, \n",
                   one->lo, left, three->lo,
                   two->lo, center, two->hi,
                   three->hi, right, one->hi);
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);
        }
    }
}

//http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_63_t *app, struct prec *recs, int num_recs, unsigned long long center, unsigned long long sum);

void printCombination (int arr[], int n, int r, struct fv_app_search_63_t *app, struct prec *recs, int num_recs, unsigned long long center, unsigned long long sum)
{
  int data[r];
  combinationUtil (arr, data, 0, n-1, 0, r, app, recs, num_recs, center, sum);
}
 
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_63_t *app, struct prec *recs, int num_recs, unsigned long long center, unsigned long long sum)
{
  if (index == r)
    {
      //work on data here.  there are 2 or 3 elements.
      if (app->twos)
        generate_63_square2 (app, center, sum, &recs[data[0]], &recs[data[1]], recs, num_recs);
      else
        generate_63_square (app, center, sum, &recs[data[0]], &recs[data[1]], &recs[data[2]]);
      return;
    }

  for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
      data[index] = arr[i];
      combinationUtil(arr, data, i + 1, end, index + 1, r, app, recs, num_recs, center, sum);
    }
}

static void
handle_progressions (struct fv_app_search_63_t *app, unsigned long long int n, struct prec *recs, int num_recs, int by, int *arr)
{
  //now we try every combination of 2 or 3.
  printCombination (arr, num_recs, by, app, recs, num_recs, n, n * 3);
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_63_t *app =
    (struct fv_app_search_63_t *) param->data;

  int *arr = malloc (sizeof (int) * max_recs);
  for (int i = 0; i < max_recs; i++)
    arr[i] = i;
  struct prec *recs = malloc (sizeof (struct prec) * max_recs);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          size_t r = fread (&num, sizeof (num), 1, app->infile);
          if (r == 0)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          num = strtoull (line, &end, 10);
        }
      n = num;
      if (app->progress)
        fv_update_log_ull (&lg, num);
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
        {
          int num_recs = 0;
          if (app->quick)
            quick_generate_progressions (app, n, &recs, &num_recs);
          else
            generate_progressions (app, n, &recs, &num_recs);
          if (num_recs > max_recs)
            {
              // we won't be hitting this with max_recs = 1000
              // but hey, no arbitrary limits.
              arr = realloc (arr, sizeof (int) * (num_recs));
              for (int i = max_recs; i < num_recs; i++)
                arr[i] = i;
            }
          if (app->twos)
            {
              if (num_recs >= 2)
                handle_progressions (app, n, recs, num_recs, 2, arr);
            }
          else
            {
              if (num_recs >= 3)
                handle_progressions (app, n, recs, num_recs, 3, arr);
            }
        }
    }

  free (arr);
  free (recs);
  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_63 (struct fv_app_search_63_t *app)
{
  run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_63_t *app = (struct fv_app_search_63_t *) state->input;
  switch (key)
    {
    case 'p':
      app->progress = 1;
      break;
    case 'q':
      app->quick = 1;
      break;
    case '2':
      app->twos = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "twos", '2', 0, 0, "Use 2 progressions instead of 3 to make a square"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { "quick", 'q', 0, 0, "All squares have roots that are 1 or 5 mod 6"},
  { "progress", 'p', 0, OPTION_HIDDEN, "Show progress information in /tmp"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:3 given a FILE containing center numbers up to 2^63/3 in value.\vWhen FILE is not provided, it is read from the standard input.  Center values must not be perfect squares.  This program is limited to 64-bit integers.  Magic squares of type 6:3 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  | X |   | X |   | A |   | F |   Find three number arithmetic progressions\n"
"  +---+---+---+   +---+---+---+   that have non-square n at their center.  The\n"  
"  | X |   | X |   | C | n | D |   progressions are A,n,B, C,n,D, and E,n,F.\n"
"  +---+---+---+   +---+---+---+   Try every combination of three progressions\n"
"  | X |   | X |   | E |   | B |   in every position to find a magic square.\n"
"  +---+---+---+   +---+---+---+\n"
"This program checks every combination of 2 or 3 near three square progressions depending on the --twos option.  A near three square progression in this case is an arithmetic progression starting with a square, continuing to a non-square and ending on a square.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_63_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (app.progress)
    fv_init_log (&lg, "search-63-small");
  return fituvalu_search_63 (&app);
}
/**
 * centers need to be pythagorean composites, e.g. numbers consisting solely of factors that are pythagorean primes.
 * they are necessarily odd numbers.
 * i checked to see if pythagorean composites become more rare as we traverse the number line, and yes they do.
 * going up in orders of magnitude the percentages are:
 * 0.4, 0.2889, 0.24, 0.21133, 0.18998, 0.17391, 0.161402
 *
 * the 'keyhole search' is to try pythagorean composites that are also perfect squares.
 * let n proceed from 2 to a very large number, going up by twos
 * let the center value be ((4*n)+1)^2
 * get progressions, and then try to fit them as if they were a 6:3 but with a square in the middle.
 * but don't worry, it won't ever work.
 *
 * also, apparently we need the pythagorean composite to have at least 3 pythagorean primes.  no doubles, no primes.
 */
//center values are 12n+1 where n is even
