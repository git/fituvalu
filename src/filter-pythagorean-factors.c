/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include <argz.h>
#include "magicsquareutil.h"

struct fv_app_filter_pythag_factor_t
{
  int num_factors;
  int invert;
  FILE *out;
  void (*dump_func) (mpz_t *, FILE *);
  int (*read_number) (FILE *, mpz_t *, char **, size_t *);
  int factor;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_filter_pythag_factor_t *app = (struct fv_app_filter_pythag_factor_t *) state->input;
  switch (key)
    {
    case 'f':
      app->factor = 1;
      break;
    case 'v':
      app->invert = 1;
      break;
    case 'i':
      app->read_number = binary_read_one_number_from_stream;
      break;
    case 'o':
      app->dump_func = display_binary_number;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static int
prime_is_fournminusone (mpz_t i)
{
  mpz_t j, r;
  mpz_inits (j, r, NULL);
  mpz_sub_ui (j, i, 1);
  int ret = mpz_mod_ui (r, j, 4);
  mpz_clears (j, r, NULL);
  return ret == 0;
}

static void
liner (struct fv_app_filter_pythag_factor_t *app, FILE *stream, mpz_t *num)
{
  ssize_t fnread, nread;
  char *line = NULL;
  size_t len = 0;
  int count = 0;
  int all_4n1 = 1;
  mpz_t root, p, prev, r;
  fnread = getdelim (&line, &len, ':', stream);
  if (fnread == -1)
    {
      mpz_set_si (*num, -1);
      return;
    }

  char *colon = strchr (line, ':');
  if (colon)
    *colon = ' ';
  mpz_set_str (*num, line, 10);
  nread = getline (&line, &len, stream);
  if (nread == -1)
    {
      mpz_set_si (*num, -1);
      return;
    }

    {
      mpz_inits (root, p, prev, r, NULL);

        {
          char *s1 = NULL;
          size_t s1len = 0;

          FILE *fp = fmemopen (line + 1, strlen (line), "r");
          while ((nread = getdelim (&s1, &s1len, ' ', fp)) != -1) 
            {
              mpz_set_str (p, s1, 10);
              if (mpz_cmp (prev, p) != 0)
                {
                  if (prime_is_fournminusone (p))
                    {
                      count++;
                    }
                  else // a non pythagorean prime, all bets are OFF kimosabe
                    {
                      all_4n1 = 0;
                      count = 0;
                      break;
                    }
                }

              size_t mlen = strlen (s1);
              if (mlen > 0)
                {
                  if (s1[mlen-1] == '\n')
                    break;
                }
              mpz_set (prev, p);
            }
          free (s1);
          fclose (fp);
        }
      mpz_clears (root, p, prev, r, NULL);
    }
  free (line);

      if (!all_4n1)
        mpz_set_ui (*num, 0);
}

//adapted from 'factor' in coreutils
void
mpz_pythag_factor (mpz_t n, unsigned long int a, int *all, int *total)
{
  if (*all == 0)
    return;
  mpz_t x, z, y, P;
  mpz_t t, t2;

  mpz_inits (t, t2, NULL);
  mpz_init_set_si (y, 2);
  mpz_init_set_si (x, 2);
  mpz_init_set_si (z, 2);
  mpz_init_set_ui (P, 1);

  unsigned long long int k = 1;
  unsigned long long int l = 1;

  while (mpz_cmp_ui (n, 1) != 0)
    {
      for (;;)
        {
          do
            {
              mpz_mul (t, x, x);
              mpz_mod (x, t, n);
              mpz_add_ui (x, x, a);

              mpz_sub (t, z, x);
              mpz_mul (t2, P, t);
              mpz_mod (P, t2, n);

              if (k % 32 == 1)
                {
                  mpz_gcd (t, P, n);
                  if (mpz_cmp_ui (t, 1) != 0)
                    goto factor_found;
                  mpz_set (y, x);
                }
            }
          while (--k != 0);

          mpz_set (z, x);
          k = l;
          l = 2 * l;
          for (unsigned long long int i = 0; i < k; i++)
            {
              mpz_mul (t, x, x);
              mpz_mod (x, t, n);
              mpz_add_ui (x, x, a);
            }
          mpz_set (y, x);
        }

    factor_found:
      do
        {
          mpz_mul (t, y, y);
          mpz_mod (y, t, n);
          mpz_add_ui (y, y, a);

          mpz_sub (t, z, y);
          mpz_gcd (t, t, n);
        }
      while (mpz_cmp_ui (t, 1) == 0);

      mpz_divexact (n, n, t);   /* divide by t, before t is overwritten */

      if (!mpz_probab_prime_p (t, 128))
        {
          mpz_pythag_factor (t, a + 1, all, total);
          if (*all == 0)
            break;
        }
      else
        {
          (*total)++;
          if (!prime_is_fournminusone (t))
            {
              *all = 0;
              break;
            }
        }

      if (mpz_probab_prime_p (n, 128))
        {
          (*total)++;
          if (!prime_is_fournminusone (n))
            {
              *all = 0;
            }
          break;
        }

      mpz_mod (x, x, n);
      mpz_mod (z, z, n);
      mpz_mod (y, y, n);
    }

  mpz_clears (P, t2, t, z, x, y, NULL);
}

int
fituvalu_filter_pythag_factor (struct fv_app_filter_pythag_factor_t *app, FILE *in)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  if (app->factor)
    {
      mpz_t retval;
      mpz_init (retval);
      while (1)
        {
          liner (app, in, &retval);
          if (mpz_cmp_ui (retval, 0) > 0)
            {
              (*app->dump_func) (&retval, app->out);
              fflush (app->out);
            }
          else if (mpz_cmp_si (retval, -1) == 0)
            break;
        }
      mpz_clear (retval);
      return 0;
    }
  mpz_t m, n;
  mpz_inits (m, n, NULL);
  while (1)
    {
      read = app->read_number (in, &n, &line, &len);
      if (read == -1)
        break;

      if (mpz_sgn (n) > 0)
        {
          mpz_set (m, n);
          int show = 0;
          int all = 1;
          int total = 0;
          mpz_pythag_factor (n, 1, &all, &total);
          if (all)
            show = 1;
          if (app->invert)
            show = !show;
          if (show)
            app->dump_func (&m, app->out);
        }
    }
  mpz_clears (m, n, NULL);

  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "invert", 'v', 0, OPTION_HIDDEN, "Invert the result", 1 },
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "factor", 'f', 0, 0, "Input is from GNU factor"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept numbers on the standard input and display the ones whose factors are only pythagorean primes.\vThe following types have this kind of number as a centermost value: 6:1, 6:3, 6:6, 6:7, 6:8, 6:9, 6:10, 6:11, 6:12, 6:14, 6:15, and 6:16.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_filter_pythag_factor_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.dump_func = display_textual_number;
  app.read_number = read_one_number_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_filter_pythag_factor (&app, stdin);
}
