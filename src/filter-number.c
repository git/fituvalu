/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

enum mode
{
  MODE_EQUAL,
  MODE_GT,
  MODE_GE,
  MODE_LT,
  MODE_LE,
};

enum {
  OPT_GT = -31,
  OPT_GE,
  OPT_LT,
  OPT_LE,
  OPT_E,
};

struct filter
{
  mpz_t num;
  enum mode mode;
};

struct fv_app_filter_number_t
{
  struct filter *filters;
  int num_filters;
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int invert;
  int all;
  FILE *infile;
  FILE *out;
  int num_args;
};

int
fituvalu_filter_number (struct fv_app_filter_number_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3][3];

  int i, j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  while (1)
    {
      read = app->read_square (app->infile, &a, &line, &len);
      if (read == -1)
        break;
      int match = 0;
      for (i = 0; i < 3; i++)
        {
          for (j = 0; j < 3; j++)
            {
              for (int k = 0; k < app->num_filters; k++)
                {
                  switch (app->filters[k].mode)
                    {
                    case MODE_EQUAL:
                      if (mpz_cmp (a[i][j], app->filters[k].num) == 0)
                        match++;
                      break;
                    case MODE_GT:
                      if (mpz_cmp (a[i][j], app->filters[k].num) > 0)
                        match++;
                      break;
                    case MODE_GE:
                      if (mpz_cmp (a[i][j], app->filters[k].num) >= 0)
                        match++;
                      break;
                    case MODE_LT:
                      if (mpz_cmp (a[i][j], app->filters[k].num) < 0)
                        match++;
                      break;
                    case MODE_LE:
                      if (mpz_cmp (a[i][j], app->filters[k].num) <= 0)
                        match++;
                      break;
                    }
                }
            }
        }
      int show = 0;
      if (app->all)
        {
          if (match == 9 )
            show = 1;
        }
      else
        {
          if (match > 0)
            show = 1;
        }
      if (app->invert)
        show = !show;
      if (show)
        app->display_square (a, app->out);
    }

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);

  if (line)
    free (line);
  return 0;
}

static void
add_filter (struct fv_app_filter_number_t *app, char *arg, enum mode m)
{
  struct filter f;

  app->filters = realloc (app->filters,
                          sizeof (struct filter) * (app->num_filters + 1));
  mpz_init_set_str (app->filters[app->num_filters].num, arg, 10);
  app->filters[app->num_filters].mode = m;
  app->num_filters++;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_filter_number_t *app = (struct fv_app_filter_number_t *) state->input;
  switch (key)
    {
    case 'a':
      app->all = 1;
      break;
    case 'v':
      app->invert = 1;
      break;
    case OPT_LT:
      add_filter (app, arg, MODE_LT);
      break;
    case OPT_LE:
      add_filter (app, arg, MODE_LE);
      break;
    case OPT_GT:
      add_filter (app, arg, MODE_GT);
      break;
    case OPT_GE:
      add_filter (app, arg, MODE_GE);
      break;
    case OPT_E:
      add_filter (app, arg, MODE_EQUAL);
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args >= 1)
        argp_error (state, "too many arguments");
      app->infile = fopen (arg, "r");
      if (app->infile == NULL)
        argp_error (state, "could not open `%s' for reading (%m)", arg);
      app->num_args++;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->num_filters == 0)
        argp_error (state, "too few filter criteria");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "invert", 'v', 0, 0, "Invert the filter criteria", 1 },
  { "gt", OPT_GT, "NUM", 0, "Show squares containing a value greater than NUM", 1 },
  { "lt", OPT_LT, "NUM", 0, "Show squares containing a value less than NUM", 1 },
  { "ge", OPT_GE, "NUM", 0, "Show squares containing NUM or more", 1 },
  { "gte", OPT_GE, "NUM", OPTION_ALIAS | OPTION_HIDDEN, "Show squares containing NUM or more", 1 },
  { "le", OPT_LE, "NUM", 0, "Show squares containing NUM or less", 1 },
  { "lte", OPT_LE, "NUM", OPTION_ALIAS | OPTION_HIDDEN, "Show squares containing NUM or less", 1 },
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text", 2 },
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text", 2 },
  { "eq", OPT_E, "NUM", 0, "Show squares containing NUM", 1 },
  { "equal", OPT_E, "NUM", OPTION_ALIAS | OPTION_HIDDEN, "Show squares containing NUM", 1 },
  { "all", 'a', 0, 0, "Require filter criteria to be true on all cells", 2 },
  { 0 },
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Accept 3x3 magic squares from FILE, and display when any cell value meets the filtering criteria.\vThe nine values must be separated by a comma and terminated by a newline.  When FILE is not provided it is read from the standard input.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_filter_number_t app;
  memset (&app, 0, sizeof (app));
  app.read_square = read_square_from_stream;
  app.display_square = display_square_record;
  app.infile = stdin;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_filter_number (&app);
}
