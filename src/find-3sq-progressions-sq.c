/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

struct fv_app_find_3sq_progressions_sq_t
{
  int tenth;
  int one;
  int gcd;
  int num_args;
  int showroot;
  int show_diff;
  int from_stdin;
  void (*display_record) (mpz_t *, mpz_t*, FILE *out);
  int (*read_number) (FILE *, mpz_t *, char **, size_t *);
  mpz_t num;
};

//from https://www.fq.math.ca/Papers1/43-2/paper43-2-1.pdf
static void
generate_progression (struct fv_app_find_3sq_progressions_sq_t *app, mpz_t *progression, mpz_t mtwo, mpz_t ntwo, FILE *out)
{
  mpz_t mn, twomn, m, n;
  mpz_inits (mn, twomn, m, n, NULL);
  mpz_sqrt (m, mtwo);
  mpz_sqrt (n, ntwo);
  mpz_mul (mn, m, n);
  mpz_mul_ui (twomn, mn, 2);
  mpz_sub (progression[0], progression[1], twomn);
  mpz_add (progression[2], progression[1], twomn);
  mpz_clears (mn, twomn, m, n, NULL);
}

static void
generate_progression_and_show (struct fv_app_find_3sq_progressions_sq_t *app, mpz_t *progression, FILE *out)
{
  mpz_t i, iroot, diff, limit, root, gcd;
  mpz_inits (i, iroot, diff, limit, root, gcd, NULL);
  mpz_cdiv_q_ui (limit, progression[1], 2);
  mpz_set_ui (i, 1);
  if (app->tenth)
    {
      mpz_cdiv_q_ui (i, limit, 3); //closest integer to sqrt(10) i guess.
      mpz_sub (i, i, limit);
      mpz_abs (i, i);
      mpz_sqrt (iroot, i);
    }
  while (1)
    {
      if (mpz_cmp (i, limit) > 0)
        break;

      mpz_sub (diff, progression[1], i);
      if (mpz_perfect_square_p (diff))
        {
          int show = 1;
          generate_progression (app, progression, i, diff, out);
          if (app->gcd)
            {
              mpz_gcd (gcd, progression[0], progression[1]);
              mpz_gcd (gcd, gcd, progression[2]);
              if (mpz_cmp_ui (gcd, 1) != 0)
                show = 0;
              /*
              else
                {
                  fprintf (stderr, "scale=16; ");
                  display_textual_number_no_newline (i, stderr);
                  fprintf (stderr, " / ");
                  display_textual_number_no_newline (limit, stderr);
                  fprintf (stderr, "\n");
                }
                */
            }
          if (show)
            {
              if (app->showroot)
                mpz_sqrt (root, progression[2]);
              app->display_record (progression, &root, out);
              if (app->one)
                break;
            }
        }
      if (mpz_cmp_ui (i, 1) == 0)
        {
          mpz_set_ui (i, 4);
          mpz_set_ui (iroot, 2);
          continue;
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, diff, limit, root, gcd, NULL);
  return;
}

int
fituvalu_find_3sq_progression_sq (struct fv_app_find_3sq_progressions_sq_t *app, FILE *in, FILE *out)
{
  mpz_t progression[3];
  mpz_inits (progression[0], progression[1], progression[2], NULL);
  if (app->from_stdin)
    {
      mpz_t num;
      mpz_init (num);
      char *line = NULL;
      size_t len = 0;
      ssize_t read;

      while (1)
        {
          read = app->read_number (in, &num, &line, &len);
          if (read == -1)
            break;
          mpz_set (progression[1], num);
          generate_progression_and_show (app, progression, out);
        }

      if (line)
        free (line);
      mpz_clear (num);
    }
  else
    {
      mpz_set (progression[1], app->num);
      generate_progression_and_show (app, progression, out);
    }
  mpz_clears (progression[0], progression[1], progression[2], NULL);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_find_3sq_progressions_sq_t *app = (struct fv_app_find_3sq_progressions_sq_t *) state->input;
  switch (key)
    {
    case 't':
      app->tenth = 1;
      break;
    case '1':
      app->one = 1;
      break;
    case 'g':
      app->gcd = 1;
      break;
    case 'i':
      app->read_number = binary_read_one_number_from_stream;
      break;
    case 'd':
      app->show_diff = 1;
      break;
    case 'n':
      app->showroot = 0;
      break;
    case 'o':
      app->display_record = display_binary_three_record_with_root;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          switch (app->num_args)
            {
            case 0:
              mpz_set_str (app->num, arg, 10);
              break;
            }
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      mpz_init (app->num);
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args == 0)
        app->from_stdin = 1;
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "gcd", 'g', 0, 0, "Require greatest common divisor of 1"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "no-root", 'n', 0, 0, "Don't show the root of the fourth number"},
  { "show-diff", 'd', 0, 0, "Also show the diff"},
  { "one", '1', 0, 0, "Stop after finding one progression"},
  { "tenth", 't', 0, 0, "Only do the final tenth of the search"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[SQUARE]",
  "Find arithmetic progressions consisting of three squares given a middle SQUARE.\vWhen SQUARE is not provided, it is read from the standard input.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_find_3sq_progressions_sq_t app;
  memset (&app, 0, sizeof (app));
  app.showroot = 1;
  app.display_record = display_three_record_with_root;
  app.read_number = read_one_number_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_find_3sq_progression_sq (&app, stdin, stdout);
}
