/* Copyright (C) 2016, 2017, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

struct fv_app_sq_seq_t
{
  int num_args;
  mpz_t args[3];
  void (*dump_func) (mpz_t *, mpz_t *, FILE *);
  FILE *out;
};

static int
pr_seq (struct fv_app_sq_seq_t *app, mpz_t start, mpz_t finish, mpz_t incr, FILE *out)
{
  mpz_t i, j;
  mpz_inits (i, j, NULL);
  for (mpz_set (i, start); mpz_cmp (i, finish) < 0; mpz_add (i, i, incr))
    for (mpz_set (j, i); mpz_cmp (j, finish) < 0; mpz_add_ui (j, j, 1))
      if (mpz_cmp (i, j) != 0)
        app->dump_func (&i, &j, app->out);
  mpz_clears (i, j, NULL);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_sq_seq_t *app = (struct fv_app_sq_seq_t *) state->input;
  switch (key)
    {
    case 'o':
      app->dump_func = display_binary_two_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 3)
        argp_error (state, "too many arguments");
      else
        {
          mpz_set_str (app->args[app->num_args], arg, 10);
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      for (int i = 0; i < 3; i++)
        mpz_init (app->args[i]);
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_NO_ARGS:
      argp_error (state, "missing argument.");
      break;
    }
  return 0;
}

int
fituvalu_pr_seq (struct fv_app_sq_seq_t *app, FILE *out)
{
  mpz_t one;
  mpz_init (one);
  mpz_set_ui (one, 1);

  int ret = 0;

  switch (app->num_args)
    {
    case 1:
      ret = pr_seq (app, one, app->args[0], one, out);
      break;
    case 2:
      ret = pr_seq (app, app->args[0], app->args[1], one, out);
      break;
    case 3:
      ret = pr_seq (app, app->args[0], app->args[2], app->args[1], out);
      break;
    }

  mpz_clear (one);
  return ret;
}

static struct argp_option
options[] =
{
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "LAST\nFIRST LAST\nFIRST INCREMENT LAST",
  "Show every unique pair of numbers.\vIf FIRST or INCREMENT is omitted, it defaults to 1.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_sq_seq_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.dump_func = display_two_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_pr_seq (&app, stdout);

}
