/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_610_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  double median;
  double max;
  double percent;
  int mult;
  int threesq;
  int subtype;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t i, iroot, j, limit, twoiroot, sum, sum_minus_middle;
};


static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_610_type_1 (struct fv_app_search_610_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_set ((*s)[2][1], w->hi);
  mpz_set ((*s)[1][0], w->lo);

  //   +---+---+---+
  //   |   |   | B |
  //   +---+---+---+
  //   | A | D |   |
  //   +---+---+---+
  //   |   | C | E |
  //   +---+---+---+

  mpz_set ((*s)[1][1], q->i);

  mpz_mul_ui (q->sum, q->i, 3);
  mpz_mul_ui (q->sum_minus_middle, q->i, 2);

  mpz_add ((*s)[0][0], (*s)[1][1], w->distance);

  mpz_set ((*s)[2][2], q->j);

  mpz_sub ((*s)[2][0], q->sum, (*s)[2][1]);
  mpz_sub ((*s)[2][0], (*s)[2][0], (*s)[2][2]);

  mpz_sub ((*s)[1][2], q->sum_minus_middle, (*s)[1][0]);
  mpz_sub ((*s)[0][1], q->sum_minus_middle, (*s)[2][1]);

    {
      //check for dups
      int dup = 0;
      if (mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0 ||
          mpz_cmp ((*s)[1][1], (*s)[0][1]) == 0)
        dup = 1;
      if (!dup)
        {
          if (mpz_perfect_square_p ((*s)[1][2]))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
generate_610_type_2 (struct fv_app_search_610_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_set ((*s)[2][1], w->lo);
  mpz_set ((*s)[1][0], w->hi);

  //   +---+---+---+
  //   |   |   | B |
  //   +---+---+---+
  //   | C | E |   |
  //   +---+---+---+
  //   |   | A | D |
  //   +---+---+---+

  mpz_set ((*s)[1][1], q->j);

  mpz_mul_ui (q->sum, q->j, 3);
  mpz_mul_ui (q->sum_minus_middle, q->j, 2);

  mpz_sub ((*s)[0][0], (*s)[1][1], w->distance);

  mpz_set ((*s)[2][2], q->i);

  mpz_sub ((*s)[2][0], q->sum, (*s)[2][1]);
  mpz_sub ((*s)[2][0], (*s)[2][0], (*s)[2][2]);

  mpz_sub ((*s)[1][2], q->sum_minus_middle, (*s)[1][0]);

  mpz_sub ((*s)[0][1], q->sum_minus_middle, (*s)[2][1]);
    {
      //check for dups
      int dup = 0;
      if (mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0 ||
          mpz_cmp ((*s)[1][1], (*s)[0][1]) == 0)
        dup = 1;
      if (!dup)
        {
          if (mpz_perfect_square_p ((*s)[1][2]))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
generate_610_type_1_and_2 (struct fv_app_search_610_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  //we start at sum counting up, and checking downwards for squares
  mpz_mul_ui (q->limit, w->hi, app->mult);

  mpz_sqrt (q->iroot, w->distance);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  while (1)
    {
      mpz_sub (q->j, q->i, w->distance);
      if (mpz_perfect_square_p (q->j))
        {
          if (app->subtype <= 1)
            generate_610_type_1 (app, w, q, s);
          if (app->subtype == 2 || app->subtype == 0)
            generate_610_type_2 (app, w, q, s);
        }

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
handle_progression (struct fv_app_search_610_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  generate_610_type_1_and_2 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_610_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[0][2], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[0][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[0][2], w->distance);
          mpz_add (w->hi, (*s)[0][2], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_610_t *app =
    (struct fv_app_search_610_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, q.sum_minus_middle, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[0][2], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[0][2], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[0][2], 1) > 0)
        generate_progressions (app, &w, &q, &s);
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (n, num, NULL);

  mpz_clears (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, q.sum_minus_middle, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_610_t *app =
    (struct fv_app_search_610_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, q.sum_minus_middle, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[0][2], w.lo);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, q.sum_minus_middle, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_610 (struct fv_app_search_610_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_610_t *app = (struct fv_app_search_610_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 's':
      app->subtype = atoi (arg);
      if (app->subtype <= 0 || app->subtype > 2)
        argp_error (state, "invalid subtype");
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'm':
      app->percent = strtod (arg, &end);
      if (end == NULL || *end != '\0' || app->percent <= 0)
        argp_error (state, "invalid argument `%s' to option --multiply", arg);
      app->percent /= 100.0;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      app->mult = ceil (app->max * app->percent);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "multiply", 'm', "PERC", 0, "Find a starting point for E by multiplying C by a value between 1 and 9755.2353 expressed as a percent (1-100)"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "subtype", 's', "NUM", 0, "Only produce subtype 1 or 2 instead of both"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:10 given a FILE containing top right values.\vWhen FILE is not provided, it is read from the standard input.  The default value for PERC is 0.001147330174680356.  Magic squares of type 6:10 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   |   | X |   |   |   | B |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating E\n"
"  | X | X | X |   | A | E | Z |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below E.\n"
"  |   | X | X |   |   | C | D |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n"
"                  +---+---+---+\n"
"                  |   |   | B |\n"
"                  +---+---+---+  (starts iterating F upwards)\n"
"                  | C | E | Z |\n"
"                  +---+---+---+\n"
"                  |   | A | F |\n"
"                  +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_610_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.median = 2.8117;
  app.max = 735193.7730;
  app.percent = (app.median * 3.0) / app.max;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_610 (&app);
}
/*

the point of this is to identify information that can help us get more 6:10s.  we want to be able to predict 'center' values (the B values) for 6:10 magic squares.

  +---+---+---+   +---+---+---+
  |   |   | X |   |   |   | B |
  +---+---+---+   +---+---+---+
  | X | X | X |   | A | E | Z |
  +---+---+---+   +---+---+---+
  |   | X | X |   |   | C | D |
  +---+---+---+   +---+---+---+
                  +---+---+---+
                  |   |   | B |
                  +---+---+---+
                  | C | E | Z |
                  +---+---+---+
                  |   | A | F |
                  +---+---+---+

we identified which B values lead to magic squares with 6 perfect squares in the 6:10 configuration, and then here we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:10 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 1200 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

118, 0, 86, 101 / 30864 = 0.003272
118, 86, 0, 101 / 30864 = 0.003272
84, 60, 60, 153 / 46287 = 0.003305
12, 72, 120, 160 / 46286 = 0.003457
84, 36, 84, 160 / 46288 = 0.003457
16, 68, 120, 164 / 46287 = 0.003543
0, 84, 120, 117 / 30860 = 0.003791
84, 0, 120, 117 / 30860 = 0.003791
84, 120, 0, 117 / 30860 = 0.003791
84, 34, 86, 176 / 46287 = 0.003802

well the percentge isn't good. they all add up to 204 (84, 34, 86).
we can see the ever-present 16, 68, 120 tuple.
but 84, 34, 86 manages to do better here.

second run is depth of 2, and breadth of 2000.

504, 1920, 15 / 2606 = 0.005756
1546, 596, 17 / 2949 = 0.005765
1920, 120, 18 / 3097 = 0.005812
1378, 1982, 11 / 1880 = 0.005851
1188, 1764, 13 / 2137 = 0.006083
1164, 1632, 14 / 2256 = 0.006206
1920, 1752, 13 / 1722 = 0.007549
1104, 1548, 18 / 2381 = 0.007560
118, 1310, 43 / 4415 = 0.009740
1546, 1310, 22 / 2215 = 0.009932


hmm the pecentage is about 3 times higher.  down from 176 hits to 22 though.
there's a worry that we're just trying to predict somewhat random numbers here, and that any of the top 3 are just as good.   there's no frequent sum,  but there's something going on with sums 1428 and 2856.  and we've had this best pair before in 6:9: 1546, 1310. how about that.

and just for kicks, let's try a depth of 1 and a breadth of 100000.

77994, 2 / 43 = 0.046512
78084, 2 / 43 = 0.046512
79912, 2 / 42 = 0.047619
87060, 2 / 39 = 0.051282
90758, 2 / 37 = 0.054054
97023, 2 / 35 = 0.057143
99564, 2 / 34 = 0.058824

i'm sure we're just in la-la guessing land here.
but maybe 99564 is worth trying.

let's say the minimum count has to be 10 or higher.

408, 20 / 7719 = 0.002591
414, 20 / 7610 = 0.002628
840, 10 / 3752 = 0.002665
700, 12 / 4501 = 0.002666
372, 23 / 8467 = 0.002716
204, 42 / 15432 = 0.002722
336, 27 / 9374 = 0.002880
420, 26 / 7503 = 0.003465
828, 14 / 3808 = 0.003676
1260, 10 / 2506 = 0.003990

1260 is the best i guess.   204 is a standout though.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

4, 2, 0, 1200 / 1048965 = 0.001144
4, 1, 1, 1200 / 1573447 = 0.000763
4, 0, 2, 1200 / 1048965 = 0.001144
3, 1, 2, 1200 / 1573448 = 0.000763
2, 2, 2, 1200 / 1573448 = 0.000763
2, 2, 0, 1200 / 1573448 = 0.000763
2, 1, 1, 1200 / 2360172 = 0.000508
2, 0, 2, 1200 / 1573448 = 0.000763
2, 0, 0, 1200 / 1573448 = 0.000763
1, 3, 2, 1200 / 1573448 = 0.000763
1, 2, 0, 1200 / 2097931 = 0.000572
1, 1, 2, 1200 / 2360172 = 0.000508
1, 1, 1, 1200 / 3146896 = 0.000381
1, 1, 0, 1200 / 3146896 = 0.000381
1, 0, 2, 1200 / 2097931 = 0.000572
1, 0, 1, 1200 / 3146896 = 0.000381
1, 0, 0, 1200 / 3146896 = 0.000381
0, 4, 2, 1200 / 1048965 = 0.001144
0, 2, 2, 1200 / 1573448 = 0.000763
0, 2, 0, 1200 / 1573448 = 0.000763
0, 1, 2, 1200 / 2097931 = 0.000572
0, 1, 1, 1200 / 3146896 = 0.000381
0, 1, 0, 1200 / 3146896 = 0.000381
0, 0, 2, 1200 / 1573448 = 0.000763
0, 0, 1, 1200 / 3146896 = 0.000381
4, 6, 2, 1032 / 786724 = 0.001312

the best is up by 4s and 2s.

results:

so looking forward for more sixes, pick one of "90758", "97023", or "99564".  Try "1260", and "1546, 1310", and "84, 34, 86".

we can recalculate quicker by using the pattern "4,2" and still get them all.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 625 in sq-seq-finder.

60, 102, 42, 130 / 46284 = 0.002809
60, 72, 72, 130 / 46286 = 0.002809
10, 50, 108, 161 / 56206 = 0.002864
94, 102, 8, 133 / 46289 = 0.002873
60, 84, 60, 136 / 46287 = 0.002938
60, 60, 84, 138 / 46285 = 0.002982
10, 84, 74, 173 / 56202 = 0.003078
60, 120, 24, 144 / 46285 = 0.003111
60, 36, 108, 145 / 46285 = 0.003133
60, 34, 110, 161 / 46284 = 0.003479

well lots of 60s.  and they frequently add up to 204 (e.g. 60+34+110).
i guess our best tuple is "60, 34, 110".  the percentage isn't very good.

no arithmetic progressions here.

we did a little better with a different starting point in terms of percentage.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

480, 1944, 17 / 2605 = 0.006526
1488, 1716, 13 / 1976 = 0.006579
1354, 494, 23 / 3418 = 0.006729
954, 1716, 16 / 2368 = 0.006757
1354, 1726, 14 / 2054 = 0.006816
1080, 1572, 17 / 2382 = 0.007137
1164, 1788, 17 / 2140 = 0.007944
1896, 1776, 15 / 1724 = 0.008701
94, 1334, 42 / 4414 = 0.009515
1522, 1334, 22 / 2215 = 0.009932

hmm, well we did a tiny bit better. 
i guess "1522, 1344" is the best of the bunch.

okay let's do depth=1, and breadth is 100000

74328, 3 / 46 = 0.065217
76008, 3 / 45 = 0.066667
80100, 3 / 43 = 0.069767
82342, 3 / 42 = 0.071429
86571, 3 / 40 = 0.075000
88457, 3 / 39 = 0.076923
98456, 3 / 35 = 0.085714

well 8 percent is high.  does it pan out?  probably not.
and 8 is a lot better than 4 percent from earlier.

let's do depth=1, breadth is 100000, min hits of 10

918, 10 / 3433 = 0.002913
852, 11 / 3698 = 0.002975
972, 10 / 3244 = 0.003083
456, 22 / 6907 = 0.003185
924, 11 / 3411 = 0.003225
900, 12 / 3504 = 0.003425
1080, 10 / 2919 = 0.003426
912, 12 / 3456 = 0.003472
840, 15 / 3755 = 0.003995
600, 21 / 5254 = 0.003997

well 600 is nice and round, and a relatively good number of hits.

so overall, looking forward for more sixes, pick one of "90758", "97023", or "99564", and then one of "86571", "88457", or "98456".  Try "1546, 1310", "84, 34, 86", "1522, 1344 ", and "60, 34, 110".  and throw in "1260" and "600".

okay, let's do the first run again but sort on hits to see how it shakes out.

4, 2, 0, 1200 / 1048958 = 0.001144
4, 1, 1, 1200 / 1573436 = 0.000763
4, 0, 2, 1200 / 1048958 = 0.001144
3, 1, 2, 1200 / 1573437 = 0.000763
2, 2, 2, 1200 / 1573437 = 0.000763
2, 2, 0, 1200 / 1573437 = 0.000763
2, 1, 1, 1200 / 2360155 = 0.000508
2, 0, 2, 1200 / 1573437 = 0.000763
2, 0, 0, 1200 / 1573437 = 0.000763
1, 3, 2, 1200 / 1573437 = 0.000763
1, 2, 0, 1200 / 2097916 = 0.000572
1, 1, 2, 1200 / 2360155 = 0.000508
1, 1, 1, 1200 / 3146873 = 0.000381
1, 1, 0, 1200 / 3146873 = 0.000381
1, 0, 2, 1200 / 2097916 = 0.000572
1, 0, 1, 1200 / 3146873 = 0.000381
1, 0, 0, 1200 / 3146873 = 0.000381
0, 4, 2, 1200 / 1048958 = 0.001144
0, 2, 2, 1200 / 1573437 = 0.000763
0, 2, 0, 1200 / 1573437 = 0.000763
0, 1, 2, 1200 / 2097916 = 0.000572
0, 1, 1, 1200 / 3146873 = 0.000381
0, 1, 0, 1200 / 3146873 = 0.000381
0, 0, 2, 1200 / 1573437 = 0.000763
0, 0, 1, 1200 / 3146873 = 0.000381
4, 6, 2, 1032 / 786719 = 0.001312

no better than before. gotta go up by 4,2 to get them all.

this concludes our analysis of anticipating B values in 6:10 magic squares.

in order to get high value 6:10s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

also we could consider running this test again but including all D values as B values.

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="90758" 1 10000000000000000000000000000 | ./search-610
sq-seq --pattern="97023" 1 10000000000000000000000000000 | ./search-610
sq-seq --pattern="99564" 1 10000000000000000000000000000 | ./search-610
sq-seq --pattern="1546,1310" 1 10000000000000000000000000000 | ./search-610
sq-seq --pattern="84,36,86" 1 10000000000000000000000000000 | ./search-610
sq-seq --pattern="1260" 1 10000000000000000000000000000 | ./search-610

sq-seq --pattern="86571" 625 10000000000000000000000000000 | ./search-610
sq-seq --pattern="88457" 625 10000000000000000000000000000 | ./search-610
sq-seq --pattern="98456" 625 10000000000000000000000000000 | ./search-610
sq-seq --pattern="1522,1344" 625 10000000000000000000000000000 | ./search-610
sq-seq --pattern="60,34,110" 625 10000000000000000000000000000 | ./search-610
sq-seq --pattern="600" 625 10000000000000000000000000000 | ./search-610
*/
