/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_612_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  double median;
  double max;
  double percent;
  int mult;
  int threesq;
  int subtype;
  int quick_abc;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t sum, i, j, iroot, distance2, twoiroot, limit, p;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
check_and_dump (struct fv_app_search_612_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3], int t)
{
  if (mpz_cmp ((*s)[2][1], (*s)[0][1]) > 0)
    {
      mpz_sub (q->p, (*s)[2][1], (*s)[0][1]);
      mpz_cdiv_q_ui (q->p, q->p, 2);
      mpz_add ((*s)[1][1], (*s)[0][1], q->p);
    }
  else
    {
      mpz_sub (q->p, (*s)[0][1], (*s)[2][1]);
      mpz_cdiv_q_ui (q->p, q->p, 2);
      mpz_add ((*s)[1][1], (*s)[2][1], q->p);
    }
  mpz_mul_ui (q->p, (*s)[1][1], 3);

  mpz_sub ((*s)[2][0], q->p, (*s)[2][1]);
  mpz_sub ((*s)[2][0], (*s)[2][0], (*s)[2][2]);
  mpz_sub ((*s)[0][2], q->p, (*s)[0][1]);
  mpz_sub ((*s)[0][2], (*s)[0][2], (*s)[0][0]);

  int dup = 0;
  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[1][0]) == 0 ||
      (t == 2 && mpz_cmp ((*s)[0][2], (*s)[1][1]) == 0))
    dup = 1;
  else
    {
      mpz_sub (q->p, q->p, (*s)[0][2]);
      mpz_sub (q->p, q->p, (*s)[1][1]);
      mpz_sub (q->p, q->p, (*s)[2][0]);
    }

  if (!dup && mpz_cmp_ui (q->p, 0) == 0)
    {
      if (mpz_perfect_square_p ((*s)[2][0]))
        {
          pthread_mutex_lock (&display_lock);
          app->display_square (*s, app->out);
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);
        }
    }
}

static void
generate_612_type_1 (struct fv_app_search_612_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  //  +---+---+---+
  //  |   | A |   |
  //  +---+---+---+
  //  | C |   | E |
  //  +---+---+---+
  //  |   | D | B |
  //  +---+---+---+
  mpz_set ((*s)[0][1], w->lo);
  mpz_set ((*s)[1][0], w->hi);

  mpz_set ((*s)[2][1], q->i);
  mpz_set ((*s)[1][2], q->j);

  mpz_add ((*s)[0][0], q->j, w->distance);

  check_and_dump (app, w, q, s, 1);
}

static void
generate_612_type_2 (struct fv_app_search_612_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  //  +---+---+---+
  //  |   | C |   |
  //  +---+---+---+
  //  | A |   | D |
  //  +---+---+---+
  //  |   | E | B |
  //  +---+---+---+
  mpz_set ((*s)[0][1], w->hi);
  mpz_set ((*s)[1][0], w->lo);

  mpz_set ((*s)[2][1], q->j);
  mpz_set ((*s)[1][2], q->i);

  mpz_add ((*s)[0][0], q->j, w->distance);

  check_and_dump (app, w, q, s, 2);
}

static void
generate_612_type_1_and_2 (struct fv_app_search_612_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_mul_ui (q->distance2, w->distance, 2);

  mpz_mul_ui (q->limit, w->hi, app->mult);

  mpz_sqrt (q->iroot, q->distance2);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  //iterate upwards, checking downwards for another square
  while (1)
    {
      mpz_sub (q->j, q->i, q->distance2);

      if (mpz_perfect_square_p (q->j))
        {
          if (app->subtype <= 1)
            generate_612_type_1 (app, w, q, s);
          if (app->subtype == 2 || app->subtype == 0)
            generate_612_type_2 (app, w, q, s);
        }

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
handle_progression (struct fv_app_search_612_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  generate_612_type_1_and_2 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_612_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[2][2], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);

  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[2][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[2][2], w->distance);
          mpz_add (w->hi, (*s)[2][2], w->distance);
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void
quick_generate_progressions (struct fv_app_search_612_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[2][2], 2);
  mpz_set_ui (w->i, 4);
  mpz_set_ui (w->iroot, 2);

  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[2][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[2][2], w->distance);
          mpz_add (w->hi, (*s)[2][2], w->distance);
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_612_t *app =
    (struct fv_app_search_612_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.sum, q.i, q.j, q.iroot, q.distance2, q.twoiroot, q.limit, q.p, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[2][2], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[2][2], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[2][2], 1) > 0)
        {
          if (app->quick_abc)
            quick_generate_progressions (app, &w, &q, &s);
          else
            generate_progressions (app, &w, &q, &s);
        }
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  
  mpz_clears (q.sum, q.i, q.j, q.iroot, q.distance2, q.twoiroot, q.limit, q.p, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_612_t *app =
    (struct fv_app_search_612_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);


  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.sum, q.i, q.j, q.iroot, q.distance2, q.twoiroot, q.limit, q.p, NULL);

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[2][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[2][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[2][2], w.lo);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (q.sum, q.i, q.j, q.iroot, q.distance2, q.twoiroot, q.limit, q.p, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_612 (struct fv_app_search_612_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *end = NULL;
  struct fv_app_search_612_t *app = (struct fv_app_search_612_t *) state->input;
  switch (key)
    {
    case 'Q':
      app->quick_abc = 1;
      break;
    case 's':
      app->subtype = atoi (arg);
      if (app->subtype <= 0 || app->subtype > 2)
        argp_error (state, "invalid subtype");
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'm':
      app->percent = strtod (arg, &end);
      if (end == NULL || *end != '\0' || app->percent <= 0)
        argp_error (state, "invalid argument `%s' to option --multiply", arg);
      break;
      app->percent /= 100.0;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      app->mult = ceil (app->max * app->percent);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "multiply", 'm', "PERC", 0, "Find a starting point for F by multiplying C by a value between 1 and 9755.2353 expressed as a percent (1-100)"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "subtype", 's', "NUM", 0, "Only produce subtype 1 or 2 instead of both"},
  { "quick-abc", 'Q', 0, OPTION_HIDDEN, "Do an abridged search"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:12 given a FILE containing bottom right values.\vWhen FILE is not provided, it is read from the standard input.  The default value for PERC is 0.00003842154799606859.  Magic squares of type 6:12 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   | X |   |   |   | A |   |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating F\n"
"  | X |   | X |   | C |   | D |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of 2*(B-A) below F.\n"
"  | X | X | X |   | Z | F | B |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n"
"                  +---+---+---+\n"
"                  |   | C |   |\n"
"                  +---+---+---+  (starts iterating F upwards)\n"
"                  | A |   | F |\n"
"                  +---+---+---+\n"
"                  | Z | D | B |\n"
"                  +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_612_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.median = 5.3913;
  app.max = 42095909.3102;
  app.percent = (app.median * 3.0) / app.max;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_612 (&app);
}
/*

the point of this is to identify information that can help us get more 6:12s.  we want to be able to predict 'center' values (the B values) for 6:12 magic squares.

  +---+---+---+   +---+---+---+
  |   | X |   |   |   | A |   |
  +---+---+---+   +---+---+---+
  | X |   | X |   | C |   | D |
  +---+---+---+   +---+---+---+
  | X | X | X |   | Z | F | B |
  +---+---+---+   +---+---+---+
                  +---+---+---+
                  |   | C |   |
                  +---+---+---+
                  | A |   | F |
                  +---+---+---+
                  | Z | D | B |
                  +---+---+---+

we identified which B values lead to magic squares with 6 perfect squares in the 6:12 configuration, and then here we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:12 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 647 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

118, 102, 86, 53 / 30994 = 0.001710
84, 102, 120, 53 / 30997 = 0.001710
84, 102, 18, 81 / 46492 = 0.001742
118, 68, 120, 55 / 30996 = 0.001774
16, 102, 86, 83 / 46491 = 0.001785
0, 84, 120, 60 / 30996 = 0.001936
84, 0, 120, 60 / 30996 = 0.001936
84, 120, 0, 60 / 30996 = 0.001936
84, 34, 86, 91 / 46492 = 0.001957
16, 68, 120, 98 / 46492 = 0.002108

well the percentge is poor.  a frequent sum of 204 appears.
we can see the ever-present 16, 68, 120 tuple as the winner here.
a few zeroes and 120s are also here.

second run is depth of 2, and breadth of 2000.

1512, 1514, 8 / 2093 = 0.003822
118, 1310, 17 / 4429 = 0.003838
1444, 596, 12 / 3103 = 0.003867
492, 1752, 11 / 2819 = 0.003902
1992, 1542, 7 / 1789 = 0.003913
1210, 1310, 10 / 2515 = 0.003976
1546, 1786, 8 / 1900 = 0.004211
1852, 1496, 8 / 1891 = 0.004231
1546, 1982, 8 / 1799 = 0.004447
1546, 1310, 13 / 2219 = 0.005858

hmm the pecentage is about 3 times higher.  down from 98 hits to 13 though.
there's no frequent sum,  and the best pair is "1546, 1310".

and just for kicks, let's try a depth of 1 and a breadth of 100000.

99658, 1 / 33 = 0.030303
53604, 2 / 61 = 0.032787
58818, 2 / 56 = 0.035714
74150, 2 / 45 = 0.044444
78044, 2 / 43 = 0.046512

i'm sure we're just in la-la guessing land here.
but maybe 78044 is worth trying.

let's say the minimum count has to be 10 or higher.

36, 85 / 87814 = 0.000968
236, 14 / 13396 = 0.001045
228, 15 / 13867 = 0.001082
348, 10 / 9085 = 0.001101
324, 11 / 9758 = 0.001127
372, 10 / 8500 = 0.001176
180, 21 / 17562 = 0.001196
252, 15 / 12545 = 0.001196
360, 12 / 8782 = 0.001366
396, 11 / 7985 = 0.001378

396 is the best i guess.  36 is a standout though.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

4, 2, 0, 647 / 1053713 = 0.000614
4, 1, 1, 647 / 1580569 = 0.000409
4, 0, 2, 647 / 1053713 = 0.000614
3, 1, 2, 647 / 1580570 = 0.000409
2, 2, 2, 647 / 1580570 = 0.000409
2, 2, 0, 647 / 1580570 = 0.000409
2, 1, 1, 647 / 2370855 = 0.000273
2, 0, 2, 647 / 1580570 = 0.000409
2, 0, 0, 647 / 1580570 = 0.000409
1, 3, 2, 647 / 1580570 = 0.000409
1, 2, 0, 647 / 2107427 = 0.000307
1, 1, 2, 647 / 2370855 = 0.000273
1, 1, 1, 647 / 3161140 = 0.000205
1, 1, 0, 647 / 3161140 = 0.000205
1, 0, 2, 647 / 2107427 = 0.000307
1, 0, 1, 647 / 3161140 = 0.000205
1, 0, 0, 647 / 3161140 = 0.000205
0, 4, 2, 647 / 1053713 = 0.000614
0, 2, 2, 647 / 1580570 = 0.000409
0, 2, 0, 647 / 1580570 = 0.000409
0, 1, 2, 647 / 2107427 = 0.000307
0, 1, 1, 647 / 3161140 = 0.000205
0, 1, 0, 647 / 3161140 = 0.000205
0, 0, 2, 647 / 1580570 = 0.000409
0, 0, 1, 647 / 3161140 = 0.000205
2, 2, 4, 561 / 1185428 = 0.000473

the best is up by 4s and 2s.

results:

so looking forward for more sixes, pick one of "78044", "74150", or "58818".  Try "396", and "1546, 1310", and "16, 68, 120".

we can recalculate quicker by using the pattern "4,2" and still get them all.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 14161 in sq-seq-finder.

68, 0, 34, 136 / 61985 = 0.002194
68, 34, 0, 136 / 61985 = 0.002194
0, 0, 102, 69 / 30997 = 0.002226
0, 102, 0, 69 / 30997 = 0.002226
0, 102, 102, 69 / 30997 = 0.002226
102, 0, 0, 69 / 30997 = 0.002226
102, 0, 102, 69 / 30997 = 0.002226
102, 102, 0, 69 / 30997 = 0.002226
102, 102, 102, 69 / 30997 = 0.002226
102, 68, 34, 115 / 46493 = 0.002473

well we have a lot of zeroes and 102s.  there are frequent sums based on 102 as a factor.
"102, 68, 34" is the best.

there are arithmetic progressions here but heading downwards. strange.

we did a little better with a different starting point in terms of percentage.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

1734, 1428, 11 / 2007 = 0.005481
626, 802, 26 / 4440 = 0.005856
170, 1258, 27 / 4436 = 0.006087
918, 510, 27 / 4434 = 0.006089
1598, 1700, 12 / 1922 = 0.006243
476, 952, 28 / 4439 = 0.006308
1394, 34, 29 / 4432 = 0.006543
0, 1428, 17 / 2221 = 0.007654
1428, 0, 17 / 2221 = 0.007654
1428, 1428, 17 / 2221 = 0.007654

hmm, well we did a bit better. 
hmm it's not often that we get zeroes and multiples in this run.

there's something going on with sums 2856 and 1428.
i guess "1598, 1700" is the best of the bunch. but 1428 is interesting.
(excluding 1394, 34 becuase it adds up to 1428.)

okay let's do depth=1, and breadth is 100000

99101, 2 / 34 = 0.058824
99110, 2 / 34 = 0.058824
99382, 2 / 34 = 0.058824
99970, 2 / 34 = 0.058824
82399, 3 / 42 = 0.071429
82450, 3 / 42 = 0.071429

well 7 percent is alright.  does it pan out?  probably not.
and 7 is better than 4.6 percent from earlier.

let's do depth=1, breadth is 100000, min hits of 10

748, 10 / 4228 = 0.002365
238, 32 / 13290 = 0.002408
374, 24 / 8455 = 0.002839
924, 10 / 3426 = 0.002919
918, 11 / 3446 = 0.003192
952, 11 / 3327 = 0.003306
1122, 10 / 2820 = 0.003546
476, 28 / 6650 = 0.004211
714, 19 / 4435 = 0.004284
1428, 17 / 2221 = 0.007654

and there's 1428 popping up.  yeah we could try that one.

so overall, looking forward for more sixes, pick one of "78044", "74150", or "58818", and then one of "99970", "82399", or "82450".  Try "1546, 1310", "16, 68, 120", "1598, 1700", and "102, 68, 34".  and throw in "396" and "1428".

okay, let's do the first run again but sort on hits to see how it shakes out.

2, 4, 0, 647 / 1053675 = 0.000614
2, 3, 1, 647 / 1580512 = 0.000409
2, 2, 2, 647 / 1580512 = 0.000409
2, 2, 0, 647 / 1580512 = 0.000409
2, 1, 3, 647 / 1580512 = 0.000409
2, 1, 1, 647 / 2370767 = 0.000273
2, 1, 0, 647 / 2107349 = 0.000307
2, 0, 4, 647 / 1053675 = 0.000614
2, 0, 2, 647 / 1580512 = 0.000409
2, 0, 1, 647 / 2107349 = 0.000307
2, 0, 0, 647 / 1580512 = 0.000409
1, 1, 4, 647 / 1580512 = 0.000409
1, 1, 2, 647 / 2370768 = 0.000273
1, 1, 1, 647 / 3161023 = 0.000205
1, 1, 0, 647 / 3161023 = 0.000205
1, 0, 1, 647 / 3161023 = 0.000205
1, 0, 0, 647 / 3161023 = 0.000205
0, 2, 4, 647 / 1053675 = 0.000614
0, 2, 2, 647 / 1580512 = 0.000409
0, 2, 1, 647 / 2107349 = 0.000307
0, 2, 0, 647 / 1580512 = 0.000409
0, 1, 1, 647 / 3161023 = 0.000205
0, 1, 0, 647 / 3161023 = 0.000205
0, 0, 2, 647 / 1580512 = 0.000409
0, 0, 1, 647 / 3161023 = 0.000205
2, 4, 6, 548 / 790257 = 0.000693
2, 4, 2, 499 / 1185385 = 0.000421

no better than before. gotta go up by 4,2 to get them all.

this concludes our analysis of anticipating B values in 6:12 magic squares.

in order to get high value 6:12s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

also we could consider running this test again but including all D values as B values.

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="78044" 1 10000000000000000000000000000 | ./search-612
sq-seq --pattern="74150" 1 10000000000000000000000000000 | ./search-612
sq-seq --pattern="58818" 1 10000000000000000000000000000 | ./search-612
sq-seq --pattern="1546,1310" 1 10000000000000000000000000000 | ./search-612
sq-seq --pattern="16,68,120" 1 10000000000000000000000000000 | ./search-612
sq-seq --pattern="396" 1 10000000000000000000000000000 | ./search-612

sq-seq --pattern="99970" 14161 10000000000000000000000000000 | ./search-612
sq-seq --pattern="82399" 14161 10000000000000000000000000000 | ./search-612
sq-seq --pattern="82450" 14161 10000000000000000000000000000 | ./search-612
sq-seq --pattern="1598,1700" 14161 10000000000000000000000000000 | ./search-612
sq-seq --pattern="102,68,34" 14161 10000000000000000000000000000 | ./search-612
sq-seq --pattern="1428" 14161 10000000000000000000000000000 | ./search-612
*/
