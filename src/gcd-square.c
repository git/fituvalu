/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <argp.h>
#include <gmp.h>
#include <string.h>
#include "magicsquareutil.h"
struct fv_app_gcd_square_t
{
  int num_filters;
  int *filter_count;
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  void (*display_square) (mpz_t s[3][3], FILE *out);
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_gcd_square_t *app = (struct fv_app_gcd_square_t *) state->input;
  int count;
  switch (key)
    {
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case 'f':

      count = atoi (arg);
      if (count <= 0)
        argp_error (state, "gcd must be over zero.");
        {
          app->filter_count =
            realloc (app->filter_count, sizeof (int) * (app->num_filters + 1));
          app->filter_count[app->num_filters] = count;
          app->num_filters++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

int
square_get_gcd (mpz_t (*a)[3][3], mpz_t *gcd)
{
  mpz_gcd (*gcd, (*a)[0][0], (*a)[0][1]);
  mpz_gcd (*gcd, *gcd, (*a)[0][2]);
  mpz_gcd (*gcd, *gcd, (*a)[1][0]);
  mpz_gcd (*gcd, *gcd, (*a)[1][1]);
  mpz_gcd (*gcd, *gcd, (*a)[1][2]);
  mpz_gcd (*gcd, *gcd, (*a)[2][0]);
  mpz_gcd (*gcd, *gcd, (*a)[2][1]);
  mpz_gcd (*gcd, *gcd, (*a)[2][2]);
}

static int
filter_gcd_square (struct fv_app_gcd_square_t *app, FILE *in, FILE *out)
{
  int i, j;
  mpz_t a[3][3], gcd;
  ssize_t read;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  mpz_init (gcd);
  char* line = NULL;
  size_t len = 0;
  while (!feof (in))
    {
      read = app->read_square (in, &a, &line, &len);
      if (read == -1)
        break;

      square_get_gcd (&a, &gcd);
      for (int i = 0; i < app->num_filters; i++)
        {
          if (mpz_cmp_ui (gcd, app->filter_count[i]) == 0)
            {
              app->display_square (a, out);
              break;
            }
        }

    }
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
  mpz_clear (gcd);
  if (line)
    free (line);
  return 0;
}

static int
show_gcd_square (struct fv_app_gcd_square_t *app,  FILE *in, FILE *out)
{
  int i, j;
  mpz_t a[3][3], gcd;
  mpz_init (gcd);
  ssize_t read;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  char* line = NULL;
  size_t len = 0;
  while (!feof (in))
    {
      read = app->read_square (in, &a, &line, &len);
      if (read == -1)
        break;

      square_get_gcd (&a, &gcd);
      display_textual_number (&gcd, stdout);

    }
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
  mpz_clear (gcd);
  if (line)
    free (line);
  return 0;
}

int
fituvalu_gcd_square (struct fv_app_gcd_square_t *app,  FILE *in, FILE *out)
{
  if (app->num_filters)
    return filter_gcd_square (app, in, out);
  else
    return show_gcd_square (app, in, out);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "filter", 'f', "GCD", 0, "Show the magic squares that have this greatest common divisor"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares from the standard input, and show the greatest common divisor.\vThe nine values must be separated by commas and terminated by a newline. --out-binary is only used with --filter.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_gcd_square_t app;
  memset (&app, 0, sizeof (app));
  app.read_square = read_square_from_stream;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_gcd_square (&app, stdin, stdout);
}
