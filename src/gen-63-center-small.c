/* Copyright (C) 2019, 2020, 2023, 2024 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <argp.h>
#include <gmp.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_gen_63_center_t
{
  FILE *infile;
  int out_binary;
  int num_columns;
  unsigned long long int *divisors;
  int num_divisors;
  unsigned long long int *known;
  int num_known;
  int minimum_matching_divisors;
  int check_known_multiples;
  int threads;
  FILE *out;
  int quicker;
  int all;
  int in_binary;
  int factor;
  int high_prime;
  int low_prime;
  int fast;
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static int
is_known_multiple (struct fv_app_gen_63_center_t *app, unsigned long long *num)
{
  for (int i = 0; i < app->num_known; i++)
    {
      //precisely matching knowns go through
      if (*num <= app->known[i])
        return 0;
      if (*num % app->known[i] == 0)
        return 1;
    }
  return 0;
}

static int
prime_is_fournminusone (unsigned long long i)
{
  return (i - 1) % 4 == 0;
}


static long long
liner (struct fv_app_gen_63_center_t *app, FILE *stream)
{
  ssize_t fnread, nread;
  char *line = NULL;
  size_t len = 0;
  int count = 0;
  unsigned long long num;
  int high_prime = 0;
  int low_prime_exceeded = 0;
  unsigned long long root = 0;
  int fast_passed = 0;
  int all_4n1 = 1;
  if (app->threads > 1)
    pthread_mutex_lock (&read_lock);
  fnread = getdelim (&line, &len, ':', stream);
  if (fnread == -1)
    {
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);
      return -1;
    }

  char *end = NULL;
  num = strtoull (line, &end, 10);
  if (end == NULL || *end != ':')
    return -1;
  nread = getline (&line, &len, stream);
  if (app->threads > 1)
    pthread_mutex_unlock (&read_lock);
  if (nread == -1)
    return -1;

    {
      if (app->high_prime || app->low_prime)
        root = sqrtl (num);
      int tw, m24, m3, imod10 = num % 10;
      tw = imod10 == 5 && num % 25 != 0;
      if (!tw)
        m24 = num % 24 != 1;
      if (!tw && !m24)
        m3 = num % 3 == 0;
      if (!tw && !m24 && !m3)
        {
          unsigned long long prev = 0;
          char *s1 = NULL;
          size_t s1len = 0;

          FILE *fp = fmemopen (line + 1, strlen (line), "r");
          while ((nread = getdelim (&s1, &s1len, ' ', fp)) != -1) 
            {
              unsigned long long p = strtoull (s1, &end, 10);
              if (prev != p)
                {
                  if (prime_is_fournminusone (p))
                    {
                      if (app->high_prime)
                        {
                          if (p > root || p > app->divisors[app->num_divisors-1])
                            high_prime = 1;
                        }
                      if (app->low_prime)
                        {
                          if (p > root || p > app->divisors[app->num_divisors-1])
                            {
                              low_prime_exceeded = 1;
                            }
                        }
                      count++;
                      if (app->fast)
                        {
                          if (count == app->minimum_matching_divisors)
                            {
                              if (p <= 1800)
                                fast_passed = 1;
                            }
                        }
                      /*
                      else
                        {
                          if (count >= app->minimum_matching_divisors)
                            break;
                        }
                        */
                    }
                  else // a non pythagorean prime, all bets are OFF kimosabe
                    {
                      all_4n1 = 0;
                      count = 0;
                      break;
                    }
                }

              size_t mlen = strlen (s1);
              if (mlen > 0)
                {
                  if (s1[mlen-1] == '\n')
                    break;
                }
              prev = p;
            }
          free (s1);
          fclose (fp);
        }
    }
  free (line);

  if (app->fast)
    return count >= app->minimum_matching_divisors && fast_passed && all_4n1 ? num : 0;
  else if (app->high_prime)
    return count >= app->minimum_matching_divisors && high_prime && all_4n1 ? num : 0;
  else if (app->low_prime)
    return count >= app->minimum_matching_divisors && !low_prime_exceeded && all_4n1 ? num : 0;
  else
    return count >= app->minimum_matching_divisors && all_4n1 ? num : 0;
}

static void*
process_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_gen_63_center_t *app =
    (struct fv_app_gen_63_center_t *) param->data;
  if (app->factor)
    {
      while (1)
        {
          long long retval = liner (app, app->infile);
          if (retval > 0)
            {
              pthread_mutex_lock (&display_lock);
              if (app->out_binary)
                fwrite (&retval, sizeof (retval), 1, app->out);
              else
                fprintf (app->out, "%llu\n", retval);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
          else if (retval == -1)
            break;
        }
      return NULL;
    }
  ssize_t read;
  char *line = NULL;
  size_t len = 0;
  char *end = NULL;
  unsigned long long int i, k, limit;
  int count = 0;
  while (1)
    {
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);
      if (app->in_binary)
        {
          read = fread (&i, sizeof (i), 1, app->infile);
          if (read == 0)
            read = -1;
        }
      else
        read = fv_getline (&line, &len, app->infile);
      if (read == -1)
        {
          if (app->threads > 1)
            pthread_mutex_unlock (&read_lock);
          break;
        }
      if (!app->in_binary)
        {
          end = NULL;
          unsigned long long int ii = strtoull (line, &end, 10);
          i = ii;
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);
      if (!app->in_binary)
        {
          if (end == NULL || *end != '\n')
            continue;
        }
      if (i % 24 != 1)
        continue;
      if (i % 3 == 0)
        continue;
      int imod10 = i % 10;
      if (imod10 == 3 || imod10 == 7)
        continue;
      else if (imod10 == 5 && i % 25 != 0)
        continue;
      if (small_is_square (i))
        continue;
      int start = 0;
      if (app->all)
        limit = i / 5;
      else
        limit = sqrtl (i);
      count = 0;
      int next = 0;
      k = i;
      for (int j = start; j < app->num_divisors; j++)
        {
          if (app->quicker && j > 2247) // a way to short-circuit some probably useless checks
            break;
          if (app->divisors[j] > limit)
            break;
          if (k % app->divisors[j] == 0)
            {
              if (!prime_is_fournminusone (app->divisors[j]))
                {
                  //our center value can only have pythagorean primes
                  next = 1;
                  break;
                }
              k /= app->divisors[j];
              count++;
              if (count >= app->minimum_matching_divisors)
                break;
              if (k <= app->divisors[j])
                break;
            }
        }
      if (next)
        {
          next = 0;
          continue;
        }
      if (count < app->minimum_matching_divisors)
        continue;
        
      if (!is_known_multiple (app, &i) || !app->check_known_multiples)
        {
          pthread_mutex_lock (&display_lock);
          if (app->out_binary)
            fwrite (&i, sizeof (i), 1, app->out);
          else
            {
              if (app->in_binary)
                fprintf (app->out, "%llu\n", i);
              else
                fprintf (app->out, "%s", line);
            }
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);
        }
    }
  if (line)
    free (line);
  return NULL;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_gen_63_center_t *app = (struct fv_app_gen_63_center_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'F':
      app->fast = 1;
      app->factor = 1;
      break;
    case 'S':
      app->low_prime = 1;
      app->factor = 1;
      break;
    case 'H':
      app->high_prime = 1;
      app->factor = 1;
      break;
    case 'f':
      app->factor = 1;
      break;
    case 'a':
      app->all = 1;
      break;
    case 'k':
      app->check_known_multiples = 1;
      break;
    case 'q':
      app->quicker = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->out_binary = 1;
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case 'm':
        {
          long long n = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0' || n <= 0)
            argp_error (state, "invalid value `%s' for --match option", arg);
          app->minimum_matching_divisors = n;
        }
      break;
    case ARGP_KEY_ARG:
      app->infile = fopen (arg, "r");
      if (app->infile == NULL)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

extern char *prime_numbers[];
static void
load_primes (struct fv_app_gen_63_center_t *app)
{
  unsigned long long int num;
  char **n = prime_numbers, *end = NULL;
  while (*n != 0)
    {
      num = strtoull (*n, &end, 10);
      if (num != 2 && num != 3)
        {
          app->divisors =
            realloc (app->divisors, (app->num_divisors + 1) *
                     sizeof (unsigned long long int));

          app->divisors[app->num_divisors] = num;
          app->num_divisors++;
        }
      *n++;
    }
}

extern char *known_63_centers[];
static void
load_known_centers (struct fv_app_gen_63_center_t *app)
{
  unsigned long long int num;
  char **n = known_63_centers, *end = NULL;
  while (*n != 0)
    {
      num = strtoull (*n, &end, 10);
      if (num != 2 && num != 3)
        {
          app->known =
            realloc (app->known, (app->num_known + 1) *
                     sizeof (unsigned long long int));

          app->known[app->num_known] = num;
          app->num_known++;
        }
      *n++;
    }
}

int
fituvalu_gen_63_center (struct fv_app_gen_63_center_t *app)
{
  load_primes (app);
  load_known_centers (app);
  run_threads (app, app->threads, process_record);
  return 0;
}

static struct argp_option
options[] =
{
  { "out-binary", 'o', 0, 0, "Output raw unsigned long longs instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "match", 'm', "NUM", 0, "Candidate must have NUM divisors (Default 3)"},
  { "quicker", 'q', 0, OPTION_HIDDEN, "Short-circuit a bunch of prime division checks"},
  { "all", 'a', 0, OPTION_HIDDEN, "Search all primes up to i / 2"},
  { "check-known", 'k', 0, OPTION_HIDDEN, "Check for known multiples"},
  { "factor", 'f', 0, 0, "Input is from GNU factor"},
  { "factor-high-primes", 'H', 0, OPTION_HIDDEN, "Input is from GNU factor but only show numbers that have a prime over sqrt(n) or 17 mil"},
  { "factor-small-primes", 'S', 0, OPTION_HIDDEN, "Input is from GNU factor but only show numbers that have a prime under sqrt(n) or 17 mil"},
  { "factor-fast", 'F', 0, OPTION_HIDDEN, "the third factor is less than 156k and the rest are 4n+1"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Generate center cells suitable for magic squares of type 6:3.  Odd numbers are passed in on the standard input or FILE and the suitable ones are shown.\vIncoming numbers are generated with `seq 1 2 10000`, etc.  The `search-63-small' program is suitable for passing this program's output to.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_gen_63_center_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.minimum_matching_divisors = 3;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_gen_63_center (&app);
}

