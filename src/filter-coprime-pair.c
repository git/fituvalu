/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct filter
{
  mpz_t num[2];
};

struct fv_app_coprime_pair_t
{
  int num_filters;
  struct filter *filter;
  void (*display_pair) (mpz_t *o, mpz_t *t, FILE *out);
  int (*read_pair) (FILE *, mpz_t *, char **, size_t *);
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  int invert;
};

int
coprime (mpz_t *progression)
{
  //progression must already be sorted
  mpz_t gcd;
  mpz_init (gcd);
  mpz_gcd (gcd, progression[0], progression[1]);
  int ret = mpz_cmp_ui (gcd, 1);
  mpz_clear (gcd);
  return ret == 0;
}

int
fituvalu_coprime_pair (struct fv_app_coprime_pair_t *app, FILE *in, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[2];

  for (int i = 0; i < 2; i++)
    mpz_init (a[i]);

  while (1)
    {
      read = app->read_pair (in, a, &line, &len);
      if (read == -1)
        break;
      if (coprime (a))
        app->display_pair (&a[0], &a[1], out);
    }

  for (int i = 0; i < 2; i++)
    mpz_clear (a[i]);

  if (line)
    free (line);
  return 0;
}

int
fituvalu_filter_coprime (struct fv_app_coprime_pair_t *app,  FILE *in, FILE *out)
{
  int i, j, k;
  char *line = NULL;
  size_t len = 0;
  mpz_t a[3][3];
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  while (1)
    {
      ssize_t read = app->read_square (in, &a, &line, &len);
      if (read == -1)
        return 0;
      int found = 0;
      int first = 0, second = 0;
      for (int k = 0; k < app->num_filters; k++)
        {
          for (i = 0; i < 3; i++)
            {
              for (j = 0; j < 3; j++)
                {
                  if (mpz_cmp (a[i][j], app->filter[k].num[0]) == 0)
                    first = 1;
                  if (mpz_cmp (a[i][j], app->filter[k].num[1]) == 0)
                    second = 1;
                }
            }
          if (first && second)
            found++;
        }
      if (found == app->num_filters)
        app->display_square (a, out);
    }

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
      
  free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_coprime_pair_t *app = (struct fv_app_coprime_pair_t *) state->input;
  switch (key)
    {
    case 'f':
        {
          char *num1 = NULL, *num2 = NULL;
          char *comma = strchr (arg, ',');
          if (!comma)
            argp_error (state, "invalid argument to --filter");
          num1 = arg;
          *comma = '\0';
          comma++;
          num2 = comma;
          mpz_t n[2];
          mpz_inits (n[0], n[1], NULL);
          if (mpz_set_str (n[0], num1, 10) == 0 &&
              mpz_set_str (n[1], num2, 10) == 0)
            {
              if (coprime (n))
                {
                  app->filter =
                    realloc (app->filter,
                             sizeof (struct filter) *
                             (app->num_filters + 1));
                  mpz_init_set (app->filter[app->num_filters].num[0], n[0]);
                  mpz_init_set (app->filter[app->num_filters].num[1], n[1]);
                  app->num_filters++;
                }
              else
                argp_error (state, "invalid argument to --filter");
            }
          else
            argp_error (state, "invalid argument to --filter");
          mpz_clears (n[0], n[1], NULL);
        }
      break;
    case 'v':
      app->invert = !app->invert;
      break;
    case 'i':
      app->read_pair = binary_read_two_numbers_from_stream;
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_pair = display_binary_two_record;
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "invert", 'v', 0, 0, "Show numbers that aren't prime"},
  { "filter", 'f', "NUM1,NUM2", 0, "Accept 3x3 magic squares and only show ones containing the given pair"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept pairs of numbers from the standard input, and if they are coprimes display them.\vThe two values must be separated by a comma and terminated by a newline.  A coprime is when the greatest common divisor between two numbers is 1.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_coprime_pair_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.read_square = read_square_from_stream;
  app.display_pair = display_two_record;
  app.read_pair = read_two_numbers_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (app.filter)
    return fituvalu_filter_coprime (&app, stdin, stdout);
  else
    return fituvalu_coprime_pair (&app, stdin, stdout);
}
