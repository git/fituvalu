/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <gmp.h>
#include <pthread.h>
#include "magicsquareutil.h"

  
#define OPT_GENERATE -644

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

enum generation_enum_t
{
  GENERATE_61,
  GENERATE_64,
  GENERATE_65,
  GENERATE_67,
  GENERATE_68,
  GENERATE_69,
  GENERATE_610,
  GENERATE_612,
  GENERATE_615B,
  GENERATE_616,
  GENERATE_MAX
};

struct fv_app_gen_six_sq_t
{
  FILE *infile;
  int numargs;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_number) (FILE *, mpz_t *, char **, size_t *);
  int threads;
  int generate[GENERATE_MAX];
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

struct rec {
  mpz_t p[3];
  mpz_t diff;
};

static void calculate_progressions (struct fv_app_gen_six_sq_t *app, mpz_t *progression, mpz_t *b, mpz_t (*s)[3][3], void (*func)(struct fv_app_gen_six_sq_t*, mpz_t *, mpz_t, mpz_t *b, mpz_t (*s)[3][3]))
{
  struct rec *progressions = NULL;
  int num_progressions = 0;

  mpz_t mn, twomn, m, n, ntwo, i, iroot, diff, limit, root;
  mpz_inits (mn, twomn, m, n, ntwo, i, iroot, diff, limit, root, NULL);
  mpz_cdiv_q_ui (limit, progression[1], 2);
  mpz_set_ui (i, 1);
  if (mpz_cmp (i, progression[1]) >= 0)
    return;
  while (1)
    {
      if (mpz_cmp (i, limit) > 0)
        break;

      mpz_sub (diff, progression[1], i);
      if (mpz_perfect_square_p (diff))
        {
          mpz_set (m, iroot);
          mpz_sqrt (n, diff);
          mpz_mul (mn, m, n);
          mpz_add (twomn, mn, mn);
          mpz_sub (progression[0], progression[1], twomn);
          mpz_add (progression[2], progression[1], twomn);

          func (app, progression, twomn, b, s); //calls handle_progression
        }
      if (mpz_cmp_ui (i, 1) == 0)
        {
          mpz_set_ui (i, 4);
          mpz_set_ui (iroot, 2);
          continue;
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }

  for (int j = 0; j < num_progressions; j++)
    {
      struct rec *rec = &progressions[j];
      mpz_clears (rec->p[0], rec->p[1], rec->p[2], rec->diff, NULL);
    }
  free (progressions);

  mpz_clears (mn, twomn, m, n, ntwo, i, iroot, diff, limit, root, NULL);
  return;
}

void
dump_square (void *data, mpz_t (*s)[3][3])
{
  struct fv_app_gen_six_sq_t *app = (struct fv_app_gen_six_sq_t *) data;
  pthread_mutex_lock (&display_lock);
  app->display_square (*s, app->out);
  //fflush (app->out);
  pthread_mutex_unlock (&display_lock);
}

static void
handle_progression (struct fv_app_gen_six_sq_t *app, mpz_t *progression, mpz_t diff, mpz_t *b, mpz_t (*s)[3][3])
{
  mpz_t twodiff;
  mpz_init (twodiff);
  mpz_add (twodiff, diff, diff);
  if (app->generate[GENERATE_64])
    search_64_3sq (progression, diff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_67])
    search_67_3sq (progression, diff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_69])
    search_69_3sq (progression, diff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_65])
    search_65_3sq (progression, diff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_61])
    search_61_3sq (progression, twodiff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_612])
    search_612_3sq (progression, twodiff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_616])
    search_616_3sq (progression, twodiff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_68])
    search_68_3sq (progression, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_610])
    search_610_3sq (progression, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_615B])
    search_615_3sq (progression, b, s, 1, dump_square, app);
  mpz_clear (twodiff);
  return;
}

static void*
process_perfect_square (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_gen_six_sq_t *app =
    (struct fv_app_gen_six_sq_t *) param->data;
  mpz_t progression[3], b[2], s[3][3], r;

  char *line = NULL;
  size_t len = 0;
  mpz_init (r);
  for (int i = 0; i < 3; i++)
    mpz_init (progression[i]);
  for (int j = 0; j < 3; j++)
    for (int k = 0; k < 3; k++)
      mpz_init (s[j][k]);
  for (int j = 0; j < 2; j++)
    mpz_init (b[j]);
  while (1)
    {
      //go get the next perfect square to work on (app->num)
      pthread_mutex_lock (&read_lock);
      if (app->read_number (app->infile, &progression[1], &line, &len) == -1)
        {
          pthread_mutex_unlock (&read_lock);
          break;
        }

      //display_textual_number (&progression[1], stdout);
      pthread_mutex_unlock (&read_lock);

      //get all of the three square progressions, and search for magic squares
      //b is space for the 2 values we're cycling through later on.
      //it equates to Y1 and Y2 in the --help.
      calculate_progressions (app, progression, b, &s, handle_progression);
    }
  free (line);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  for (int i = 0; i < 2; i++)
    mpz_clear (b[i]);

  for (int i = 0; i < 3; i++)
    mpz_clear (progression[i]);

  mpz_clear (r);
  return NULL;
}

int
fituvalu_gen_six_sq (struct fv_app_gen_six_sq_t *app)
{
  run_threads (app, app->threads, process_perfect_square);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "generate", OPT_GENERATE, "TYPE[,TYPE]", 0, "Only search for magic squares of the given TYPEs"},
  { 0 }
};

static int
parse_generation_string (struct argp_state *state, char *str)
{
  struct fv_app_gen_six_sq_t *app = (struct fv_app_gen_six_sq_t *) state->input;
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;
  /* mixing it up by using fmemopen */
  FILE *fp = fmemopen (str, strlen (str) + 1, "r");
  while ((nread = getdelim (&line, &len, ',', fp)) != -1)
    {
      size_t n = strspn (" \t", line);
      char *s = strdup (&line[n]);
      if (s[strlen (s) - 1] == ',')
        s[strlen (s) - 1] = '\0';
      if (strcmp (s, "6:1") == 0)
        app->generate[GENERATE_61] = 1;
      else if (strcmp (s, "6:4") == 0)
        app->generate[GENERATE_64] = 1;
      else if (strcmp (s, "6:5") == 0)
        app->generate[GENERATE_65] = 1;
      else if (strcmp (s, "6:7") == 0)
        app->generate[GENERATE_67] = 1;
      else if (strcmp (s, "6:8") == 0)
        app->generate[GENERATE_68] = 1;
      else if (strcmp (s, "6:9") == 0)
        app->generate[GENERATE_69] = 1;
      else if (strcmp (s, "6:10") == 0)
        app->generate[GENERATE_610] = 1;
      else if (strcmp (s, "6:12") == 0)
        app->generate[GENERATE_612] = 1;
      else if (strcmp (s, "6:15") == 0)
        app->generate[GENERATE_615B] = 1;
      else if (strcmp (s, "6:16") == 0)
        app->generate[GENERATE_616] = 1;
      else
        argp_error (state, "invalid square type '%s'", s);
      free (s);
    }
  fclose (fp);
  return 1;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_gen_six_sq_t *app = (struct fv_app_gen_six_sq_t *) state->input;
  switch (key)
    {
    case OPT_GENERATE:
      parse_generation_string (state, arg);
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case 'i':
      app->read_number = binary_read_one_number_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->numargs > 1)
        argp_error (state, "too many arguments.");
      app->numargs++;
      if (strcmp (arg, "-") == 0)
        app->infile = stdin;
      else
        app->infile = fopen (arg, "r");
      if (app->infile == NULL)
        argp_error (state, "could not opening `%s' for reading");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
        {
          int generate[GENERATE_MAX];
          memset (generate, 0, sizeof (generate));
          if (memcmp (generate, app->generate, sizeof (generate)) == 0)
            {
              for (int i = 0; i < GENERATE_MAX; i++)
                app->generate[i] = 1;
            }
        }
      break;
    }
  return 0;
}

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Generate 3x3 magic squares with 6 perfect squares or more by generating 3sq progressions from a given set of squares in FILE.  For each progression of three squares we search for magic squares of the following types: 6:1, 6:4, 6:5, 6:7, 6:8, 6:9, 6:10, 6:12, 6:15, and 6:16.\vWe also inadvertently find magic squares of the forms: 6:2, 6:6, 6:11, 6:13, and 6:14.  Suitable values for --generate are: 6:1, 6:4, 6:5, 6:7, 6:8, 6:9, 6:10, 6:12, 6:15, and 6:16.  When FILE is not specified or it is `-', the squares are read from the standard input.", 0
};

int
main (int argc, char **argv)
{
  struct fv_app_gen_six_sq_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.read_number = read_one_number_from_stream;
  app.out = stdout;
  app.infile = stdin;
  app.threads = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);

  return fituvalu_gen_six_sq (&app);
}
