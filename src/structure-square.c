/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

struct filter_rec
{
  int idx;
  double min;
  double max;
};
struct fv_app_find_structure_type_t
{
  int twobytwo;
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int dist;
  int minmax;
  int perc;
  int num_filters;
  struct filter_rec *filters;
  FILE *out;
  int percentage_histogram;
};

static void
show_minmax (struct fv_app_find_structure_type_t *app, mpz_t s[3][3])
{
  mpz_t mi, ma, sum;
  mpz_inits (mi, ma, sum, NULL);
  magic_square_get_magic_number (s, &sum);
  magic_square_get_lowest_number (s, &mi);
  magic_square_get_largest_number (s, &ma);
  mpf_t mif, maf, suf;
  mpf_inits (mif, maf, suf, NULL);
  mpf_set_z (suf, sum);
  mpf_set_z (mif, mi);
  mpf_set_z (maf, ma);
  mpf_div (maf, maf, suf);
  mpf_div (mif, mif, suf);
  display_textual_number_no_newline (sum, app->out);
  fprintf (app->out, ", ");
  display_floating_point_number (mif, app->out);
  fprintf (app->out, ", ");
  display_floating_point_number (maf, app->out);
  fprintf (app->out, ",\n");
  mpf_clears (mif, maf, suf, NULL);
  mpz_clears (mi, ma, sum, NULL);
  return;
}

static void
show_twobytwo (struct fv_app_find_structure_type_t *app, mpz_t s[3][3])
{
  mpz_t sum;
  mpz_init (sum);
  mpz_add (sum, s[0][0], s[0][1]);
  mpz_add (sum, sum, s[1][0]);
  mpz_add (sum, sum, s[1][1]);
  display_textual_number_no_newline (sum, app->out);
  fprintf (app->out, ", ");
  mpz_add (sum, s[0][1], s[0][2]);
  mpz_add (sum, sum, s[1][1]);
  mpz_add (sum, sum, s[1][2]);
  display_textual_number_no_newline (sum, app->out);
  fprintf (app->out, ", ");
  mpz_add (sum, s[1][0], s[1][1]);
  mpz_add (sum, sum, s[2][0]);
  mpz_add (sum, sum, s[2][1]);
  display_textual_number_no_newline (sum, app->out);
  fprintf (app->out, ", ");
  mpz_add (sum, s[1][1], s[1][2]);
  mpz_add (sum, sum, s[2][1]);
  mpz_add (sum, sum, s[2][2]);
  display_textual_number_no_newline (sum, app->out);
  fprintf (app->out, ",\n");
  mpz_clear (sum);
}

static void
swap_mpz (mpz_t *src, mpz_t *dst)
{
  mpz_t sw;
  mpz_init (sw);
  mpz_set (sw, *src);
  mpz_set (*src, *dst);
  mpz_set (*dst, sw);
  mpz_clear (sw);
}

static void
get_structure_type (struct fv_app_find_structure_type_t *app, mpz_t s[3][3], mpz_t (*n)[6][3])
{

  mpz_set ((*n)[0][0], s[0][0]);
  mpz_set ((*n)[0][1], s[1][1]);
  mpz_set ((*n)[0][2], s[2][2]);
  if (mpz_cmp ((*n)[0][0], (*n)[0][2]) > 0)
    swap_mpz (&(*n)[0][0], &(*n)[0][2]);

  mpz_set ((*n)[1][0], s[0][2]);
  mpz_set ((*n)[1][1], s[1][1]);
  mpz_set ((*n)[1][2], s[2][0]);
  if (mpz_cmp ((*n)[1][0], (*n)[1][2]) > 0)
    swap_mpz (&(*n)[1][0], &(*n)[1][2]);

  mpz_set ((*n)[2][0], s[2][1]);
  mpz_set ((*n)[2][1], s[0][0]);
  mpz_set ((*n)[2][2], s[1][2]);
  if (mpz_cmp ((*n)[2][0], (*n)[2][2]) > 0)
    swap_mpz (&(*n)[2][0], &(*n)[2][2]);

  mpz_set ((*n)[3][0], s[0][1]);
  mpz_set ((*n)[3][1], s[2][2]);
  mpz_set ((*n)[3][2], s[1][0]);
  if (mpz_cmp ((*n)[3][0], (*n)[3][2]) > 0)
    swap_mpz (&(*n)[3][0], &(*n)[3][2]);

  mpz_set ((*n)[4][0], s[0][1]);
  mpz_set ((*n)[4][1], s[2][0]);
  mpz_set ((*n)[4][2], s[1][2]);
  if (mpz_cmp ((*n)[4][0], (*n)[4][2]) > 0)
    swap_mpz (&(*n)[4][0], &(*n)[4][2]);

  mpz_set ((*n)[5][0], s[1][0]);
  mpz_set ((*n)[5][1], s[0][2]);
  mpz_set ((*n)[5][2], s[2][1]);
  if (mpz_cmp ((*n)[5][0], (*n)[5][2]) > 0)
    swap_mpz (&(*n)[5][0], &(*n)[5][2]);
}

static void
show_structure_type (struct fv_app_find_structure_type_t *app, mpz_t s[3][3])
{
  mpz_t n[6][3];
  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (n[i][j]);

  get_structure_type (app, s, &n);

  mpz_t congruum;
  mpz_init (congruum);
  display_textual_number_no_newline (n[0][1], app->out);
  fprintf (app->out, ", ");
  mpz_sub (congruum, n[0][1], n[0][0]);
  display_textual_number_no_newline (congruum, app->out);
  fprintf (app->out, ", ");
  mpz_sub (congruum, n[1][1], n[1][0]);
  display_textual_number_no_newline (congruum, app->out);
  fprintf (app->out, ",\n");
  mpz_clear (congruum);
  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (n[i][j]);
}

static void
show_structure_type_percentage (struct fv_app_find_structure_type_t *app, mpz_t s[3][3])
{
  mpz_t n[6][3];
  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (n[i][j]);

  get_structure_type (app, s, &n);

  for (int i = 0; i < 6; i++)
    {
      if (mpz_perfect_square_p (n[i][0]) &&
          mpz_perfect_square_p (n[i][1]) &&
          mpz_perfect_square_p (n[i][2]))
        {
          mpf_t a, b, r;
          mpf_inits (a, b, r, NULL);
          mpf_set_z (a, n[i][1]);
          mpf_set_z (b, n[i][0]);
          display_textual_number_no_newline (n[i][1], app->out);
          fprintf (app->out, ", ");
          mpf_div (r, b, a);
          display_floating_point_number (r, app->out);
          fprintf (app->out, ",\n");
          mpf_clears (a, b, r, NULL);
        }
    }
  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (n[i][j]);
}


static int
is_right_side_up (mpz_t (*s)[3][3])
{
  int ret = 0;
  mpz_t diff1, diff2, diff3;
  mpz_inits (diff1, diff2, diff3, NULL);
  mpz_sub (diff1, (*s)[1][1], (*s)[0][0]);
  mpz_sub (diff2, (*s)[1][0], (*s)[0][2]);
  mpz_sub (diff3, (*s)[0][1], (*s)[2][0]);
  if (mpz_cmp (diff1, diff2) == 0 &&
      mpz_cmp (diff2, diff3) == 0 &&
      mpz_cmp_ui (diff1, 0) > 0)
    ret = 1;
  mpz_clears (diff1, diff2, diff3, NULL);
  return ret;
}

static void
rotate_right_side_up (mpz_t (*s)[3][3])
{
  for (int l = 0; l < 4; l++)
    {
      rotate_square (s);
      if (is_right_side_up (s))
        return;
    }
  flip_square_horizontally (s);
  for (int l = 0; l < 4; l++)
    {
      rotate_square (s);
      if (is_right_side_up (s))
        return;
    }
  flip_square_vertically (s);
  for (int l = 0; l < 4; l++)
    {
      rotate_square (s);
      if (is_right_side_up (s))
        return;
    }
}

static int
square_matcher (int ar[5][2], int sq[3][3], mpz_t (*s)[3][3], int mul)
{
  int ret = 0;
  for (int i = 0; i < 5; i++)
    if (!sq[ar[i][0]][ar[i][1]])
      return ret;
  ret = 1;

  mpz_t diff, diff2;
  mpz_inits (diff, diff2, NULL);

  mpz_sub (diff, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
  mpz_sub (diff2, (*s)[ar[2][0]][ar[2][1]], (*s)[ar[1][0]][ar[1][1]]);
  if (mpz_cmp (diff, diff2) != 0)
    ret = 0;
  else
    {
      mpz_sub (diff, (*s)[ar[3][0]][ar[3][1]], (*s)[ar[4][0]][ar[4][1]]);

      mpz_mul_ui (diff2, diff2, mul); //was dist
      if (mpz_cmp (diff2, diff) != 0)
        ret = 0;
    }
  mpz_clears (diff, diff2, NULL);
  return ret;
}

static void
dump_closeness (FILE *out, mpz_t dist, mpz_t num)
{
//  display_textual_number_no_newline (num, out);
 // fprintf (out, ", ");
  mpf_t n, d, r;
  mpf_inits (n, d, r, NULL);
  mpf_set_z (n, num);
  mpf_set_z (d, dist);
  mpf_mul (d, d, d);
  mpf_div (r, n, d);
  //mpz_t dd;
  //mpz_init (dd);
  //mpz_mul (dd, dist, dist);
  //display_textual_number_no_newline (dd, out);
  //mpz_clear (dd);
  //fprintf (out, ", ");
  display_floating_point_number (r, out);
  //fprintf (out, "\n");
  mpf_clears (n, d, r, NULL);
}

static int
check (int ar[5][2], int mul, int sq[3][3], mpz_t (*s)[3][3], mpz_t *result)
{
  int found = 0;
  if (square_matcher (ar, sq, s, mul))
    {
      found = 1;
      mpz_set ((*result), (*s)[ar[3][0]][ar[3][1]]);
    }
  return found;
}

static int
get_dist_type (mpz_t (*s)[3][3], int sq[3][3], int *d, int *c, mpz_t *dist, mpz_t *result)
{
    {
      int ar[5][2] = {{2,1}, {1,1}, {0,1}, {2,2}, {0,2}}; //d1c1
      if (check (ar, 1, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 1;
          *c = 1;
          return 1;
        }
    }
    {
      int ar[5][2] = {{2,1}, {1,1}, {0,1}, {2,0}, {0,0}}; // d1c2
      if (check (ar, 1, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 1;
          *c = 2;
          return 1;
        }
    }
    {
      int ar[5][2] = {{2,1}, {0,2}, {1,0}, {0,1}, {2,0}}; // d1c3
      if (check (ar, 1, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 1;
          *c = 3;
          return 1;
        }
      
    }
    {
      int ar[5][2] = {{1,2}, {2,0}, {0,1}, {0,2}, {2,1}}; // d1c4
      if (check (ar, 1, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 1;
          *c = 4;
          return 1;
        }
    }
    {
      int ar[5][2] = {{1,2}, {2,0}, {0,1}, {1,1}, {0,0}}; // d1c5
      if (check (ar, 1, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 1;
          *c = 5;
          return 1;
        }
    }
    {
      int ar[5][2] = {{1,2}, {2,0}, {0,1}, {2,2}, {1,1}}; // d1c6
      if (check (ar, 1, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 1;
          *c = 6;
          return 1;
        }
    }
    {
      int ar[5][2] = {{0,0}, {1,1}, {2,2}, {0,2}, {2,1}}; // d1c7
      if (check (ar, 1, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 1;
          *c = 7;
          return 1;
        }
    }
    {
      int ar[5][2] = {{0,0}, {1,1}, {2,2}, {0,1}, {2,0}}; // d1c8
      if (check (ar, 1, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 1;
          *c = 8;
          return 1;
        }
    }

    {
      int ar[5][2] = {{0,0}, {1,1}, {2,2}, {0,1}, {1,2}}; // d2c1
      if (check (ar, 2, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 2;
          *c = 1;
          return 1;
        }
    }
    {
      int ar[5][2] = {{0,0}, {1,1}, {2,2}, {1,0}, {2,1}}; // d2c2
      if (check (ar, 2, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 2;
          *c = 2;
          return 1;
        }
    }
    {
      int ar[5][2] = {{2,1}, {0,2}, {1,0}, {0,1}, {1,2}}; // d2c3
      if (check (ar, 2, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 2;
          *c = 3;
          return 1;
        }
    }
    {
      int ar[5][2] = {{1,0}, {2,2}, {0,1}, {1,2}, {2,1}}; // d2c4
      if (check (ar, 2, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 2;
          *c = 4;
          return 1;
        }
    }
    {
      int ar[5][2] = {{2,1}, {0,2}, {1,0}, {2,2}, {0,0}}; // d2c5
      if (check (ar, 2, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 2;
          *c = 5;
          return 1;
        }
    }
    {
      int ar[5][2] = {{1,2}, {2,0}, {0,1}, {2,2}, {0,0}}; // d2c6
      if (check (ar, 2, sq, s, result))
        {
          mpz_sub (*dist, (*s)[ar[1][0]][ar[1][1]], (*s)[ar[0][0]][ar[0][1]]);
          *d = 2;
          *c = 6;
          return 1;
        }
    }
  return 0;
}

static void
dump_null_record (FILE *out)
{
  fprintf (out, "d0c0,\n");
  return;
}

static void
show_dist (struct fv_app_find_structure_type_t *app, mpz_t (*s)[3][3])
{
  int sq[3][3];
  int num_squares = 0;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      if (mpz_perfect_square_p ((*s)[i][j]))
        num_squares++;

  if (num_squares < 5)
    {
      dump_null_record (app->out);
      return;
    }
  if (!is_magic_square (*s, 1))
    {
      dump_null_record (app->out);
      return;
    }

  mpz_t result, dist;
  mpz_inits (result, dist, NULL);

  int results[2][8];
  memset (results, 0, sizeof (results));
  for (int q = 0; q < 12; q++)
    {
      switch (q)
        {
        case 0: break;
        case 1: rotate_square (s); break;
        case 2: rotate_square (s); break;
        case 3: rotate_square (s); break;
        case 4: rotate_square (s); flip_square_vertically (s); break;
        case 5: rotate_square (s); break;
        case 6: rotate_square (s); break;
        case 7: rotate_square (s); break;
        case 8: rotate_square (s); flip_square_horizontally (s); break;
        case 9: rotate_square (s); break;
        case 10: rotate_square (s); break;
        case 11: rotate_square (s); break;
        }
  
      memset (sq, 0, sizeof (sq));
      for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
          sq[i][j] = mpz_perfect_square_p ((*s)[i][j]);
  
      int d, c;
      if (get_dist_type (s, sq, &d, &c, &dist, &result))
        {
          if (results[d-1][c-1] == 0)
            {
              dump_closeness (app->out, dist, result);
              fprintf (app->out, ", d%dc%d, ", d, c);
            }
          results[d-1][c-1]++;
        }
    }
  mpz_clears (result, dist, NULL);

  int num = 0;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < 8; j++)
      num += results[i][j];

  if (num == 0)
    {
      dump_null_record (app->out);
      return;
    }
  /*
  for (int i = 0; i < 8; i++)
    if (results[0][i])
      fprintf (out, "d1c%d, ", i+1);
  for (int i = 0; i < 6; i++)
    if (results[1][i])
      fprintf (out, "d2c%d, ", i+1);
  */
  fprintf (app->out, "\n");

  return;
}

static int
filter_by_percentage (struct fv_app_find_structure_type_t *app, struct filter_rec *rec, mpz_t s[3][3])
{
  int ret = 0;
  mpz_t n[6][3];
  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (n[i][j]);

  get_structure_type (app, s, &n);

  int count = 0;
  for (int i = 0; i < 6; i++)
    {
      if (mpz_perfect_square_p (n[i][0]) &&
          mpz_perfect_square_p (n[i][1]) &&
          mpz_perfect_square_p (n[i][2]))
        {
          mpf_t a, b, r;
          mpf_inits (a, b, r, NULL);
          mpf_set_z (a, n[i][1]);
          mpf_set_z (b, n[i][0]);
          mpf_div (r, b, a);
          count++;
          //is r between MIN and MAX?
          if (count == rec->idx)
            {
              if (mpf_cmp_d (r, rec->min) >= 0 &&
                  mpf_cmp_d (r, rec->max) < 0)
                ret = 1;
            }
          mpf_clears (a, b, r, NULL);
        }
    }
  for (int i = 0; i < 6; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (n[i][j]);
  return ret;
}

int
fituvalu_find_structure_type (struct fv_app_find_structure_type_t *app, FILE *in)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int histogram[2][10];
  memset (histogram, 0, sizeof (histogram));

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  while (1)
    {
      read = app->read_square (in, &s, &line, &len);
      if (read == -1)
        break;
      if (is_magic_square (s, 1))
        {
          if (app->perc)
            show_structure_type_percentage (app, s);
          else if (app->num_filters)
            {
              int found = 0;
              for (int i = 0; i < app->num_filters; i++)
                {
                  if (filter_by_percentage (app, &app->filters[i], s))
                    {
                      found = 1;
                      break;
                    }
                }
              if (found)
                app->display_square (s, app->out);
            }
          else if (app->percentage_histogram)
            {
              struct filter_rec rec;

              for (int i = 0; i < 2; i++)
                {
                  int count = 0;
                  for (double j = 0; j < 0.9; j += 0.1, count++)
                    {
                      rec.idx = i + 1;
                      rec.min = j;
                      rec.max = j + 0.1;
                      if (filter_by_percentage (app, &rec, s))
                        histogram[i][count]++;
                    }
                }
            }
          else if (app->minmax)
            show_minmax (app, s);
          else if (app->twobytwo)
            show_twobytwo (app, s);
          else if (app->dist)
            show_dist (app, &s);
          else
            show_structure_type (app, s);
        }
    }

  if (app->percentage_histogram)
    {
      int count = 0;
      for (double j = 0; j < 0.9; j += 0.1, count++)
        printf ("%d, %2.1lf, %d\n", 1, j + 0.1, histogram[0][count]);
      count = 0;
      for (double j = 0; j < 0.9; j += 0.1, count++)
        printf ("%d, %2.1lf, %d\n", 2, j + 0.1, histogram[1][count]);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  if (line)
    free (line);
  is_magic_square_fini ();
  return 0;
}

static int
parse_filter_rec (char *s, struct filter_rec *rec)
{
  int retval = sscanf (s, "%d,%lf,%lf", &rec->idx, &rec->min, &rec->max);
  if (rec->idx != 1 && rec->idx != 2)
    return 0;
  if (rec->min < 0 || rec->min >= rec->max)
    return 0;
  if (rec->max < 0)
    return 0;
  if (rec->min > 1)
    return 0;
  if (rec->max > 1)
    return 0;
  return retval == 3;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_find_structure_type_t *app = (struct fv_app_find_structure_type_t *) state->input;
  switch (key)
    {
    case 'h':
      app->percentage_histogram = 1;
      break;
    case 'f':
        {
          struct filter_rec rec;
          if (parse_filter_rec (arg, &rec))
            {
              app->filters =
                realloc (app->filters,
                         sizeof (struct filter_rec) * (app->num_filters + 1));
              app->filters[app->num_filters] = rec;
              app->num_filters++;
            }
          else
            argp_error (state, "invalid filter '%s'", arg);
        }
      break;
    case 'p':
      app->perc = 1;
      break;
    case 'm':
      app->minmax = 1;
      break;
    case 'd':
      app->dist = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case '2':
      app->twobytwo = 1;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "twobytwo", '2', 0, OPTION_HIDDEN, "Show the sum of each 2x2 region"},
  { "dist", 'd', 0, OPTION_HIDDEN, "Show how far we had to iterate (for fives or better)"},
  { "min-max", 'm', 0, OPTION_HIDDEN, "Show min and max values as a percent of the magic number"},
  { "percentage", 'p', 0, 0, "Instead of showing the congrua show percentage"},
  { "filter", 'f', "1|2,MIN,MAX", 0, "Show magic squares that have a congruum percentage between MIN and MAX, on the 1st or 2nd congrua."},
  { "percentage-histogram", 'h', 0, 0, "Show a histogram of congrua percentages"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares from the standard input, and determine the structure: the congrua and the value of the center square.\vThe nine values must be separated by a comma and terminated by a newline.  Percentage refers to the lower value of the three square progression divided by the center value.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_find_structure_type_t app;
  memset (&app, 0, sizeof (app));
  app.read_square = read_square_from_stream;
  app.display_square = display_square_record;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  is_magic_square_init ();
  return fituvalu_find_structure_type (&app, stdin);
}
