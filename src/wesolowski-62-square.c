/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_wesolowski_62_square_t
{
  int num_args;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  mpz_t x;
  mpz_t y;
  mpz_t z;
  int odd;
  int xyz;
  int threads;
  FILE *infile;
  int from_stdin;
  int in_binary;
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_62_square (struct fv_app_wesolowski_62_square_t *app, mpz_t *x, mpz_t *y, mpz_t *z, mpz_t (*s)[3][3])
{
  // (xy-z)^2          | (xz+y)^2                    | x^2+(y^2*z^2)
  // (x + yz)^2        | (x^2 + y^2) * (z^2 + 1) / 2 | (xz - y)^2
  // (x^2 * z^2) + y^2 | (yz - x)^2                  | (xy + z)^2
  mpz_t xy, xz, yz, x2, y2, z2, p, q, sum_minus_middle;
  mpz_inits (xy, xz, yz, x2, y2, z2, p, q, sum_minus_middle, NULL);
  mpz_mul (x2, *x, *x);
  mpz_mul (y2, *y, *y);
  mpz_mul (z2, *z, *z);
  mpz_mul (xy, *x, *y);
  mpz_mul (xz, *x, *z);
  mpz_mul (yz, *y, *z);

  mpz_mul (p, y2, z2);
  mpz_add ((*s)[0][2], x2, p);

  mpz_mul (p, x2, z2);
  mpz_add ((*s)[2][0], y2, p);

  mpz_add (p, x2, y2);
  mpz_add_ui (q, z2, 1);

  mpz_mul ((*s)[1][1], p, q);
  mpz_cdiv_q_ui ((*s)[1][1], (*s)[1][1], 2);

  mpz_mul_ui (sum_minus_middle, (*s)[1][1], 2);

  mpz_sub ((*s)[1][2], xz, *y);
  mpz_mul ((*s)[1][2], (*s)[1][2], (*s)[1][2]);

  mpz_sub ((*s)[2][1], yz, *x);
  mpz_mul ((*s)[2][1], (*s)[2][1], (*s)[2][1]);

  mpz_add ((*s)[2][2], xy, *z);
  mpz_mul ((*s)[2][2], (*s)[2][2], (*s)[2][2]);

  mpz_sub ((*s)[0][0], sum_minus_middle, (*s)[2][2]);
  mpz_sub ((*s)[0][1], sum_minus_middle, (*s)[2][1]);
  mpz_sub ((*s)[1][0], sum_minus_middle, (*s)[1][2]);

  pthread_mutex_lock (&display_lock);
  app->display_square (*s, app->out);
  fflush (app->out);
  pthread_mutex_unlock (&display_lock);

  mpz_clears (xy, xz, yz, x2, y2, z2, p, q, sum_minus_middle, NULL);
}


static void*
process_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_wesolowski_62_square_t *app =
    (struct fv_app_wesolowski_62_square_t *) param->data;
  //x(n) = 3*x(n-1) + 2*z(n-1)
  //y(n) = x(n-1)
  //z(n) = x(n-1) + z(n-1)
  mpz_t x, y, z, twoz, threex;
  mpz_inits (x, y, z, twoz, threex, NULL);

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  while (1)
    {
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      mpz_mul_ui (twoz, app->z, 2);
      mpz_mul_ui (threex, app->x, 3);

      mpz_add (x, threex, twoz);
      mpz_set (y, app->x);
      mpz_add (z, app->x, app->z);

      mpz_set (app->x, x);
      mpz_set (app->y, y);
      mpz_set (app->z, z);

      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (app->xyz)
        {
          pthread_mutex_lock (&display_lock);
          display_threesq (x, y, z, app->out);
          pthread_mutex_unlock (&display_lock);
        }
      else
        generate_62_square (app, &x, &y, &z, &s);

      if (app->odd)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&read_lock);
          mpz_mul_ui (twoz, app->z, 2);
          mpz_mul_ui (threex, app->x, 3);

          mpz_add (x, threex, twoz);
          mpz_set (y, app->x);
          mpz_add (z, app->x, app->z);

          mpz_set (app->x, x);
          mpz_set (app->y, y);
          mpz_set (app->z, z);
          if (app->threads > 1)
            pthread_mutex_unlock (&read_lock);
        }
    }

  mpz_clears (x, y, z, twoz, threex, NULL);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  return NULL;
}

static void*
process_xyz_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_wesolowski_62_square_t *app =
    (struct fv_app_wesolowski_62_square_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  mpz_t x, y, z;
  mpz_inits (x, y, z, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &x, &y, &z, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &x, &y, &z, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (app->xyz)
        {
          pthread_mutex_lock (&display_lock);
          display_threesq (x, y, z, app->out);
          pthread_mutex_unlock (&display_lock);
        }
      else
        generate_62_square (app, &x, &y, &z, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (x, y, z, NULL);


  if (line)
    free (line);
  return NULL;
}
int
fituvalu_wesolowski_62_square (struct fv_app_wesolowski_62_square_t *app)
{
  if (app->from_stdin)
    run_threads (app, app->threads, process_xyz_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static int
calculate_xyz (struct fv_app_wesolowski_62_square_t *app, mpz_t n, mpz_t *x, mpz_t *y, mpz_t *z)
{
  mpf_t sqrt3, p, q, r, s, fx, fy, fz;
  mpf_inits (sqrt3, p, q, r, s, fx, fy, fz, NULL);
  mpf_set_ui (p, 3);
  mpf_sqrt (sqrt3, p);

  mpf_add_ui (p, sqrt3, 3);
  mpf_add_ui (q, sqrt3, 2);

  //x = ((3 + sqrt(3))*(2 + sqrt(3))^(n + 1) + (3 - sqrt(3))*(2 - sqrt(3))^(n + 1))/6,
 
  mpf_pow_ui (q, q, mpz_get_ui (n) + 1);
  mpf_mul (r, p, q);

  mpf_set_ui (p, 3);
  mpf_sub (q, p, sqrt3);
  mpf_set_ui (p, 2);
  mpf_sub (p, p, sqrt3);
  mpf_pow_ui (p, p, mpz_get_ui (n) + 1);
  mpf_mul (s, p, q);
  mpf_add (fx, r, s);
  mpf_div_ui (fx, fx, 6);

  //y = ((3 + sqrt(3))*(2 + sqrt(3))^n + (3 - sqrt(3))*(2 - sqrt(3))^n)/6,

  mpf_add_ui (p, sqrt3, 3);
  mpf_add_ui (q, sqrt3, 2);
  mpf_mul (r, p, q);
  mpf_pow_ui (r, r, mpz_get_ui (n));

  mpf_set_ui (p, 3);
  mpf_sub (q, p, sqrt3);
  mpf_set_ui (p, 2);
  mpf_sub (p, p, sqrt3);
  mpf_mul (s, p, q);
  mpf_pow_ui (s, s, mpz_get_ui (n));
  mpf_add (fy, r, s);
  mpf_div_ui (fy, fy, 6);

  //z = 2^n*((1 + sqrt(3)/2)^(n + 1) - (1 - sqrt(3)/2)^(n + 1))/sqrt(3).

  mpf_div_ui (p, sqrt3, 2);
  mpf_add_ui (p, p, 1);
  mpf_pow_ui (p, p, mpz_get_ui (n) + 1);

  mpf_div_ui (q, sqrt3, 2);
  mpf_set_ui (r, 1);
  mpf_sub (s, r, q);
  mpf_pow_ui (s, s, mpz_get_ui (n) + 1);

  mpf_sub (fz, p, s);

  mpf_set_ui (p, 2);
  mpf_pow_ui (q, p, mpz_get_ui (n));
  mpf_mul (fz, fz, q);
  mpf_div (fz, fz, sqrt3);

  mpf_ceil (fx, fx);
  mpf_ceil (fy, fy);
  mpf_ceil (fz, fz);

  mpz_set_f (*x, fx);
  mpz_set_f (*y, fy);
  mpz_set_f (*z, fz);
  mpf_clears (sqrt3, p, q, r, s, fx, fy, fz, NULL);
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_wesolowski_62_square_t *app = (struct fv_app_wesolowski_62_square_t *) state->input;
  switch (key)
    {
    case 'i':
      app->in_binary = 1;
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case 's':
        {
          mpz_t n;
          mpz_init_set_str (n, arg, 10);
          if (calculate_xyz (app, n, &app->x, &app->y, &app->z) == 0)
            app->num_args = 3;
          mpz_clear (n);
        }
      break;
    case 'x':
      app->xyz = 1;
      break;
    case 'c':
      app->odd = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 4)
        argp_error (state, "too many arguments");
      else
        {
          switch (app->num_args)
            {
            case 0: mpz_set_str (app->x, arg, 10); break;
            case 1: mpz_set_str (app->y, arg, 10); break;
            case 2: mpz_set_str (app->z, arg, 10); break;
            }
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      mpz_inits (app->x, app->y, app->z, NULL);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args > 0 && app->num_args < 3)
        argp_error (state, "too few arguments");
      else if (app->num_args == 0)
        app->from_stdin = 1;
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "center-odd", 'c', 0, 0, "Only make magic squares with an odd center value"},
  { "show-xyz", 'x', 0, 0, "Show X Y Z instead of magic squares"},
  { "start", 's', "NUM", 0, "Start at N=NUM instead of 1"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[X Y Z]\n--start=NUM",
  "Generate 3x3 magic squares of the type 6:2 given X Y Z values.  If X Y Z are not provided (and --start is also not provided) they are read from the standard input.\vThis solution was developed by Arkadiusz Wesolowski.  Suitable values for X Y Z are 3 1 1.  Only one magic square is created per record on the standard input.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_wesolowski_62_square_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.threads = 1;
  app.infile = stdin;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_wesolowski_62_square (&app);
}
