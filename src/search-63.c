/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

int max_recs = 1000;
//int max_recs = 3;
struct fv_app_search_63_t
{
  int in_binary;
  int num_args;
  int threads;
  int from_stdin;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int debug;
  int threesq;
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
  int twos;
  int quick;
  int progress;
};

struct fv_app_log_t lg;

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}


struct prec
{
  mpz_t lo;
  mpz_t hi;
};

//http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_63_t *app, struct prec *recs, int num_recs, mpz_t center);

void printCombination (int arr[], int n, int r, struct fv_app_search_63_t *app, struct prec *recs, int num_recs, mpz_t center)
{
  int data[r];
  combinationUtil (arr, data, 0, n-1, 0, r, app, recs, num_recs, center);
}

static void
handle_progressions (struct fv_app_search_63_t *app, mpz_t n, struct prec *recs, int num_recs, int by, int *arr)
{
  if (app->debug)
    error (0, 0, "Checking %d progressions", num_recs);
  printCombination (arr, num_recs, by, app, recs, num_recs, n);
}

static int
square_is_twelvenminusone (mpz_t i)
{
  mpz_t n;
  mpz_init (n);
  mpz_sub_ui (n, i, 1);
  int ret = mpz_divisible_ui_p (n, 12);
  mpz_clear (n);
  return ret;
}

static int
detect (mpz_t num, int *advance)
{
  mpz_t root, p1root, p2root, p1, p2;
  mpz_inits (root, p1root, p2root, p1, p2, NULL);
  //we return 1 if the next number in the sequence is 4 squares ahead
  mpz_sqrt (root, num);

  mpz_add_ui (p1root, root, 2);
  mpz_add_ui (p2root, root, 4);
  mpz_mul (p1, p1root, p1root);
  mpz_mul (p2, p2root, p2root);

  int checkp1 = square_is_twelvenminusone (p1);
  int checkp2 = square_is_twelvenminusone (p2);

  mpz_clears (root, p1root, p2root, p1, p2, NULL);
  if (checkp1 && !checkp2)
    return 0;
  else if (!checkp1 && checkp2)
    return 1;
  else if (checkp1 && checkp2)
    *advance = 1;
  return 0;
}
static void
quick_generate_progressions (struct fv_app_search_63_t *app, mpz_t n, struct prec **recs, int *num_recs)
{
  mpz_t i, iroot, distance, j, nroot;
  mpz_inits (i, iroot, distance, j, nroot, NULL);

  mpz_sqrt (iroot, n);
  mpz_add_ui (iroot, iroot, 1);
  if (mpz_even_p (iroot))
    mpz_add_ui (iroot, iroot, 1);
  mpz_mul (i, iroot, iroot);
  int advance = 0;
  int up_by_four = detect (i, &advance);
  if (advance)
    {
      mpz_add_ui (iroot, iroot, 4);
      mpz_mul (i, iroot, iroot);
      up_by_four = detect (i, &advance);
    }
  while (1)
    {
      mpz_sub (distance, i, n);
      if (mpz_cmp (distance, n) > 0)
        break;
      mpz_sub (j, n, distance);

      if (mpz_perfect_square_p (j))
        {
          if (*num_recs >= max_recs)
            *recs = realloc (*recs, sizeof (struct prec) * ((*num_recs) + 1));
          mpz_init_set ((*recs)[*num_recs].lo, j);
          mpz_init_set ((*recs)[*num_recs].hi, i);
          (*num_recs)++;
        }

      if (up_by_four)
        {
          //advance i by 4 squares
          mpz_mul_ui (nroot, iroot, 8);
          mpz_add (i, i, nroot);
          mpz_add_ui (i, i, 16);
          mpz_add_ui (iroot, iroot, 4);
        }
      else
        {
          //advance i by 2 squares
          mpz_mul_ui (nroot, iroot, 4);
          mpz_add (i, i, nroot);
          mpz_add_ui (i, i, 4);
          mpz_add_ui (iroot, iroot, 2);
        }

      up_by_four = !up_by_four;

    }
  mpz_clears (i, iroot, distance, j, nroot, NULL);
  return;
}

static void
generate_progressions (struct fv_app_search_63_t *app, mpz_t n, struct prec **recs, int *num_recs)
{
  mpz_t i, iroot, diff, limit, mm, nn, mn, twomn, twoiroot;
  mpz_inits (i, iroot, diff, limit, mm, nn, mn, twomn, twoiroot, NULL);

  mpz_cdiv_q_ui (limit, n, 2);
  mpz_set_ui (i, 1);
  mpz_set_ui (iroot, 1);

  while (1)
    {
      if (mpz_cmp (i, limit) > 0)
        break;

      mpz_sub (diff, n, i);

      if (mpz_perfect_square_p (diff))
        {
          mpz_set (mm, iroot);
          mpz_sqrt (nn, diff);
          mpz_mul (mn, mm, nn);
          mpz_mul_ui (twomn, mn, 2);
          if (*num_recs >= max_recs)
            *recs = realloc (*recs, sizeof (struct prec) * ((*num_recs) + 1));
          mpz_init_set ((*recs)[*num_recs].lo, n);
          mpz_sub ((*recs)[*num_recs].lo, (*recs)[*num_recs].lo, twomn);
          mpz_init_set ((*recs)[*num_recs].hi, n);
          mpz_add ((*recs)[*num_recs].hi, (*recs)[*num_recs].hi, twomn);
          (*num_recs)++;
        }

      mpz_mul_ui (twoiroot, iroot, 2);
      mpz_add (i, i, twoiroot);
      mpz_incr (i);
      mpz_incr (iroot);
    }
  mpz_clears (i, iroot, diff, limit, mm, nn, mn, twomn, twoiroot, NULL);
  return;
}

static int
search_square (mpz_t s, struct prec *recs, int num_recs)
{
  for (int i = 0; i < num_recs; i++)
    {
      if (mpz_cmp (s, recs[i].lo) == 0)
        return 1;
      if (mpz_cmp (s, recs[i].hi) == 0)
        return 1;
    }
  return 0;
}

static void
generate_63_square2 (struct fv_app_search_63_t *app, mpz_t center, struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  mpz_t sum, top;
  mpz_inits (sum, top, NULL);
  mpz_mul_ui (sum, center, 3);

  mpz_sub (top, sum, one->lo);
  mpz_sub (top, top, two->hi);
  // whip through our known squares before doing a more expensive check
  if (search_square (top, recs, num_recs) || mpz_perfect_square_p (top))
    {
      mpz_t bottom;
      mpz_init (bottom);

      mpz_sub (bottom, sum, two->lo);
      mpz_sub (bottom, bottom, one->hi);

      if (search_square (bottom, recs, num_recs) || mpz_perfect_square_p (bottom))
        {
          mpz_t s[3][3];
          for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
              mpz_init (s[i][j]);
          mpz_set (s[0][0], one->lo);
          mpz_set (s[0][2], two->hi);
          mpz_set (s[0][1], top);
          mpz_set (s[1][1], center);
          mpz_set (s[2][1], bottom);
          mpz_set (s[2][0], two->lo);
          mpz_set (s[2][2], one->hi);

          mpz_sub (s[1][0], sum, s[0][0]);
          mpz_sub (s[1][0], s[1][0], s[2][0]);

          mpz_sub (s[1][2], sum, s[0][2]);
          mpz_sub (s[1][2], s[1][2], s[2][2]);

          rotate_square (&s);
          pthread_mutex_lock (&display_lock);
          app->display_square (s, app->out);
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);

          for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
              mpz_clear (s[i][j]);
        }
      mpz_clear (bottom);
    }



  mpz_clears (sum, top, NULL);
}

static void
generate_63_square (struct fv_app_search_63_t *app, mpz_t center, struct prec *one, struct prec *two, struct prec *three)
{
  //try to make a 6:3 magic square from three progressions.
  //one->lo, center, one->hi
  //two->lo, center, two->hi
  //three->lo, center, three->hi

  if (mpz_cmp (one->lo, two->lo) > 0 &&
      mpz_cmp (three->hi, two->lo) > 0)
    {
      mpz_t diag1, row1;
      mpz_inits (diag1, row1, NULL);
      mpz_add (diag1, one->lo, center);
      mpz_add (diag1, diag1, one->hi);
      mpz_add (row1, one->lo, two->lo);
      mpz_add (row1, row1, three->hi);
      if (mpz_cmp (diag1, row1) == 0)
        {
          mpz_t sum, left, right, s[3][3];
          mpz_inits (sum, left, right, NULL);
          mpz_mul_ui (sum, center, 3);

          mpz_sub (left, sum, one->lo);
          mpz_sub (left, left, three->lo);

          mpz_sub (right, sum, three->hi);
          mpz_sub (right, right, one->hi);

          for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
              mpz_init (s[i][j]);
          mpz_set (s[0][0], one->lo);
          mpz_set (s[0][1], two->lo);
          mpz_set (s[0][2], three->hi);
          mpz_set (s[1][0], left);
          mpz_set (s[1][1], center);
          mpz_set (s[1][2], right);
          mpz_set (s[2][0], three->lo);
          mpz_set (s[2][1], two->hi);
          mpz_set (s[2][2], one->hi);
          rotate_square (&s);
          flip_square_vertically (&s);
          pthread_mutex_lock (&display_lock);
          app->display_square (s, app->out);
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);
          for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
              mpz_clear (s[i][j]);
          mpz_clears (sum, left, right, NULL);
        }
      mpz_clears (diag1, row1, NULL);
    }
}

 
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_63_t *app, struct prec *recs, int num_recs, mpz_t center)
{
  if (index == r)
    {
      //work on data here.  there are either 2 or 3 elements.
      if (app->twos)
        generate_63_square2 (app, center, &recs[data[0]], &recs[data[1]], recs, num_recs);
      else
        generate_63_square (app, center, &recs[data[0]], &recs[data[1]], &recs[data[2]]);
      return;
    }

  for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
      data[index] = arr[i];
      combinationUtil(arr, data, i + 1, end, index + 1, r, app, recs, num_recs, center);
    }
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_63_t *app =
    (struct fv_app_search_63_t *) param->data;

  mpz_inits (n, num, NULL);
  struct prec *recs = malloc (sizeof (struct prec) * max_recs);
  int *arr = malloc (max_recs * sizeof (int));
  for (int j = 0; j < max_recs; j++)
    arr[j] = j;
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (n, app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (num, line, 10);
        }
      mpz_set (n, num);
      if (app->progress)
        fv_update_log_mpz (&lg, &num);
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
        {
          int num_recs = 0;
          if (app->quick)
            quick_generate_progressions (app, n, &recs, &num_recs);
          else
            generate_progressions (app, n, &recs, &num_recs);

          if (num_recs >= max_recs)
            {
              arr = realloc (arr, sizeof (int) * num_recs);
              for (int i = max_recs; i < num_recs; i++)
                arr[i] = i;
              max_recs = num_recs;
            }
          if (app->twos)
            {
              if (num_recs >= 2)
                handle_progressions (app, n, recs, num_recs, 2, arr);
            }
          else
            {
              if (num_recs >= 3)
                handle_progressions (app, n, recs, num_recs, 3, arr);
            }
          for (int i = 0; i < num_recs; i++)
            mpz_clears (recs[i].lo, recs[i].hi, NULL);
        }
    }
  mpz_clears (n, num, NULL);
  free (arr);

  free (recs);
  if (line)
    free (line);
  return NULL;
}

static void
process_threesq_record (struct fv_app_search_63_t *app, mpz_t n, mpz_t *a, struct prec **recs, int *num_recs)
{
  if (*num_recs > max_recs)
    *recs = realloc (*recs, sizeof (struct prec) * ((*num_recs) + 1));
  mpz_init_set ((*recs)[*num_recs].lo, a[0]);
  mpz_init_set ((*recs)[*num_recs].hi, a[2]);
  (*num_recs)++;
  int *arr = malloc (sizeof (int) * *num_recs);
  for (int i = 0; i < *num_recs; i++)
    arr[i] = i;
  if (app->twos)
    {
      if (*num_recs >= 2)
        handle_progressions (app, n, *recs, *num_recs, 2, arr);
    }
  else
    {
      if (*num_recs >= 3)
        handle_progressions (app, n, *recs, *num_recs, 3, arr);
    }
  free (arr);
  return;
}

int
search_63_threesq (struct fv_app_search_63_t *app, FILE *in)
{
  //it's not threaded because it doesn't need to be.
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3];
  mpz_t n;

  for (int i = 0; i < 3; i++)
    mpz_init (a[i]);
  mpz_init (n);

  struct prec *recs = malloc (sizeof (struct prec) * max_recs);
  int num_recs = 0;
  while (1)
    {
      read = app->read_tuple (in, a, &line, &len);
      if (read == -1)
        break;

      // when we switch to a different center number we start over
      if (mpz_cmp (a[1], n) != 0)
        {
          for (int i = 0; i < num_recs; i++)
            mpz_clears (recs[i].lo, recs[i].hi, NULL);
          num_recs = 0;
        }

      process_threesq_record (app, a[1], a, &recs, &num_recs);
      mpz_set (n, a[1]);
    }
  for (int i = 0; i < num_recs; i++)
    mpz_clears (recs[i].lo, recs[i].hi, NULL);

  for (int i = 0; i < 3; i++)
    mpz_clear (a[i]);

  mpz_clear (n);

  if (line)
    free (line);
  return 0;
}

int
fituvalu_search_63 (struct fv_app_search_63_t *app)
{
  if (app->threesq)
    return search_63_threesq (app, stdin);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_63_t *app = (struct fv_app_search_63_t *) state->input;
  switch (key)
    {
    case 'p':
      app->progress = 1;
      break;
    case 'q':
      app->quick = 1;
      break;
    case '2':
      app->twos = 1;
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'd':
      app->debug = 1;
      break;
    case 'i':
      app->in_binary = 1;
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "twos", '2', 0, 0, "Use 2 progressions instead of 3 to make a square"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "debug", 'd', 0, OPTION_HIDDEN, "Show extra information while running"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in near progressions instead of center values"},
  { "quick", 'q', 0, 0, "All squares have roots that are 1 or 5 mod 6"},
  { "progress", 'p', 0, OPTION_HIDDEN, "Show progress information in /tmp"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:3 given a FILE containing center values.\vWhen FILE is not provided, it is read from the standard input.  Center values must not be perfect squares.  Magic squares of type 6:3 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  | X |   | X |   | A |   | F |   Find three number arithmetic progressions\n"
"  +---+---+---+   +---+---+---+   that have non-square n at their center.  The\n"  
"  | X |   | x |   | C | n | D |   progressions are A,n,B, C,n,D, and E,n,F.\n"
"  +---+---+---+   +---+---+---+   Try every combination of three progressions\n"
"  | X |   | X |   | E |   | B |   in every position to find a magic square.\n"
"  +---+---+---+   +---+---+---+\n"
"This program checks combinations of three square progressions every time it finds a new one to see if it forms a magic square.  This differs from search-63-small which only checks once at the end.  We check every combination of 2 or 3 near three square progressions depending on the --twos option.  A near three square progression in this case is an arithmetic progression starting with a square, continuing to a non-square and ending on a square.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_63_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.display_square = display_square_record;
  app.read_tuple = read_three_numbers_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (app.progress)
    fv_init_log (&lg, "search-63");
  return fituvalu_search_63 (&app);
}
/*
$ cat ~/Documents/squares/unique-sixes | ./type-square -f 6:3 | ./rotate-square -6 |  ./mine-pairs --coords --divide | sort  | grep "^0,0, 0,2" | cut -f 5 -d, | ./percent-histogram
0 |
1 |
2 |
3 |
4 |
5 |======================================================================
6 |======================================================================
7 |======================================================================
8 |===================================
9 |
  +---+---+---+
  | X |   | X |
  +---+---+---+
  | x |   | x |
  +---+---+---+
  | x |   | x |
  +---+---+---+

  0,0 is at least half of 0,2
*/
