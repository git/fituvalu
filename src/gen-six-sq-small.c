/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <gmp.h>
#include <pthread.h>
#include "magicsquareutil.h"

  
#define OPT_GENERATE -644

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

enum generation_enum_t
{
  GENERATE_61,
  GENERATE_64,
  GENERATE_65,
  GENERATE_67,
  GENERATE_68,
  GENERATE_69,
  GENERATE_610,
  GENERATE_612,
  GENERATE_615B,
  GENERATE_616,
  GENERATE_MAX
};

struct fv_app_gen_six_sq_t
{
  FILE *infile;
  int numargs;
  FILE *out;
  int threads;
  int generate[GENERATE_MAX];
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void calculate_progressions (struct fv_app_gen_six_sq_t *app, unsigned long long int *progression, unsigned long long int *b, unsigned long long int (*s)[3][3], void (*func)(struct fv_app_gen_six_sq_t*, unsigned long long int *, unsigned long long int, unsigned long long int *b, unsigned long long int (*s)[3][3]))
{
  unsigned long long int i = 1, iroot = 1, limit = progression[1] / 2, diff;
  unsigned long long int twomn;

  while (i <= limit)
    {
      diff = progression[1] - i;
      if (small_is_square (diff))
        {
          twomn = iroot * sqrtl (diff) * 2;

          progression[0] = progression[1] - twomn;
          progression[2] = progression[1] + twomn;

          func (app, progression, twomn, b, s); //calls handle_progression
        }
      i += (iroot * 2) + 1;
      iroot++;
    }

  return;
}

void
dump_square (void *data, char *fmt, unsigned long long int (*s)[3][3])
{
  struct fv_app_gen_six_sq_t *app = (struct fv_app_gen_six_sq_t *) data;
  pthread_mutex_lock (&display_lock);
  fprintf (app->out, fmt,
           (*s)[0][0], (*s)[0][1], (*s)[0][2],
           (*s)[1][0], (*s)[1][1], (*s)[1][2],
           (*s)[2][0], (*s)[2][1], (*s)[2][2]);
  fflush (app->out);
  pthread_mutex_unlock (&display_lock);
}

static void
handle_progression (struct fv_app_gen_six_sq_t *app, unsigned long long int *progression, unsigned long long int diff, unsigned long long int *b, unsigned long long int (*s)[3][3])
{
  unsigned long long int twodiff = diff + diff;
  if (app->generate[GENERATE_64])
    search_64_3sq_small (progression, diff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_67])
    search_67_3sq_small (progression, diff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_69])
    search_69_3sq_small (progression, diff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_65])
    search_65_3sq_small (progression, diff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_61])
    search_61_3sq_small (progression, twodiff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_612])
    search_612_3sq_small (progression, twodiff, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_68])
    search_68_3sq_small (progression, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_610])
    search_610_3sq_small (progression, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_615B])
    search_615_3sq_small (progression, b, s, 1, dump_square, app);
  if (app->generate[GENERATE_616])
    search_616_3sq_small (progression, twodiff, b, s, 1, dump_square, app);
  return;
}

static void*
process_perfect_square (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_gen_six_sq_t *app =
    (struct fv_app_gen_six_sq_t *) param->data;
  unsigned long long int progression[3], b[2], s[3][3];

  char *line = NULL;
  size_t len = 0;
  char *end = NULL;
  while (1)
    {
      //go get the next perfect square to work on (app->num)
      pthread_mutex_lock (&read_lock);

      int read = fv_getline (&line, &len, app->infile);
      if (read == -1)
        {
          pthread_mutex_unlock (&read_lock);
          break;
        }
      progression[1] = strtoull (line, &end, 10);

      pthread_mutex_unlock (&read_lock);

      //get all of the three square progressions, and search for magic squares
      //b is space for the 2 values we're cycling through later on.
      //it equates to Y1 and Y2 in the --help.
      calculate_progressions (app, progression, b, &s, handle_progression);
    }
  free (line);

  return NULL;
}

int
fituvalu_gen_six_sq (struct fv_app_gen_six_sq_t *app)
{
  run_threads (app, app->threads, process_perfect_square);
  return 0;
}

static struct argp_option
options[] =
{
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "generate", OPT_GENERATE, "TYPE[,TYPE]", 0, "Only search for magic squares of the given TYPEs"},
  { 0 }
};

static int
parse_generation_string (struct argp_state *state, char *str)
{
  struct fv_app_gen_six_sq_t *app = (struct fv_app_gen_six_sq_t *) state->input;
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;
  /* mixing it up by using fmemopen */
  FILE *fp = fmemopen (str, strlen (str) + 1, "r");
  while ((nread = getdelim (&line, &len, ',', fp)) != -1)
    {
      size_t n = strspn (" \t", line);
      char *s = strdup (&line[n]);
      if (s[strlen (s) - 1] == ',')
        s[strlen (s) - 1] = '\0';
      if (strcmp (s, "6:1") == 0)
        app->generate[GENERATE_61] = 1;
      else if (strcmp (s, "6:4") == 0)
        app->generate[GENERATE_64] = 1;
      else if (strcmp (s, "6:5") == 0)
        app->generate[GENERATE_65] = 1;
      else if (strcmp (s, "6:7") == 0)
        app->generate[GENERATE_67] = 1;
      else if (strcmp (s, "6:8") == 0)
        app->generate[GENERATE_68] = 1;
      else if (strcmp (s, "6:9") == 0)
        app->generate[GENERATE_69] = 1;
      else if (strcmp (s, "6:10") == 0)
        app->generate[GENERATE_610] = 1;
      else if (strcmp (s, "6:12") == 0)
        app->generate[GENERATE_612] = 1;
      else if (strcmp (s, "6:15") == 0)
        app->generate[GENERATE_615B] = 1;
      else if (strcmp (s, "6:16") == 0)
        app->generate[GENERATE_616] = 1;
      else
        argp_error (state, "invalid square type '%s'", s);
      free (s);
    }
  fclose (fp);
  return 1;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_gen_six_sq_t *app = (struct fv_app_gen_six_sq_t *) state->input;
  switch (key)
    {
    case OPT_GENERATE:
      parse_generation_string (state, arg);
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_ARG:
      if (app->numargs > 1)
        argp_error (state, "too many arguments.");
      app->numargs++;
      if (strcmp (arg, "-") == 0)
        app->infile = stdin;
      else
        app->infile = fopen (arg, "r");
      if (app->infile == NULL)
        argp_error (state, "could not opening `%s' for reading");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
        {
          int generate[GENERATE_MAX];
          memset (generate, 0, sizeof (generate));
          if (memcmp (generate, app->generate, sizeof (generate)) == 0)
            {
              for (int i = 0; i < GENERATE_MAX; i++)
                app->generate[i] = 1;
            }
        }
      break;
    }
  return 0;
}

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Generate 3x3 magic squares with 6 perfect squares or more by generating 3sq progressions from a given set of squares in FILE.  For each progression of three squares we search for magic squares of the following types: 6:1, 6:4, 6:5, 6:7, 6:8, 6:10, 6:12, 6:15, and 6:16.\vWe also inadvertently find magic squares of the forms: 6:2, 6:6, 6:11, 6:13, and 6:14.  Suitable values for --generate are: 6:1, 6:4, 6:5, 6:7, 6:8, 6:9, 6:10, 6:12, 6:15, and 6:16.  When FILE is not specified or it is `-', the squares are read from the standard input.", 0
};

int
main (int argc, char **argv)
{
  struct fv_app_gen_six_sq_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.infile = stdin;
  app.threads = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);

  return fituvalu_gen_six_sq (&app);
}
