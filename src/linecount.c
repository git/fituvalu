/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>

static char *
get_filename ()
{
  char *filename = NULL;
  asprintf (&filename, "%s/%s-progress.%ld", P_tmpdir,
            program_invocation_short_name, getpid ());
  return filename;
}

void
remove_linecount_file ()
{
  char *filename = get_filename ();
  if (filename)
    remove (filename);
  free (filename);
}

void
linecount_handle_sigint (int sig)
{
  exit (1);
}

void
init_linecount (int *counter)
{
  *counter = 0;
  atexit (remove_linecount_file);
  signal(SIGINT, linecount_handle_sigint);
}

void
update_linecount (int *counter)
{
  char *filename = get_filename ();
  if (filename)
    {
      FILE *fp = fopen (filename, *counter % 1000 ? "a" : "w");
      if (fp)
        {
          fprintf (fp, "%ld, %d\n", time (NULL), *counter);
          fclose (fp);
        }
      free (filename);
    }
  (*counter)++;
}
