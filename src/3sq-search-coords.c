/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <gmp.h>
#include <pthread.h>
#include "magicsquareutil.h"

  
#define OPT_GENERATE -644

size_t
get_auto_max_percentage (mpz_t *num, mpf_t *perc)
{
  size_t n = mpz_sizeinbase (*num, 10);
  switch (n)
    {
    case 0: case 1: case 2: case 3: case 4: case 5:
      mpf_set_str (*perc, "1.0", 10);
      break;
    case 6:
      mpf_set_str (*perc, "0.5", 10);
      break;
    case 7:
      mpf_set_str (*perc, "0.1", 10);
      break;
    case 8:
      mpf_set_str (*perc, "0.001", 10);
      break;
    case 9:
      mpf_set_str (*perc, "0.00001", 10);
      break;
    case 10:
      mpf_set_str (*perc, "0.00000001", 10);
      break;
    case 11: case 12: case 13: case 14:
      mpf_set_str (*perc, "0.0000000001", 10);
      break;
    default:
      mpf_set_str (*perc, "0.1", 10);
        {
          int max = n - 2;
          for (int i = 0; i < max; i++)
            mpf_div_ui (*perc, *perc, 10);
        }
      break;
    }
  return n;
}

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

enum max_strat_enum
{
  MAX_STRAT_NONE,
  MAX_STRAT_AUTO,
};

struct fv_app_3sq_search_coords_t
{
  mpz_t max_floor;
  FILE *infile;
  int numargs;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
  int threads;
  int generate[2][8];
  int do_dist;
  int do_two_dist;
  mpf_t min;
  mpf_t max;
  enum max_strat_enum max_strat;
  int offset;
  int step;
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_record
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

struct rec {
  mpz_t p[3];
  mpz_t diff;
};

void
dump_square (void *data, mpz_t (*s)[3][3])
{
  struct fv_app_3sq_search_coords_t *app = (struct fv_app_3sq_search_coords_t *) data;
  pthread_mutex_lock (&display_lock);
  app->display_square (*s, app->out);
  //fflush (app->out);
  pthread_mutex_unlock (&display_lock);
}

static void
search (struct fv_app_3sq_search_coords_t *app, mpz_t *progression, mpz_t high, mpz_t low, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], void (*func)(struct fv_app_3sq_search_coords_t *, mpz_t *, mpz_t, mpz_t *, mpz_t (*)[3][3]))
{
  mpz_t i, iroot, j, limit;
  mpz_inits (i, iroot, j, limit, NULL);
  mpz_set (i, high);
  mpz_sqrt (iroot, i);
  mpz_mul (i, iroot, iroot);

  mpz_sub_ui (iroot, iroot, 1);
  mpz_sub (i, i, iroot);
  mpz_sub (i, i, iroot);
  mpz_sub_ui (i, i, 1);

  if (app->offset)
    {
      for (int q = 0; q < app->offset; q++)
        {
          mpz_sub_ui (iroot, iroot, 1);
          mpz_sub (i, i, iroot);
          mpz_sub (i, i, iroot);
          mpz_sub_ui (i, i, 1);
        }
    }
  while (1)
    {
      mpz_sub (j, i, distance);
      if (mpz_cmp (j, low) < 0)
        break;

      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);

          //fill
          func (app, progression, distance, b, s);
        }
      for (int q = 0; q < app->step; q++)
        {
          mpz_sub_ui (iroot, iroot, 1);
          mpz_sub (i, i, iroot);
          mpz_sub (i, i, iroot);
          mpz_sub_ui (i, i, 1);
        }
    }
  mpz_clears (i, iroot, j, limit, NULL);
}

static void
layout (int ar[5][2], mpz_t *progression, mpz_t *b, mpz_t (*s)[3][3])
{
  mpz_set ((*s)[ar[0][0]][ar[0][1]], progression[0]);
  mpz_set ((*s)[ar[1][0]][ar[1][1]], progression[1]);
  mpz_set ((*s)[ar[2][0]][ar[2][1]], progression[2]);
  mpz_set ((*s)[ar[3][0]][ar[3][1]], b[0]);
  mpz_set ((*s)[ar[4][0]][ar[4][1]], b[1]);
}

static int
check_squares (int xr[4][2], mpz_t (*s)[3][3])
{
  int found = 0;
  for (int i = 0; i < 4; i++)
    {
      if (mpz_perfect_square_p ((*s)[xr[i][0]][xr[i][1]]))
        {
          found = 1;
          break;
        }
    }
  return found;
}

static void
solve_d1c1 (mpz_t (*s)[3][3])
{
  mpz_t sum;
  mpz_init (sum);
  mpz_add (sum, (*s)[1][0], (*s)[1][1]);
  mpz_add (sum, sum, (*s)[1][2]);

  mpz_sub ((*s)[0][0], sum, (*s)[1][0]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][0]);

  mpz_sub ((*s)[0][2], sum, (*s)[1][2]);
  mpz_sub ((*s)[0][2], (*s)[0][2], (*s)[2][2]);

  mpz_sub ((*s)[0][1], sum, (*s)[0][0]);
  mpz_sub ((*s)[0][1], (*s)[0][1], (*s)[0][2]);

  mpz_sub ((*s)[2][1], sum, (*s)[2][0]);
  mpz_sub ((*s)[2][1], (*s)[2][1], (*s)[2][2]);

  mpz_clear (sum);
}

static void
solve_d1c2 (mpz_t (*s)[3][3])
{
  return solve_d1c1 (s);
}

static void
solve_d1c3 (mpz_t (*s)[3][3], mpz_t *sum)
{
  mpz_sub (*sum, (*s)[1][2], (*s)[1][0]);
  mpz_cdiv_q_ui (*sum, *sum, 2);
  mpz_add ((*s)[1][1], (*s)[1][0], *sum);
  mpz_add (*sum, (*s)[1][0], (*s)[1][1]);
  mpz_add (*sum, *sum, (*s)[1][2]);

  mpz_sub ((*s)[0][0], *sum, (*s)[1][0]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][0]);

  mpz_sub ((*s)[0][1], *sum, (*s)[1][1]);
  mpz_sub ((*s)[0][1], (*s)[0][1], (*s)[2][1]);

  mpz_sub ((*s)[2][2], *sum, (*s)[1][2]);
  mpz_sub ((*s)[2][2], (*s)[2][2], (*s)[0][2]);
}

static void
solve_d1c4 (mpz_t (*s)[3][3], mpz_t *sum)
{
  solve_d1c3 (s, sum);
}

static void
solve_d1c5 (mpz_t (*s)[3][3], mpz_t *sum)
{
  mpz_mul_ui (*sum, (*s)[1][1], 3);

  mpz_sub ((*s)[0][0], *sum, (*s)[1][1]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][2]);

  mpz_sub ((*s)[2][0], *sum, (*s)[1][0]);
  mpz_sub ((*s)[2][0], (*s)[2][0], (*s)[0][0]);

  mpz_sub ((*s)[0][1], *sum, (*s)[1][1]);
  mpz_sub ((*s)[0][1], (*s)[0][1], (*s)[2][1]);

  mpz_sub ((*s)[1][2], *sum, (*s)[1][1]);
  mpz_sub ((*s)[1][2], (*s)[1][2], (*s)[1][0]);
}

static void
solve_d1c6 (mpz_t (*s)[3][3], mpz_t *sum)
{
  return solve_d1c5 (s, sum);
}

static void
solve_d1c7 (mpz_t (*s)[3][3], mpz_t *sum)
{
  mpz_add (*sum, (*s)[2][0], (*s)[1][1]);
  mpz_add (*sum, *sum, (*s)[0][2]);

  mpz_sub ((*s)[0][0], *sum, (*s)[1][1]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][2]);

  mpz_sub ((*s)[0][1], *sum, (*s)[0][0]);
  mpz_sub ((*s)[0][1], (*s)[0][1], (*s)[0][2]);

  mpz_sub ((*s)[2][1], *sum, (*s)[1][1]);
  mpz_sub ((*s)[2][1], (*s)[2][1], (*s)[0][1]);

  mpz_sub ((*s)[1][2], *sum, (*s)[1][1]);
  mpz_sub ((*s)[1][2], (*s)[1][2], (*s)[1][0]);
}

static void
solve_d1c8 (mpz_t (*s)[3][3], mpz_t *sum)
{
  return solve_d1c7 (s, sum);
}

static void
solve_d2c1 (mpz_t (*s)[3][3], mpz_t *sum)
{
  mpz_add (*sum, (*s)[2][0], (*s)[1][1]);
  mpz_add (*sum, *sum, (*s)[0][2]);

  mpz_sub ((*s)[0][1], *sum, (*s)[1][1]);
  mpz_sub ((*s)[0][1], (*s)[0][1], (*s)[2][1]);

  mpz_sub ((*s)[0][0], *sum, (*s)[0][1]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[0][2]);

  mpz_sub ((*s)[1][0], *sum, (*s)[1][1]);
  mpz_sub ((*s)[1][0], (*s)[1][0], (*s)[1][2]);

  mpz_sub ((*s)[2][2], *sum, (*s)[1][1]);
  mpz_sub ((*s)[2][2], (*s)[2][2], (*s)[0][0]);
}

static void
solve_d2c2 (mpz_t (*s)[3][3], mpz_t *sum)
{
  return solve_d2c1 (s, sum);
}

static void
solve_d2c3 (mpz_t (*s)[3][3], mpz_t *sum)
{
  mpz_sub (*sum, (*s)[1][2], (*s)[1][0]);
  mpz_cdiv_q_ui (*sum, *sum, 2);
  mpz_add ((*s)[1][1], (*s)[1][0], *sum);

  mpz_add (*sum, (*s)[0][1], (*s)[1][1]);
  mpz_add (*sum, *sum, (*s)[2][1]);

  mpz_sub ((*s)[0][0], *sum, (*s)[1][1]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][2]);

  mpz_sub ((*s)[0][2], *sum, (*s)[0][1]);
  mpz_sub ((*s)[0][2], (*s)[0][2], (*s)[0][0]);

  mpz_sub ((*s)[2][0], *sum, (*s)[1][0]);
  mpz_sub ((*s)[2][0], (*s)[2][0], (*s)[0][0]);
}

static void
solve_d2c4 (mpz_t (*s)[3][3], mpz_t *sum)
{
  return solve_d2c3 (s, sum);
}

static void
solve_d2c5 (mpz_t (*s)[3][3], mpz_t *sum)
{
  mpz_sub (*sum, (*s)[0][2], (*s)[2][0]);
  mpz_cdiv_q_ui (*sum, *sum, 2);
  mpz_add ((*s)[1][1], (*s)[2][0], *sum);

  mpz_add (*sum, (*s)[2][0], (*s)[1][1]);
  mpz_add (*sum, *sum, (*s)[0][2]);

  mpz_sub ((*s)[0][0], *sum, (*s)[1][0]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][0]);

  mpz_sub ((*s)[2][1], *sum, (*s)[1][1]);
  mpz_sub ((*s)[2][1], (*s)[2][1], (*s)[0][1]);

  mpz_sub ((*s)[1][2], *sum, (*s)[0][2]);
  mpz_sub ((*s)[1][2], (*s)[1][2], (*s)[2][2]);
}

static void
solve_d2c6 (mpz_t (*s)[3][3], mpz_t *sum)
{
  solve_d2c5 (s, sum);
}

static int
valid_d1c1 (mpz_t (*s)[3][3])
{
  int valid = 1;
  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0)
    valid = 0;
  return valid;
}

static int
valid_d1c2 (mpz_t (*s)[3][3])
{
  return valid_d1c1 (s);
}

static int
valid_d1c3 (mpz_t (*s)[3][3], mpz_t sum)
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0 ||
      mpz_cmp ((*s)[0][2], (*s)[1][2]) == 0)
    return 0;
  mpz_t sum2;
  mpz_init (sum2);
  mpz_add (sum2, (*s)[0][0], (*s)[0][1]);
  mpz_add (sum2, sum2, (*s)[0][2]);
  int match = mpz_cmp (sum, sum2) == 0;
  mpz_clear (sum2);
  return match;
}

static int
valid_d1c4 (mpz_t (*s)[3][3], mpz_t sum)
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0 ||
      mpz_cmp ((*s)[0][2], (*s)[1][2]) == 0)
    return 0;
  mpz_t sum2;
  mpz_init (sum2);
  mpz_add (sum2, (*s)[0][0], (*s)[1][1]);
  mpz_add (sum2, sum2, (*s)[2][2]);
  int match = mpz_cmp (sum, sum2) == 0;
  mpz_clear (sum2);
  return match;
}

static int
valid_d1c5 (mpz_t (*s)[3][3])
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[0][1]) == 0)
    return 0;
  return 1;
}

static int
valid_d1c6 (mpz_t (*s)[3][3])
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[0][1]) == 0)
    return 0;
  return 1;
}

static int
valid_d1c7 (mpz_t (*s)[3][3])
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[1][0]) == 0)
    return 0;
  return 1;
}

static int
valid_d1c8 (mpz_t (*s)[3][3])
{
  return valid_d1c7 (s);
}

static int
valid_d2c1 (mpz_t (*s)[3][3])
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
      mpz_cmp ((*s)[0][0], (*s)[0][1]) == 0 ||
      mpz_cmp ((*s)[0][1], (*s)[0][2]) == 0)
    return 0;
  return 1;
}

static int
valid_d2c2 (mpz_t (*s)[3][3])
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
      mpz_cmp ((*s)[0][2], (*s)[1][2]) == 0)
    return 0;
  return 1;
}

static int
valid_d2c3 (mpz_t (*s)[3][3], mpz_t sum1)
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[1][2]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[0][1]) == 0)
    return 0;
  mpz_t sum2;
  mpz_init (sum2);
  mpz_add (sum2, (*s)[2][0], (*s)[1][1]);
  mpz_add (sum2, sum2, (*s)[0][2]);
  int match = mpz_cmp (sum1, sum2) == 0;
  mpz_clear (sum2);
  if (!match)
    return 0;
  return 1;
}

static int
valid_d2c4 (mpz_t (*s)[3][3], mpz_t sum1)
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[0][1]) == 0 ||
      mpz_cmp ((*s)[1][1], (*s)[1][0]) == 0)
    return 0;
  mpz_t sum2;
  mpz_init (sum2);
  mpz_add (sum2, (*s)[2][0], (*s)[1][1]);
  mpz_add (sum2, sum2, (*s)[0][2]);
  int match = mpz_cmp (sum1, sum2) == 0;
  mpz_clear (sum2);
  if (!match)
    return 0;
  return 1;
}

static int
valid_d2c5 (mpz_t (*s)[3][3])
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
      mpz_cmp ((*s)[0][2], (*s)[1][2]) == 0)
    return 0;
  return 1;
}

static int
valid_d2c6 (mpz_t (*s)[3][3])
{
  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
      mpz_cmp ((*s)[0][1], (*s)[0][2]) == 0)
    return 0;
  return 1;
}

static void
fill_distdown (struct fv_app_3sq_search_coords_t *app, mpz_t *progression, mpz_t dist, mpz_t *b, mpz_t (*s)[3][3])
{
  mpz_t sum;
  mpz_init (sum);
  if (app->generate[0][0])
    {
      //ar are the ordering of the layout of cells,
      //  e.g.  the progression[3], and b[2]
      //xr are the cells that get calculated
      int ar[5][2] = {{1,0}, {1,1}, {1,2}, {2,0}, {2,2}};
      int xr[4][2] = {{0,0}, {0,1}, {0,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d1c1 (s);

      if (valid_d1c1 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][1])
    {
      int ar[5][2] = {{1,2}, {1,1}, {1,0}, {2,2}, {2,0}};
      int xr[4][2] = {{0,0}, {0,1}, {0,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d1c2 (s);

      if (valid_d1c2 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][2])
    {
      int ar[5][2] = {{1,0}, {0,2}, {2,1}, {1,2}, {2,0}};
      int xr[4][2] = {{0,0}, {0,1}, {1,1}, {2,2}};
      layout (ar, progression, b, s);
      solve_d1c3 (s, &sum);

      if (valid_d1c3 (s, sum))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][3])
    {
      int ar[5][2] = {{2,1}, {0,2}, {1,0}, {2,0}, {1,2}};
      int xr[4][2] = {{0,0}, {0,1}, {1,1}, {2,2}};
      layout (ar, progression, b, s);
      solve_d1c4 (s, &sum);

      if (valid_d1c4 (s, sum))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][4])
    {
      int ar[5][2] = {{1,0}, {0,2}, {2,1}, {1,1}, {2,2}};
      int xr[4][2] = {{0,0}, {0,1}, {1,2}, {2,0}};
      layout (ar, progression, b, s);
      solve_d1c5 (s, &sum);

      if (valid_d1c5 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][5])
    {
      int ar[5][2] = {{2,1}, {0,2}, {1,0}, {2,2}, {1,1}};
      int xr[4][2] = {{0,0}, {0,1}, {1,2}, {2,0}};
      layout (ar, progression, b, s);
      solve_d1c6 (s, &sum);

      if (valid_d1c6 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][6])
    {
      int ar[5][2] = {{0,2}, {1,1}, {2,0}, {2,2}, {1,0}};
      int xr[4][2] = {{0,0}, {0,1}, {1,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d1c7 (s, &sum);

      if (valid_d1c7 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][7])
    {
      int ar[5][2] = {{2,0}, {1,1}, {0,2}, {1,0}, {2,2}};
      int xr[4][2] = {{0,0}, {0,1}, {1,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d1c8 (s, &sum);

      if (valid_d1c8 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }

  mpz_clear (sum);
}

static void
fill_twodistdown (struct fv_app_3sq_search_coords_t *app, mpz_t *progression, mpz_t dist, mpz_t *b, mpz_t (*s)[3][3])
{
  mpz_t sum;
  mpz_init (sum);
  if (app->generate[1][0])
    {
      int ar[5][2] = {{0,2}, {1,1}, {2,0}, {1,2}, {2,1}};
      int xr[4][2] = {{0,0}, {0,1}, {1,0}, {2,2}};
      layout (ar, progression, b, s);
      solve_d2c1 (s, &sum);

      if (valid_d2c1 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][1])
    {
      int ar[5][2] = {{2,0}, {1,1}, {0,2}, {2,1}, {1,2}};
      int xr[4][2] = {{0,0}, {0,1}, {1,0}, {2,2}};
      layout (ar, progression, b, s);
      solve_d2c2 (s, &sum);

      if (valid_d2c2 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][2])
    {
      int ar[5][2] = {{0,1}, {2,2}, {1,0}, {2,1}, {1,2}};
      int xr[4][2] = {{0,0}, {0,2}, {1,1}, {2,0}};
      layout (ar, progression, b, s);
      solve_d2c3 (s, &sum);

      if (valid_d2c3 (s, sum))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][3])
    {
      int ar[5][2] = {{1,0}, {2,2}, {0,1}, {1,2}, {2,1}};
      int xr[4][2] = {{0,0}, {0,2}, {1,1}, {2,0}};
      layout (ar, progression, b, s);
      solve_d2c4 (s, &sum);

      if (valid_d2c4 (s, sum))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][4])
    {
      int ar[5][2] = {{0,1}, {2,2}, {1,0}, {0,2}, {2,0}};
      int xr[4][2] = {{0,0}, {1,1}, {1,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d2c5 (s, &sum);

      if (valid_d2c5 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][5])
    {
      int ar[5][2] = {{1,0}, {2,2}, {0,1}, {2,0}, {0,2}};
      int xr[4][2] = {{0,0}, {1,1}, {1,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d2c6 (s, &sum);

      if (valid_d2c6 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }

  mpz_clear (sum);
}
/*
static void
fill (struct fv_app_3sq_search_coords_t *app, mpz_t *progression, mpz_t dist, mpz_t *b, mpz_t (*s)[3][3])
{
  mpz_t sum;
  mpz_init (sum);
  if (app->generate[0][0])
    {
      //ar are the ordering of the layout of cells,
      //  e.g.  the progression[3], and b[2]
      //xr are the cells that get calculated
      int ar[5][2] = {{1,0}, {1,1}, {1,2}, {2,0}, {2,2}};
      int xr[4][2] = {{0,0}, {0,1}, {0,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d1c1 (s);

      if (valid_d1c1 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][1])
    {
      int ar[5][2] = {{1,2}, {1,1}, {1,0}, {2,2}, {2,0}};
      int xr[4][2] = {{0,0}, {0,1}, {0,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d1c2 (s);

      if (valid_d1c2 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][2])
    {
      int ar[5][2] = {{1,0}, {0,2}, {2,1}, {1,2}, {2,0}};
      int xr[4][2] = {{0,0}, {0,1}, {1,1}, {2,2}};
      layout (ar, progression, b, s);
      solve_d1c3 (s, &sum);

      if (valid_d1c3 (s, sum))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][3])
    {
      int ar[5][2] = {{2,1}, {0,2}, {1,0}, {2,0}, {1,2}};
      int xr[4][2] = {{0,0}, {0,1}, {1,1}, {2,2}};
      layout (ar, progression, b, s);
      solve_d1c4 (s, &sum);

      if (valid_d1c4 (s, sum))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][4])
    {
      int ar[5][2] = {{1,0}, {0,2}, {2,1}, {1,1}, {2,2}};
      int xr[4][2] = {{0,0}, {0,1}, {1,2}, {2,0}};
      layout (ar, progression, b, s);
      solve_d1c5 (s, &sum);

      if (valid_d1c5 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][5])
    {
      int ar[5][2] = {{2,1}, {0,2}, {1,0}, {2,2}, {1,1}};
      int xr[4][2] = {{0,0}, {0,1}, {1,2}, {2,0}};
      layout (ar, progression, b, s);
      solve_d1c6 (s, &sum);

      if (valid_d1c6 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][6])
    {
      int ar[5][2] = {{0,2}, {1,1}, {2,0}, {2,2}, {1,0}};
      int xr[4][2] = {{0,0}, {0,1}, {1,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d1c7 (s, &sum);

      if (valid_d1c7 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[0][7])
    {
      int ar[5][2] = {{2,0}, {1,1}, {0,2}, {1,0}, {2,2}};
      int xr[4][2] = {{0,0}, {0,1}, {1,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d1c8 (s, &sum);

      if (valid_d1c8 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][0])
    {
      int ar[5][2] = {{0,2}, {1,1}, {2,0}, {1,2}, {2,1}};
      int xr[4][2] = {{0,0}, {0,1}, {1,0}, {2,2}};
      layout (ar, progression, b, s);
      solve_d2c1 (s, &sum);

      if (valid_d2c1 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][1])
    {
      int ar[5][2] = {{2,0}, {1,1}, {0,2}, {2,1}, {1,2}};
      int xr[4][2] = {{0,0}, {0,1}, {1,0}, {2,2}};
      layout (ar, progression, b, s);
      solve_d2c2 (s, &sum);

      if (valid_d2c2 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][2])
    {
      int ar[5][2] = {{0,1}, {2,2}, {1,0}, {2,1}, {1,2}};
      int xr[4][2] = {{0,0}, {0,2}, {1,1}, {2,0}};
      layout (ar, progression, b, s);
      solve_d2c3 (s, &sum);

      if (valid_d2c3 (s, sum))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][3])
    {
      int ar[5][2] = {{1,0}, {2,2}, {0,1}, {1,2}, {2,1}};
      int xr[4][2] = {{0,0}, {0,2}, {1,1}, {2,0}};
      layout (ar, progression, b, s);
      solve_d2c4 (s, &sum);

      if (valid_d2c4 (s, sum))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][4])
    {
      int ar[5][2] = {{0,1}, {2,2}, {1,0}, {0,2}, {2,0}};
      int xr[4][2] = {{0,0}, {1,1}, {1,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d2c5 (s, &sum);

      if (valid_d2c5 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }
  if (app->generate[1][5])
    {
      int ar[5][2] = {{1,0}, {2,2}, {0,1}, {2,0}, {0,2}};
      int xr[4][2] = {{0,0}, {1,1}, {1,2}, {2,1}};
      layout (ar, progression, b, s);
      solve_d2c6 (s, &sum);

      if (valid_d2c6 (s))
        {
          if (check_squares (xr, s))
            dump_square (app, s);
        }
    }

  mpz_clear (sum);
}
*/

static void
apply_percent (mpf_t percent, mpz_t *high, mpz_t distance)
{
  // 0 -> distance -> distance * distance
  // 0 percent is distance, 1.0 is distance * distance
  mpz_t distance2subdistance;
  mpz_init (distance2subdistance);
  mpz_mul (distance2subdistance, distance, distance);
  mpz_sub (distance2subdistance, distance2subdistance, distance);
  mpf_t n, r;
  mpf_inits (n, r, NULL);
  mpf_set_z (n, distance2subdistance);
  mpf_mul (r, n, percent);
  mpz_set_f (*high, r);
  mpz_add (*high, *high, distance);
  mpf_clears (n, r, NULL);
  mpz_clear (distance2subdistance);
}

static void
handle_progression (struct fv_app_3sq_search_coords_t *app, mpz_t *progression, mpz_t diff, mpz_t *b, mpz_t (*s)[3][3])
{

  mpz_t high, low, twodiff;
  mpz_inits (high, low, twodiff, NULL);
  mpz_mul (high, diff, diff);
  mpz_set_ui (low, 0);
  mpf_t m;
  mpf_init (m);
  if (mpf_cmp_ui (app->max, 1) != 0)
    {
      if (app->max_strat == MAX_STRAT_AUTO)
        {
          get_auto_max_percentage (&high, &m);
          apply_percent (m, &high, diff);
        }
      else
        apply_percent (app->max, &high, diff);
      //display_textual_number (&high, stdout);
      if (mpz_cmp (high, app->max_floor) > 0 &&
          mpz_cmp_ui (app->max_floor, 0) != 0)
        mpz_set (high, app->max_floor);
    }

  if (mpf_cmp_ui (app->min, 0) != 0)
    apply_percent (app->min, &low, diff);
  if (app->do_dist)
    search (app, progression, high, low, diff, b, s, fill_distdown);

  mpz_add (twodiff, diff, diff);
  if (mpf_cmp_ui (app->max, 1) != 0)
    {
      apply_percent (app->max, &high, twodiff);
      //display_textual_number (&high, stdout);
      if (mpz_cmp (high, app->max_floor) > 0 &&
          mpz_cmp_ui (app->max_floor, 0) != 0)
        mpz_set (high, app->max_floor);
    }

  if (mpf_cmp_ui (app->min, 0) != 0)
    apply_percent (app->min, &low, twodiff);
  if (app->do_two_dist)
    search (app, progression, high, low, twodiff, b, s, fill_twodistdown);

  mpf_clear (m);
  mpz_clears (high, low, twodiff, NULL);
  return;
}

static void*
process_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_3sq_search_coords_t *app =
    (struct fv_app_3sq_search_coords_t *) param->data;
  mpz_t progression[3], b[2], s[3][3], d;

  char *line = NULL;
  size_t len = 0;
  mpz_init (d);
  for (int i = 0; i < 3; i++)
    mpz_init (progression[i]);
  for (int j = 0; j < 3; j++)
    for (int k = 0; k < 3; k++)
      mpz_init (s[j][k]);
  for (int j = 0; j < 2; j++)
    mpz_init (b[j]);
  while (1)
    {
      //go get the next record to work on
      pthread_mutex_lock (&read_lock);
      if (app->read_tuple (app->infile, progression, &line, &len) == -1)
        {
          pthread_mutex_unlock (&read_lock);
          break;
        }

      pthread_mutex_unlock (&read_lock);

      mpz_sub (d, progression[1], progression[0]);
      if (mpz_cmp_ui (d, 0) > 0)
        handle_progression (app, progression, d, b, &s);
    }
  free (line);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  for (int i = 0; i < 2; i++)
    mpz_clear (b[i]);

  for (int i = 0; i < 3; i++)
    mpz_clear (progression[i]);

  mpz_clear (d);
  return NULL;
}

int
fituvalu_3sq_search_coords (struct fv_app_3sq_search_coords_t *app)
{
  run_threads (app, app->threads, process_record);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "generate", OPT_GENERATE, "TYPE[,TYPE]", 0, "Only search for magic squares of the given TYPEs"},
  { "min", 'm', "PERC", OPTION_HIDDEN, "Set minimum search bounds (Default 0)"},
  { "max", 'x', "auto|PERC", 0, "Set maximum search bounds (Default 100)"},
  { "floor", 'f', "NUM", OPTION_HIDDEN, "truncate max to NUM"},
  { "show", 's', "TYPE", 0, "Display the layout of TYPE"},
  { "step", 'S', "NUM", OPTION_HIDDEN, "do every NUM squares instead of 1"},
  { "offset", 'O', "NUM", OPTION_HIDDEN, "start at NUM squares ahead of the beginning"},
  { 0 }
};

static int
parse_generation_string (struct argp_state *state, char *str)
{
  struct fv_app_3sq_search_coords_t *app = (struct fv_app_3sq_search_coords_t *) state->input;
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;
  /* mixing it up by using fmemopen */
  FILE *fp = fmemopen (str, strlen (str) + 1, "r");
  while ((nread = getdelim (&line, &len, ',', fp)) != -1)
    {
      size_t n = strspn (" \t", line);
      char *s = strdup (&line[n]);
      if (s[strlen (s) - 1] == ',')
        s[strlen (s) - 1] = '\0';
      int dist, coordset;
      int retval = sscanf (s, "d%dc%d", &dist, &coordset);
      if (retval != 2)
        argp_error (state, "malformed type sequence `%s'", s);
      if (dist != 1 && dist != 2)
        argp_error (state, "malformed type sequence `%s'", s);
      if (dist == 1)
        {
          if (coordset < 1 || coordset > 8)
            argp_error (state, "malformed type sequence `%s'", s);
          app->do_dist = 1;
        }
      else
        {
          if (coordset < 1 || coordset > 6)
            argp_error (state, "malformed type sequence `%s'", s);
          app->do_two_dist = 1;
        }

      app->generate[dist-1][coordset-1] = 1;
      free (s);
    }
  fclose (fp);
  return 1;
}

static void
dump_type (FILE *out, int ar[5][2])
{
  char s[3][3];
  memset (s, 32, sizeof (s));
  char letter = 'A';
  for (int i = 0; i < 5; i++)
    {
      s[ar[i][0]][ar[i][1]] = letter;
      letter++;
    }
  fprintf (out, "  +---+---+---+\n");
  fprintf (out, "  | %c | %c | %c |\n", s[0][0], s[0][1], s[0][2]);
  fprintf (out, "  +---+---+---+\n");
  fprintf (out, "  | %c | %c | %c |\n", s[1][0], s[1][1], s[1][2]);
  fprintf (out, "  +---+---+---+\n");
  fprintf (out, "  | %c | %c | %c |\n", s[2][0], s[2][1], s[2][2]);
  fprintf (out, "  +---+---+---+\n");
}

static int
show_type (FILE *out, char *arg)
{
  if (strcmp (arg, "d1c1") == 0)
    {
      int ar[5][2] = {{1,0}, {1,1}, {1,2}, {2,0}, {2,2}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d1c2") == 0)
    {
      int ar[5][2] = {{1,2}, {1,1}, {1,0}, {2,2}, {2,0}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d1c3") == 0)
    {
      int ar[5][2] = {{1,0}, {0,2}, {2,1}, {1,2}, {2,0}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d1c4") == 0)
    {
      int ar[5][2] = {{2,1}, {0,2}, {1,0}, {2,0}, {1,2}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d1c5") == 0)
    {
      int ar[5][2] = {{1,0}, {0,2}, {2,1}, {1,1}, {2,2}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d1c6") == 0)
    {
      int ar[5][2] = {{2,1}, {0,2}, {1,0}, {2,2}, {1,1}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d1c7") == 0)
    {
      int ar[5][2] = {{0,2}, {1,1}, {2,0}, {2,2}, {1,0}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d1c8") == 0)
    {
      int ar[5][2] = {{2,0}, {1,1}, {0,2}, {1,0}, {2,2}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d2c1") == 0)
    {
      int ar[5][2] = {{0,2}, {1,1}, {2,0}, {1,2}, {2,1}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d2c2") == 0)
    {
      int ar[5][2] = {{2,0}, {1,1}, {0,2}, {2,1}, {1,2}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d2c3") == 0)
    {
      int ar[5][2] = {{0,1}, {2,2}, {1,0}, {2,1}, {1,2}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d2c4") == 0)
    {
      int ar[5][2] = {{1,0}, {2,2}, {0,1}, {1,2}, {2,1}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d2c5") == 0)
    {
      int ar[5][2] = {{0,1}, {2,2}, {1,0}, {0,2}, {2,0}};
      dump_type (out, ar);
    }
  else if (strcmp (arg, "d2c6") == 0)
    {
      int ar[5][2] = {{1,0}, {2,2}, {0,1}, {2,0}, {0,2}};
      dump_type (out, ar);
    }
  else
    return 0;
  return 1;
}
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_3sq_search_coords_t *app = (struct fv_app_3sq_search_coords_t *) state->input;
  switch (key)
    {
    case 's':
      if (show_type (stdout, arg) == 0)
        argp_error (state, "Invalid value `%s' for --show.", arg);
      exit (0);
      break;
    case 'S':
      app->step = atoi (arg);
      if (app->step <= 0)
        argp_error (state, "Invalid value `%s' for --step.", arg);
      break;
    case 'O':
      app->offset = atoi (arg);
      if (app->offset < 0)
        argp_error (state, "Invalid value `%s' for --offset.", arg);
      break;
    case 'f':
      mpz_set_str (app->max_floor, arg, 10);
      break;
    case 'm':
      if (mpf_set_str (app->min, arg, 10))
        argp_error (state, "Invalid value `%s' for --minimum.", arg);
      if (mpf_cmp_ui (app->min, 0) < 0)
        argp_error (state, "Invalid value `%s' for --minimum.", arg);
      mpf_div_ui (app->min, app->min, 100);
      break;
    case 'x':
      if (strcmp (arg, "auto") == 0)
        app->max_strat = MAX_STRAT_AUTO;
      else
        {
          if (mpf_set_str (app->max, arg, 10))
            argp_error (state, "Invalid value `%s' for --maximum.", arg);
          if (mpf_cmp_ui (app->max, 0) < 0)
            argp_error (state, "Invalid value `%s' for --maximum.", arg);
          mpf_div_ui (app->max, app->max, 100);
        }
      break;
    case OPT_GENERATE:
      parse_generation_string (state, arg);
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->numargs > 1)
        argp_error (state, "too many arguments.");
      app->numargs++;
      if (strcmp (arg, "-") == 0)
        app->infile = stdin;
      else
        app->infile = fopen (arg, "r");
      if (app->infile == NULL)
        argp_error (state, "could not opening `%s' for reading");
      break;
    case ARGP_KEY_INIT:
      app->max_strat = MAX_STRAT_NONE;
      mpz_init (app->max_floor);
      mpf_inits (app->min, app->max, NULL);
      mpf_set_ui (app->max, 1);
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
        {
          if (mpf_cmp (app->min, app->max) > 0)
            argp_error (state, "--maximum is less than --minimum");
          int generate[2][8];
          memset (generate, 0, sizeof (generate));
          if (memcmp (generate, app->generate, sizeof (generate)) == 0)
            {
              for (int i = 0; i < 2; i++)
                for (int j = 0; j < 8; j++)
                  app->generate[i][j] = 1;
              app->generate[0][1] = 0;
              app->generate[1][1] = 0;
              app->generate[1][3] = 0;
              app->generate[1][5] = 0;
              //app->generate[1][7] = 0;
              app->do_dist = 1;
              app->do_two_dist = 1;
            }
        }
      break;
    }
  return 0;
}

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "There are 14 ways to generate 3x3 magic squares with 5 or more perfect squares, by laying out a three square progression and then iterating over two cells.  This program reads in a series of three square progressions, iterates over two cells, then does the 14 transformations and then checks to see if a 6th perfect square was created.\vSuitable TYPE values for --generate are: d1c1 d1c2 d1c3 d1c4 d1c5 d1c6 d1c7 d1c8 d2c1 d2c2 d2c3 d2c4 d2c5 d2c6.  `d1c1' means distance 1, coordinate set 1.  d2 means two distances (e.g. difference between squares).  Different coordinate sets find a variety of magic square types.  When FILE is not specified or it is `-', the three square progressions are read from the standard input.  Using a value of `auto' for --max means a suitable value will be used given the number of digits in the third member of the three square progression.", 0
};

int
main (int argc, char **argv)
{
  struct fv_app_3sq_search_coords_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.read_tuple = read_three_numbers_from_stream;
  app.out = stdout;
  app.infile = stdin;
  app.threads = 1;
  app.step = 1;
  app.offset = 0;
  argp_parse (&argp, argc, argv, 0, 0, &app);

  return fituvalu_3sq_search_coords (&app);
}
