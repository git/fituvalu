/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_613_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  int chance_of_seven;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int threesq;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, sum, sum_minus_middle, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t i, iroot, j, twoiroot, limit;
};


static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_613_type_1 (struct fv_app_search_613_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  int dup = 0;
  //     +---+---+---+
  //     |   |   | A |
  //     +---+---+---+
  //     | D | B |   |
  //     +---+---+---+
  //     | C |   | E |
  //     +---+---+---+

  //we start at distance counting up, and checking downwards for squares
  mpz_set (q->limit, w->sum);

  mpz_sqrt (q->iroot, w->distance);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  mpz_set ((*s)[2][0], w->hi);
  mpz_set ((*s)[0][2], w->lo);
  while (1)
    {

      mpz_set ((*s)[2][2], q->i);

      mpz_sub (q->j, q->i, w->distance);

      mpz_add ((*s)[0][1], (*s)[2][2], w->distance);

      mpz_set ((*s)[1][0], q->j);

      mpz_sub ((*s)[0][0], w->sum_minus_middle, (*s)[2][2]);
      mpz_sub ((*s)[1][2], w->sum_minus_middle, (*s)[1][0]);
      mpz_sub ((*s)[2][1], w->sum_minus_middle, (*s)[0][1]);

      //check for dups
      dup = 0;
      if (mpz_cmp ((*s)[1][1], (*s)[2][2]) == 0)
        dup = 1;
      else if (app->chance_of_seven &&
               mpz_sgn ((*s)[0][0]) < 0 &&
               mpz_sgn ((*s)[1][2]) < 0 &&
               mpz_sgn ((*s)[2][1]) < 0)
        dup = 1;
      if (!dup)
        {
          if (mpz_perfect_square_p ((*s)[0][1]) &&
              mpz_perfect_square_p ((*s)[1][0]))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
handle_progression (struct fv_app_search_613_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  generate_613_type_1 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_613_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_mul_ui (w->sum, (*s)[1][1], 3);
  mpz_mul_ui (w->sum_minus_middle, (*s)[1][1], 2);

  mpz_cdiv_q_ui (w->limit, (*s)[1][1], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[1][1], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[1][1], w->distance);
          mpz_add (w->hi, (*s)[1][1], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_613_t *app =
    (struct fv_app_search_613_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.twoiroot, q.limit, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[1][1], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[1][1], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[1][1], 1) > 0)
        generate_progressions (app, &w, &q, &s);
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (q.i, q.iroot, q.j, q.twoiroot, q.limit, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_613_t *app =
    (struct fv_app_search_613_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.twoiroot, q.limit, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[1][1], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[1][1], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[1][1], w.lo);
      mpz_mul_ui (w.sum, s[1][1], 3);
      mpz_mul_ui (w.sum_minus_middle, s[1][1], 2);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (q.i, q.iroot, q.j, q.twoiroot, q.limit, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_613 (struct fv_app_search_613_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_613_t *app = (struct fv_app_search_613_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case '3':
      app->threesq = 1;
      break;
    case 'N':
      app->chance_of_seven = 0;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "allow-three-negatives", 'N', 0, OPTION_HIDDEN, "Allow squares with three negative numbers"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:13 given a FILE containing center values.\vWhen FILE is not provided, it is read from the standard input.  Magic squares of type 6:13 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   | X | X |   |   | F | A |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating E\n"
"  | X | X |   |   | D | B |   |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below E.\n"
"  | X |   | X |   | C |   | E |   F is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_613_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.chance_of_seven = 1;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_613 (&app);
}
/*

the point of this is to identify information that can help us get more 6:13s.  we want to be able to predict 'center' values (the B values) for 6:13 magic squares.

  +---+---+---+   +---+---+---+
  |   | X | X |   |   | F | A |
  +---+---+---+   +---+---+---+
  | X | X |   |   | D | B |   |
  +---+---+---+   +---+---+---+
  | X |   | X |   | C |   | E |
  +---+---+---+   +---+---+---+

we identified which B values lead to magic squares with 6 perfect squares in the 6:13 configuration, and then here we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:13 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 4238 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

120, 0, 48, 352 / 37696 = 0.009338
120, 48, 0, 352 / 37696 = 0.009338
112, 8, 48, 532 / 56522 = 0.009412
0, 64, 104, 358 / 37700 = 0.009496
64, 0, 104, 358 / 37700 = 0.009496
64, 104, 0, 358 / 37700 = 0.009496
64, 48, 56, 538 / 56524 = 0.009518
0, 112, 56, 364 / 37705 = 0.009654
112, 0, 56, 364 / 37705 = 0.009654
112, 56, 0, 364 / 37705 = 0.009654

well we're almost at a percent which is nice, and the frequent sum is the same among all of them: 168.  plenty of zeroes and 112s.
it doesn't really matter which one we pick here, but we want to pick a tuple so we'll go with "64, 48, 56".

second run is depth of 2, and breadth of 2000.

1156, 1784, 31 / 2174 = 0.014259
1408, 1952, 28 / 1909 = 0.014667
1072, 1364, 39 / 2627 = 0.014846
1680, 840, 38 / 2543 = 0.014943
1960, 1148, 31 / 2064 = 0.015019
840, 1596, 40 / 2627 = 0.015226
1176, 1008, 45 / 2935 = 0.015332
1200, 1740, 35 / 2183 = 0.016033
1260, 1680, 37 / 2186 = 0.016926
1800, 1140, 37 / 2182 = 0.016957

hmm the pecentage is about 2 times higher.  down from 538 hits to 37 though.
there's a frequent sum of 2940.  the best pair seems to be "1800, 1140".

and just for kicks, let's try a depth of 1 and a breadth of 100000.

77162, 3 / 44 = 0.068182
61752, 4 / 56 = 0.071429
85100, 3 / 41 = 0.073171
92856, 3 / 38 = 0.078947
99288, 3 / 35 = 0.085714

i'm sure we're just in la-la guessing land here.
but maybe 99288 is worth trying.

let's say the minimum count has to be 10 or higher.

3080, 14 / 1040 = 0.013462
3640, 12 / 881 = 0.013621
1680, 26 / 1908 = 0.013627
840, 54 / 3810 = 0.014173
4368, 11 / 735 = 0.014966
3360, 15 / 957 = 0.015674
2940, 18 / 1093 = 0.016468
5124, 11 / 629 = 0.017488
5460, 12 / 592 = 0.020270
5880, 12 / 550 = 0.021818

5880 is the best i guess.  840 is a standout though.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

1, 1, 1, 4238 / 3161952 = 0.001340
1, 1, 0, 4238 / 3161952 = 0.001340
1, 0, 1, 4238 / 3161952 = 0.001340
1, 0, 0, 4238 / 3161952 = 0.001340
0, 1, 1, 4238 / 3161952 = 0.001340
0, 1, 0, 4238 / 3161952 = 0.001340
0, 0, 1, 4238 / 3161952 = 0.001340
1, 1, 2, 3888 / 2371464 = 0.001639

stinker.  gotta go up by 1s to get them all.

results:

so looking forward for more sixes, pick one of "99288", "92856", or "85100".  Try "5880", and "1800, 1140", and "64, 48, 56".

and we can't recalculate any quicker by using a pattern.  we gotta check every square to get them all.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 841 in sq-seq-finder.

84, 8, 76, 429 / 56506 = 0.007592
36, 104, 112, 288 / 37699 = 0.007639
92, 48, 28, 433 / 56514 = 0.007662
36, 48, 84, 435 / 56514 = 0.007697
36, 104, 28, 439 / 56521 = 0.007767
116, 112, 120, 214 / 27301 = 0.007839
84, 56, 112, 296 / 37704 = 0.007851
84, 56, 28, 445 / 56519 = 0.007873
92, 48, 112, 298 / 37688 = 0.007907
116, 120, 112, 226 / 27307 = 0.008276

there are all kinds of frequent sums here. nearly 1 percent is okay.
"116, 120, 112" is the best.

no arithmetic progressions.

we did a little worse a different starting point in terms of percentage.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

1508, 1972, 25 / 1840 = 0.013587
1232, 1708, 30 / 2181 = 0.013755
1772, 1168, 30 / 2179 = 0.013768
1932, 1176, 29 / 2062 = 0.014064
696, 1740, 38 / 2628 = 0.014460
1508, 928, 38 / 2626 = 0.014471
1856, 580, 43 / 2627 = 0.016368
1044, 580, 65 / 3938 = 0.016506
1044, 1392, 47 / 2634 = 0.017844
812, 1624, 48 / 2636 = 0.018209

well 2436 is a frequent sum. (812+1624+48)
the best of the bunch is "812, 1624". but it seems bad because 1624/2=812.

we actually did a little bit better than earlier too.

okay let's do depth=1, and breadth is 100000

97502, 3 / 36 = 0.083333
99132, 3 / 35 = 0.085714
99260, 3 / 35 = 0.085714
99284, 3 / 35 = 0.085714
52780, 6 / 66 = 0.090909
87164, 4 / 41 = 0.097561

9 percent is in the good category, and 52780 jumps way out.
does it pan out? probably not.

let's do depth=1, breadth is 100000, min hits of 10

812, 50 / 3931 = 0.012719
3150, 13 / 1017 = 0.012783
1508, 28 / 2121 = 0.013201
4620, 10 / 695 = 0.014388
2436, 19 / 1317 = 0.014427
5075, 10 / 634 = 0.015773
4524, 12 / 711 = 0.016878
5460, 11 / 591 = 0.018613
4060, 16 / 794 = 0.020151
6300, 12 / 514 = 0.023346

6300 is a nice round number.  812 is a standout.

so overall, looking forward for more sixes, pick one of "99288", "92856", or "85100", and then one of "52780", "99284", or "87164".  Try "1800, 1140", "64, 48, 56", "812, 1624", and "116, 120, 112".  and throw in "5880" and "6300".

okay, let's do the first run again but sort on hits to see how it shakes out.

1, 1, 1, 4238 / 3161925 = 0.001340
1, 1, 0, 4238 / 3161925 = 0.001340
1, 0, 1, 4238 / 3161925 = 0.001340
1, 0, 0, 4238 / 3161925 = 0.001340
0, 1, 1, 4238 / 3161925 = 0.001340
0, 1, 0, 4238 / 3161925 = 0.001340
0, 0, 1, 4238 / 3161925 = 0.001340
1, 1, 2, 3888 / 2371444 = 0.001640

no better than before. gotta go up by 1s to get them all.

this concludes our analysis of anticipating B values in 6:13 magic squares.

in order to get high value 6:13s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

also we could consider running this test again but including all D values as B values.

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="99288" 1 10000000000000000000000000000 | ./search-613
sq-seq --pattern="92856" 1 10000000000000000000000000000 | ./search-613
sq-seq --pattern="85100" 1 10000000000000000000000000000 | ./search-613
sq-seq --pattern="1800,1140" 1 10000000000000000000000000000 | ./search-613
sq-seq --pattern="64,48,56" 1 10000000000000000000000000000 | ./search-613
sq-seq --pattern="5880" 1 10000000000000000000000000000 | ./search-613

sq-seq --pattern="52780" 841 10000000000000000000000000000 | ./search-613
sq-seq --pattern="99284" 841 10000000000000000000000000000 | ./search-613
sq-seq --pattern="87164" 841 10000000000000000000000000000 | ./search-613
sq-seq --pattern="812,1624" 841 10000000000000000000000000000 | ./search-613
sq-seq --pattern="116,120,112" 841 10000000000000000000000000000 | ./search-613
sq-seq --pattern="6300" 841 10000000000000000000000000000 | ./search-613
*/
