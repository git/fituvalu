#include <stdio.h>
#include <stdlib.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include "magicsquareutil.h"

struct fv_app_filter_pythagorean_factors_t
{
  unsigned long long int *divisors;
  int num_divisors;
  int num_factors;
  int invert;
  FILE *out;
  int in_binary;
  int out_binary;
  FILE *infile;
  int sqrt;
};

extern char *prime_numbers_4n1[];
static int
fourn1 (unsigned long long i)
{
  return (i - 1) % 4 == 0;
}
static void
load_primes (struct fv_app_filter_pythagorean_factors_t *app)
{
  unsigned long long int num;
  char **n = prime_numbers_4n1, *end = NULL;
  while (*n != 0)
    {
      num = strtoull (*n, &end, 10);
      if (fourn1 (num))
        {
          app->divisors =
            realloc (app->divisors, (app->num_divisors + 1) *
                     sizeof (unsigned long long int));

          app->divisors[app->num_divisors] = num;
          app->num_divisors++;
        }
      *n++;
    }
}

static int
has_num_py_factors (struct fv_app_filter_pythagorean_factors_t *app, unsigned long long int num)
{
  int count = 0;
  int start = 0;
  unsigned long long int limit = num / 2;
  unsigned long long root = sqrtl (num);
  if (app->sqrt)
    {
      for (int i = 0; i < app->num_divisors; i++)
        {
          if (app->divisors[i] >= root)
            {
              start = i;
              break;
            }
        }
    }
  else
    limit = root;
  for (int i = start; i < app->num_divisors; i++)
    {
      if (app->divisors[i] > limit)
        break;
      if (num % app->divisors[i] == 0)
        {
          count++;
          if (count >= app->num_factors)
            break;
        }
    }
  return count >= app->num_factors;
}

static int
fituvalu_filter_pythagorean_factors_filter (struct fv_app_filter_pythagorean_factors_t *app, FILE *in, FILE *out)
{
  ssize_t read;
  char *line = NULL;
  size_t len = 0;
  char *end = NULL;
  unsigned long long int i;

  if (app->num_factors > 0)
    load_primes (app);
  while (1)
    {
      unsigned long long int ii;
      if (app->in_binary)
        read = fread (&ii, sizeof (ii), 1, in);
      else
        read = fv_getline (&line, &len, in);
      if (read == -1)
        break;
      else if (!app->in_binary)
        ii = strtoull (line, &end, 10);
      i = ii;
      if (!app->in_binary)
        {
          if (end == NULL || *end != '\n')
            continue;
        }

      if (app->num_factors == 0)
        {
          if ((i - 1) % 4 == 0)
            {
              //okay but is it prime
              int flag = 0;
              if (i <= 1)
                flag = 1;
              for (int j = 2; j <= i / 2; ++j)
                {
                  if (i % j == 0)
                    {
                      flag = 1;
                      break;
                    }
                }
              if ((!app->invert && !flag) ||
                  (app->invert && flag))
                {
                  if (app->out_binary)
                    fwrite (&i, sizeof (i), 1, out);
                  else
                    {
                      if (app->in_binary)
                        fprintf (out, "%llu\n", i);
                      else
                        fprintf (out, "%s", line);
                    }
                  fflush (out);
                }
            }
        }
      else
        {
          //okay does it have num_factors pythagorean factors?
          if (has_num_py_factors (app, i))
            {
              if (app->out_binary)
                fwrite (&i, sizeof (i), 1, out);
              else
                {
                  if (app->in_binary)
                    fprintf (out, "%llu\n", i);
                  else
                    fprintf (out, "%s", line);
                }
              fflush (out);
            }
        }

    }
  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_filter_pythagorean_factors_t *app = (struct fv_app_filter_pythagorean_factors_t *) state->input;
  switch (key)
    {
    case 's':
      app->sqrt = 1;
      break;
    case 'f':
      app->num_factors = atoi (arg);
      if (app->num_factors <= 0)
        argp_error (state, "invalid argument for --factor");
      break;
    case 'v':
      app->invert = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->out_binary = 1;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "invert", 'v', 0, OPTION_HIDDEN, "Invert the result", 1 },
  { "out-binary", 'o', 0, 0, "Output raw unsigned long longs instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text (with -f)"},
  { "filter", 'f', "NUM", 0, "Show numbers that have NUM or more factors"},
  { "sqrt", 's', 0, OPTION_HIDDEN, "Go from sqrt(n) to n/2"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "NUM",
  "Accept numbers on the standard input and display the ones whose factors are only pythagorean primes.\vThe following types have this kind of number as a centermost value: 6:1, 6:3, 6:6, 6:7, 6:8, 6:9, 6:10, 6:11, 6:12, 6:14, 6:15, and 6:16.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_filter_pythagorean_factors_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.infile = stdin;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_filter_pythagorean_factors_filter (&app, app.infile, app.out);
}
