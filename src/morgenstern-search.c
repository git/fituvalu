/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <gmp.h>
#include <pthread.h>
#include "magicsquareutil.h"

#define OPT_INT -644

struct fv_app_morgenstern_search_t;
static int
no_filter (mpz_t a[3][3], int num)
{
  return 1;
}

struct fv_app_morgenstern_search_t
{
  int alt;
  int active[8];
  int (*order[8]) (struct fv_app_morgenstern_search_t *,
                   struct morgenstern_per_thread_data_t *);
  int type[8];
  int max_order;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  void (*display_record) (mpz_t a, mpz_t b, mpz_t c, FILE *out);
  int inmem;
  FILE *infile;
  int in_binary;
  int filter_num_squares;
  int num_args;
  mpz_t max;
  int (*filter_square1) (mpz_t a[3][3],int);
  int (*filter_square2) (mpz_t a[3][3],int);
  int (*filter_square3) (mpz_t a[3][3],int);
  int (*filter_square4) (mpz_t a[3][3],int);
  int (*filter_square5) (mpz_t a[3][3],int);
  int (*filter_square6) (mpz_t a[3][3],int);
  int (*filter_square7) (mpz_t a[3][3],int);
  int (*filter_square8) (mpz_t a[3][3],int);
  FILE *out;
  int threads;
  int threesq;
  pthread_mutex_t display_lock;
  int ul;
};

static int
morgenstern_type_1 (struct fv_app_morgenstern_search_t *app,
                    struct morgenstern_per_thread_data_t *pt)
{
  if (app->alt)
    morgenstern_type_1_calc_step_1b (pt);
  else
    morgenstern_type_1_calc_step_1 (pt);
  morgenstern_type_1_calc_step_2 (pt);
  return app->filter_square1 (pt->a, app->filter_num_squares);
}

static int
morgenstern_type_2 (struct fv_app_morgenstern_search_t *app,
                    struct morgenstern_per_thread_data_t *pt)
{
  morgenstern_type_2_calc_step_1 (pt);
  morgenstern_type_2_calc_step_2 (pt);
  return app->filter_square2 (pt->a, app->filter_num_squares);
}

static int
morgenstern_type_3 (struct fv_app_morgenstern_search_t *app,
                    struct morgenstern_per_thread_data_t *pt)
{
  morgenstern_type_3_calc_step_1 (pt);
  morgenstern_type_3_calc_step_2 (pt);
  return app->filter_square3 (pt->a, app->filter_num_squares);
}

static int
morgenstern_type_4 (struct fv_app_morgenstern_search_t *app,
                    struct morgenstern_per_thread_data_t *pt)
{
  morgenstern_type_4_calc_step_1 (pt);
  morgenstern_type_4_calc_step_2 (pt);
  return app->filter_square4 (pt->a, app->filter_num_squares);
}

static int
morgenstern_type_5 (struct fv_app_morgenstern_search_t *app,
                    struct morgenstern_per_thread_data_t *pt)
{
  morgenstern_type_5_calc_step_1 (pt);
  morgenstern_type_5_calc_step_2 (pt);
  return app->filter_square5 (pt->a, app->filter_num_squares);
}

static int
morgenstern_type_6 (struct fv_app_morgenstern_search_t *app,
                    struct morgenstern_per_thread_data_t *pt)
{
  morgenstern_type_6_calc_step_1 (pt);
  morgenstern_type_6_calc_step_2 (pt);
  return app->filter_square6 (pt->a, app->filter_num_squares);
}

static int
morgenstern_type_7 (struct fv_app_morgenstern_search_t *app,
                    struct morgenstern_per_thread_data_t *pt)
{
  morgenstern_type_7_calc_step_1 (pt);
  morgenstern_type_7_calc_step_2 (pt);
  return app->filter_square7 (pt->a, app->filter_num_squares);
}

static int
morgenstern_type_8 (struct fv_app_morgenstern_search_t *app,
                    struct morgenstern_per_thread_data_t *pt)
{
  morgenstern_type_8_calc_step_1 (pt);
  morgenstern_type_8_calc_step_2 (pt);
  return app->filter_square8 (pt->a, app->filter_num_squares);
}

static void
search_3sq (mpz_t m1, mpz_t n1, mpz_t m2, mpz_t n2, void *data, void *p)
{
  struct morgenstern_per_thread_data_t *pt = (struct morgenstern_per_thread_data_t *)p;
  struct fv_app_morgenstern_search_t *app = (struct fv_app_morgenstern_search_t *) data;
  morgenstern_precalc (m1, n1, m2, n2, pt);

  if (app->active[0])
    {
      morgenstern_type_1_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[2][2], pt->a[1][1], pt->a[0][0], app->out);
      app->display_record (pt->a[2][0], pt->a[1][1], pt->a[0][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[1])
    {
      morgenstern_type_2_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[0][1], pt->a[1][1], pt->a[2][1], app->out);
      app->display_record (pt->a[1][0], pt->a[1][1], pt->a[1][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[2])
    {
      morgenstern_type_3_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[0][1], pt->a[1][1], pt->a[2][1], app->out);
      app->display_record (pt->a[2][2], pt->a[1][1], pt->a[0][0], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[3])
    {
      morgenstern_type_4_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[0][1], pt->a[2][2], pt->a[1][0], app->out);
      app->display_record (pt->a[0][1], pt->a[2][0], pt->a[1][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[4])
    {
      morgenstern_type_5_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[1][0], pt->a[0][2], pt->a[2][1], app->out);
      app->display_record (pt->a[1][2], pt->a[0][0], pt->a[2][1], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[5])
    {
      morgenstern_type_6_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[1][2], pt->a[0][0], pt->a[2][1], app->out);
      app->display_record (pt->a[0][1], pt->a[2][0], pt->a[1][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[6])
    {
      morgenstern_type_7_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[1][0], pt->a[0][2], pt->a[2][1], app->out);
      app->display_record (pt->a[0][1], pt->a[2][2], pt->a[1][0], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[7])
    {
      morgenstern_type_8_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[2][1], pt->a[1][1], pt->a[0][1], app->out);
      app->display_record (pt->a[2][0], pt->a[1][1], pt->a[0][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }
}

static void
search (mpz_t m1, mpz_t n1, mpz_t m2, mpz_t n2, void *data, void *p)
{
  struct morgenstern_per_thread_data_t *pt =
    (struct morgenstern_per_thread_data_t *) p;
  struct fv_app_morgenstern_search_t *app =
    (struct fv_app_morgenstern_search_t *) data;
  morgenstern_precalc (m1, n1, m2, n2, pt);

  pt->prev_type = -1;
  for (int i = 0; i < app->max_order; i++)
    {
      int display = (*app->order[i])(app, pt);
      pt->prev_type = app->type[i];
      if (display)
        {
          if (app->threads > 1)
            pthread_mutex_lock(&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock(&app->display_lock);
        }
    }
}

static void
search_3sq_int (unsigned long m1, unsigned long n1, unsigned long m2, unsigned long n2, void *data, void *p)
{
  struct morgenstern_per_thread_data_t *pt = (struct morgenstern_per_thread_data_t *)p;
  struct fv_app_morgenstern_search_t *app = (struct fv_app_morgenstern_search_t *) data;
  morgenstern_precalc_int (m1, n1, m2, n2, pt);

  if (app->active[0])
    {
      morgenstern_type_1_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[2][2], pt->a[1][1], pt->a[0][0], app->out);
      app->display_record (pt->a[2][0], pt->a[1][1], pt->a[0][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[1])
    {
      morgenstern_type_2_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[0][1], pt->a[1][1], pt->a[2][1], app->out);
      app->display_record (pt->a[1][0], pt->a[1][1], pt->a[1][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[2])
    {
      morgenstern_type_3_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[0][1], pt->a[1][1], pt->a[2][1], app->out);
      app->display_record (pt->a[2][2], pt->a[1][1], pt->a[0][0], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[3])
    {
      morgenstern_type_4_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[0][1], pt->a[2][2], pt->a[1][0], app->out);
      app->display_record (pt->a[0][1], pt->a[2][0], pt->a[1][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[4])
    {
      morgenstern_type_5_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[1][0], pt->a[0][2], pt->a[2][1], app->out);
      app->display_record (pt->a[1][2], pt->a[0][0], pt->a[2][1], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[5])
    {
      morgenstern_type_6_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[1][2], pt->a[0][0], pt->a[2][1], app->out);
      app->display_record (pt->a[0][1], pt->a[2][0], pt->a[1][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[6])
    {
      morgenstern_type_7_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[1][0], pt->a[0][2], pt->a[2][1], app->out);
      app->display_record (pt->a[0][1], pt->a[2][2], pt->a[1][0], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }

  if (app->active[7])
    {
      morgenstern_type_8_calc_step_1 (pt);
      if (app->threads > 1)
        pthread_mutex_lock(&app->display_lock);
      app->display_record (pt->a[2][1], pt->a[1][1], pt->a[0][1], app->out);
      app->display_record (pt->a[2][0], pt->a[1][1], pt->a[0][2], app->out);
      if (app->threads > 1)
        pthread_mutex_unlock(&app->display_lock);
    }
}

static void
search_int (unsigned long m1, unsigned long n1, unsigned long m2, unsigned long n2, void *data, void *p)
{
  struct morgenstern_per_thread_data_t *pt =
    (struct morgenstern_per_thread_data_t *) p;
  struct fv_app_morgenstern_search_t *app =
    (struct fv_app_morgenstern_search_t *) data;
  morgenstern_precalc_int (m1, n1, m2, n2, pt);

  pt->prev_type = -1;
  for (int i = 0; i < app->max_order; i++)
    {
      int display = (*app->order[i])(app, pt);
      pt->prev_type = app->type[i];
      if (display)
        {
          if (app->threads > 1)
            pthread_mutex_lock(&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock(&app->display_lock);
        }
    }
}

int
fituvalu_morgenstern_search (struct fv_app_morgenstern_search_t *app, FILE *in)
{
  if (app->in_binary)
    {
      if (app->inmem)
        morgenstern_search_dual_binary_mem (in, app->infile,
                                            app->threesq ? search_3sq : search,
                                            app->threads, morgenstern_per_thread_init, morgenstern_per_thread_destroy, app);
      else
        morgenstern_search_dual_binary (in, app->infile,
                                        app->threesq ? search_3sq: search,
                                        app->threads, morgenstern_per_thread_init, morgenstern_per_thread_destroy, app);
    }
  else if (app->ul)
    {
      if (app->inmem)
        morgenstern_search_dual_mem_int (in, app->infile,
                                        app->threesq ? search_3sq_int :search_int,
                                        app->threads, morgenstern_per_thread_init, morgenstern_per_thread_destroy, app);
      else
        morgenstern_search_dual_int (in, app->infile,
                                        app->threesq ? search_3sq_int: search_int,
                                        app->threads, morgenstern_per_thread_init, morgenstern_per_thread_destroy, app);
    }
  else
    {
      if (app->inmem)
        morgenstern_search_dual_mem (in, app->infile,
                                     app->threesq ? search_3sq : search,
                                     app->threads, morgenstern_per_thread_init, morgenstern_per_thread_destroy, app);
      else
        morgenstern_search_dual (in, app->infile,
                                 app->threesq ? search_3sq : search,
                                 app->threads, morgenstern_per_thread_init, morgenstern_per_thread_destroy, app);
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "alt", 'a', 0, OPTION_HIDDEN, "Calculate alternate type 1" },
  { "filter", 'f', "NUM", 0, "Only show magic squares with at least NUM perfect squares" },
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "mem", 'm', 0, 0, "Load numbers from FILE and stdin into memory"},
  { "threads", 't', "NUM", 0, "With -m, spread the work across NUM threads"},
  { NULL, '1', 0, 0, "Generate morgenstern type 1 magic squares"},
  { NULL, '2', 0, 0, "Generate morgenstern type 2 magic squares"},
  { NULL, '3', 0, 0, "Generate morgenstern type 3 magic squares"},
  { NULL, '4', 0, 0, "Generate morgenstern type 4 magic squares"},
  { NULL, '5', 0, 0, "Generate morgenstern type 5 magic squares"},
  { NULL, '6', 0, 0, "Generate morgenstern type 6 magic squares"},
  { NULL, '7', 0, 0, "Generate morgenstern type 7 magic squares"},
  { NULL, '8', 0, 0, "Generate morgenstern type 8 magic squares"},
  { "show", 's', "TYPE", 0, "Show the configuration for TYPE (1-8) and exit"},
  { "3sq", 'q', 0, 0, "Generate 3 square progressions instead of squares"},
  { "int", OPT_INT, 0, 0, "Use unsigned integers instead of GMP for m,n,p,r"},
  { 0 }
};

static void
show_configuration (int type, FILE *out)
{
  switch (type)
    {
    case 1:
      fprintf (out, "\
               +-------+-------+-------+\n\
               |  A^2  |       |  C^2  |\n\
               +-------+-------+-------+\n\
               |       |  E^2  |       |\n\
               +-------+-------+-------+\n\
               |  G^2  |       |  I^2  |\n\
               +-------+-------+-------+\n");
      break;
    case 2:
      fprintf (out, "\
               +-------+-------+-------+\n\
               |       |  B^2  |       |\n\
               +-------+-------+-------+\n\
               |  D^2  |  E^2  |  F^2  |\n\
               +-------+-------+-------+\n\
               |       |  H^2  |       |\n\
               +-------+-------+-------+\n");
      break;
    case 3:
      fprintf (out, "\
               +-------+-------+-------+\n\
               |  A^2  |  B^2  |       |\n\
               +-------+-------+-------+\n\
               |       |  E^2  |       |\n\
               +-------+-------+-------+\n\
               |       |  H^2  |  I^2  |\n\
               +-------+-------+-------+\n");
      break;
    case 4:
      fprintf (out, "\
               +-------+-------+-------+\n\
               |       |  B^2  |       |\n\
               +-------+-------+-------+\n\
               |  D^2  |       |  F^2  |\n\
               +-------+-------+-------+\n\
               |  G^2  |       |  I^2  |\n\
               +-------+-------+-------+\n");
      break;
    case 5:
      fprintf (out, "\
               +-------+-------+-------+\n\
               |  A^2  |       |  C^2  |\n\
               +-------+-------+-------+\n\
               |  D^2  |       |  F^2  |\n\
               +-------+-------+-------+\n\
               |       |  H^2  |       |\n\
               +-------+-------+-------+\n");
      break;
    case 6:
      fprintf (out, "\
               +-------+-------+-------+\n\
               |  A^2  |  B^2  |       |\n\
               +-------+-------+-------+\n\
               |       |       |  F^2  |\n\
               +-------+-------+-------+\n\
               |  G^2  |  H^2  |       |\n\
               +-------+-------+-------+\n");
      break;
    case 7:
      fprintf (out, "\
               +-------+-------+-------+\n\
               |       |  B^2  |  C^2  |\n\
               +-------+-------+-------+\n\
               |  D^2  |       |       |\n\
               +-------+-------+-------+\n\
               |       |  H^2  |  I^2  |\n\
               +-------+-------+-------+\n");
      break;
    case 8:
      fprintf (out, "\
               +-------+-------+-------+\n\
               |       |  B^2  |  C^2  |\n\
               +-------+-------+-------+\n\
               |       |  E^2  |       |\n\
               +-------+-------+-------+\n\
               |  G^2  |  H^2  |       |\n\
               +-------+-------+-------+\n");
      break;
    default:
      break;
    }
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_morgenstern_search_t *app = (struct fv_app_morgenstern_search_t *) state->input;
  switch (key)
    {
    case 'a':
      app->alt = 1;
      break;
    case OPT_INT:
      app->ul = 1;
      break;
    case 'q':
      app->threesq = 1;
      break;
    case 's':
      show_configuration (atoi (arg), stdout);
      exit (1);
      break;
    case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8':
      app->active[key - '1'] = 1;
      app->type[app->max_order] = key - '1' + 1;
      switch (key - '1')
        {
        case 0:
          app->order[app->max_order] = morgenstern_type_1;
          break;
        case 1:
          app->order[app->max_order] = morgenstern_type_2;
          break;
        case 2:
          app->order[app->max_order] = morgenstern_type_3;
          break;
        case 3:
          app->order[app->max_order] = morgenstern_type_4;
          break;
        case 4:
          app->order[app->max_order] = morgenstern_type_5;
          break;
        case 5:
          app->order[app->max_order] = morgenstern_type_6;
          break;
        case 6:
          app->order[app->max_order] = morgenstern_type_7;
          break;
        case 7:
          app->order[app->max_order] = morgenstern_type_8;
          break;
        }
      app->max_order++;
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case 'm':
      app->inmem = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'f':
      app->filter_num_squares = atoi (arg);
      app->filter_square1 = morgenstern_type_1_filter;
      app->filter_square2 = morgenstern_type_2_filter;
      app->filter_square3 = morgenstern_type_3_filter;
      app->filter_square4 = morgenstern_type_4_filter;
      app->filter_square5 = morgenstern_type_5_filter;
      app->filter_square6 = morgenstern_type_6_filter;
      app->filter_square7 = morgenstern_type_7_filter;
      app->filter_square8 = morgenstern_type_8_filter;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      app->display_record = binary_display_threesq;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 1)
        argp_error (state, "too many arguments");
      else
        {
          if (strcmp (arg, "-") == 0)
            {
              app->infile = stdin;
              app->num_args++;
            }
          else if (access (arg, R_OK) == 0)
            {
              app->infile = fopen (arg, "r");
              if (!app->infile)
                argp_error (state, "couldn't open `%s' for reading (%m)", arg);
              app->num_args++;
            }
          else
            argp_error (state, "couldn't open `%s' for reading (%m)", arg);
        }
      break;
    case ARGP_KEY_NO_ARGS:
      argp_error (state, "missing argument");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->inmem && !app->infile)
        argp_error (state, "-m must be used with FILE specified as argument");
      break;
    }
  return 0;
}

static struct argp
argp =
{
  options, parse_opt, "FILE",
  "Generate 3x3 magic squares with 5 perfect squares or more by creating two arithmetic progressions of three perfect squares with the smallest value in common, or the center value in common.\vThe standard input provides the parametric \"MN\" values -- two values per record to assist in the transformation.  Use the \"mn-seq\" program to provide this data on the standard input.  When the type of configuration isn't specified, all of them are generated.  When -m is specified, the standard input contains record numbers instead of \"MN\" values.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_morgenstern_search_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.filter_square1 = no_filter;
  app.filter_square2 = no_filter;
  app.filter_square3 = no_filter;
  app.filter_square4 = no_filter;
  app.filter_square5 = no_filter;
  app.filter_square6 = no_filter;
  app.filter_square7 = no_filter;
  app.filter_square8 = no_filter;
  app.out = stdout;
  app.threads = 1;
  app.display_record = display_threesq;
  pthread_mutex_init (&app.display_lock, NULL);
  argp_parse (&argp, argc, argv, ARGP_IN_ORDER, 0, &app);
  if (app.max_order == 0)
    {
      app.max_order = 8;
      // we use this order so that some calculations can be reused.
      app.active[0] = 1;
      app.type[0] = 5;
      app.order[0] = morgenstern_type_5;

      app.active[1] = 1;
      app.type[1] = 6;
      app.order[1] = morgenstern_type_6;

      app.active[2] = 1;
      app.type[2] = 4;
      app.order[2] = morgenstern_type_4;

      app.active[3] = 1;
      app.type[3] = 7;
      app.order[3] = morgenstern_type_7;

      app.active[4] = 1;
      app.type[4] = 3;
      app.order[4] = morgenstern_type_3;

      app.active[5] = 1;
      app.type[5] = 8;
      app.order[5] = morgenstern_type_8;

      app.active[6] = 1;
      app.type[6] = 2;
      app.order[6] = morgenstern_type_2;

      app.active[7] = 1;
      app.type[7] = 1;
      app.order[7] = morgenstern_type_1;

    }
  return fituvalu_morgenstern_search (&app, stdin);
}
