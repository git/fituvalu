/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <argz.h>
#include <glob.h>
#include "magicsquareutil.h"

struct fv_app_merge_gmp_t
{
  int glob;
  char *output;
  char *tmpdir;
  int removefiles;
  char **args;
  void (*dump_record) (mpz_t *, int, FILE *);
  int (*read_record) (FILE *, mpz_t *, int, char **, size_t *);
  int num_columns;
  int num_args;
};

static void
add_file (struct fv_app_merge_gmp_t *app, char *filename)
{
  app->args = realloc (app->args, (app->num_args + 1) * sizeof (char*));
  app->args[app->num_args] = strdup (filename);
  app->num_args++;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_merge_gmp_t *app = (struct fv_app_merge_gmp_t *) state->input;
  switch (key)
    {
    case 'N':
      app->glob = 1;
      break;
    case 'O':
      app->output = arg;
      break;
    case 'T':
        {
          DIR *d = opendir (arg);
          if (d)
            {
              app->tmpdir = arg;
              closedir (d);
            }
          else
            argp_error (state, "no such directory `%s'", arg);
        }
      break;
    case 'r':
      app->removefiles = 1;
      break;
    case 'n':
      app->num_columns = atoi (arg);
      break;
    case 'i':
      app->read_record = binary_read_numbers_from_stream;
      break;
    case 'o':
      app->dump_record = disp_binary_record;
      break;
    case ARGP_KEY_ARG:
        {
          if (app->glob)
            {
              glob_t buf;
              memset (&buf, 0, sizeof (buf));
              if (glob (arg, 0, 0, &buf) == 0)
                {
                  for (int i = 0; i < buf.gl_pathc; i++)
                    add_file (app, buf.gl_pathv[i]);
                  globfree (&buf);
                }
              else
                argp_error (state, "could not match `%s'", arg);
            }
          else
            {
              FILE *fp = fopen (arg, "r");
              if (fp)
                {
                  add_file (app, arg);
                  fclose (fp);
                }
              else
                argp_error (state, "could not open `%s' for reading (%m)", arg);
            }
        }
      break;
    case ARGP_KEY_NO_ARGS:
      argp_error (state, "missing argument.");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

int
compar (const void *left, const void *right, int num_columns)
{
  mpz_t *l = (mpz_t *) left;
  mpz_t *r = (mpz_t *) right;
  for (int i = 0; i < num_columns; i++)
    {
      int ret = mpz_cmp (l[i], r[i]);
      if (ret)
        return ret;
    }
  return 0;
}

static void
merge (struct fv_app_merge_gmp_t *app, char *file1, char *file2, FILE *out)
{
  char *linea = NULL;
  size_t lena = 0;
  ssize_t reada;
  char *lineb = NULL;
  size_t lenb = 0;
  ssize_t readb;
  mpz_t a[app->num_columns], b[app->num_columns];

  for (int i = 0; i < app->num_columns; i++)
    mpz_inits (a[i], b[i], NULL);

  FILE *infile1 = fopen (file1, "r");
  FILE *infile2 = fopen (file2, "r");

  //we kick off with some priming reads
  reada = app->read_record (infile1, a, app->num_columns, &linea, &lena);
  readb = app->read_record (infile2, b, app->num_columns, &lineb, &lenb);
  while (1)
    {
      if (reada != -1 && readb != -1)
        {
          int ret = compar (a, b, app->num_columns);
          if (ret < 0)
            {
              app->dump_record (a, app->num_columns, out);
              reada =
                app->read_record (infile1, a, app->num_columns, &linea, &lena);
            }
          else if (ret > 0)
            {
              app->dump_record (b, app->num_columns, out);
              readb =
                app->read_record (infile2, b, app->num_columns, &lineb, &lenb);
            }
          else
            {
              app->dump_record (a, app->num_columns, out);
              app->dump_record (b, app->num_columns, out);
              reada =
                app->read_record (infile1, a, app->num_columns, &linea, &lena);
              readb =
                app->read_record (infile2, b, app->num_columns, &lineb, &lenb);
            }
        }
      else if (reada == -1 && readb == -1)
        break;
      else if (reada == -1)
        {
          app->dump_record (b, app->num_columns, out);
          readb =
            app->read_record (infile2, b, app->num_columns, &lineb, &lenb);
        }
      else if (readb == -1)
        {
          app->dump_record (a, app->num_columns, out);
          reada =
            app->read_record (infile1, a, app->num_columns, &linea, &lena);
        }
    }

  fclose (infile1);
  fclose (infile2);

  for (int i = 0; i < app->num_columns; i++)
    mpz_clears (a[i], b[i], NULL);

  if (linea)
    free (linea);
  if (lineb)
    free (lineb);
}

static char *
get_tmp_file (struct fv_app_merge_gmp_t *app)
{
  char *file = "fv.merge.XXXXXXX";
  char *template = NULL;
  asprintf (&template, "%s/%s", app->tmpdir, file);
  int fd = mkstemp (template);
  if (fd == -1)
    {
      free (template);
      template = NULL;
    }
  else
    close (fd);
  return template;
}

static void
enqueue (char *rec, char **argz, size_t *len)
{
  argz_add (argz, len, rec);
}

static char *
dequeue (char **argz, size_t *len)
{
  if (*len == 0)
    return NULL;
  char *ret = strdup (*argz);
  argz_delete (argz, len, *argz);
  return ret;
}

static size_t
queue_count (char *argz, size_t len)
{
  return argz_count ((const char*)argz, len);
}

/*
static void
dequeue_next_two_files (char **bucket, size_t *len, char **file1, char **file2)
{
  *file1 = NULL;
  *file2 = NULL;
  *file1 = dequeue (bucket, len);
  if (!*file1)
    return;
  *file2 = dequeue (bucket, len);
}

static bool
is_original_file (struct fv_app_merge_gmp_t *app, char *file)
{
  for (int i = 0; i < app->num_args; i++)
    if (strcmp (file, app->args[i]) == 0)
      return true;
  return false;
}

static void
merge_bucket (struct fv_app_merge_gmp_t *app, char **inbucket, size_t *inlen, char **outbucket, size_t *outlen, FILE *out)
{
  if (queue_count (*inbucket, *inlen) == 2)
    {
      //okay try to merge the final 2.
      char *file1, *file2;
      dequeue_next_two_files (inbucket, inlen, &file1, &file2);
      if (!file1)
        return;
      if (!file2)
        {
          enqueue (file1, outbucket, outlen);
          free (file1);
          return;
        }
      merge (app, file1, file2, out);
      bool original = is_original_file (app, file1);
      if ((original && app->removefiles) || !original)
        remove (file1);
      original = is_original_file (app, file2);
      if ((original && app->removefiles) || !original)
        remove (file2);
      return;
    }

  while (1)
    {
      char *file1, *file2;
      dequeue_next_two_files (inbucket, inlen, &file1, &file2);
      if (!*file1)
        break;
      if (!*file2)
        {
          enqueue (file1, outbucket, outlen);
          free (file1);
          break;
        }
      char *tmpfile = get_tmp_file (app);
      FILE *tmp = fopen (tmpfile, "w");
      merge (app, file1, file2, tmp);
      fclose (tmp);
      enqueue (tmpfile, outbucket, outlen);
      free (tmpfile);
      bool original = is_original_file (app, file1);
      if ((original && app->removefiles) || !original)
        remove (file1);
      original = is_original_file (app, file2);
      if ((original && app->removefiles) || !original)
        remove (file2);
      free (file1);
      free (file2);
    }
}

int
fituvalu_merge_gmp (struct fv_app_merge_gmp_t *app, FILE *out)
{
  char *files = NULL;
  size_t len = 0;
  char *more_files = NULL;
  size_t mlen = 0;
  for (int i = 0; i < app->num_args; i++)
    enqueue (app->args[i], &files, &len);

  while (1)
    {
      merge_bucket (app, &files, &len, &more_files, &mlen, out);
      free (files);
      if (queue_count (more_files, mlen) == 0)
        break;
      files = more_files;
      mlen = len;
    }
  if (more_files)
    free (more_files);

  return 0;
}
*/

static void
dump_file (struct fv_app_merge_gmp_t *app, FILE *infile, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3];
  for (int i = 0; i < app->num_columns; i++)
    mpz_init (a[i]);
  while (1)
    {
      read = app->read_record (infile, a, app->num_columns, &line, &len);
      if (read == -1)
        break;
      app->dump_record (a, app->num_columns, out);
    }
  for (int i = 0; i < app->num_columns; i++)
    mpz_clear (a[i]);
  if (line)
    free (line);
  return;
}

int
fituvalu_merge_gmp2 (struct fv_app_merge_gmp_t *app, FILE *out)
{
  char *files = NULL;
  size_t len = 0;
  char *more_files = NULL;
  size_t mlen = 0;

  if (app->num_args == 1)
    {
      FILE *infile = fopen (app->args[0], "r");
      if (infile)
        {
          dump_file (app, infile, out);
          fclose (infile);
          if (app->removefiles)
            remove (app->args[0]);
        }
      return 0;
    }

  for (int i = 0; i < app->num_args; i++)
    enqueue (app->args[i], &files, &len);

  //first pass, do the pairs of files and maybe delete the files
  bool final = false;
  if (queue_count (files, len) == 2)
    final = true;
  while (1)
    {
      char *file1 = dequeue (&files, &len);
      if (!file1)
        break;
      char *file2 = dequeue (&files, &len);
      if (!file2)
        {
          enqueue (file1, &files, &len);
          free (file1);
          break;
        }
      if (final)
        {
          //fprintf (stderr, "merging '%s' and '%s' into stdout\n", file1, file2);
          merge (app, file1, file2, out);
          if (app->removefiles)
            {
              remove (file1);
              remove (file2);
            }
          break;
        }
      else
        {
          char *tmpfile = get_tmp_file (app);
          FILE *tmp = fopen (tmpfile, "w");
          //fprintf (stderr, "merging '%s' and '%s' into '%s'\n", file1, file2, tmpfile);
          merge (app, file1, file2, tmp);
          fclose (tmp);
          enqueue (tmpfile, &more_files, &mlen);
          free (tmpfile);
          if (app->removefiles)
            {
              remove (file1);
              remove (file2);
            }
        }
      free (file1);
      free (file2);
    }
  if (final)
    return 0;

  //second pass contains merged files, and maybe a lone original file
  char *extra_file =  NULL;
  if (queue_count (files, len) == 1)
    {
      extra_file = dequeue (&files, &len);
      enqueue (extra_file, &more_files, &mlen);
    }
  final = false;
  while (1)
    {
      if (queue_count (more_files, mlen) == 2)
        {
          //fprintf (stderr,"heyyy, just two left!\n");
        final = true;
        }
      //else if (queue_count (more_files, mlen) == 1)
          //fprintf (stderr,"heyyy, just one left!\n");
      char *file1 = dequeue (&more_files, &mlen);
      if (!file1)
        break;
      char *file2 = dequeue (&more_files, &mlen);
      if (!file2)
        {
          enqueue (file1, &more_files, &mlen);
          free (file1);
          break;
        }

      if (final)
        {
          //fprintf (stderr, "merging '%s' and '%s' into stdout\n", file1, file2);
        merge (app, file1, file2, out);
        }
      else
        {
          char *tmpfile = get_tmp_file (app);
          //fprintf (stderr, "merging '%s' and '%s' into '%s'\n", file1, file2, tmpfile);
          FILE *tmp = fopen (tmpfile, "w");
          merge (app, file1, file2, tmp);
          fclose (tmp);
          enqueue (tmpfile, &more_files, &mlen);
          free (tmpfile);
        }
      if (extra_file && strcmp (file1, extra_file) == 0)
        {
          if (app->removefiles)
            remove (file1);
        }
      else
        remove (file1);
      if (extra_file && strcmp (file2, extra_file) == 0)
        {
          if (app->removefiles)
            remove (file2);
        }
      else
        remove (file2);
      free (file1);
      free (file2);
    }
  if (extra_file)
    free (extra_file);
  return 0;
}

static struct argp_option
options[] =
{
  { "remove-files", 'r', 0, 0, "Remove FILEs as they are merged"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "num-columns", 'n', "NUM", 0, "A record has NUM columns (default 9)"},
  { "tmpdir", 'T', "DIR", 0, "Use DIR as a temporary directory"},
  { "output", 'O', "FILE", 0, "Write result to FILE instead of standard output"},
  { "name", 'N', 0, 0, "Arguments are globs not filnenames"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "FILE...\n--name \"pattern\"",
  "Merge a set of already sorted FILEs.\vThe --name option is for when there are a large number of files to merge and the shell complains of too many files.",
  0
};

static void
setup_tmp_dir (struct fv_app_merge_gmp_t *app)
{
  app->tmpdir = getenv("TMPDIR");
  if (!app->tmpdir)
    app->tmpdir = P_tmpdir;
  if (!app->tmpdir)
    app->tmpdir = strdup ("/tmp");
}

int
main (int argc, char **argv)
{
  struct fv_app_merge_gmp_t app;
  memset (&app, 0, sizeof (app));
  app.dump_record = disp_record;
  app.read_record = read_numbers_from_stream;
  app.num_columns = 9;
  setup_tmp_dir (&app);
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (app.output)
    {
      FILE *fp = fopen (app.output, "w");
      int ret = fituvalu_merge_gmp2 (&app, fp);
      fclose (fp);
      return ret;
    }
  else
    return fituvalu_merge_gmp2 (&app, stdout);
}
