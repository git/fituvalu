/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "magicsquareutil.h"


struct fv_app_walk_3sq_t
{
  void (*display_tuple) (mpz_t s[3], FILE *out);
  int num_args;
  mpq_t *args;
  mpz_t starting_square;
  mpz_t diff;
  int precision;
  int max_iterations;
};

static int
get_nearest_progression (mpq_t num, mpz_t *s, mpz_t *out, mpz_t prev)
{
  int retval = 0;
  mpf_t n;
  mpf_init (n);
  mpf_set_q (n, num);
  mpf_floor (n, n);
  mpf_add_ui (n, n, 1);
  mpz_set_f (*out, n);
  mpf_clear (n);
  mpz_add (s[1], s[0], *out);
  mpz_t root;
  mpz_init (root);
  mpz_sqrt (root, s[1]);
  if (!mpz_perfect_square_p (s[1]))
    {
      mpz_mul (s[1], root, root);
      mpz_sub (*out, s[1], s[0]);
    }
  //okay s[0] and s[1] are now squares.
  //now we iterate to find the last.
  //and we cycle down
  mpz_add (s[2], s[1], *out);
  while (!mpz_perfect_square_p (s[2]) ||
         !mpz_perfect_square_p (s[1]))
    {
      //advance root downwards
      mpz_sub_ui (root, root, 1);
      mpz_sub (s[1], s[1], root);
      mpz_sub (s[1], s[1], root);
      mpz_sub_ui (s[1], s[1], 1);
      if (mpz_cmp (s[1], prev) <= 0)
        {
          //we have cycled too far.
          retval = -1;
          break;
        }

      mpz_sub (*out, s[1], s[0]);
      mpz_add (s[2], s[1], *out);
    }
  mpz_clear (root);
  return retval;
}

int
fituvalu_walk_3sq_progression (struct fv_app_walk_3sq_t *app, FILE *out)
{
  int count = 1;
  mpz_t s[3];
  mpz_inits (s[0], s[1], s[2], NULL);
  mpz_set (s[0], app->starting_square);
  mpz_add (s[1], app->starting_square, app->diff);
  mpz_add (s[2], s[1], app->diff);
  if (!mpz_perfect_square_p (s[0]))
    return 0;
  if (!mpz_perfect_square_p (s[1]))
    return 0;
  if (!mpz_perfect_square_p (s[2]))
    return 0;
  app->display_tuple (s, out);
  //the idea is that s[0] stays the same and we modify s[1]
  //we multiply the diff by the next fraction
  //so we're constantly growing the diff.
  //at every iteration, we improve the fraction
  int cur_fraction = 0;
  mpz_t diff, prev_diff, limit;
  mpz_inits (diff, prev_diff, limit, NULL);
  mpz_set (diff, app->diff);
  mpz_set (limit, s[0]);
  mpz_set (prev_diff, diff);
  while (1)
    {
      mpq_t n;
      mpq_init (n);
      mpq_set_z (n, diff);
      mpq_mul (n, n, app->args[cur_fraction]);
      int retval = get_nearest_progression (n, s, &diff, limit);
      mpq_clear (n);
      if (retval < 0)
        break;
      app->display_tuple (s, out);
      mpz_set (limit, s[1]);
      //now improve the fraction.
       //in our example, diff is 319176 and fraction is 6135480/319176
       //so the numerator moves to the denominator
       //and there's a new numerator in the form of s[1]-s[0]
      mpq_set_num (app->args[cur_fraction], diff);
      mpq_set_den (app->args[cur_fraction], prev_diff);
      mpq_canonicalize (app->args[cur_fraction]);
      cur_fraction++;
      if (cur_fraction >= app->num_args)
        cur_fraction = 0;
      mpz_set (prev_diff, diff);
      count++;
      if (app->max_iterations && count + 1 > app->max_iterations)
        break;
    }
  mpz_clears (diff, prev_diff, limit, NULL);
  mpz_clears (s[0], s[1], s[2], NULL);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_walk_3sq_t *app = (struct fv_app_walk_3sq_t *) state->input;
  switch (key)
    {
    case 'i':
      app->max_iterations = atoi (arg);
      break;
    case 'l':
      fprintf (stdout, "1 24 840/24\n");
      fprintf (stdout, "49 120 240/120 5280/240\n");
      fprintf (stdout, "289 336 2520/336 18480/2520\n");
      fprintf (stdout, "529 840 3696/840 41496/3696\n");
      fprintf (stdout, "961 720 10920/720 47880/10920\n");
      fprintf (stdout, "1681 229680 253344/229680 7843920/253344\n");
      fprintf (stdout, "2209 2016 22440/2016 122400/22440\n");
      fprintf (stdout, "2401 1320 31824/1320 103224/31824\n");
      fprintf (stdout, "5041 2184 73920/2184 196560/73920\n");
      fprintf (stdout, "5329 10296 31920/10296 480480/31920\n");
      fprintf (stdout, "6241 3960 77280/3960 286440/77280\n");
      fprintf (stdout, "7921 14280 50160/14280 679320/50160\n");
      fprintf (stdout, "9409 148200 342240/148200 5266800/342240\n");
      fprintf (stdout, "10609 31416 43680/31416 1328040/43680\n");
      fprintf (stdout, "12769 17160 100800/17160 895440/100800\n");
      fprintf (stdout, "14161 6864 20064/6864 107640/20064 198360/107640 577200/198360\n");
      fprintf (stdout, "16129 4896 267960/4896 556920/267960 9501096/556920\n");
      fprintf (stdout, "18769 57960 74256/57960 2430456/74256\n");
      fprintf (stdout, "22801 47424 128520/47424 2170560/128520\n");
      fprintf (stdout, "25921 6840 26520/6840 245520/26520 448800/245520\n");
      fprintf (stdout, "27889 10920 425040/10920 1047480/425040 15127560/1047480\n");
      fprintf (stdout, "36481 35880 354144/35880 2109744/354144\n");
      fprintf (stdout, "37249 104880 160776/104880 4478376/160776\n");
      fprintf (stdout, "39601 708624 1271424/708624 25050480/1271424\n");
      fprintf (stdout, "49729 805896 1759296/805896 28604880/1759296\n");
      fprintf (stdout, "54289 46200 574560/46200 2893800/574560\n");
      fprintf (stdout, "883813441 244407480 444424584/24407480 924222000/444424584 1280762184/924222000\n");
      //for more, run the sdt and reduce and uniq the results.
      exit (0);
      break;
    case 'p':
      app->precision = atoi (arg);
      break;
    case 'o':
      app->display_tuple = display_binary_three_record;
      break;
    case ARGP_KEY_ARG:
      if (mpz_cmp_ui (app->starting_square, 0) == 0)
        mpz_set_str (app->starting_square, arg, 10);
      else if (mpz_cmp_ui (app->diff, 0) == 0)
        mpz_set_str (app->diff, arg, 10);
      else
        {
          app->args = realloc (app->args, (app->num_args  + 1) * sizeof (mpq_t));
          mpq_init (app->args[app->num_args]);
          mpq_set_str (app->args[app->num_args], arg, 10);
          app->num_args++;
        }
      break;
    case ARGP_KEY_NO_ARGS:
      argp_error (state, "missing argument.");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      mpz_init (app->starting_square);
      mpz_init (app->diff);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args < 1)
        argp_error (state, "missing argument.");
      break;
    }

  return 0;
}

static struct argp_option
options[] =
{
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "precision", 'p', "NUM", 0, "Use NUM bits of precision for calculations"},
  { "list-params", 'l', 0, 0, "Show some other arguments that work"},
  { "iterations", 'i', "NUM", 0, "Only show NUM progressions"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "SQUARE DIFF FRACTION...",
  "Generate arithmetic progressions of squares by keeping the first square the same, and multiplying the distance to the next square by a series of floating point numbers.\vFor example, try with arguments:  49 120 240/120 5280/240\nHow do you come up with these numbers?  Consider the following:\n\
sq1,    sq2,       sq3,     diff\n\
49,     169,       289,      120\n\
49,     289,       529,      240, 240/120 = 2.000\n\
49,    5329,     10609,     5280, 5280/240 = 22.000\n\
49,    9409,     18769,     9360, 9360/5280 = 1.772727\n\
49,   180625,   361201,   180576, 180576/9360 = 19.29230769\n\
49,   319225,   638401,   319176, 319176/180576 = 1.7675385964491229\n\
49,  6135529, 12271009,  6135480, 6135480/319176 = 19.222873900293255\n\
49, 10843849, 21687649, 10843800, 10843800/6135480 = 1.767392282266424\n\
The coefficent between diffs alternates between 1.77 and 19.23, indicating a repeating pattern.",
  0
};


int
main (int argc, char **argv)
{
  struct fv_app_walk_3sq_t app;
  memset (&app, 0, sizeof (app));
  app.display_tuple = display_three_record;
  app.precision = 65536;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  mpf_set_default_prec (app.precision);
  fituvalu_walk_3sq_progression (&app, stdout);
  return 0;
}
