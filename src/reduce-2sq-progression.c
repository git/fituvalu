/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct fv_app_reduce_2sq_progression_t
{
  void (*display_pair) (mpz_t *o, mpz_t *t, FILE *out);
  int (*read_pair) (FILE *, mpz_t *, char **, size_t *);
  int filter;
};

int
reduce_two_square_progression (mpz_t *progression)
{
  int reduced = 0;
  //progression must already be sorted
  //and all 2 numbers must be perfect squares
  mpz_t gcd;
  mpz_init (gcd);
  mpz_gcd (gcd, progression[0], progression[1]);
  if (mpz_cmp_ui (gcd, 1) > 0)
    {
      mpz_t d;
      mpz_init (d);
      int squares_retained = 1;
      for (int i = 0; i < 2; i++)
        {
          mpz_cdiv_q (d, progression[i], gcd);
          if (!mpz_perfect_square_p (d) &&
              mpz_perfect_square_p (progression[i]))
            {
              squares_retained = 0;
              break;
            }
        }
      mpz_clear (d);
      if (squares_retained)
        {
          reduced = 1;
          for (int i = 0; i < 2; i++)
            mpz_cdiv_q (progression[i], progression[i], gcd);
        }
    }
  mpz_clear (gcd);
  return reduced;
}

int
fituvalu_reduce_2sq_progression (struct fv_app_reduce_2sq_progression_t *app, FILE *in, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[2];

  for (int i = 0; i < 2; i++)
    mpz_init (a[i]);

  while (1)
    {
      read = app->read_pair (in, a, &line, &len);
      if (read == -1)
        break;
      int reduced = reduce_two_square_progression (a);
      if (reduced && app->filter)
        ;
      else
        app->display_pair (&a[0], &a[1], out);
    }

  for (int i = 0; i < 2; i++)
    mpz_clear (a[i]);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_reduce_2sq_progression_t *app = (struct fv_app_reduce_2sq_progression_t *) state->input;
  switch (key)
    {
    case 'f':
      app->filter = 1;
      break;
    case 'i':
      app->read_pair = binary_read_two_numbers_from_stream;
      break;
    case 'o':
      app->display_pair = display_binary_two_record;
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "filter", 'f', 0, 0, "Only show records that don't reduce"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept pairs of squares from the standard input, and reduce them to their smallest values.\vThe two values must be separated by a comma and terminated by a newline, and must be in ascending order.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_reduce_2sq_progression_t app;
  memset (&app, 0, sizeof (app));
  app.display_pair = display_two_record;
  app.read_pair = read_two_numbers_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_reduce_2sq_progression (&app, stdin, stdout);
}
