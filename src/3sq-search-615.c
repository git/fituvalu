/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <unistd.h>
#include "magicsquareutil.h"
#include "linecount.h"
int linecount;

struct fv_app_3sq_search_615_t
{
  int numargs;
  int high;
  int tries;
  FILE *infile;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
};

struct rec
{
  mpz_t ap[3];
};

static void
create_square (struct fv_app_3sq_search_615_t *app, struct rec *rec, mpz_t y1, mpz_t y2, FILE *out)
{
  mpz_t s[3][3];
  mpz_t sum, p;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  mpz_inits (sum, p, NULL);

  mpz_set (s[0][1], rec->ap[0]);
  mpz_set (s[2][2], rec->ap[1]);
  mpz_set (s[1][0], rec->ap[2]);
  mpz_set (s[0][2], y1);
  mpz_set (s[2][1], y2);

  mpz_sub (p, s[2][1], s[0][1]);
  mpz_cdiv_q_ui (sum, p, 2);
  mpz_add (s[1][1], s[0][1], sum);

  mpz_add (sum, s[0][1], s[1][1]);
  mpz_add (sum, sum, s[2][1]);

  mpz_add (p, s[0][1], s[0][2]);
  mpz_sub (s[0][0], sum, p);

  mpz_add (p, s[1][0], s[1][1]);
  mpz_sub (s[1][2], sum, p);

  mpz_add (p, s[2][1], s[2][2]);
  mpz_sub (s[2][0], sum, p);

  app->display_square (s, out);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (sum, p, NULL);
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_3sq_search_615_t *app = (struct fv_app_3sq_search_615_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'h':
      app->high = 1;
      break;
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->numargs >= 2)
        argp_error (state, "too many arguments");
      else if (app->numargs == 1)
        {
          app->tries = atoi (arg);
          if (app->tries <= 0)
            argp_error (state, "TRIES must be 1 or more. "
                        " For infinite, do not specify.");
        }
      else
        {
          if (strcmp (arg, "-") == 0)
            app->infile = stdin;
          else
            app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading", arg);
        }
      app->numargs++;

      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      app->infile = stdin;
      break;
    }
  return 0;
}

int
fituvalu_3sq_search_615 (struct fv_app_3sq_search_615_t *app, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  struct rec rec;
  mpz_t b[2], progression[3];
  mpz_inits (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], NULL);
  mpz_inits (progression[0], progression[1], progression[2], NULL);
  while (1)
    {
      read = app->read_tuple (app->infile, rec.ap, &line, &len);
      if (read == -1)
        break;
      update_linecount (&linecount);
      for (int j = 0; j < 3; j++)
        mpz_set (progression[j], rec.ap[j]);
      for (int j = 0; j < app->tries || app->tries == 0; j++)
        {
          generate_progression_kahn_kwong (progression, app->high);
          create_square (app, &rec, progression[1], progression[2], out);
          for (int k = 0; k < 3; k++)
            mpz_set (rec.ap[k], progression[k]);
        }
    }
  if (line)
    free (line);
  mpz_clears (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], NULL);
  mpz_clears (progression[0], progression[1], progression[2], NULL);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "high", 'h', 0, 0, "Use a higher alternative sequence for Y1 and Y2"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE] [TRIES]",
  "Try to make a 3x3 magic square with 5 or more perfect squares by giving it a series of 3 square progressions in FILE.  The program calculates two other squares, as a progression starting at X3.\vWhen FILE is `-' or is unspecified it is read from the standard input.  If TRIES is not specified it goes forever.\nEach square is laid out like:\n\
  +------+------+------+\n\
  |      |  X1  |  Y1  |\n\
  +------+------+------+\n\
  |  X3  |      |      |\n\
  +------+------+------+\n\
  |      |  Y2  |  X2  |\n\
  +------+------+------+\nThe 615 in the name comes from the fact we're trying to make a square of the form 6:15, but it can also find squares of type 6:12 and 6:4.  This is equivalent to a morgenstern type 7.",
  0
};

int
main (int argc, char **argv)
{
  init_linecount (&linecount);
  struct fv_app_3sq_search_615_t app;
  memset (&app, 0, sizeof (app));

  app.display_square = display_square_record;
  app.read_tuple = read_three_numbers_from_stream;
  app.infile = stdin;

  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_3sq_search_615 (&app, stdout);
}
