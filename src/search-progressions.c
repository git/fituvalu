/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_progressions_t
{
  int num_args;
  int from_stdin;
  void (*display_record) (mpz_t *, FILE *out);
  int (*read_number) (FILE *, mpz_t *, char **, size_t *);
  mpz_t num;
  int max_segments;
  int segment;
  int debug;
  int threads;
  FILE *out;
};

//from https://www.fq.math.ca/Papers1/43-2/paper43-2-1.pdf
static void
generate_progression (struct fv_app_search_progressions_t *app, mpz_t *progression, mpz_t mtwo, mpz_t ntwo)
{
  mpz_t mn, twomn, m, n;
  mpz_inits (mn, twomn, m, n, NULL);
  mpz_sqrt (m, mtwo);
  mpz_sqrt (n, ntwo);
  mpz_mul (mn, m, n);
  mpz_mul_ui (twomn, mn, 2);
  mpz_sub (progression[0], progression[1], twomn);
  mpz_add (progression[2], progression[1], twomn);
  mpz_clears (mn, twomn, m, n, NULL);
}

static void
calculate_segment_length (int max_seg, mpz_t total, mpz_t *seg)
{
  mpf_t segment, max_segments, segment_length, m, n;
  mpf_inits (segment, max_segments, segment_length, m, n, NULL);

  mpf_set_ui (segment, 1);
  mpf_set_ui (max_segments, max_seg);

  mpf_div (m, segment, max_segments);
  mpf_set_z (n, total);

  mpf_mul (segment_length, n, m);

  mpz_set_f (*seg, segment_length);
  mpf_clears (segment, max_segments, segment_length, m, n, NULL);
}

static void
calculate_segment (int seg, int max_seg, mpz_t *start, mpz_t *end)
{
  mpz_t root, start_root, end_root, segment_length;
  mpz_inits (root, start_root, end_root, segment_length, NULL);

  // okay we have a total set of ROOT squares
  mpz_sqrt (root, *end);

  // of our total, the segment we'll be working on is SEGMENT LENGTH squares
  calculate_segment_length (max_seg, root, &segment_length);

  // START ROOT is just the i-th chunk
  mpz_mul_ui (start_root, segment_length, seg - 1);

  // END ROOT is just start plus our length
  mpz_add (end_root, start_root, segment_length);

  // finally, translate back to squares
  mpz_mul (*start, start_root, start_root);
  mpz_mul (*end, end_root, end_root);

  mpz_clears (root, start_root, end_root, segment_length, NULL);
}

struct thread_data_t
{
  void *data;
  int seg;
  int max_seg;
  mpz_t n;
};

static void*
generate_and_show_for_segment (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_progressions_t *app =
    (struct fv_app_search_progressions_t *) param->data;

  mpz_t i, iroot, diff, limit, mn, twomn, n, progression[3], ti;
  mpz_inits (i, iroot, diff, limit, mn, twomn, n, progression[0],
             progression[1], progression[2], ti, NULL);


  mpz_set (progression[1], param->n);

  mpz_cdiv_q_ui (limit, progression[1], 2);
  mpz_set_ui (i, 1);
  mpz_set_ui (iroot, 1);
  if (param->max_seg != 1)
    {
      calculate_segment (param->seg, param->max_seg, &i, &limit);
      mpz_sqrt (iroot, i);
    }

  if (app->debug)
    {
      printf ("going from ");
      display_textual_number_no_newline (i, stdout);
      printf (" to ");
      display_textual_number_no_newline (limit, stdout);
      printf ("\n");
    }

  while (mpz_cmp (i, limit) <= 0)
    {
      mpz_sub (diff, progression[1], i);
      if (mpz_perfect_square_p (diff))
        {
          mpz_sqrt (n, diff);

          mpz_mul (mn, iroot, n);
          mpz_mul_ui (twomn, mn, 2);

          mpz_sub (progression[0], progression[1], twomn);
          mpz_add (progression[2], progression[1], twomn);

          if (app->threads > 1)
            pthread_mutex_lock (&display_lock);

          app->display_record (progression, app->out);

          if (app->threads > 1)
            pthread_mutex_unlock (&display_lock);
        }
      mpz_mul_ui (ti, iroot, 2);
      mpz_add (i, i, ti);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }

  mpz_clears (i, iroot, diff, limit, mn, twomn, n, progression[0],
             progression[1], progression[2], ti, NULL);
  return NULL;
}

static void
run_threads (void *data, int num_threads, mpz_t n, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      param[i].seg = i + 1;
      param[i].max_seg = num_threads;
      mpz_init (param[i].n); //leak, not cleared anywhere
      mpz_set (param[i].n, n);

      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_progression_and_show (struct fv_app_search_progressions_t *app, mpz_t n)
{
  run_threads (app, app->threads, n, generate_and_show_for_segment);
  return;
}

int
fituvalu_search_progression (struct fv_app_search_progressions_t *app, FILE *in)
{
  if (app->from_stdin)
    {
      mpz_t num;
      mpz_init (num);
      char *line = NULL;
      size_t len = 0;
      ssize_t read;

      while (1)
        {
          read = app->read_number (in, &num, &line, &len);
          if (read == -1)
            break;
          generate_progression_and_show (app, num);
        }

      if (line)
        free (line);
      mpz_clear (num);
    }
  else
    generate_progression_and_show (app, app->num);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_progressions_t *app = (struct fv_app_search_progressions_t *) state->input;
  switch (key)
    {
    case 't':
      app->threads = atoi (arg);
      break;
    case 'd':
      app->debug = 1;
      break;
    case 's':
        {
          int m, n;
          int retval = sscanf (arg, "%d/%d", &m, &n);
          if (retval != 2 || m <= 0 || n < m || n <= 0)
            argp_error (state, "invalid argument `%s' to option --segment", arg);

          app->segment = m;
          app->max_segments = n;
        }
      break;
    case 'i':
      app->read_number = binary_read_one_number_from_stream;
      break;
    case 'o':
      app->display_record = display_binary_three_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          switch (app->num_args)
            {
            case 0:
              mpz_set_str (app->num, arg, 10);
              break;
            }
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      mpz_init (app->num);
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args == 0)
        app->from_stdin = 1;
      if (app->max_segments != 1 && app->threads > 1)
        argp_error (state, "--threads and --segment cannot be used together");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "debug", 'd', 0, OPTION_HIDDEN, "Show extra information while running"},
  { "segment", 's', "N/M", 0, "Only do a portion, do segment N of M" },
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[CENTER]",
  "Find arithmetic progressions consisting of two squares around a given CENTER.\vWhen CENTER is not provided, it is read from the standard input.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_progressions_t app;
  memset (&app, 0, sizeof (app));
  app.display_record = display_three_record;
  app.read_number = read_one_number_from_stream;
  app.segment = 1;
  app.max_segments = 1;
  app.threads = 1;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_progression (&app, stdin);
}
