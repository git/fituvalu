/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_split_gmp_t
{
  int num_zeroes;
  int lines;
  FILE *infile;
  char *suffix;
  void (*dump_record) (mpz_t *, int, FILE *);
  int (*read_record) (FILE *, mpz_t *, int, char **, size_t *);
  int piece_count;
  int num_columns;
  int num_threads;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_split_gmp_t *app = (struct fv_app_split_gmp_t *) state->input;
  switch (key)
    {
    case 't':
      app->num_threads = atoi (arg);
      break;
    case 'z':
      app->num_zeroes = atoi (arg);
      break;
    case 'n':
      app->num_columns = atoi (arg);
      break;
    case 'i':
      app->read_record = binary_read_numbers_from_stream;
      break;
    case 'N':
      app->piece_count = atoi (arg);
      break;
    case 'o':
      app->dump_record = disp_binary_record;
      break;
    case 'l':
      app->lines = atoi (arg);
      break;
    case ARGP_KEY_ARG:
      if (app->infile)
        {
          if (app->suffix)
            argp_error (state, "too many arguments");
          else
            app->suffix = arg;
        }
      else
        {
          if (strcmp (arg, "-") == 0)
            app->infile = stdin;
          else
            {
              app->infile = fopen (arg, "r");
              if (!app->infile)
                argp_error (state, "could not open `%s' for reading (%m)", arg);
            }
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static FILE *
next_outfile (struct fv_app_split_gmp_t *app)
{
  char *fmt = NULL;
  char *f = NULL;
  asprintf (&fmt, "%%s%%0%dd", app->num_zeroes);
  if (app->suffix)
    asprintf (&f, fmt, app->suffix, app->piece_count);
  else
    asprintf (&f, fmt, "", app->piece_count);
  free (fmt);
  
  app->piece_count++;
  FILE *out = fopen (f, "w");
  free (f);
  return out;
}

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void*
process_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t*) arg;
  struct fv_app_split_gmp_t *app = (struct fv_app_split_gmp_t *) param->data;
  int lines = 0;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[app->num_columns];

  for (int i = 0; i < app->num_columns; i++)
    mpz_init (a[i]);

  FILE *out = next_outfile (app);
  while (1)
    {
      pthread_mutex_lock (&read_lock);
      read = app->read_record (app->infile, a, app->num_columns, &line, &len);
      lines++;
      if (read == -1)
        {
          pthread_mutex_unlock (&read_lock);
          break;
        }
      if (lines > app->lines)
        {
          if (out)
            fclose (out);
          out = next_outfile (app);
          lines = 1;
        }
      pthread_mutex_unlock (&read_lock);
      app->dump_record (a, app->num_columns, out);
    }
  for (int i = 0; i < app->num_columns; i++)
    mpz_clear (a[i]);

  if (line)
    free (line);
  if (out)
    fclose (out);
  return 0;
}

int
fituvalu_split_gmp (struct fv_app_split_gmp_t *app)
{
  run_threads (app, app->num_threads, process_record);
}

static struct argp_option
options[] =
{
  { "lines", 'l', "NUMBER", 0, "Put NUMBER records per output file (default 10000)"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "numeric-suffixes", 'N', "FROM", 0, "Use numeric suffixes starting at FROM"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "num-columns", 'n', "NUM", 0, "A record has NUM columns (default 9)"},
  { "num-zeroes", 'z', "NUM", 0, "Suffixes have NUM leading zeroes (default 4)"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE [SUFFIX]]",
  "Split FILE into pieces.\vIf FILE is not specified, it is read from the standard input.  The output files are numbered.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_split_gmp_t app;
  memset (&app, 0, sizeof (app));
  app.dump_record = disp_record;
  app.read_record = read_numbers_from_stream;
  app.lines = 10000;
  app.num_columns = 9;
  app.num_zeroes = 4;
  app.num_threads = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (!app.infile)
    app.infile = stdin;
  return fituvalu_split_gmp (&app);
}
