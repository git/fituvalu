#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <argp.h>
#include <pthread.h>
#include <math.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_2sq_search_63_small_t
{
  FILE *out;
  int threads;
  int numargs;
  int positive;
  unsigned long long i;
  unsigned long long root;
  int exhaustive;
  int quicker;
};

int
small_is_square2 (unsigned long long num, long double *root)
{
   *root = sqrtl (num);
   if ((unsigned long long)(*root) * (unsigned long long)(*root) == num)
     return 1;
   return 0;
}

static void
create_square (struct fv_app_2sq_search_63_small_t *app, unsigned long long int (*p)[3], unsigned long long int  *b, unsigned long long int  *c, unsigned long long int  diff)
{
  long long int s[3][3];
  s[2][0] = (*p)[0];
  s[1][1] = (*p)[1];
  s[0][2] = (*p)[2];
  s[0][0] = b[0];
  s[2][1] = b[1];
  s[0][1] = c[0];
  s[2][2] = c[1];
  s[1][0] = s[2][2] + diff;
  s[1][2] = s[0][0] - diff;
  pthread_mutex_lock (&display_lock);
  fprintf (app->out,
           "%lld, %lld, %lld, "
           "%lld, %lld, %lld, "
           "%lld, %lld, %lld,\n",
           s[0][0], s[0][1], s[0][2],
           s[1][0], s[1][1], s[1][2],
           s[2][0], s[2][1], s[2][2]);
  fflush (app->out);
  pthread_mutex_unlock (&display_lock);
}

static void
search_positive (struct fv_app_2sq_search_63_small_t *app, unsigned long long int match, unsigned long long (*p)[3])
{
  if ((*p)[1] % 2 == 0)
    return;
  unsigned long long int sum = (*p)[0] + (*p)[1] + (*p)[2];
  unsigned long long int i = match;
  unsigned long long int iroot = sqrtl (i);
  unsigned long long int diff, limiter;
  unsigned long long int j, jroot;
  long double diffroot;
  unsigned long long int b[2], c[2];
  i = iroot * iroot;
  while (1)
    {
      if (i > (*p)[0])
        break;
      diff = match + i;
      limiter = (*p)[2] + (*p)[2];
      if (small_is_square2 (diff, &diffroot))
        {
          b[0] = i;
          b[1] = diff;
          j = (*p)[2];
          jroot = sqrtl (j);
          j = jroot * jroot;
          unsigned long long int targetsum = sum - i - (*p)[1];
          if (small_is_square2 (targetsum, &diffroot))
            {
              while (1)
                {
                  diff = match + j;
                  if (j > limiter)
                    break;
                  if (small_is_square2 (diff, &diffroot))
                    {
                      c[0] = j;
                      c[1] = diff;
                      targetsum = (*p)[1] + b[0] + c[1];
                      if (targetsum == sum)
                        {
                          if (c[0] != b[0] && c[1] != b[1] &&
                              c[0] != (*p)[0] && c[1] != (*p)[2] &&
                              b[0] != (*p)[0] && b[1] != (*p)[2])
                            {
                              create_square (app, p, b, c, match);
                            }
                        }
                    }
                  j += jroot;
                  j += jroot;
                  j++;
                  jroot++;
                }
            }
        }

      i += iroot;
      i += iroot;
      i++;
      iroot++;
    }
}

/*
static int
pair_search2 (struct fv_app_2sq_search_616_t *app, struct rec *rec, mpz_t *b, mpz_t *c, mpz_t match, mpz_t *middle, mpz_t *sum, FILE *out)
{
  int found = 0;
  mpz_t i, iroot, j, jroot, diff, limiter, targetsum;
  mpz_inits (i, iroot, j, jroot, diff, limiter, targetsum, NULL);
  mpz_set (i, match);
  mpz_sqrt (iroot, i);
  mpz_mul (i, iroot, iroot);

  mpz_sub (match, rec->ap[1], rec->ap[0]);
  mpz_cdiv_q_ui (match, match, 2);
  mpz_add (*middle, rec->ap[0], match);
  mpz_add (*sum, rec->ap[0], rec->ap[1]);
  mpz_add (*sum, *sum, *middle);
  while (1)
    {
      if (mpz_cmp (i, rec->ap[0]) > 0)
        break;
      mpz_add (diff, match, i);
      //char *buf = malloc (mpz_sizeinbase (diff, 10) + 2);
      //mpz_get_str (buf, 10, diff);
      //fprintf (stdout, "%s\n", buf);
      //free (buf);
      mpz_add (limiter, rec->ap[1], rec->ap[1]);
      if (mpz_perfect_square_p (diff))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], diff);
          mpz_set (j, rec->ap[2]);
          mpz_sqrt (jroot, j);
          mpz_mul (j, jroot, jroot);
                  
          mpz_sub (targetsum, *sum, b[0]);
          mpz_sub (targetsum, targetsum, *middle);

          if (mpz_perfect_square_p (targetsum))
            {
              while (1)
                {
                  mpz_add (diff, match, j);
                  if (mpz_cmp (j, limiter) > 0)
                    break;
                  if (mpz_perfect_square_p (diff))
                    {
                      mpz_set (c[0], j);
                      mpz_set (c[1], diff);

                      mpz_add (targetsum, *middle, b[0]);
                      mpz_add (targetsum, targetsum, c[1]);
                      if (mpz_cmp (targetsum, *sum) == 0)
                        {
                          if (mpz_cmp (c[0], b[0]) != 0 &&
                              mpz_cmp (c[1], b[1]) != 0 &&
                              mpz_cmp (c[0], rec->ap[0]) != 0 &&
                              mpz_cmp (c[1], rec->ap[1]) != 0 &&
                              mpz_cmp (b[0], rec->ap[0]) != 0 &&
                              mpz_cmp (b[1], rec->ap[1]) != 0)
                            {
                              create_square (app, rec, b, c, middle, match, out);
                            }
                        }
                    }
                  mpz_add (j, j, jroot);
                  mpz_add (j, j, jroot);
                  mpz_add_ui (j, j, 1);
                  mpz_add_ui (jroot, jroot, 1);
                }
            }
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, jroot, diff, limiter, targetsum, NULL);
  return found;
  return 0;
}
*/
static void
search_positive2(struct fv_app_2sq_search_63_small_t *app, unsigned long long (*p)[3], unsigned long long distance)
{
  unsigned long long int sum = (*p)[0] + (*p)[1] + (*p)[2];
  unsigned long long int i = 4;
  unsigned long long int root = 2;
  long double diffroot;
  unsigned long long int diff;
  unsigned long long int s[3][3];

  while (1)
    {
      if (i > distance)
        break;
      diff = i + distance;
          
      if (diff + (*p)[2]  > sum)
        break;
      if (diff != (*p)[1] && small_is_square2 (diff, &diffroot))
        {
          //okay we have two parts.  we have xox and xxo

          s[2][0] = (*p)[0]; // square
          s[2][2] = diff; //square
          s[2][1] = sum - s[2][0] - s[2][2]; //square?
          if (small_is_square2 (s[2][1], &diffroot))
            {
              s[0][2] = (*p)[2]; // square
              s[0][1] = i; //square
              s[0][0] = sum - s[0][1] - s[0][2]; //square?
              if (small_is_square2 (s[0][0], &diffroot))
                {
                  s[1][1] = (*p)[1];
                  s[1][0] = sum - s[0][0] - s[2][0];
                  s[1][2] = sum - s[0][2] - s[2][2];
                  pthread_mutex_lock (&display_lock);
                  fprintf (app->out,
                           "%llu, %llu, %llu, "
                           "%llu, %llu, %llu, "
                           "%llu, %llu, %llu,\n",
                           s[0][0], s[0][1], s[0][2],
                           s[1][0], s[1][1], s[1][2],
                           s[2][0], s[2][1], s[2][2]);
                  fflush (app->out);
                  pthread_mutex_unlock (&display_lock);
                }
            }
        }
      i += root;
      i += root;
      i++;
      root++;
    }
}
static void
search_positive3(struct fv_app_2sq_search_63_small_t *app, unsigned long long (*p)[3], unsigned long long distance)
{
  unsigned long long int sum = (*p)[0] + (*p)[1] + (*p)[2];
  long long int i = (*p)[2];
  long long int root = sqrtl (i);
  long double diffroot;
  unsigned long long int diff;
  unsigned long long int s[3][3];

  s[2][0] = (*p)[0];
  s[1][1] = (*p)[1];
  s[0][2] = (*p)[2];

  root--;
  i -= root;
  i -= root;
  i--;

  while (1)
    {
      long long int reach = i - distance;
      if (reach <= 0)
        break;
      s[2][1] = i;
      s[0][0] = i - distance;
      if (s[2][1] == s[1][1])
        {
          root--;
          i -= root;
          i -= root;
          i--;
          continue;
        }

      if (app->quicker)
        {
          int d1mod10 = distance % 10;
          int d2mod10;
          if (s[1][1] > s[0][0])
            d2mod10 = (s[1][1] - s[0][0]) % 10;
          else
            d2mod10 = (s[0][0] - s[1][1]) % 10;
          int elide = 1;
          if ((d1mod10 == 0 && d2mod10 == 0) ||
              (d1mod10 == 0 && d2mod10 == 4) ||
              (d1mod10 == 0 && d2mod10 == 6) ||
              (d1mod10 == 4 && d2mod10 == 0) ||
              (d1mod10 == 4 && d2mod10 == 6) ||
              (d1mod10 == 6 && d2mod10 == 0) ||
              (d1mod10 == 6 && d2mod10 == 6))
            elide = 0;
          if ((d1mod10 == 0 && d2mod10 == 0) ||
              (d1mod10 == 4 && d2mod10 == 0) ||
              (d1mod10 == 6 && d2mod10 == 0) ||
              (d1mod10 == 0 && d2mod10 == 4) ||
              (d1mod10 == 6 && d2mod10 == 4) ||
              (d1mod10 == 0 && d2mod10 == 6) ||
              (d1mod10 == 0 && d2mod10 == 6))
            elide = 0;
          if (elide)
            {
              root--;
              i -= root;
              i -= root;
              i--;
              continue;
            }
        }

      if (small_is_square2 (s[0][0], &diffroot))
        {
          s[1][2] = s[0][0] - distance;
          unsigned long long int j = s[2][0];
          unsigned long long int jroot = sqrtl (j);
          //unsigned long long int limit = j * j;
          unsigned long long int limit = j + (distance * distance);

          j += jroot;
          j += jroot;
          j++;
          jroot++;

          while (1)
            {
              if (j > limit)
                break;
              s[0][1] = j;
              unsigned long long int row = s[0][0] + s[0][1] + s[0][2];
              if (sum == row)
                {
                  s[2][2] = j + distance;
                  if (small_is_square2 (s[2][2], &diffroot))
                    {
                      s[1][0] = s[2][2] + distance;
                      pthread_mutex_lock (&display_lock);
                      fprintf (app->out,
                               "%llu, %llu, %llu, "
                               "%llu, %llu, %lld, "
                               "%llu, %llu, %llu,\n",
                               s[0][0], s[0][1], s[0][2],
                               s[1][0], s[1][1], s[1][2],
                               s[2][0], s[2][1], s[2][2]);
                      fflush (app->out);
                      pthread_mutex_unlock (&display_lock);
                    }
                }
              j += jroot;
              j += jroot;
              j++;
              jroot++;
            }
        }
      root--;
      i -= root;
      i -= root;
      i--;
    }
}

static void
search_negative (struct fv_app_2sq_search_63_small_t *app, unsigned long long match, unsigned long long (*p)[3])
{
  if ((*p)[1] % 2 == 0)
    return;
  unsigned long long int i = 4;
  unsigned long long int iroot = 2;
  unsigned long long int limiter = match * 2;
  unsigned long long int b[2], c[2];
  long double diffroot;
  unsigned long long int sum = (*p)[0] + (*p)[1] + (*p)[2];
  while (1)
    {
      if (i > match)
        break;
      unsigned long long int diff = i + match;
      if (small_is_square2 (diff, &diffroot))
        {
          b[0] = i;
          b[1] = diff;
          unsigned long long int j = 4;
          unsigned long long int jroot = 2;
          unsigned long long int targetsum = sum - i - (*p)[1];
          if (small_is_square2 (targetsum, &diffroot))
            {
              while (1)
                {
                  diff = j + match;
                  if (j > limiter)
                    break;
                  if (small_is_square2 (diff, &diffroot))
                    {
                      c[0] = j;
                      c[1] = diff;
                      targetsum = (*p)[1] + i + diff;
                      if (targetsum == sum)
                        {
                          if (c[0] != b[0] && c[1] != b[1] &&
                              c[0] != (*p)[0] && c[1] != (*p)[2] &&
                              b[0] != (*p)[0] && b[1] != (*p)[2])
                            {
                              create_square (app, p, b, c, match);
                            }
                        }
                    }
                  j += jroot;
                  j += jroot;
                  j++;
                  jroot++;
                }
            }
        }
      i += iroot;
      i += iroot;
      i++;
      iroot++;
    }
}

struct thread_data_t
{
  void *data;
};

static unsigned long long int
gcd (unsigned long long int n1, unsigned long long int n2)
{
  while (n1 != n2)
    {
      if(n1 > n2)
        n1 -= n2;
      else
        n2 -= n1;
    }
  return n1;
}

static void*
process_perfect_square (void *arg)
{
  unsigned long long n;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_2sq_search_63_small_t *app =
    (struct fv_app_2sq_search_63_small_t *) param->data;

  while (1)
    {
      //go get the next perfect square to work on (app->num)
      pthread_mutex_lock (&read_lock);

      app->i += app->root;
      app->i += app->root;
      app->i++;
      app->root++;

      n = app->i;
      pthread_mutex_unlock (&read_lock);

      unsigned long long int j = 4;
      unsigned long long int jroot = 2;
      unsigned long long int progression[3];
      while (1)
        {
          if (j >= n)
            break;

          if (app->exhaustive)
            {
              progression[0] = j;
              unsigned long long int diff = (n - j) / 2;
              progression[1] = j + diff;
              progression[2] = n;
              search_positive3 (app, &progression, diff);
            }
          else
            {
              if (gcd (n, j) == 1)
                {
                  progression[0] = j;
                  unsigned long long int diff = (n - j) / 2;
                  progression[1] = j + diff;
                  progression[2] = n;
                  if (app->positive)
                    search_positive (app, diff, &progression);
                  else
                    search_negative (app, diff, &progression);
                }
            }

          j += jroot;
          j += jroot;
          j++;
          jroot++;
        }
    }
  return NULL;
}

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static int
fituvalu_2sq_search_63_small (struct fv_app_2sq_search_63_small_t *app)
{
  run_threads (app, app->threads, process_perfect_square);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_2sq_search_63_small_t *app =
    (struct fv_app_2sq_search_63_small_t *) state->input;
  switch (key)
    {
    case 'e':
      app->exhaustive = 1;
      break;
    case 'p':
      app->positive = 1;
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_ARG:
      app->numargs++;
      if (app->numargs > 1)
        argp_error (state, "too many arguments.");
      {
        char *end = NULL;
        app->i = strtoull (arg, &end, 10);
        if (end == NULL || *end != '\0')
          argp_error (state, "invalid START value `%s'", arg);
        app->root = sqrtl (app->i);
        app->i = app->root * app->root;
      }
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "positive", 'p', 0, 0, "Only find squares with all positive numbers"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "exhaustive", 'e', 0, OPTION_HIDDEN, "Use an exhaustive algorithm"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[START]",
  "Find 3x3 magic squares of the form 6:3.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_2sq_search_63_small_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.threads = 1;
  app.i = 4;
  app.root = 2;
  app.quicker = 0;
  argp_parse (&argp, argc, argv, 0, 0, &app);

  return fituvalu_2sq_search_63_small (&app);
}
