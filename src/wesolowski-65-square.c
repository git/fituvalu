/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

struct fv_app_wesolowski_65_square_t
{
  int num_args;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int in_binary;
  mpz_t n;
  int threesq;
  FILE *infile;
};

struct thread_data_t
{
  void *data;
};

static int
generate_65_square (struct fv_app_wesolowski_65_square_t *app, mpz_t *p, mpf_t c, mpz_t (*s)[3][3])
{
  int found = 0;
  mpz_t sum, sum_minus_middle, distance;
  mpz_inits (sum, sum_minus_middle, distance, NULL);
  mpz_sub (distance, p[1], p[0]);
  mpz_set ((*s)[1][1], p[1]);
  mpz_set ((*s)[1][0], p[0]);
  mpz_set ((*s)[1][2], p[2]);
  mpz_mul_ui (sum, (*s)[1][1], 3);
  mpz_mul_ui (sum_minus_middle, (*s)[1][1], 2);


  //scan ahead [2][2] until we get a match on [2][0] and [2][1]
    {
      mpz_t i, iroot, j, limit, twoiroot;
      mpz_inits (i, iroot, j, limit, twoiroot, NULL);


      mpf_mpz_multiply (i, (*s)[1][1], c);
      if (mpz_cmp_ui (i, 0) == 0)
        mpz_sqrt (iroot, distance);
      else
        mpz_sqrt (iroot, i);
      mpz_mul (i, iroot, iroot);

      mpz_set (limit, sum_minus_middle); //converges to 1/9 of [1][1]
      while (mpz_cmp (i, limit) <= 0)
        {
          mpz_add (j, i, distance);

          if (mpz_perfect_square_p (j))
            {
              mpz_set ((*s)[2][2], i);
              mpz_set ((*s)[2][0], j);
              mpz_sub ((*s)[2][1], sum, (*s)[2][0]);
              mpz_sub ((*s)[2][1], (*s)[2][1], (*s)[2][2]);
              if (mpz_perfect_square_p ((*s)[2][1]))
                {
                  int dup = 0;
                  if (mpz_cmp ((*s)[2][0], (*s)[1][1]) == 0 ||
                      mpz_cmp ((*s)[2][1], (*s)[1][2]) == 0 ||
                      mpz_cmp ((*s)[2][2], (*s)[1][1]) == 0)
                    dup = 1;
                  if (!dup)
                    {
                      mpz_sub ((*s)[0][0], sum_minus_middle, (*s)[2][2]);
                      mpz_sub ((*s)[0][1], sum_minus_middle, (*s)[2][1]);
                      mpz_sub ((*s)[0][2], sum_minus_middle, (*s)[2][0]);
                      app->display_square (*s, app->out);
                      fflush (app->out);
                      found = 1;
                      break;
                    }
                }
            }

          mpz_mul_ui (twoiroot, iroot, 2);
          mpz_add (i, i, twoiroot);
          mpz_incr (i);
          mpz_incr (iroot);
        }


      mpz_clears (i, iroot, j, limit, twoiroot, NULL);
    }

  mpz_clears (sum, sum_minus_middle, distance, NULL);
  return found;
}

static void
generate_3sq (struct fv_app_wesolowski_65_square_t *app, mpz_t n, mpz_t *a, mpz_t *b)
{
  mpz_t w, x, y, z, n2, twon, fourn, sixn;
  mpz_inits (w, x, y, z, n2, twon, fourn, sixn, NULL);
  mpz_mul (n2, n, n);
  mpz_mul_ui (twon, n, 2);
  mpz_mul_ui (fourn, n, 4);
  mpz_mul_ui (sixn, n, 6);

  // w = 6n^2 + 6n + 2
  // x = 2n + 1
  // y = 3n^2 + 2n
  // z = 3n^2 + 4n + 1

  mpz_mul_ui (w, n2, 6);
  mpz_add (w, w, sixn);
  mpz_add_ui (w, w, 2);

  mpz_add_ui (x, twon, 1);

  mpz_mul_ui (y, n2, 3);
  mpz_add (y, y, twon);

  mpz_mul_ui (z, n2, 3);
  mpz_add (z, z, fourn);
  mpz_add_ui (z, z, 1);

    {
      mpz_t w2, x2, y2, z2, p, q, sum_minus_middle;
      mpz_inits (w2, x2, y2, z2, p, q, sum_minus_middle, NULL);
      mpz_mul (w2, w, w);
      mpz_mul (x2, x, x);
      mpz_mul (y2, y, y);
      mpz_mul (z2, z, z);

      mpz_mul (p, x2, y2);
      mpz_mul (q, w2, z2);
      mpz_add (a[1], p, q);
      mpz_mul_ui (sum_minus_middle, a[1], 2);

      mpz_mul (p, x2, z2);
      mpz_mul (q, w2, y2);
      mpz_add (b[1], p, q);

      mpz_mul (p, w, y);
      mpz_mul (q, x, z);
      mpz_sub (b[0], p, q);
      mpz_mul (b[0], b[0], b[0]);

      mpz_mul (p, w, z);
      mpz_mul (q, x, y);
      mpz_sub (a[0], p, q);
      mpz_mul (a[0], a[0], a[0]);

      mpz_mul (p, w, y);
      mpz_mul (q, x, z);
      mpz_add (b[2], p, q);
      mpz_mul (b[2], b[2], b[2]);


      mpz_sub (a[2], sum_minus_middle, a[0]);


      mpz_clears (w2, x2, y2, z2, p, q, sum_minus_middle, NULL);
    }
  mpz_clears (w, x, y, z, n2, twon, fourn, sixn, NULL);
}

int
process_record (struct fv_app_wesolowski_65_square_t *app)
{
  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  mpz_t a[3];
  mpz_t b[3];
  for (int i = 0; i < 3; i++)
    mpz_inits (a[i], b[i], NULL);
  mpz_t n;
  mpz_init (n);
  mpf_t c1, c2;
  mpf_inits (c1, c2, NULL);
  while (1)
    {
      mpz_set (n, app->n);
      mpz_add_ui (app->n, app->n, 1);

      generate_3sq (app, n, a, b);
      if (generate_65_square (app, a, c1, &s))
        mpf_mpz_divide (c1, s[2][2], s[1][1]);
      if (generate_65_square (app, b, c2, &s))
        mpf_mpz_divide (c2, s[1][1], s[2][2]);
    }
  mpf_clears (c1, c2, NULL);

  mpz_clear (n);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  for (int i = 0; i < 3; i++)
    mpz_clears (a[i], b[i], NULL);
}

int
process_threesq_record (struct fv_app_wesolowski_65_square_t *app)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  mpz_t a[3];
  mpz_inits (a[0], a[1], a[2], NULL);
  mpf_t c1;
  mpf_init (c1);
  while (1)
    {
      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &a[0], &a[1], &a[2], &line, &len);
          if (read == -1)
            break;
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &a[0], &a[1], &a[2], &line, &len);
          if (read == -1)
              break;
        }
      if (generate_65_square (app, a, c1, &s))
        mpf_mpz_divide (c1, s[2][2], s[1][1]);
    }

  mpf_clear (c1);
  mpz_clears (a[0], a[1], a[2], NULL);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);


  if (line)
    free (line);
  return 0;
}

int
fituvalu_wesolowski_65_square (struct fv_app_wesolowski_65_square_t *app)
{
  if (app->threesq)
    return process_threesq_record (app);
  else
    return process_record (app);
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_wesolowski_65_square_t *app = (struct fv_app_wesolowski_65_square_t *) state->input;
  switch (key)
    {
    case '3':
      app->threesq = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          mpz_set_str (app->n, arg, 10);
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      mpz_init (app->n);
      break;
    case ARGP_KEY_FINI:
      if (!app->threesq)
        {
          if (app->num_args != 1)
            argp_error (state, "too few arguments");
        }
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "NUM",
  "Generate 6:5 magic squares given NUM.\vThe parametric solution for the 3sq progressions used in this program was developed by Arkadiusz Wesolowski.  We take advantage of the fact that the ratio of cells 2,2 to 1,1 converges to 1/9, and is a perfect square.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_wesolowski_65_square_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.infile = stdin;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_wesolowski_65_square (&app);
}
