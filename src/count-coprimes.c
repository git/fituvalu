/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct fv_app_count_coprime_pairs_t
{
  int gt;
  int *only_show;
  int num_only_show;
  int (*read_numbers)(FILE *, mpz_t *, int, char **, size_t *);
  void (*disp_record) (mpz_t *vec, int size, FILE *out);
  int allow_negatives;
};

int
count_coprime_pairs (struct fv_app_count_coprime_pairs_t *app, mpz_t *a)
{
  struct mpz_pair *pairs = NULL;
  int num_pairs = 0;
  get_coprime_pairs (a, app->allow_negatives, &pairs, &num_pairs);
  for (int i = 0; i < num_pairs; i++)
    {
      mpz_clear (pairs[i].num[0]);
      mpz_clear (pairs[i].num[1]);
    }
  free (pairs);
  return num_pairs;
}

int
fituvalu_display_coprime_count (struct fv_app_count_coprime_pairs_t *app, FILE *stream)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[SIZE];

  int i;
  for (i = 0; i < SIZE; i++)
    mpz_init (a[i]);

  while (1)
    {
      read = app->read_numbers (stream, a, SIZE, &line, &len);
      if (read == -1)
        break;
      int count = count_coprime_pairs (app, a);
      if (app->num_only_show)
        {
          for (int i = 0; i < app->num_only_show; i++)
            {
              if (app->only_show[i] == count)
                {
                  app->disp_record (a, SIZE, stdout);
                  break;
                }
              else if (app->gt && count >= app->only_show[i])
                {
                  app->disp_record (a, SIZE, stdout);
                  break;
                }
            }
        }
      else
        {
          fprintf (stdout, "%d\n", count);
          fflush (stdout);
        }
    }

  for (i = 0; i < SIZE; i++)
    mpz_clear (a[i]);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_count_coprime_pairs_t *app = (struct fv_app_count_coprime_pairs_t *) state->input;
  switch (key)
    {
    case 'a':
      app->allow_negatives = 1;
      break;
    case 'g':
      app->gt = 1;
      break;
    case 'i':
      app->read_numbers = binary_read_numbers_from_stream;
      break;
    case 'o':
      app->disp_record = disp_binary_record;
      break;
    case 'f':
        {
          int num = atoi (arg);
          if (num >= 0 && num <= 36)
            {
              app->only_show =
                realloc (app->only_show, (app->num_only_show + 1) * sizeof (int));
              app->only_show[app->num_only_show] = atoi (arg);
              app->num_only_show++;
            }
          else
            argp_error (state, "%s is an invalid count for -f", arg);
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "filter", 'f', "NUM", 0, "Instead of showing the count, show the magic square if it has NUM coprime pairs"},
  { "gt", 'g', 0, 0, "Show magic squares with NUM or more coprime pairs (with -f)"},
  { "allow-negative", 'a', 0, 0, "Allow negative numbers"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares from the standard input and count the coprime pairs.\vThe nine values must be separated by a comma and terminated by a newline.  Option --out-binary is only used with --filter.  A 3x3 magic square has at most 36 coprime pairs.  A coprime is when the greatest common divisor between two numbers is 1.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_count_coprime_pairs_t app;
  memset (&app, 0, sizeof (app));
  app.read_numbers = read_numbers_from_stream;
  app.disp_record = disp_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_display_coprime_count (&app, stdin);
}
