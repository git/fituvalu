/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_69_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  double median;
  double max;
  double percent;
  int mult;
  int threesq;
  int subtype;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t i, iroot, j, limit, twoiroot, sum;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_69_type_1 (struct fv_app_search_69_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  //     +---+---+---+
  //     |   |   | B |
  //     +---+---+---+
  //     | A |   | D |
  //     +---+---+---+
  //     | E | C |   |
  //     +---+---+---+

  mpz_set ((*s)[1][0], w->lo);
  mpz_set ((*s)[2][1], w->hi);
  mpz_set ((*s)[1][2], q->i);

  mpz_set ((*s)[2][0], q->j);

  mpz_sub ((*s)[0][1], (*s)[2][0], w->distance);

  if (mpz_cmp ((*s)[0][1], (*s)[2][1]) > 0)
    {
      mpz_sub (q->sum, (*s)[0][1], (*s)[2][1]);
      mpz_cdiv_q_ui (q->sum, q->sum, 2);
      mpz_add ((*s)[1][1], (*s)[2][1], q->sum);
    }
  else
    {
      mpz_sub (q->sum, (*s)[2][1], (*s)[0][1]);
      mpz_cdiv_q_ui (q->sum, q->sum, 2);
      mpz_add ((*s)[1][1], (*s)[0][1], q->sum);
    }
  mpz_mul_ui (q->sum, (*s)[1][1], 3);
  mpz_sub ((*s)[0][0], q->sum, (*s)[1][0]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][0]);

  mpz_sub ((*s)[2][2], q->sum, (*s)[1][2]);
  mpz_sub ((*s)[2][2], (*s)[2][2], (*s)[0][2]);

    {
      //check for dups
      int dup = 0;
      if (mpz_cmp ((*s)[2][0], (*s)[1][0]) == 0 ||
          mpz_cmp ((*s)[1][1], (*s)[2][0]) == 0)
        dup = 1;
      else
        {
          mpz_sub (q->sum, q->sum, (*s)[0][0]);
          mpz_sub (q->sum, q->sum, (*s)[1][1]);
          mpz_sub (q->sum, q->sum, (*s)[2][2]);
        }

      if (!dup && mpz_cmp_ui (q->sum, 0) == 0)
        {
          if (mpz_perfect_square_p ((*s)[2][2]))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
generate_69_type_2 (struct fv_app_search_69_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  //     +---+---+---+
  //     |   |   | B |
  //     +---+---+---+
  //     | C |   | E |
  //     +---+---+---+
  //     | D | A |   |
  //     +---+---+---+

  mpz_set ((*s)[2][1], w->lo);
  mpz_set ((*s)[1][0], w->hi);
  mpz_set ((*s)[1][2], q->j);
  mpz_set ((*s)[2][0], q->i);

  mpz_add ((*s)[0][1], (*s)[2][0], w->distance);


  if (mpz_cmp ((*s)[0][1], (*s)[2][1]) > 0)
    {
      mpz_sub (q->sum, (*s)[0][1], (*s)[2][1]);
      mpz_cdiv_q_ui (q->sum, q->sum, 2);
      mpz_add ((*s)[1][1], (*s)[2][1], q->sum);
    }
  else
    {
      mpz_sub (q->sum, (*s)[2][1], (*s)[0][1]);
      mpz_cdiv_q_ui (q->sum, q->sum, 2);
      mpz_add ((*s)[1][1], (*s)[0][1], q->sum);
    }
  mpz_mul_ui (q->sum, (*s)[1][1], 3);
  mpz_sub ((*s)[0][0], q->sum, (*s)[1][0]);
  mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[2][0]);

  mpz_sub ((*s)[2][2], q->sum, (*s)[1][2]);
  mpz_sub ((*s)[2][2], (*s)[2][2], (*s)[0][2]);

    {
      //check for dups
      int dup = 0;
      if (mpz_cmp ((*s)[2][0], (*s)[1][0]) == 0 ||
          mpz_cmp ((*s)[1][1], (*s)[2][0]) == 0)
        dup = 1;
      else
        {
          mpz_sub (q->sum, q->sum, (*s)[0][0]);
          mpz_sub (q->sum, q->sum, (*s)[1][1]);
          mpz_sub (q->sum, q->sum, (*s)[2][2]);
        }

      if (!dup && mpz_cmp_ui (q->sum, 0) == 0)
        {
          if (mpz_perfect_square_p ((*s)[2][2]))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
generate_69_type_1_and_2 (struct fv_app_search_69_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  //we start counting down, and checking downwards for squares
  mpz_mul_ui (q->limit, w->hi, app->mult);

  mpz_sqrt (q->iroot, w->distance);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  while (1)
    {
      mpz_sub (q->j, q->i, w->distance);

      if (mpz_perfect_square_p (q->j))
        {
          if (app->subtype <= 1)
            generate_69_type_1 (app, w, q, s);
          if (app->subtype == 2 || app->subtype == 0)
            generate_69_type_2 (app, w, q, s);
        }

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
handle_progression (struct fv_app_search_69_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  generate_69_type_1_and_2 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_69_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[0][2], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[0][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[0][2], w->distance);
          mpz_add (w->hi, (*s)[0][2], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void
quick_generate_progressions (struct fv_app_search_69_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[0][2], 10);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[0][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[0][2], w->distance);
          mpz_add (w->hi, (*s)[0][2], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_69_t *app =
    (struct fv_app_search_69_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, NULL);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[0][2], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[0][2], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[0][2], 1) > 0)
        generate_progressions (app, &w, &q, &s);
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_69_t *app =
    (struct fv_app_search_69_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, NULL);

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[0][2], w.lo);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_69 (struct fv_app_search_69_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_69_t *app = (struct fv_app_search_69_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 's':
      app->subtype = atoi (arg);
      if (app->subtype <= 0 || app->subtype > 2)
        argp_error (state, "invalid subtype");
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'm':
      app->percent = strtod (arg, &end);
      if (end == NULL || *end != '\0' || app->percent <= 0)
        argp_error (state, "invalid argument `%s' to option --multiply", arg);
      app->percent /= 100.0;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      app->mult = ceil (app->max * app->percent);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "multiply", 'm', "PERC", 0, "Find a starting point for F by multiplying C by a value between 1 and 9755.2353 expressed as a percent (1-100)"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "subtype", 's', "NUM", 0, "Only produce subtype 1 or 2 instead of both"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:9 given a FILE containing top right values.\vWhen FILE is not provided, it is read from the standard input.  The default value for PERC is 0.006751823647016226.  Magic squares of type 6:9 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   |   | X |   |   |   | B |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating F\n"
"  | X |   | X |   | A |   | F |   upwards, checking for a new square E at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below F.\n"
"  | X | X | X |   | E | C | Z |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n"
"                  +---+---+---+\n"
"                  |   |   | B |\n"
"                  +---+---+---+  (starts iterating E upwards)\n"
"                  | C |   | D |\n"
"                  +---+---+---+\n"
"                  | E | A | Z |\n"
"                  +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_69_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.median = 2.2768;
  app.max = 101163.7797;
  app.percent = (app.median * 3.0) / app.max;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_69 (&app);
}
/*

the point of this is to identify information that can help us get more 6:9s.  we want to be able to predict 'center' values (the B values) for 6:9 magic squares.

  +---+---+---+   +---+---+---+
  |   |   | X |   |   |   | B |
  +---+---+---+   +---+---+---+
  | X |   | X |   | A |   | F |
  +---+---+---+   +---+---+---+
  | X | X | X |   | E | C | Z |
  +---+---+---+   +---+---+---+
                  +---+---+---+
                  |   |   | B |
                  +---+---+---+
                  | C |   | D |
                  +---+---+---+
                  | E | A | Z |
                  +---+---+---+

we identified which B values lead to magic squares with 6 perfect squares in the 6:9 configuration, and then here we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:9 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 737 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

34, 56, 78, 233 / 56332 = 0.004136
118, 34, 86, 166 / 39762 = 0.004175
16, 102, 86, 197 / 46385 = 0.004247
118, 119, 120, 113 / 26511 = 0.004262
84, 34, 86, 202 / 46393 = 0.004354
118, 102, 86, 136 / 30930 = 0.004397
0, 118, 120, 118 / 26512 = 0.004451
118, 0, 120, 118 / 26512 = 0.004451
118, 120, 0, 118 / 26512 = 0.004451
16, 68, 120, 220 / 46391 = 0.004742

well, 6:7 and 6:8 share 6:7 share this tuple 16, 68, 120.  and it's an arithmetic progression.  what does it say that this tuple keeps coming up?  i guess it means that this pattern is just generally good at generating 3sq progressions.

204 is a popular sum (16+68+120).
the percentage isn't particularly good.

second run is depth of 2, and breadth of 2000.

594, 358, 51 / 6648 = 0.007671
1546, 1548, 16 / 2052 = 0.007797
594, 1310, 26 / 3327 = 0.007815
1882, 1814, 14 / 1720 = 0.008140
1546, 1786, 17 / 1906 = 0.008919
1882, 1310, 18 / 1989 = 0.009050
594, 1786, 26 / 2667 = 0.009749
594, 834, 46 / 4439 = 0.010363
118, 1310, 46 / 4424 = 0.010398
1546, 1310, 30 / 2229 = 0.013459

hmm the pecentage is about 3 times higher.  down from 220 hits to 30 though.
there's a worry that we're just trying to predict somewhat random numbers here, and that any of the top 3 are just as good.   there's no frequent sum,  but there's something going on with sums 1428 and 2856.  and look at all the 1310s.  so weird.

and just for kicks, let's try a depth of 1 and a breadth of 100000.

36864, 4 / 90 = 0.044444
73702, 2 / 45 = 0.044444
73728, 2 / 45 = 0.044444
76016, 2 / 44 = 0.045455
83732, 2 / 40 = 0.050000
84966, 2 / 40 = 0.050000
85459, 2 / 39 = 0.051282

i'm sure we're just in la-la guessing land here.
but maybe 85459 is worth trying.

let's say the minimum count has to be 10 or higher.

644, 10 / 4900 = 0.002041
588, 11 / 5367 = 0.002050
504, 13 / 6261 = 0.002076
720, 10 / 4383 = 0.002282
612, 12 / 5155 = 0.002328
396, 19 / 7971 = 0.002384
564, 14 / 5595 = 0.002502
684, 12 / 4615 = 0.002600
840, 10 / 3758 = 0.002661
1026, 10 / 3078 = 0.003249

1026 is the best i guess.   no real standouts.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

4, 2, 0, 1380 / 1051201 = 0.001313
4, 1, 1, 1380 / 1576801 = 0.000875
4, 0, 2, 1380 / 1051201 = 0.001313
3, 1, 2, 1380 / 1576802 = 0.000875
2, 2, 2, 1380 / 1576802 = 0.000875
2, 2, 0, 1380 / 1576802 = 0.000875
2, 1, 1, 1380 / 2365203 = 0.000583
2, 0, 2, 1380 / 1576802 = 0.000875
2, 0, 0, 1380 / 1576802 = 0.000875
1, 3, 2, 1380 / 1576802 = 0.000875
1, 2, 0, 1380 / 2102403 = 0.000656
1, 1, 2, 1380 / 2365203 = 0.000583
1, 1, 1, 1380 / 3153604 = 0.000438
1, 1, 0, 1380 / 3153604 = 0.000438
1, 0, 2, 1380 / 2102403 = 0.000656
1, 0, 1, 1380 / 3153604 = 0.000438
1, 0, 0, 1380 / 3153604 = 0.000438
0, 4, 2, 1380 / 1051201 = 0.001313
0, 2, 2, 1380 / 1576802 = 0.000875
0, 2, 0, 1380 / 1576802 = 0.000875
0, 1, 2, 1380 / 2102403 = 0.000656
0, 1, 1, 1380 / 3153604 = 0.000438
0, 1, 0, 1380 / 3153604 = 0.000438
0, 0, 2, 1380 / 1576802 = 0.000875
0, 0, 1, 1380 / 3153604 = 0.000438
4, 6, 2, 1130 / 788401 = 0.001433
2, 2, 4, 1121 / 1182602 = 0.000948

the best is up by 4s and 2s.

results:

so looking forward for more sixes, pick one of "83732", "84966", or "85459".  Try "1026", and "1546, 1310", and "16, 68, 120".

we can recalculate quicker by using the pattern "4,2" and still get them all.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 1225 in sq-seq-finder.

0, 84, 84, 184 / 37563 = 0.004898
84, 0, 0, 184 / 37563 = 0.004898
84, 0, 84, 184 / 37563 = 0.004898
84, 84, 0, 184 / 37563 = 0.004898
84, 84, 84, 184 / 37563 = 0.004898
56, 112, 84, 185 / 37567 = 0.004925
56, 84, 28, 279 / 56334 = 0.004953
0, 56, 112, 196 / 37572 = 0.005217
56, 0, 112, 196 / 37572 = 0.005217
56, 112, 0, 196 / 37572 = 0.005217

well, lots of zeroes and 84s.
there are lots of sums based on a factor of 56.
i guess our best tuple is "56, 112, 84".  the percentage isn't very good.

no arithmetic progressions here.

we did a little better with a different starting point in terms of percentage.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

84, 1344, 49 / 4423 = 0.011078
840, 1680, 28 / 2519 = 0.011116
1344, 504, 39 / 3433 = 0.011360
0, 1848, 20 / 1724 = 0.011601
1848, 0, 20 / 1724 = 0.011601
1848, 1848, 20 / 1724 = 0.011601
1064, 1708, 27 / 2291 = 0.011785
560, 1820, 32 / 2671 = 0.011981
1988, 868, 27 / 2227 = 0.012124
1512, 1344, 35 / 2232 = 0.015681

hmm, well we did a tiny bit better. 
i guess "1512, 1344" is the best of the bunch.
this sure is one of the stranger types.  lots of zeroes hanging around.

okay let's do depth=1, and breadth is 100000

85155, 3 / 41 = 0.073171
87234, 3 / 40 = 0.075000
87978, 3 / 39 = 0.076923
89496, 3 / 39 = 0.076923
89740, 3 / 39 = 0.076923
90174, 3 / 38 = 0.078947
69440, 4 / 50 = 0.080000
93330, 3 / 37 = 0.081081
96264, 3 / 36 = 0.083333

well 8 percent is high.  does it pan out?  probably not.
and 8 is a lot better than 5 percent from earlier.

let's do depth=1, breadth is 100000, min hits of 10
2240, 11 / 1417 = 0.007763
924, 32 / 3436 = 0.009313
1680, 18 / 1889 = 0.009529
840, 36 / 3774 = 0.009539
2688, 12 / 1182 = 0.010152
2772, 12 / 1150 = 0.010435
2520, 14 / 1261 = 0.011102
1848, 20 / 1724 = 0.011601
3360, 12 / 949 = 0.012645
4340, 10 / 734 = 0.013624

well i suppose the 840 stands out with 36 hits.
but 4340 is the best.

so overall, looking forward for more sixes, pick one of "83732", "84966", or "85459", and then one of "69440", "93330", or "96264".  Try "1546, 1310", "16, 68, 120", "1512, 1344", and "56, 84, 28".  and throw in "1026" and "4340".

okay, let's do the first run again but sort on hits to see how it shakes out.

2, 4, 0, 1380 / 1051191 = 0.001313
2, 3, 1, 1380 / 1576786 = 0.000875
2, 2, 2, 1380 / 1576786 = 0.000875
2, 2, 0, 1380 / 1576786 = 0.000875
2, 1, 3, 1380 / 1576786 = 0.000875
2, 1, 1, 1380 / 2365178 = 0.000583
2, 1, 0, 1380 / 2102381 = 0.000656
2, 0, 4, 1380 / 1051191 = 0.001313
2, 0, 2, 1380 / 1576786 = 0.000875
2, 0, 1, 1380 / 2102381 = 0.000656
2, 0, 0, 1380 / 1576786 = 0.000875
1, 1, 4, 1380 / 1576786 = 0.000875
1, 1, 2, 1380 / 2365179 = 0.000583
1, 1, 1, 1380 / 3153571 = 0.000438
1, 1, 0, 1380 / 3153571 = 0.000438
1, 0, 1, 1380 / 3153571 = 0.000438
1, 0, 0, 1380 / 3153571 = 0.000438
0, 2, 4, 1380 / 1051191 = 0.001313
0, 2, 2, 1380 / 1576786 = 0.000875
0, 2, 1, 1380 / 2102381 = 0.000656
0, 2, 0, 1380 / 1576786 = 0.000875
0, 1, 1, 1380 / 3153571 = 0.000438
0, 1, 0, 1380 / 3153571 = 0.000438
0, 0, 2, 1380 / 1576786 = 0.000875
0, 0, 1, 1380 / 3153571 = 0.000438
2, 4, 6, 1130 / 788394 = 0.001433

no better than before. gotta go up by 2,4 with this starting point to get them all.

this concludes our analysis of anticipating B values in 6:9 magic squares.

in order to get high value 6:9s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

also we could consider running this test again but including all D values as B values.

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="83732" 1 10000000000000000000000000000 | ./search-69
sq-seq --pattern="84966" 1 10000000000000000000000000000 | ./search-69
sq-seq --pattern="85459" 1 10000000000000000000000000000 | ./search-69
sq-seq --pattern="1546,1310" 1 10000000000000000000000000000 | ./search-69
sq-seq --pattern="16,68,120" 1 10000000000000000000000000000 | ./search-69
sq-seq --pattern="1026" 1 10000000000000000000000000000 | ./search-69

sq-seq --pattern="69440" 1225 10000000000000000000000000000 | ./search-69
sq-seq --pattern="93330" 1225 10000000000000000000000000000 | ./search-69
sq-seq --pattern="96264" 1225 10000000000000000000000000000 | ./search-69
sq-seq --pattern="1512,1344" 1225 10000000000000000000000000000 | ./search-69
sq-seq --pattern="56,84,28" 1225 10000000000000000000000000000 | ./search-69
sq-seq --pattern="4340" 1225 10000000000000000000000000000 | ./search-69
*/
