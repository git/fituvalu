#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <argp.h>
#include <pthread.h>
#include <math.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_3sq_search_61_small_t
{
  FILE *out;
  int threads;
  int numargs;
  FILE *infile;
  int in_binary;
};

int
small_is_square2 (unsigned long long num, long double *root)
{
   *root = sqrtl (num);
   if ((unsigned long long)(*root) * (unsigned long long)(*root) == num)
     return 1;
   return 0;
}

struct thread_data_t
{
  void *data;
};

static int
read_numbers (FILE *stream, unsigned long long int *a, char **line, size_t *len, int binary)
{
  ssize_t read = 0;

  int size = 3;
  if (binary)
    {
      size_t ret = fread (a, sizeof (unsigned long long int), size, stream);
      if (ret != size)
        read = -1; //short read
    }
  else
    read = read_ull_numbers (stream, a, size, line, len);
  return read;
}

static void*
process_record (void *arg)
{
  char *line = NULL;
  size_t len = 0;
  unsigned long long int s[3][3];
  unsigned long long progression[3];
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_3sq_search_61_small_t *app =
    (struct fv_app_3sq_search_61_small_t *) param->data;

  while (1)
    {
      //go get the next progression to work on
      pthread_mutex_lock (&read_lock);

        {
          unsigned long long int p[3];
          if (read_numbers (app->infile, p, &line, &len, app->in_binary) < 0)
            {
              pthread_mutex_unlock (&read_lock);
              break;
            }
          memcpy (progression, p, sizeof (p));
        }
      pthread_mutex_unlock (&read_lock);

      //operate on progression
      s[2][0] = progression[0];
      s[1][1] = progression[1];
      s[0][2] = progression[2];

      unsigned long long int p0finaldigit = progression[0] % 10;
      unsigned long long int p1finaldigit = progression[1] % 10;
      unsigned long long int p2finaldigit = progression[2] % 10;
      int omit = 1;
      if (p0finaldigit == 1 && p1finaldigit == 1 && p2finaldigit == 1)
        omit = 0;
      else if (p0finaldigit == 1 && p1finaldigit == 5 && p2finaldigit == 9)
        omit = 0;
      else if (p0finaldigit == 5 && p1finaldigit == 5 && p2finaldigit == 5)
        omit = 0;
      else if (p0finaldigit == 9 && p1finaldigit == 5 && p2finaldigit == 1)
        omit = 0;
      else if (p0finaldigit == 9 && p1finaldigit == 9 && p2finaldigit == 9)
        omit = 0;

      if (omit)
        continue;
      unsigned long long int distance = s[1][1] - s[2][0];
      unsigned long long int twodistance = distance * 2;

      unsigned long long int sum = s[2][0] + s[1][1];
      sum = sum + s[0][2];
  
      //iterate over s[2][1], proceeding downwards
      s[2][1] = sum;
      unsigned long long int root = sqrtl (s[2][1]);
      s[2][1] = root * root;

      root--;
      s[2][1] -= root;
      s[2][1] -= root;
      s[2][1]--;

      long double diffroot;
      while (1)
        {
          if (root <= 0)
            break;
          if (twodistance > s[2][1]) //sorry, s[1][2] can't be negative
            break;
          s[1][2] = s[2][1] - twodistance;

          int check = 0;
          unsigned long long int num4mod10 = s[1][2] % 10;
          unsigned long long int num5mod10 = s[2][1] % 10;
          if (p0finaldigit == 5 && p2finaldigit == 5)
            {
              if (num4mod10 == 1 && num5mod10 == 1)
                check = 1;
              else if (num4mod10 == 9 && num5mod10 == 9)
                check = 1;
            }
          else if (p0finaldigit == 1 && p2finaldigit == 1)
            {
              if (num4mod10 == 1 && num5mod10 == 1)
                check = 1;
            }
          else if (p0finaldigit == 1 && p2finaldigit == 9)
            {
              if (num4mod10 == 1 && num5mod10 == 9)
                check = 1;
            }
          else if (p0finaldigit == 9 && p2finaldigit == 1)
            {
              if (num4mod10 == 9 && num5mod10 == 1)
                check = 1;
            }
          else if (p0finaldigit == 9 && p2finaldigit == 9)
            {
              if (num4mod10 == 9 && num5mod10 == 9)
                check = 1;
            }
          if (small_is_square2 (s[1][2], &diffroot) && check)
            {
              s[2][2] = sum - s[2][1];
              s[2][2] = s[2][2] - s[2][0];
              if (small_is_square2 (s[2][2], &diffroot))
                {
                  unsigned long long int sum_minus_middle = sum - s[1][1];
                  s[0][0] = sum_minus_middle - s[2][2];
                  s[0][1] = sum_minus_middle - s[2][1];
                  s[1][0] = sum_minus_middle - s[1][2];
                  if (s[0][0] != s[2][2])
                    {
                      pthread_mutex_lock (&display_lock);
                      fprintf (app->out,
                               "%llu, %lld, %llu, "
                               "%lld, %llu, %llu, "
                               "%llu, %llu, %llu,\n",
                               s[0][0], s[0][1], s[0][2],
                               s[1][0], s[1][1], s[1][2],
                               s[2][0], s[2][1], s[2][2]);
                      fflush (app->out);
                      pthread_mutex_unlock (&display_lock);
                    }
                }

            }
          root--;
          s[2][1] -= root;
          s[2][1] -= root;
          s[2][1]--;
        }
    }
  return NULL;
}

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static int
fituvalu_3sq_search_61_small (struct fv_app_3sq_search_61_small_t *app)
{
  run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_3sq_search_61_small_t *app =
    (struct fv_app_3sq_search_61_small_t *) state->input;
  switch (key)
    {
    case 'i':
      app->in_binary = 1;
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_ARG:
      app->numargs++;
      if (app->numargs > 1)
        argp_error (state, "too many arguments.");
      {
        app->infile = fopen (arg, "r");
        if (!app->infile)
          argp_error (state, "could not open `%s' for reading", arg);
      }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "in-binary", 'i', 0, 0, "Read in unsigned long longs instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the form 6:1.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_3sq_search_61_small_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.threads = 1;
  app.infile = stdin;
  argp_parse (&argp, argc, argv, 0, 0, &app);

  return fituvalu_3sq_search_61_small (&app);
}
