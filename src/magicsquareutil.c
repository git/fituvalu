/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <sys/time.h>
#include "magicsquareutil.h"

mpz_t row1;
mpz_t row2;
mpz_t row3;
mpz_t col1;
mpz_t col2;
mpz_t col3;
mpz_t diag1;
mpz_t diag2;

void
is_magic_square_init ()
{
  mpz_inits (row1, row2, row3, col1, col2, col3, diag1, diag2, NULL);
}

void
is_magic_square_fini ()
{
  mpz_clears (row1, row2, row3, col1, col2, col3, diag1, diag2, NULL);
}

int 
is_distinct (mpz_t a[3][3])
{
  int i, j, k, l;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++)
        for (l = 0; l < 3; l++)
          if (mpz_cmp (a[i][j], a[k][l]) == 0 && (i != k || j != l))
            return 0;
  return 1;
}

int 
is_magic_square (mpz_t a[3][3], int distinct)
{
  mpz_add (row1, a[0][0], a[0][1]);
  mpz_add (row1, row1, a[0][2]);

  mpz_add (row2, a[1][0], a[1][1]);
  mpz_add (row2, row2, a[1][2]);
  int res = mpz_cmp (row1, row2);
  if (res)
    return 0;

  mpz_add (row3, a[2][0], a[2][1]);
  mpz_add (row3, row3, a[2][2]);
  res = mpz_cmp (row3, row2);
  if (res)
    return 0;

  mpz_add (col1, a[0][0], a[1][0]);
  mpz_add (col1, col1, a[2][0]);
  res = mpz_cmp (col1, row1);
  if (res)
    return 0;

  mpz_add (col2, a[0][1], a[1][1]);
  mpz_add (col2, col2, a[2][1]);
  res = mpz_cmp (col2, col1);
  if (res)
    return 0;

  mpz_add (col3, a[0][2], a[1][2]);
  mpz_add (col3, col3, a[2][2]);
  res = mpz_cmp (col3, col2);
  if (res)
    return 0;

  mpz_add (diag1, a[0][0], a[1][1]);
  mpz_add (diag1, diag1, a[2][2]);
  res = mpz_cmp (diag1, col3);
  if (res)
    return 0;

  mpz_add (diag2, a[0][2], a[1][1]);
  mpz_add (diag2, diag2, a[2][0]);
  res = mpz_cmp (diag2, diag1);
  if (res)
    return 0;

  if (distinct)
    return is_distinct (a);
  return 1;
}

int
read_square_from_stream (FILE *stream, mpz_t (*a)[3][3], char **line, size_t *len)
{
  char s[3];
  char delim = ',';
  s[0] = delim;
  s[1] = '\n';
  s[2] = '\0';
  int i, j;
  ssize_t read;
  for (i = 0; i < 3; i++)
    {
      for (j = 0; j < 3; j++)
        {
          if (i == 2 && j == 2)
            read = fv_getline (line, len, stream);
          else
            read = fv_getdelim (line, len, delim, stream);
          if (read == -1)
            break;
          char *end = strpbrk (*line, s);
          if (end)
            *end = '\0';
          mpz_set_str ((*a)[i][j], *line, 10);
        }
      if (read == -1)
        break;
    }
  return read;
}

int
binary_read_square_from_stream (FILE *stream, mpz_t (*a)[3][3], char **s, size_t *len)
{
  int i, j;
  ssize_t read;
  for (i = 0; i < 3; i++)
    {
      for (j = 0; j < 3; j++)
        {
          read = mpz_inp_raw ((*a)[i][j], stream);
          if (!read)
            break;
        }
      if (!read)
        break;
    }
  if (read == 0)
    read = -1;
  return read;
}

static int
read_numbers_from_stream_delim (FILE *stream, mpz_t *a, int size, char **line, size_t *len, char delim)
{
  char s[3];
  s[0] = delim;
  s[1] = '\n';
  s[2] = '\0';
  int i;
  ssize_t read = 0;
  for (i = 0; i < size; i++)
    {
      if (i == size - 1)
        read = fv_getline (line, len, stream);
      else
        read = fv_getdelim (line, len, delim, stream);
      if (read == -1)
        break;
      char *end = strpbrk (*line, s);
      if (end)
        *end = '\0';
      int retval = mpz_set_str (a[i], *line, 10);
      if (retval < 0)
        i--; /* probably takes us to EOF */
    }
  return read;
}

int
read_tabbed_numbers_from_stream (FILE *stream, mpz_t *a, int size, char **line, size_t *len)
{
  return read_numbers_from_stream_delim (stream, a, size, line, len, '\t');
}

int
read_numbers_from_stream (FILE *stream, mpz_t *a, int size, char **line, size_t *len)
{
  return read_numbers_from_stream_delim (stream, a, size, line, len, ',');
}

int
binary_read_numbers_from_stream (FILE *stream, mpz_t *a, int size, char **line, size_t *len)
{
  int i;
  ssize_t read = 0;
  for (i = 0; i < size; i++)
    {
      read = mpz_inp_raw (a[i], stream);
      if (!read)
        break;
    }
  if (!read)
    read = -1;
  return read;
}

int
read_one_number_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return read_numbers_from_stream (stream, a, 1, line, len);
}

int
binary_read_one_number_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return binary_read_numbers_from_stream (stream, a, 1, line, len);
}

int
read_two_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return read_numbers_from_stream (stream, a, 2, line, len);
}

int
binary_read_two_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return binary_read_numbers_from_stream (stream, a, 2, line, len);
}

int
read_three_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return read_numbers_from_stream (stream, a, 3, line, len);
}

int
binary_read_three_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return binary_read_numbers_from_stream (stream, a, 3, line, len);
}

int
read_four_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return read_numbers_from_stream (stream, a, 4, line, len);
}

int
binary_read_four_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return binary_read_numbers_from_stream (stream, a, 4, line, len);
}

int
read_nine_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return read_numbers_from_stream (stream, a, 9, line, len);
}

int
binary_read_nine_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len)
{
  return binary_read_numbers_from_stream (stream, a, 9, line, len);
}

int
count_squares (mpz_t a[3][3])
{
  int i, j;
  int count = 0;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      if (mpz_perfect_square_p (a[i][j]))
        count++;
  return count;
}

void
small_read_square_and_run (FILE *stream, void (*iterfunc)(unsigned long long, unsigned long long, unsigned long long, void (*)(unsigned long long*, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *), void (*checkfunc)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), unsigned long long start, unsigned long long finish, void *data)
{
  ssize_t read;
  char *line = NULL;
  size_t len = 0;
  unsigned long long i;
  char *end = NULL;
  while (1)
    {
      read = fv_getline (&line, &len, stream);
      if (read == -1)
        break;
      end = NULL;
      i = strtoull (line, &end, 10);
      iterfunc (i, start, finish, checkfunc, data);
    }
  free (line);
  return;
}

void
read_square_and_run (FILE *stream, void (*iterfunc)(mpz_t, mpz_t, mpz_t, unsigned long long, void (*)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void*), void *), void (*checkfunc)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void*), mpz_t start, mpz_t finish, unsigned long long incr, void *out)
{
  mpz_t i;
  mpz_init (i);
  ssize_t read;
  char *line = NULL;
  size_t len = 0;
  while (1)
    {
      read = fv_getline (&line, &len, stream);
      if (read == -1)
        break;
      char *end = strchr (line, '\n');
      if (end)
        *end = '\0';
      mpz_set_str (i, line, 10);
      iterfunc (i, start, finish, incr, checkfunc, out);
    }
  mpz_clear (i);
  free (line);
  return;
}

void
binary_read_square_and_run (FILE *stream, void (*iterfunc)(mpz_t, mpz_t, mpz_t, unsigned long long, void (*)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void*), void *), void (*checkfunc)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void*), mpz_t start, mpz_t finish, unsigned long long incr, void *data)
{
  mpz_t i;
  mpz_init (i);
  ssize_t read;
  while (1)
    {
      read = mpz_inp_raw (i, stream);
      if (!read)
        break;
      iterfunc (i, start, finish, incr, checkfunc, data);
    }
  mpz_clear (i);
  return;
}

int
small_is_square (long long num)
{
  if (num < 0)
    return 0;
   long double root = sqrtl (num);
   if ((long long)root * (long long)root == num)
     return 1;
   return 0;
}

void
small_loop_and_run (void (*iterfunc)(unsigned long long, unsigned long long, unsigned long long, void (*)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *), void (*checkfunc)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), unsigned long long start, unsigned long long finish, void *data)
{
  unsigned long long root, lastroot, i;
  for (i = start; i < finish; i++)
    {
      if (small_is_square (i))
        {
          iterfunc (i, start, finish, checkfunc, data);
          root = sqrtl (i);
          lastroot = sqrtl (finish);
          do
            {
              i += root;
              i += root;
              i++;
              iterfunc (i, start, finish, checkfunc, data);
              root++;
            }
          while (root < lastroot);
          break;
        }
    }
}

void
loop_and_run (void (*iterfunc)(mpz_t, mpz_t, mpz_t, unsigned long long, void (*)(mpz_t*, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *), void (*checkfunc)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), mpz_t start, mpz_t finish, unsigned long long incr, void *data)
{
  mpz_t i, j, k, root, lastroot;
  mpz_inits (i, j, k, root, lastroot, NULL);
  for (mpz_set (i, start); mpz_cmp (i, finish) < 0; mpz_add_ui (i, i, 1))
    {
      if (mpz_perfect_square_p (i))
        {
          iterfunc (i, start, finish, incr, checkfunc, data);
          mpz_sqrt (root, i);
          mpz_sqrt (lastroot, finish);
          do
            {
              mpz_add (i, i, root);
              mpz_add (i, i, root);
              mpz_add_ui (i, i, 1);
              iterfunc (i, start, finish, incr, checkfunc, data);
              mpz_add_ui (root, root, 1);
            }
          while (mpz_cmp (root, lastroot) < 0);
          break;
        }
    }
  mpz_clears (i, j, k, root, lastroot, NULL);
}

static void
dump_num_to_stream (mpz_t *i, FILE *out)
{
  char *buf = malloc (mpz_sizeinbase (*i, 10) + 2);
  mpz_get_str (buf, 10, *i);
  fprintf (out, "%s", buf);
  free (buf);
}

void
disp_record (mpz_t *vec, int size, FILE *out)
{
  for (int i = 0; i < size; i++)
    {
      dump_num_to_stream (&vec[i], out);
      fprintf (out, ", ");
    }
  fprintf (out, "\n");
  fflush (out);
}

void
disp_binary_record (mpz_t *vec, int size, FILE *out)
{
  for (int i = 0; i < size; i++)
    mpz_out_raw (out, vec[i]);
  fflush (out);
}
void
display_three_record (mpz_t *progression, FILE *out)
{
  disp_record (progression, 3, out);
}

void
display_four_record (mpz_t *progression, FILE *out)
{
  disp_record (progression, 4, out);
  fflush (out);
}

void
display_binary_four_record (mpz_t *progression, FILE *out)
{
  disp_binary_record (progression, 4, out);
}

void
display_six_record (mpz_t *progression, FILE *out)
{
  disp_record (progression, 6, out);
  fflush (out);
}

void
display_binary_six_record (mpz_t *progression, FILE *out)
{
  disp_binary_record (progression, 6, out);
}

void
display_three_record_with_root (mpz_t *progression, mpz_t *root, FILE *out)
{
  for (int i = 0; i < 3; i++)
    {
      dump_num_to_stream (&progression[i], out);
      fprintf (out, ", ");
    }
  if (mpz_cmp_ui (*root, 0) != 0)
    {
      dump_num_to_stream (root, out);
      fprintf (out, ", ");
    }
  fprintf (out, "\n");
  fflush (out);
}

void
display_binary_three_record (mpz_t *progression, FILE *out)
{
  disp_binary_record (progression, 3, out);
}

void
display_binary_three_record_with_root (mpz_t *progression, mpz_t *root, FILE *out)
{
  for (int i = 0; i < 3; i++)
    mpz_out_raw (out, progression[i]);
  if (mpz_cmp_ui (*root, 0) != 0)
    mpz_out_raw (out, *root);
  fflush (out);
}

void
display_nine_record (mpz_t *progression, FILE *out)
{
  disp_record (progression, 9, out);
  fflush (out);
}

void
display_binary_two_record (mpz_t *one, mpz_t *two, FILE *out)
{
  mpz_out_raw (out, *one);
  mpz_out_raw (out, *two);
  fflush (out);
}

void
display_two_record (mpz_t *one, mpz_t *two, FILE *out)
{
  dump_num_to_stream (one, out);
  fprintf (out, ", ");
  dump_num_to_stream (two, out);
  fprintf (out, "\n");
  fflush (out);
}

void
display_binary_nine_record (mpz_t *progression, FILE *out)
{
  disp_binary_record (progression, 9, out);
}

void
display_square_record (mpz_t s[3][3], FILE *out)
{
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      {
        dump_num_to_stream (&s[i][j], out);
        fprintf (out, ", ");
      }
  fprintf (out, "\n");
  fflush (out);
}

void
display_binary_square_record (mpz_t s[3][3], FILE *out)
{
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_out_raw (out, s[i][j]);
  fflush (out);
}

static void
small_seq (unsigned long long int m, unsigned long long int n, int finish, void *data, void (*search)(unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), unsigned long long _m2, unsigned long long _n2)
{
  search (m, n, _m2, _n2, data);
  unsigned long long int s = m + n;
  if (s < finish)
    {
      unsigned long long int m2 = s;
      unsigned long long int n2 = m;
      if ((n2 & 1) == 0)
        small_seq (m2, n2, finish, data, search, _m2, _n2);
      else
        {
          s = m2 + n2;
          if (s < finish)
            {
              small_seq (s, m2, finish, data, search, _m2, _n2);
              small_seq (s, n2, finish, data, search, _m2, _n2);
            }
        }
      n2 = n;
      if ((n2 & 1) == 0)
        small_seq (m2, n2, finish, data, search, _m2, _n2);
      else
        {
          s = m2 + n2;
          if (s < finish)
            {
              small_seq (s, m2, finish, data, search, _m2, _n2);
              small_seq (s, n2, finish, data, search, _m2, _n2);
            }
        }
    }
}

int
small_morgenstern_search (unsigned long long max, FILE *in, void (*search) (unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *data)
{
  ssize_t read;
  char *line = NULL;
  size_t len = 0;
  unsigned long long m, n;
  char *end = NULL;
  while (1)
    {
      read = fv_getdelim (&line, &len, ',', in);
      if (read == -1)
        break;
      end = NULL;
      m = strtoull (line, &end, 10);
      read = fv_getline (&line, &len, in);
      if (read == -1)
        break;
      end = NULL;
      n = strtoull (line, &end, 10);
      small_seq (2, 1, max, data, search, m, n);
    }
  if (line)
    free (line);
  return 0;
}

int
reduce_three_square_progression (mpz_t *progression)
{
  int reduced = 0;
  //progression must already be sorted
  //and all 3 numbers must be perfect squares
  mpz_t gcd;
  mpz_init (gcd);
  mpz_set (gcd, progression[0]);
  mpz_gcd (gcd, gcd, progression[1]);
  mpz_gcd (gcd, gcd, progression[2]);
  if (mpz_cmp_ui (gcd, 1) > 0)
    {
      mpz_t d;
      mpz_init (d);
      int squares_retained = 1;
      for (int i = 0; i < 3; i++)
        {
          mpz_cdiv_q (d, progression[i], gcd);
          if (!mpz_perfect_square_p (d) &&
              mpz_perfect_square_p (progression[i]))
            {
              squares_retained = 0;
              break;
            }
        }
      mpz_clear (d);
      if (squares_retained)
        {
          reduced = 1;
          for (int i = 0; i < 3; i++)
            mpz_cdiv_q (progression[i], progression[i], gcd);
        }
    }
  mpz_clear (gcd);
  return reduced;
}

static void
_dual_inner (FILE *in, mpz_t m, mpz_t n, void (*search)(mpz_t, mpz_t, mpz_t, mpz_t, void *, void *), void *data, void *pt)
{
  ssize_t read;
  char *line = NULL;
  size_t len = 0;
  mpz_t r, s;
  mpz_inits (r, s, NULL);
  while (1)
    {
      read = fv_getdelim (&line, &len, ',', in);
      if (read == -1)
        break;
      char *comma = strchr (line, ',');
      if (comma)
        *comma = '\0';
      mpz_set_str (r, line, 10);
      read = fv_getline (&line, &len, in);
      if (read == -1)
        break;
      mpz_set_str (s, line, 10);
      search (m, n, r, s, data, pt);
    }
  mpz_clears (r, s, NULL);
  if (line)
    free (line);
}

static void
_dual_inner_int (FILE *in, unsigned long m, unsigned long n, void (*search_int)(unsigned long, unsigned long, unsigned long, unsigned long, void *, void *), void *data, void *pt)
{
  ssize_t read;
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long r, s;
  while (1)
    {
      read = fv_getdelim (&line, &len, ',', in);
      if (read == -1)
        break;
      char *comma = strchr (line, ',');
      if (comma)
        *comma = '\0';
      end = NULL;
      r = strtoul (line, &end, 10);
      read = fv_getline (&line, &len, in);
      if (read == -1)
        break;
      end = NULL;
      s = strtoul (line, &end, 10);
      search_int (m, n, r, s, data, pt);
    }
  if (line)
    free (line);
}


void
morgenstern_search_dual (FILE *in1, FILE *in2, void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *), int threads, void* (*init)(), void (*destroy)(void*),void *data)
{
  //in2 is rewindable, in1 is not.
  ssize_t read;
  char *line = NULL;
  size_t len = 0;
  mpz_t m, n;
  mpz_inits (m, n, NULL);
  void *pt = NULL;
  if (init)
    pt = init ();
  while (1)
    {
      read = fv_getdelim (&line, &len, ',', in1);
      if (read == -1)
        break;
      char *comma = strchr (line, ',');
      if (comma)
        *comma = '\0';
      mpz_set_str (m, line, 10);
      read = fv_getline (&line, &len, in1);
      if (read == -1)
        break;
      mpz_set_str (n, line, 10);
      rewind (in2);
      _dual_inner (in2, m, n, search, data, pt);
    }
  if (pt && destroy)
    destroy (pt);
  mpz_clears (m, n, NULL);
  if (line)
    free (line);
  return;
}

void
morgenstern_search_dual_int (FILE *in1, FILE *in2, void (*search_int) (unsigned long, unsigned long, unsigned long, unsigned long, void *, void *), int threads, void* (*init)(), void (*destroy)(void*),void *data)
{
  //in2 is rewindable, in1 is not.
  ssize_t read;
  char *line = NULL, *end = NULL;
  size_t len = 0;
  void *pt = NULL;
  unsigned long m, n;
  if (init)
    pt = init ();
  while (1)
    {
      read = fv_getdelim (&line, &len, ',', in1);
      if (read == -1)
        break;
      char *comma = strchr (line, ',');
      if (comma)
        *comma = '\0';
      end = NULL;
      m = strtoul (line, &end, 10);
      read = fv_getline (&line, &len, in1);
      if (read == -1)
        break;
      end = NULL;
      n = strtoul (line, &end, 10);
      rewind (in2);
      _dual_inner_int (in2, m, n, search_int, data, pt);
    }
  if (pt && destroy)
    destroy (pt);
  if (line)
    free (line);
  return;
}

static void
_dual_binary_inner (FILE *in, mpz_t m, mpz_t n, void (*search)(mpz_t, mpz_t, mpz_t, mpz_t, void *, void *), void *data, void *pt)
{
  ssize_t read;
  mpz_t r, s;
  mpz_inits (r, s, NULL);
  while (1)
    {
      read = mpz_inp_raw (r, in);
      if (!read)
        break;
      read = mpz_inp_raw (s, in);
      if (!read)
        break;
      search (m, n, r, s, data, pt);
    }
  mpz_clears (r, s, NULL);
  return;
}

void
morgenstern_search_dual_binary (FILE *in1, FILE *in2, void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *), int threads, void* (*init)(), void (*destroy)(void*),void *data)
{
  ssize_t read;
  mpz_t m, n;
  mpz_inits (m, n, NULL);
  void *pt = NULL;
  if (init)
    pt = init ();
  while (1)
    {
      read = mpz_inp_raw (m, in1);
      if (!read)
        break;
      read = mpz_inp_raw (n, in1);
      if (!read)
        break;
      rewind (in2);
      _dual_binary_inner (in2, m, n, search, data, pt);
    }
  if (pt && destroy)
    destroy (pt);
  mpz_clears (m, n, NULL);
  return;
}

void
display_textual_number (mpz_t *i, FILE *out)
{
  dump_num_to_stream (i, out);
  fprintf (out, "\n");
  fflush (out);
}

void
display_textual_number_no_newline (mpz_t i, FILE *out)
{
  char *buf = malloc (mpz_sizeinbase (i, 10) + 2);
  mpz_get_str (buf, 10, i);
  fprintf (out, "%s", buf);
  free (buf);
  fflush (out);
}

void
display_binary_number (mpz_t *i, FILE *out)
{
  mpz_out_raw (out, *i);
  fflush (out);
}

int
fv_getline (char **line, size_t *len, FILE *stream)
{
  ssize_t read;
  while (1)
    {
      read = getline (line, len, stream);
      if (read == -1)
        break;
      if ((*line)[0] == '#')
        continue;
      break;
    }
  return read;
}

int
fv_getdelim (char **line, size_t *len, int delim, FILE *stream)
{
  ssize_t read;
  while (1)
    {
      read = getdelim (line, len, delim, stream);
      if (read == -1)
        break;
      if ((*line)[0] == '#')
        {
          if (strchr (*line, '\n'))
            {
              /* crapola, we're in a special case here.
               * we've sucked in a comment and gone clear over
               * a number to the next comma. */
              char *nl = strrchr (*line, '\n');
              nl++;
              if (nl[0] == '#')
                {
                  read = getline (line, len, stream);
                  continue;
                }
              memcpy (*line, nl, strlen (nl) + 1);
              break;
            }
          else
            {
              read = getline (line, len, stream);
              if (read == -1)
                break;
            }
          continue;
        }
      break;
    }
  return read;
}

static void
get_lowest (mpz_t a[3][3], mpz_t *low)
{
  mpz_t i;
  mpz_set_si (*low, -1);
  mpz_init (i);

  int j, k;
  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      {
        if (mpz_sgn (a[j][k]) > 0 &&
            (mpz_cmp (a[j][k], *low) < 0 || mpz_cmp_si (*low, -1) == 0))
          mpz_set (*low, a[j][k]);
      }
  if (mpz_sgn (*low) < 0)
    mpz_abs (*low, *low);
  mpz_clear (i);
}

void
reduce_square (mpz_t a[3][3])
{
  int j, k;
  mpz_t low, gcd;
  mpz_inits (low, gcd, NULL);
  get_lowest (a, &low);
  mpz_set (gcd, low);
  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      {
        if (mpz_cmp (a[j][k], low) == 0)
          continue;
        mpz_gcd (gcd, gcd, a[j][k]);
      }

  //printf("gcd is ");
    //display_textual_number (&gcd, stdout);
  if (mpz_cmp_ui (gcd, 1) > 0)
    {
      int squares_retained = 1;
      mpz_t root, d;
      mpz_inits (root, d, NULL);
      mpz_sqrt (root, gcd);
      mpz_mul (gcd, root, root);
      while (1)
        {
          squares_retained = 1;
          for (j = 0; j < 3; j++)
            {
              for (k = 0; k < 3; k++)
                {
                  mpz_mod (d, a[j][k], gcd);
                  if (mpz_cmp_ui (d, 0) != 0)
                    {
                      squares_retained = 0;
                      break;
                    }
                  mpz_cdiv_q (d, a[j][k], gcd);
                  if (mpz_perfect_square_p (a[j][k]))
                    {
                      if (!mpz_perfect_square_p (d))
                        {
                          squares_retained = 0;
                          break;
                        }
                    }
                }
              if (squares_retained == 0)
                break;
            }
          if (squares_retained)
            break;
          if (mpz_cmp_ui (gcd, 1) == 0)
            break;
          mpz_sub_ui (root, root, 1);
          mpz_sub (gcd, gcd, root);
          mpz_sub (gcd, gcd, root);
          mpz_sub_ui (gcd, gcd, 1);
        }
      mpz_clears (root, d, NULL);


      if (squares_retained)
        {
          for (j = 0; j < 3; j++)
            for (k = 0; k < 3; k++)
              mpz_cdiv_q (a[j][k], a[j][k], gcd);
        }
    }
  mpz_clears (low, gcd, NULL);
}

struct irec
{
  unsigned long mn[2];
};

struct rec
{
  mpz_t mn[2];
};

static void
load_mn_binary (FILE *in, struct rec **recs, int *numrecs)
{
  ssize_t read;
  mpz_t m, n;
  mpz_inits (m, n, NULL);
  while (1)
    {
      read = mpz_inp_raw (m, in);
      if (!read)
        break;
      read = mpz_inp_raw (n, in);
      if (!read)
        break;
      *recs = realloc (*recs, (*numrecs + 1) * sizeof (struct rec));
      mpz_init_set ((*recs)[*numrecs].mn[0], m);
      mpz_init_set ((*recs)[*numrecs].mn[1], n);
      (*numrecs)++;
    }
  mpz_clears (m, n, NULL);
}

static void
load_mn (FILE *in, struct rec **recs, int *numrecs)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t m, n;
  mpz_inits (m, n, NULL);
  while (1)
    {
      read = fv_getdelim (&line, &len, ',', in);
      if (read == -1)
        break;
      char *comma = strchr (line, ',');
      if (comma)
        *comma = '\0';
      mpz_set_str (m, line, 10);
      read = fv_getline (&line, &len, in);
      if (read == -1)
        break;
      mpz_set_str (n, line, 10);
      *recs = realloc (*recs, (*numrecs + 1) * sizeof (struct rec));
      mpz_init_set ((*recs)[*numrecs].mn[0], m);
      mpz_init_set ((*recs)[*numrecs].mn[1], n);
      (*numrecs)++;
    }
  if (line)
    free (line);
  mpz_clears (m, n, NULL);
}

static void
load_mn_int (FILE *in, struct irec **recs, int *numrecs)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  unsigned long m, n;
  char *end = NULL;
  while (1)
    {
      read = fv_getdelim (&line, &len, ',', in);
      if (read == -1)
        break;
      char *comma = strchr (line, ',');
      if (comma)
        *comma = '\0';
      end = NULL;
      m = strtoul (line, &end, 10);
      read = fv_getline (&line, &len, in);
      if (read == -1)
        break;
      end = NULL;
      n = strtoul (line, &end, 10);
      *recs = realloc (*recs, (*numrecs + 1) * sizeof (struct irec));
      (*recs)[*numrecs].mn[0] = m;
      (*recs)[*numrecs].mn[1] = n;

      (*numrecs)++;
    }
  if (line)
    free (line);
}

struct thread_idata_t
{
  FILE *in;
  struct irec *in2recs;
  int num_in2recs;
  void (*search_int) (unsigned long, unsigned long, unsigned long, unsigned long, void *, void *);
  void *data;
  pthread_mutex_t *read_lock;
  void *pt;
};

static void
run_threads_int (FILE *in1, struct irec *in2recs, int num_in2recs, void (*search_int) (unsigned long, unsigned long, unsigned long, unsigned long, void *, void *), void *data, int num_threads, void* (*func)(void*), void* (*init)(), void (*destroy)(void*))
{
  int retval;
  pthread_mutex_t read_lock;
  pthread_mutex_init (&read_lock, NULL);
  pthread_t threads[num_threads];
  struct thread_idata_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].in = in1;
      param[i].in2recs = in2recs;
      param[i].num_in2recs = num_in2recs;
      param[i].search_int = search_int;
      param[i].data = data;
      param[i].read_lock = &read_lock;
      if (init)
	 param[i].pt = init();
      else
         param[i].pt = NULL;
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
  for (int i = 0; i < num_threads; i++)
    {
      if (param[i].pt && destroy)
        destroy (param[i].pt);
    }
}

struct thread_data_t
{
  FILE *in;
  struct rec *in2recs;
  int num_in2recs;
  void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *);
  void *data;
  pthread_mutex_t *read_lock;
  void *pt;
};

static void
run_threads (FILE *in1, struct rec *in2recs, int num_in2recs, void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *), void *data, int num_threads, void* (*func)(void*), void* (*init)(), void (*destroy)(void*))
{
  int retval;
  pthread_mutex_t read_lock;
  pthread_mutex_init (&read_lock, NULL);
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].in = in1;
      param[i].in2recs = in2recs;
      param[i].num_in2recs = num_in2recs;
      param[i].search = search;
      param[i].data = data;
      param[i].read_lock = &read_lock;
      if (init)
	 param[i].pt = init();
      else
         param[i].pt = NULL;
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
  for (int i = 0; i < num_threads; i++)
    {
      if (param[i].pt && destroy)
        destroy (param[i].pt);
    }
}

static void*
_morgenstern_search_dual_binary_mem (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;

  FILE *in = param->in;
  struct rec *in2recs = param->in2recs;
  int num_in2recs = param->num_in2recs;
  void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *) = param->search;
  void *data = param->data;
  void *pt = param->pt;
  /*
   **
   ***
   ****
   *****
   ******
   */
  unsigned long long i;
  ssize_t read;
  while (1)
    {
      pthread_mutex_lock (param->read_lock);
      read = fread (&i, sizeof (i), 1, in);
      pthread_mutex_unlock (param->read_lock);
      if (!read)
        break;
      if (i >= num_in2recs)
        continue;
      for (unsigned long long j = 0; j < i; j++)
        search (in2recs[i].mn[0], in2recs[i].mn[1],
                in2recs[j].mn[0], in2recs[j].mn[1], data, pt);
    }

  return NULL;
}

void
morgenstern_search_dual_binary_mem (FILE *in1, FILE *in2, void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *), int threads, void* (*init)(), void (*destroy)(void*), void *data)
{
  struct rec *in2recs = NULL;
  int num_in2recs = 0;
  load_mn_binary (in2, &in2recs, &num_in2recs);
  run_threads (in1, in2recs, num_in2recs, search, data, threads,
               _morgenstern_search_dual_binary_mem, init, destroy);

  for (int i = 0; i < num_in2recs; i++)
    {
      mpz_clear (in2recs[i].mn[0]);
      mpz_clear (in2recs[i].mn[1]);
    }
  free (in2recs);
}

static void*
_morgenstern_search_dual_mem (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;

  FILE *in = param->in;
  struct rec *in2recs = param->in2recs;
  int num_in2recs = param->num_in2recs;
  void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *) = param->search;
  void *data = param->data;
  void *pt = param->pt;
    /*
     **
     ***
     ****
     *****
     ******
     */
  unsigned long long i;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  char *end = NULL;
  char *limit_str = getenv ("FV_REC_LOW_LIMIT");
  mpz_t low_limit;
  mpz_init (low_limit);
  if (limit_str)
    mpz_set_str (low_limit, limit_str, 10);
  while (1)
    {
      pthread_mutex_lock (param->read_lock);
      read = fv_getline (&line, &len, in);
      pthread_mutex_unlock (param->read_lock);
      if (read == -1)
        break;
      i = strtoull (line, &end, 10);
      if (i >= num_in2recs)
        continue;
      for (unsigned long long j = 0; j < i; j++)
        {
          if (limit_str)
            {
              int nope = 1;
              if (mpz_cmp (in2recs[i].mn[0], low_limit) > 0 ||
                  mpz_cmp (in2recs[i].mn[1], low_limit) > 0 ||
                  mpz_cmp (in2recs[j].mn[1], low_limit) > 0 ||
                  mpz_cmp (in2recs[j].mn[1], low_limit) > 0)
                nope = 0;
              if (nope)
                continue;
            }

            search (in2recs[j].mn[0], in2recs[j].mn[1],
                    in2recs[i].mn[0], in2recs[i].mn[1], data, pt);
            search (in2recs[i].mn[0], in2recs[i].mn[1],
                    in2recs[j].mn[0], in2recs[j].mn[1], data, pt);
        }
    }

  mpz_clear (low_limit);
  //clean up
  if (line)
    free (line);

  return NULL;
}

static void*
_morgenstern_search_dual_mem_int (void *arg)
{
  struct thread_idata_t *param = (struct thread_idata_t *) arg;

  FILE *in = param->in;
  struct irec *in2recs = param->in2recs;
  int num_in2recs = param->num_in2recs;
  void (*search_int) (unsigned long, unsigned long, unsigned long, unsigned long, void *, void *) = param->search_int;
  void *data = param->data;
  void *pt = param->pt;
    /*
     **
     ***
     ****
     *****
     ******
     */
  unsigned long i;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  char *end = NULL;
  char *limit_str = getenv ("FV_REC_LOW_LIMIT");
  unsigned long low_limit;
  if (limit_str)
    low_limit = strtoul (limit_str, &end, 10);
  while (1)
    {
      pthread_mutex_lock (param->read_lock);
      read = fv_getline (&line, &len, in);
      pthread_mutex_unlock (param->read_lock);
      if (read == -1)
        break;
      i = strtoul (line, &end, 10);
      if (i >= num_in2recs)
        continue;
      for (unsigned long j = 0; j < i; j++)
        {
          if (limit_str)
            {
              int nope = 1;
              if (in2recs[i].mn[0] > low_limit ||
                  in2recs[i].mn[1] > low_limit ||
                  in2recs[j].mn[1] > low_limit ||
                  in2recs[j].mn[1] > low_limit)
                nope = 0;
              if (nope)
                continue;
            }

            search_int (in2recs[j].mn[0], in2recs[j].mn[1],
                        in2recs[i].mn[0], in2recs[i].mn[1], data, pt);
            search_int (in2recs[i].mn[0], in2recs[i].mn[1],
                        in2recs[j].mn[0], in2recs[j].mn[1], data, pt);
        }
    }

  //clean up
  if (line)
    free (line);

  return NULL;
}

void
morgenstern_search_dual_mem (FILE *in1, FILE *in2, void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *), int num_threads, void* (*init)(), void (*destroy)(void*), void *data)
{
  struct rec *in2recs = NULL;
  int num_in2recs = 0;
  load_mn (in2, &in2recs, &num_in2recs);
  run_threads (in1, in2recs, num_in2recs, search, data, num_threads,
               _morgenstern_search_dual_mem, init, destroy);

  for (int i = 0; i < num_in2recs; i++)
    {
      mpz_clear (in2recs[i].mn[0]);
      mpz_clear (in2recs[i].mn[1]);
    }
  free (in2recs);
}

void
morgenstern_search_dual_mem_int (FILE *in1, FILE *in2, void (*search_int) (unsigned long, unsigned long, unsigned long, unsigned long, void *, void *), int num_threads, void* (*init)(), void (*destroy)(void*), void *data)
{
  struct irec *in2recs = NULL;
  int num_in2recs = 0;
  load_mn_int (in2, &in2recs, &num_in2recs);
  run_threads_int (in1, in2recs, num_in2recs, search_int, data, num_threads,
               _morgenstern_search_dual_mem_int, init, destroy);

  free (in2recs);
}

void
display_threesq (mpz_t a, mpz_t b, mpz_t c, FILE *out)
{
  display_textual_number_no_newline (a, out);
  fprintf (out, ", ");
  display_textual_number_no_newline (b, out);
  fprintf (out, ", ");
  display_textual_number_no_newline (c, out);
  fprintf (out, "\n");
}

void
binary_display_threesq (mpz_t a, mpz_t b, mpz_t c, FILE *out)
{
  mpz_out_raw (out, a);
  mpz_out_raw (out, b);
  mpz_out_raw (out, c);
  fflush (out);
}

void
generate_progression_kahn_kwong (mpz_t *progression, int high)
{
  //https://www.fq.math.ca/Papers1/43-2/paper43-2-1.pdf
  int sign1 = 1, sign2 = 1, sign3 = 1, sign4 = 1;
  if (high)
    {
      sign1 = -1; sign2 = 1; sign3 = -1; sign4 = 1;
    }
  mpz_t threea, threeb, twoa, fourb, r1, r2, r3, a, b;
  mpz_inits (threea, threeb, twoa, fourb, a, b, NULL);
  mpz_inits (r1, r2, r3, NULL);
  mpz_sqrt (r1, progression[0]);
  mpz_sqrt (r2, progression[1]);
  mpz_sqrt (r3, progression[2]);
  if (sign1 > 0)
    mpz_abs (b, r2);
  else
    mpz_neg (b, r2);
  mpz_mul_ui (threeb, b, 3);
  if (sign2 > 0)
    mpz_abs (a, r1);
  else
    mpz_neg (a, r1);
  mpz_mul_ui (twoa, a, 2);
  if (sign3 > 0)
    mpz_abs (b, r2);
  else
    mpz_neg (b, r2);
  mpz_mul_ui (fourb, b, 4);
  if (sign4 > 0)
    mpz_abs (a, r1);
  else
    mpz_neg (a, r1);
  mpz_mul_ui (threea, a, 3);
  mpz_set (progression[0], r3);
  mpz_sub (progression[1], threeb, twoa);
  mpz_sub (progression[2], fourb, threea);
  mpz_mul (progression[0], progression[0], progression[0]);
  mpz_mul (progression[1], progression[1], progression[1]);
  mpz_mul (progression[2], progression[2], progression[2]);
  mpz_clears (threea, threeb, twoa, fourb, a, b, NULL);
  mpz_clears (r1, r2, r3, NULL);
}

void
rotate_square (mpz_t (*result)[3][3])
{
  //rotates left
  int i, j;
  mpz_t a[3][3];
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  mpz_set (a[0][0],(*result)[0][0]);
  mpz_set (a[0][1],(*result)[0][1]);
  mpz_set (a[0][2],(*result)[0][2]);
  mpz_set (a[1][0],(*result)[1][0]);
  mpz_set (a[1][1],(*result)[1][1]);
  mpz_set (a[1][2],(*result)[1][2]);
  mpz_set (a[2][0],(*result)[2][0]);
  mpz_set (a[2][1],(*result)[2][1]);
  mpz_set (a[2][2],(*result)[2][2]);
  mpz_set ((*result)[0][0], a[0][2]);
  mpz_set ((*result)[0][1], a[1][2]);
  mpz_set ((*result)[0][2], a[2][2]);
  mpz_set ((*result)[1][0], a[0][1]);
  mpz_set ((*result)[1][1], a[1][1]);
  mpz_set ((*result)[1][2], a[2][1]);
  mpz_set ((*result)[2][0], a[0][0]);
  mpz_set ((*result)[2][1], a[1][0]);
  mpz_set ((*result)[2][2], a[2][0]);
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
}

void
flip_square_horizontally (mpz_t (*result)[3][3])
{
  int i, j;
  mpz_t a[3][3];
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);
  mpz_set (a[0][0], (*result)[0][2]);
  mpz_set (a[0][1], (*result)[0][1]);
  mpz_set (a[0][2], (*result)[0][0]);
  mpz_set (a[1][0], (*result)[1][2]);
  mpz_set (a[1][1], (*result)[1][1]);
  mpz_set (a[1][2], (*result)[1][0]);
  mpz_set (a[2][0], (*result)[2][2]);
  mpz_set (a[2][1], (*result)[2][1]);
  mpz_set (a[2][2], (*result)[2][0]);
  mpz_set ((*result)[0][0], a[0][0]);
  mpz_set ((*result)[0][1], a[0][1]);
  mpz_set ((*result)[0][2], a[0][2]);
  mpz_set ((*result)[1][0], a[1][0]);
  mpz_set ((*result)[1][1], a[1][1]);
  mpz_set ((*result)[1][2], a[1][2]);
  mpz_set ((*result)[2][0], a[2][0]);
  mpz_set ((*result)[2][1], a[2][1]);
  mpz_set ((*result)[2][2], a[2][2]);
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
}

void
flip_square_vertically (mpz_t (*result)[3][3])
{
  int i, j;
  mpz_t a[3][3];
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);
  mpz_set (a[0][0], (*result)[2][0]);
  mpz_set (a[0][1], (*result)[2][1]);
  mpz_set (a[0][2], (*result)[2][2]);
  mpz_set (a[1][0], (*result)[1][0]);
  mpz_set (a[1][1], (*result)[1][1]);
  mpz_set (a[1][2], (*result)[1][2]);
  mpz_set (a[2][0], (*result)[0][0]);
  mpz_set (a[2][1], (*result)[0][1]);
  mpz_set (a[2][2], (*result)[0][2]);
  mpz_set ((*result)[0][0], a[0][0]);
  mpz_set ((*result)[0][1], a[0][1]);
  mpz_set ((*result)[0][2], a[0][2]);
  mpz_set ((*result)[1][0], a[1][0]);
  mpz_set ((*result)[1][1], a[1][1]);
  mpz_set ((*result)[1][2], a[1][2]);
  mpz_set ((*result)[2][0], a[2][0]);
  mpz_set ((*result)[2][1], a[2][1]);
  mpz_set ((*result)[2][2], a[2][2]);
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
}

void
search_61_3sq (mpz_t *progression, mpz_t twodistance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void*, mpz_t (*)[3][3]), void *data)
{
  mpz_t i, iroot, j, sum, p;
  mpz_inits (i, iroot, j, sum, p, NULL);
  mpz_set_ui (i, 4);
  mpz_set_ui (iroot, 2);
  while (1)
    {
      if (mpz_cmp (i, twodistance) > 0)
        break;

      mpz_add (j, twodistance, i);
      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);
          mpz_add (sum, progression[0], progression[1]);
          mpz_add (sum, sum, progression[2]);

          mpz_set ((*s)[2][0], progression[0]);
          mpz_set ((*s)[1][1], progression[1]);
          mpz_set ((*s)[0][2], progression[2]);
          mpz_set ((*s)[1][2], b[0]);
          mpz_set ((*s)[2][1], b[1]);

          mpz_add (p, (*s)[2][1], (*s)[1][1]);
          mpz_sub ((*s)[0][1], sum, p);

          mpz_add (p, (*s)[0][2], (*s)[1][2]);
          mpz_sub ((*s)[2][2], sum, p);

          mpz_add (p, (*s)[1][1], (*s)[1][2]);
          mpz_sub ((*s)[1][0], sum, p);

          mpz_add (p, (*s)[0][1], (*s)[0][2]);
          mpz_sub ((*s)[0][0], sum, p);
          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[0][1]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[2][2]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[1][0]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[0][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) != 0 &&
                      mpz_cmp ((*s)[2][0], (*s)[2][1]) != 0)
                    func (data, s);
                }
            }
          else
            func (data, s);
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, sum, p, NULL);
}

void
search_64_3sq (mpz_t *progression, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  mpz_t i, iroot, j, sum, p;
  mpz_inits (i, iroot, j, sum, p, NULL);
  mpz_set (i, progression[2]);
  mpz_sqrt (iroot, i);

  mpz_sub_ui (iroot, iroot, 1);
  mpz_sub (i, i, iroot);
  mpz_sub (i, i, iroot);
  mpz_sub_ui (i, i, 1);

  while (1)
    {
      mpz_sub (j, i, distance);
      if (mpz_cmp_ui (j, 0) < 0)
        break;
      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);

          mpz_add (sum, b[0], b[1]);
          mpz_sub ((*s)[2][0], b[1], distance);
          mpz_add (sum, sum, (*s)[2][0]);

          mpz_set ((*s)[1][2], progression[0]);
          mpz_set ((*s)[0][0], progression[1]);
          mpz_set ((*s)[2][1], progression[2]);
          mpz_set ((*s)[0][2], b[0]);
          mpz_set ((*s)[1][1], b[1]);

          mpz_add (p, (*s)[0][0], (*s)[0][2]);
          mpz_sub ((*s)[0][1], sum, p);

          mpz_add (p, (*s)[0][2], (*s)[1][2]);
          mpz_sub ((*s)[2][2], sum, p);

          mpz_add (p, (*s)[1][1], (*s)[1][2]);
          mpz_sub ((*s)[1][0], sum, p);

          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[2][0]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[0][1]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[2][2]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[1][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if (mpz_cmp ((*s)[1][1], (*s)[1][2]) != 0)
                    func (data, s);
                }
            }
          else
            func (data, s);
        }
      mpz_sub_ui (iroot, iroot, 1);
      mpz_sub (i, i, iroot);
      mpz_sub (i, i, iroot);
      mpz_sub_ui (i, i, 1);
    }
  mpz_clears (i, iroot, j, sum, p, NULL);
}

void
search_65_3sq (mpz_t *progression, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  mpz_t i, iroot, j, limit, sum, p;
  mpz_inits (i, iroot, j, limit, sum, p, NULL);
  mpz_set_ui (i, 4);
  mpz_set_ui (iroot, 2);
  mpz_mul_ui (limit, progression[1], 2);
  while (1)
    {
      if (mpz_cmp (i, limit) > 0)
        break;

      mpz_add (j, distance, i);
      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);
          mpz_add (sum, progression[0], progression[1]);
          mpz_add (sum, sum, progression[2]);

          mpz_set ((*s)[0][1], progression[2]);
          mpz_set ((*s)[1][1], progression[1]);
          mpz_set ((*s)[2][1], progression[0]);
          mpz_set ((*s)[0][2], b[0]);
          mpz_set ((*s)[2][2], b[1]);

          mpz_add (p, (*s)[0][2], (*s)[2][2]);
          mpz_sub ((*s)[1][2], sum, p);

          mpz_add (p, (*s)[0][1], (*s)[0][2]);
          mpz_sub ((*s)[0][0], sum, p);

          mpz_add (p, (*s)[1][1], (*s)[1][2]);
          mpz_sub ((*s)[1][0], sum, p);

          mpz_add (p, (*s)[2][1], (*s)[2][2]);
          mpz_sub ((*s)[2][0], sum, p);

          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[1][2]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[0][0]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[1][0]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[2][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) != 0 &&
                      mpz_cmp ((*s)[1][1], (*s)[0][2]) != 0)
                    func (data, s);
                }
            }
          else
            func (data, s);
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, limit, sum, p, NULL);
  return;
}

void
search_67_3sq (mpz_t *progression, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  mpz_t i, iroot, j, sum, p;
  mpz_inits (i, iroot, j, sum, p, NULL);
  mpz_set (i, progression[0]);
  mpz_sqrt (iroot, i);

  mpz_sub_ui (iroot, iroot, 1);
  mpz_sub (i, i, iroot);
  mpz_sub (i, i, iroot);
  mpz_sub_ui (i, i, 1);

  while (1)
    {

      mpz_sub (j, i, distance);
      if (mpz_cmp_ui (j, 0) < 0)
        break;
      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);
          mpz_add (sum, progression[0], progression[1]);
          mpz_add (sum, sum, progression[2]);

          mpz_set ((*s)[0][0], progression[0]);
          mpz_set ((*s)[1][1], progression[1]);
          mpz_set ((*s)[2][2], progression[2]);
          mpz_set ((*s)[0][1], b[0]);
          mpz_set ((*s)[2][0], b[1]);

          mpz_add (p, (*s)[0][1], (*s)[1][1]);
          mpz_sub ((*s)[2][1], sum, p);

          mpz_add (p, (*s)[0][0], (*s)[0][1]);
          mpz_sub ((*s)[0][2], sum, p);

          mpz_add (p, (*s)[0][2], (*s)[2][2]);
          mpz_sub ((*s)[1][2], sum, p);

          mpz_add (p, (*s)[0][0], (*s)[2][0]);
          mpz_sub ((*s)[1][0], sum, p);
          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[2][1]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[0][2]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[1][2]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[1][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                func (data, s);
            }
          else
            func (data, s);
        }
      mpz_sub_ui (iroot, iroot, 1);
      mpz_sub (i, i, iroot);
      mpz_sub (i, i, iroot);
      mpz_sub_ui (i, i, 1);
    }
  mpz_clears (i, iroot, j, sum, p, NULL);
}

void
search_68_3sq (mpz_t *progression, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  mpz_t i, iroot, j, diff, sum, p;
  mpz_inits (i, iroot, j, diff, sum, p, NULL);
  mpz_set_ui (i, 4);
  mpz_set_ui (iroot, 2);
  while (1)
    {
      if (mpz_cmp (i, progression[0]) > 0)
        break;

      mpz_sub (diff, progression[0], i);
      mpz_add (j, progression[0], diff);
      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);

          mpz_add (sum, progression[0], progression[1]);
          mpz_add (sum, sum, progression[2]);

          mpz_set ((*s)[0][2], progression[0]);
          mpz_set ((*s)[1][1], progression[1]);
          mpz_set ((*s)[2][0], progression[2]);
          mpz_set ((*s)[1][0], b[0]);
          mpz_set ((*s)[2][1], b[1]);

          mpz_add (p, (*s)[2][0], (*s)[2][1]);
          mpz_sub ((*s)[2][2], sum, p);

          mpz_add (p, (*s)[1][0], (*s)[1][1]);
          mpz_sub ((*s)[1][2], sum, p);

          mpz_add (p, (*s)[1][1], (*s)[2][1]);
          mpz_sub ((*s)[0][1], sum, p);

          mpz_add (p, (*s)[1][0], (*s)[2][0]);
          mpz_sub ((*s)[0][0], sum, p);
          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[2][2]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[1][2]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[0][1]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[0][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) != 0 &&
                      mpz_cmp ((*s)[0][1], (*s)[0][2]) != 0)
                    func (data, s);
                }
            }
          else
            func (data, s);
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, diff, sum, p, NULL);
}

void
search_69_3sq (mpz_t *progression, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  int found = 0;
  mpz_t i, iroot, j, limit, sum, p;
  mpz_inits (i, iroot, j, limit, sum, p, NULL);
  mpz_mul_ui (i, distance, 8);
  mpz_sqrt (iroot, i);
  mpz_mul (i, iroot, iroot);

  mpz_mul_ui (limit, distance, 1);

  while (1)
    {
      if (mpz_cmp (i, limit) < 0)
        break;

      mpz_sub (j, i, distance);
      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);
          mpz_set ((*s)[0][1], progression[0]);
          mpz_set ((*s)[2][2], progression[1]);
          mpz_set ((*s)[1][0], progression[2]);
          mpz_set ((*s)[2][1], b[0]);
          mpz_set ((*s)[0][0], b[1]);

          mpz_sub (p, (*s)[2][2], (*s)[0][0]);
          mpz_cdiv_q_ui (p, p, 2);
          mpz_add ((*s)[1][1], (*s)[0][0], p);

          mpz_add (sum, (*s)[0][0], (*s)[1][1]);
          mpz_add (sum, sum, (*s)[2][2]);


          mpz_add (p, (*s)[0][0], (*s)[0][1]);
          mpz_sub ((*s)[0][2], sum, p);

          mpz_add (p, (*s)[1][0], (*s)[1][1]);
          mpz_sub ((*s)[1][2], sum, p);

          mpz_add (p, (*s)[1][1], (*s)[0][2]);
          mpz_sub ((*s)[2][0], sum, p);
          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[1][1]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[0][2]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[1][2]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[2][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if (mpz_cmp ((*s)[1][1], (*s)[0][0]) != 0 &&
                      mpz_cmp ((*s)[2][1], (*s)[2][2]) != 0)
                    {
                      mpz_t col;
                      mpz_init (col);
                      mpz_add (col, (*s)[0][0], (*s)[1][0]);
                      mpz_add (col, col, (*s)[2][0]);
                      if (mpz_cmp (sum, col) == 0)
                        func (data, s);
                      mpz_clear (col);
                    }
                }
            }
          else
            func (data, s);
        }
      mpz_sub_ui (iroot, iroot, 1);
      mpz_sub (i, i, iroot);
      mpz_sub (i, i, iroot);
      mpz_sub_ui (i, i, 1);
    }
  mpz_clears (i, iroot, j, limit, sum, p, NULL);
}

void
search_610_3sq (mpz_t *progression, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  mpz_t i, iroot, j, diff, k, sum, p;
  mpz_inits (i, iroot, j, diff, k, sum, p, NULL);
  mpz_set (i, progression[0]);
  mpz_sqrt (iroot, i);
  mpz_add (i, i, iroot);
  mpz_add (i, i, iroot);
  mpz_add_ui (i, i, 1);
  mpz_add_ui (iroot, iroot, 1);

  while (1)
    {
      mpz_sub (diff, i, progression[0]);
      mpz_add (j, diff, i);
      
      //okay how do we check if we're done?
      mpz_sub (diff, i, progression[1]);
      if (mpz_cmp (diff, progression[1]) > 0)
        break;

      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);
          mpz_set ((*s)[2][1], progression[0]);
          mpz_set ((*s)[1][1], progression[1]);
          mpz_set ((*s)[0][1], progression[2]);
          mpz_set ((*s)[0][2], b[0]);
          mpz_set ((*s)[1][0], b[1]);

          mpz_add (sum, (*s)[0][1], (*s)[1][1]);
          mpz_add (sum, sum, (*s)[2][1]);

          mpz_add (p, (*s)[0][1], (*s)[0][2]);
          mpz_sub ((*s)[0][0], sum, p);

          mpz_add (p, (*s)[1][0], (*s)[1][1]);
          mpz_sub ((*s)[1][2], sum, p);

          mpz_add (p, (*s)[0][2], (*s)[1][1]);
          mpz_sub ((*s)[2][0], sum, p);

          mpz_add (p, (*s)[0][0], (*s)[1][1]);
          mpz_sub ((*s)[2][2], sum, p);
          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[0][0]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[1][2]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[2][0]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[2][2]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if (mpz_cmp ((*s)[1][1], (*s)[0][2]) != 0 &&
                      mpz_cmp ((*s)[2][0], (*s)[2][1]) != 0 &&
                      mpz_cmp ((*s)[1][1], (*s)[1][0]) != 0)
                    func (data, s);
                }
            }
          else
            func (data, s);
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, diff, k, sum, p, NULL);
}

void
search_612_3sq (mpz_t *progression, mpz_t twodistance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  mpz_t i, iroot, j, sum, p;
  mpz_inits (i, iroot, j, sum, p, NULL);
  mpz_set_ui (i, 4);
  mpz_set_ui (iroot, 2);
  while (1)
    {
      if (mpz_cmp (i, twodistance) > 0)
        break;

      mpz_add (j, twodistance, i);
      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);
          mpz_set ((*s)[1][0], progression[0]);
          mpz_set ((*s)[0][2], progression[1]);
          mpz_set ((*s)[2][1], progression[2]);
          mpz_set ((*s)[0][1], b[0]);
          mpz_set ((*s)[1][2], b[1]);

          mpz_sub (p, (*s)[2][1], (*s)[0][1]);
          mpz_cdiv_q_ui (sum, p, 2);
          mpz_add ((*s)[1][1], (*s)[0][1], sum);

          mpz_add (sum, (*s)[0][1], (*s)[1][1]);
          mpz_add (sum, sum, (*s)[2][1]);

          mpz_add (p, (*s)[0][2], (*s)[1][2]);
          mpz_sub ((*s)[2][2], sum, p);

          mpz_add (p, (*s)[2][2], (*s)[2][1]);
          mpz_sub ((*s)[2][0], sum, p);

          mpz_add (p, (*s)[0][1], (*s)[0][2]);
          mpz_sub ((*s)[0][0], sum, p);
          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[1][1]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[2][2]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[2][0]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[0][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if (mpz_cmp ((*s)[1][1], (*s)[0][2]) != 0 &&
                      mpz_cmp ((*s)[1][1], (*s)[2][2]) != 0 &&
                      mpz_cmp ((*s)[1][1], (*s)[1][0]) != 0)
                    {
                      mpz_add (p, (*s)[0][0], (*s)[1][1]);
                      mpz_add (p, p, (*s)[2][2]);
                      if (mpz_cmp (p, sum) == 0)
                        func (data, s);
                    }
                }
            }
          else
            func (data, s);
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, sum, p, NULL);
}

void
search_615_3sq (mpz_t *progression, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  mpz_t i, iroot, j, diff, max, sum, p;
  mpz_inits (i, iroot, j, diff, max, sum, p, NULL);
  mpz_set (i, progression[0]);
  mpz_sqrt (iroot, i);
  mpz_mul (i, iroot, iroot);

  //iterate i once
  mpz_add (i, i, iroot);
  mpz_add (i, i, iroot);
  mpz_add_ui (i, i, 1);
  mpz_add_ui (iroot, iroot, 1);

  mpz_cdiv_q_ui (max, progression[2], 2);

  while (1)
    {
      mpz_sub (diff, i, progression[0]);
      mpz_add (j, i, diff);
      if (mpz_cmp (i, max) > 0)
        break;
      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);
          mpz_set ((*s)[1][0], progression[0]);
          mpz_set ((*s)[2][2], progression[1]);
          mpz_set ((*s)[0][1], progression[2]);
          mpz_set ((*s)[0][2], b[0]);
          mpz_set ((*s)[2][1], b[1]);

          mpz_sub (p, (*s)[2][1], (*s)[0][1]);
          mpz_cdiv_q_ui (sum, p, 2);
          mpz_add ((*s)[1][1], (*s)[0][1], sum);

          mpz_add (sum, (*s)[0][1], (*s)[1][1]);
          mpz_add (sum, sum, (*s)[2][1]);

          mpz_add (p, (*s)[0][1], (*s)[0][2]);
          mpz_sub ((*s)[0][0], sum, p);

          mpz_add (p, (*s)[1][0], (*s)[1][1]);
          mpz_sub ((*s)[1][2], sum, p);

          mpz_add (p, (*s)[2][1], (*s)[2][2]);
          mpz_sub ((*s)[2][0], sum, p);
          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[1][1]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[0][0]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[1][2]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[2][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if (mpz_cmp ((*s)[2][1], (*s)[2][2]) != 0 &&
                      mpz_cmp ((*s)[1][1], (*s)[0][1]) != 0)
                    func (data, s);
                }
            }
          else
            func (data, s);
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, diff, max, sum, p, NULL);
}

void
search_616_3sq (mpz_t *progression, mpz_t twodistance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  mpz_t i, iroot, j, sum, p;
  mpz_inits (i, iroot, j, sum, p, NULL);
  mpz_set_ui (i, 4);
  mpz_set_ui (iroot, 2);
  while (1)
    {
      if (mpz_cmp (i, twodistance) > 0)
        break;

      mpz_add (j, twodistance, i);
      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);
          mpz_set ((*s)[2][1], progression[0]);
          mpz_set ((*s)[0][2], progression[1]);
          mpz_set ((*s)[1][0], progression[2]);
          mpz_set ((*s)[0][0], b[0]);
          mpz_set ((*s)[2][2], b[1]);

          mpz_sub (p, (*s)[2][2], (*s)[0][0]);
          mpz_cdiv_q_ui (sum, p, 2);
          mpz_add ((*s)[1][1], (*s)[0][0], sum);

          mpz_add (sum, (*s)[0][0], (*s)[1][1]);
          mpz_add (sum, sum, (*s)[2][2]);

          mpz_add (p, (*s)[2][1], (*s)[1][1]);
          mpz_sub ((*s)[0][1], sum, p);

          mpz_add (p, (*s)[1][0], (*s)[1][1]);
          mpz_sub ((*s)[1][2], sum, p);

          mpz_add (p, (*s)[2][1], (*s)[2][2]);
          mpz_sub ((*s)[2][0], sum, p);
          if (check)
            {
              int show = 0;
              if (mpz_perfect_square_p ((*s)[1][1]))
                show = 1;
              else
                {
                  if (mpz_perfect_square_p ((*s)[0][1]))
                    show = 1;
                  else
                    {
                      if (mpz_perfect_square_p ((*s)[1][2]))
                        show = 1;
                      else
                        {
                          if (mpz_perfect_square_p ((*s)[2][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if (mpz_cmp ((*s)[1][1], (*s)[0][2]) != 0 &&
                      mpz_cmp ((*s)[0][0], (*s)[0][1]) != 0)
                    func (data, s);
                }
            }
          else
            func (data, s);
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, sum, p, NULL);
}

void
magic_square_get_lowest_number (mpz_t a[3][3], mpz_t *low)
{
  mpz_set_si (*low, -1);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      if (mpz_cmp (a[i][j], *low) < 0 || mpz_cmp_si (*low, -1) == 0)
        mpz_set (*low, a[i][j]);
}

void
magic_square_get_largest_number (mpz_t a[3][3], mpz_t *m)
{
  mpz_set_ui (*m, 0);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      if (mpz_cmp (a[i][j], *m) > 0)
        mpz_set (*m, a[i][j]);
}

void
magic_square_get_magic_number (mpz_t a[3][3], mpz_t *m)
{
  mpz_set_ui (*m, 0);
  mpz_add (*m, a[0][0], a[1][1]);
  mpz_add (*m, *m, a[2][2]);
}

static void
convert_mpf_text (char *buf, FILE *out)
{
  char *s = strdup (buf);
  //-0.225072074826084296873e-1
  if (buf[0] == '-')
    fprintf (out, "-");
  char *e = strchr (s, 'e');
  char *dot = strchr (s, '.');
  if (e && dot)
    {
      dot++;
      char *ptr = dot;
      *e = '\0';
      e++;
      int numzeroes = atoi (e);
      if (numzeroes > 0)
        {
          for (int i = 0; i < numzeroes; i++)
            {
              fprintf (out, "%c", *ptr);
              ptr++;
            }
          fprintf (out, ".%s", ptr);
        }
      else if (numzeroes < 0)
        {
          numzeroes *= -1;
          fprintf (out, "0.");
          for (int i = 0; i < numzeroes; i++)
            fprintf (out, "0");
          fprintf (out, "%s", ptr);
        }
      else
        {
          fprintf (out, "0.%s", ptr);
        }
    }
  else
    fprintf (out, "%s", s);
  free (s);
}

void
display_floating_point_number (mpf_t num, FILE *out)
{
  char *buf = NULL;
  size_t sizbuf = 0;
  FILE *mem = open_memstream (&buf, &sizbuf);
  mpf_out_str (mem, 10, 0, num);
  fclose (mem);
  convert_mpf_text (buf, out);
  free (buf);
}

int types[512] =  /* 3x3 means 9 bits total which means a size of 2^9 */
{
  /* no squares */
  [0b000000000] = 0,

  /* one square */
  [0b000000001] = 1,
  [0b000000010] = 2,
  [0b000000100] = 1,
  [0b000001000] = 2,
  [0b000010000] = 3,
  [0b000100000] = 2,
  [0b001000000] = 1,
  [0b010000000] = 2,
  [0b100000000] = 1,

  /* two squares */
  [0b000000011] = 1,
  [0b000000101] = 2,
  [0b000000110] = 1,
  [0b000001001] = 1,
  [0b000001010] = 3,
  [0b000001100] = 4,
  [0b000010001] = 5,
  [0b000010010] = 6,
  [0b000010100] = 5,
  [0b000011000] = 6,
  [0b000100001] = 4,
  [0b000100010] = 3,
  [0b000100100] = 1,
  [0b000101000] = 7,
  [0b000110000] = 6,
  [0b001000001] = 2,
  [0b001000010] = 4,
  [0b001000100] = 8,
  [0b001001000] = 1,
  [0b001010000] = 5,
  [0b001100000] = 4,
  [0b010000001] = 4,
  [0b010000010] = 7,
  [0b010000100] = 4,
  [0b010001000] = 3,
  [0b010010000] = 6,
  [0b010100000] = 3,
  [0b011000000] = 1,
  [0b100000001] = 8,
  [0b100000010] = 4,
  [0b100000100] = 2,
  [0b100001000] = 4,
  [0b100010000] = 5,
  [0b100100000] = 1,
  [0b101000000] = 2,
  [0b110000000] = 1,

  /* three squares */
  [0b000000111] = 1,
  [0b000001011] = 2,
  [0b000001101] = 3,
  [0b000001110] = 4,
  [0b000010011] = 5,
  [0b000010101] = 6,
  [0b000010110] = 5,
  [0b000011001] = 5,
  [0b000011010] = 7,
  [0b000011100] = 8,
  [0b000100011] = 4,
  [0b000100101] = 3,
  [0b000100110] = 2,
  [0b000101001] = 9,
  [0b000101010] = 10,
  [0b000101100] = 9,
  [0b000110001] = 8,
  [0b000110010] = 7,
  [0b000110100] = 5,
  [0b000111000] = 11,
  [0b001000011] = 3,
  [0b001000101] = 12,
  [0b001000110] = 13,
  [0b001001001] = 1,
  [0b001001010] = 4,
  [0b001001100] = 13,
  [0b001010001] = 6,
  [0b001010010] = 8,
  [0b001010100] = 14,
  [0b001011000] = 5,
  [0b001100001] = 15,
  [0b001100010] = 16,
  [0b001100100] = 13,
  [0b001101000] = 9,
  [0b001110000] = 8,
  [0b010000011] = 9,
  [0b010000101] = 15,
  [0b010000110] = 9,
  [0b010001001] = 4,
  [0b010001010] = 10,
  [0b010001100] = 16,
  [0b010010001] = 8,
  [0b010010010] = 11,
  [0b010010100] = 8,
  [0b010011000] = 7,
  [0b010100001] = 16,
  [0b010100010] = 10,
  [0b010100100] = 4,
  [0b010101000] = 10,
  [0b010110000] = 7,
  [0b011000001] = 3,
  [0b011000010] = 9,
  [0b011000100] = 13,
  [0b011001000] = 2,
  [0b011010000] = 5,
  [0b011100000] = 4,
  [0b100000011] = 13,
  [0b100000101] = 12,
  [0b100000110] = 3,
  [0b100001001] = 13,
  [0b100001010] = 16,
  [0b100001100] = 15,
  [0b100010001] = 14,
  [0b100010010] = 8,
  [0b100010100] = 6,
  [0b100011000] = 8,
  [0b100100001] = 13,
  [0b100100010] = 4,
  [0b100100100] = 1,
  [0b100101000] = 9,
  [0b100110000] = 5,
  [0b101000001] = 12,
  [0b101000010] = 15,
  [0b101000100] = 12,
  [0b101001000] = 3,
  [0b101010000] = 6,
  [0b101100000] = 3,
  [0b110000001] = 13,
  [0b110000010] = 9,
  [0b110000100] = 3,
  [0b110001000] = 4,
  [0b110010000] = 5,
  [0b110100000] = 2,
  [0b111000000] = 1,

  /* four squares */
  [0b000001111] = 1,
  [0b000010111] = 2,
  [0b000011011] = 3,
  [0b000011101] = 4,
  [0b000011110] = 5,
  [0b000100111] = 1,
  [0b000101011] = 6,
  [0b000101101] = 7,
  [0b000101110] = 6,
  [0b000110011] = 5,
  [0b000110101] = 4,
  [0b000110110] = 3,
  [0b000111001] = 8,
  [0b000111010] = 9,
  [0b000111100] = 8,
  [0b001000111] = 10,
  [0b001001011] = 1,
  [0b001001101] = 10,
  [0b001001110] = 11,
  [0b001010011] = 4,
  [0b001010101] = 12,
  [0b001010110] = 13,
  [0b001011001] = 2,
  [0b001011010] = 5,
  [0b001011100] = 13,
  [0b001100011] = 14,
  [0b001100101] = 15,
  [0b001100110] = 16,
  [0b001101001] = 17,
  [0b001101010] = 18,
  [0b001101100] = 19,
  [0b001110001] = 20,
  [0b001110010] = 21,
  [0b001110100] = 13,
  [0b001111000] = 8,
  [0b010000111] = 17,
  [0b010001011] = 6,
  [0b010001101] = 14,
  [0b010001110] = 18,
  [0b010010011] = 8,
  [0b010010101] = 20,
  [0b010010110] = 8,
  [0b010011001] = 5,
  [0b010011010] = 9,
  [0b010011100] = 21,
  [0b010100011] = 18,
  [0b010100101] = 14,
  [0b010100110] = 6,
  [0b010101001] = 18,
  [0b010101010] = 22,
  [0b010101100] = 18,
  [0b010110001] = 21,
  [0b010110010] = 9,
  [0b010110100] = 5,
  [0b010111000] = 9,
  [0b011000011] = 7,
  [0b011000101] = 15,
  [0b011000110] = 19,
  [0b011001001] = 1,
  [0b011001010] = 6,
  [0b011001100] = 16,
  [0b011010001] = 4,
  [0b011010010] = 8,
  [0b011010100] = 13,
  [0b011011000] = 3,
  [0b011100001] = 14,
  [0b011100010] = 18,
  [0b011100100] = 11,
  [0b011101000] = 6,
  [0b011110000] = 5,
  [0b100000111] = 10,
  [0b100001011] = 16,
  [0b100001101] = 15,
  [0b100001110] = 14,
  [0b100010011] = 13,
  [0b100010101] = 12,
  [0b100010110] = 4,
  [0b100011001] = 13,
  [0b100011010] = 21,
  [0b100011100] = 20,
  [0b100100011] = 11,
  [0b100100101] = 10,
  [0b100100110] = 1,
  [0b100101001] = 19,
  [0b100101010] = 18,
  [0b100101100] = 17,
  [0b100110001] = 13,
  [0b100110010] = 5,
  [0b100110100] = 2,
  [0b100111000] = 8,
  [0b101000011] = 15,
  [0b101000101] = 23,
  [0b101000110] = 15,
  [0b101001001] = 10,
  [0b101001010] = 14,
  [0b101001100] = 15,
  [0b101010001] = 12,
  [0b101010010] = 20,
  [0b101010100] = 12,
  [0b101011000] = 4,
  [0b101100001] = 15,
  [0b101100010] = 14,
  [0b101100100] = 10,
  [0b101101000] = 7,
  [0b101110000] = 4,
  [0b110000011] = 19,
  [0b110000101] = 15,
  [0b110000110] = 7,
  [0b110001001] = 11,
  [0b110001010] = 18,
  [0b110001100] = 14,
  [0b110010001] = 13,
  [0b110010010] = 8,
  [0b110010100] = 4,
  [0b110011000] = 5,
  [0b110100001] = 16,
  [0b110100010] = 6,
  [0b110100100] = 1,
  [0b110101000] = 6,
  [0b110110000] = 3,
  [0b111000001] = 10,
  [0b111000010] = 17,
  [0b111000100] = 10,
  [0b111001000] = 1,
  [0b111010000] = 2,
  [0b111100000] = 1,

  /* five squares */
  [0b000011111] = 1,
  [0b000101111] = 2,
  [0b000110111] = 1,
  [0b000111011] = 3,
  [0b000111101] = 4,
  [0b000111110] = 3,
  [0b001001111] = 5,
  [0b001010111] = 6,
  [0b001011011] = 1,
  [0b001011101] = 6,
  [0b001011110] = 7,
  [0b001100111] = 8,
  [0b001101011] = 9,
  [0b001101101] = 10,
  [0b001101110] = 11,
  [0b001110011] = 12,
  [0b001110101] = 13,
  [0b001110110] = 14,
  [0b001111001] = 15,
  [0b001111010] = 16,
  [0b001111100] = 17,
  [0b010001111] = 9,
  [0b010010111] = 15,
  [0b010011011] = 3,
  [0b010011101] = 12,
  [0b010011110] = 16,
  [0b010100111] = 9,
  [0b010101011] = 18,
  [0b010101101] = 19,
  [0b010101110] = 18,
  [0b010110011] = 16,
  [0b010110101] = 12,
  [0b010110110] = 3,
  [0b010111001] = 16,
  [0b010111010] = 20,
  [0b010111100] = 16,
  [0b011000111] = 10,
  [0b011001011] = 2,
  [0b011001101] = 8,
  [0b011001110] = 11,
  [0b011010011] = 4,
  [0b011010101] = 13,
  [0b011010110] = 17,
  [0b011011001] = 1,
  [0b011011010] = 3,
  [0b011011100] = 14,
  [0b011100011] = 19,
  [0b011100101] = 21,
  [0b011100110] = 11,
  [0b011101001] = 9,
  [0b011101010] = 18,
  [0b011101100] = 11,
  [0b011110001] = 12,
  [0b011110010] = 16,
  [0b011110100] = 7,
  [0b011111000] = 3,
  [0b100001111] = 8,
  [0b100010111] = 6,
  [0b100011011] = 14,
  [0b100011101] = 13,
  [0b100011110] = 12,
  [0b100100111] = 5,
  [0b100101011] = 11,
  [0b100101101] = 10,
  [0b100101110] = 9,
  [0b100110011] = 7,
  [0b100110101] = 6,
  [0b100110110] = 1,
  [0b100111001] = 17,
  [0b100111010] = 16,
  [0b100111100] = 15,
  [0b101000111] = 22,
  [0b101001011] = 8,
  [0b101001101] = 22,
  [0b101001110] = 21,
  [0b101010011] = 13,
  [0b101010101] = 23,
  [0b101010110] = 13,
  [0b101011001] = 6,
  [0b101011010] = 12,
  [0b101011100] = 13,
  [0b101100011] = 21,
  [0b101100101] = 22,
  [0b101100110] = 8,
  [0b101101001] = 10,
  [0b101101010] = 19,
  [0b101101100] = 10,
  [0b101110001] = 13,
  [0b101110010] = 12,
  [0b101110100] = 6,
  [0b101111000] = 4,
  [0b110000111] = 10,
  [0b110001011] = 11,
  [0b110001101] = 21,
  [0b110001110] = 19,
  [0b110010011] = 17,
  [0b110010101] = 13,
  [0b110010110] = 4,
  [0b110011001] = 7,
  [0b110011010] = 16,
  [0b110011100] = 12,
  [0b110100011] = 11,
  [0b110100101] = 8,
  [0b110100110] = 2,
  [0b110101001] = 11,
  [0b110101010] = 18,
  [0b110101100] = 9,
  [0b110110001] = 14,
  [0b110110010] = 3,
  [0b110110100] = 1,
  [0b110111000] = 3,
  [0b111000011] = 10,
  [0b111000101] = 22,
  [0b111000110] = 10,
  [0b111001001] = 5,
  [0b111001010] = 9,
  [0b111001100] = 8,
  [0b111010001] = 6,
  [0b111010010] = 15,
  [0b111010100] = 6,
  [0b111011000] = 1,
  [0b111100001] = 8,
  [0b111100010] = 9,
  [0b111100100] = 5,
  [0b111101000] = 2,
  [0b111110000] = 1,

  /* six squares */
  [0b000111111] = 1,
  [0b001011111] = 2,
  [0b001101111] = 3,
  [0b001110111] = 4,
  [0b001111011] = 5,
  [0b001111101] = 6,
  [0b001111110] = 7,
  [0b010011111] = 5,
  [0b010101111] = 8,
  [0b010110111] = 5,
  [0b010111011] = 9,
  [0b010111101] = 10,
  [0b010111110] = 9,
  [0b011001111] = 3,
  [0b011010111] = 6,
  [0b011011011] = 1,
  [0b011011101] = 4,
  [0b011011110] = 7,
  [0b011100111] = 11,
  [0b011101011] = 8,
  [0b011101101] = 11,
  [0b011101110] = 12,
  [0b011110011] = 10,
  [0b011110101] = 13,
  [0b011110110] = 7,
  [0b011111001] = 5,
  [0b011111010] = 9,
  [0b011111100] = 7,
  [0b100011111] = 4,
  [0b100101111] = 3,
  [0b100110111] = 2,
  [0b100111011] = 7,
  [0b100111101] = 6,
  [0b100111110] = 5,
  [0b101001111] = 14,
  [0b101010111] = 15,
  [0b101011011] = 4,
  [0b101011101] = 15,
  [0b101011110] = 13,
  [0b101100111] = 14,
  [0b101101011] = 11,
  [0b101101101] = 16,
  [0b101101110] = 11,
  [0b101110011] = 13,
  [0b101110101] = 15,
  [0b101110110] = 4,
  [0b101111001] = 6,
  [0b101111010] = 10,
  [0b101111100] = 6,
  [0b110001111] = 11,
  [0b110010111] = 6,
  [0b110011011] = 7,
  [0b110011101] = 13,
  [0b110011110] = 10,
  [0b110100111] = 3,
  [0b110101011] = 12,
  [0b110101101] = 11,
  [0b110101110] = 8,
  [0b110110011] = 7,
  [0b110110101] = 4,
  [0b110110110] = 1,
  [0b110111001] = 7,
  [0b110111010] = 9,
  [0b110111100] = 5,
  [0b111000111] = 16,
  [0b111001011] = 3,
  [0b111001101] = 14,
  [0b111001110] = 11,
  [0b111010011] = 6,
  [0b111010101] = 15,
  [0b111010110] = 6,
  [0b111011001] = 2,
  [0b111011010] = 5,
  [0b111011100] = 4,
  [0b111100011] = 11,
  [0b111100101] = 14,
  [0b111100110] = 3,
  [0b111101001] = 3,
  [0b111101010] = 8,
  [0b111101100] = 3,
  [0b111110001] = 4,
  [0b111110010] = 5,
  [0b111110100] = 2,
  [0b111111000] = 1,

  /* seven squares */
  [0b001111111] = 1,
  [0b010111111] = 2,
  [0b011011111] = 1,
  [0b011101111] = 3,
  [0b011110111] = 4,
  [0b011111011] = 2,
  [0b011111101] = 4,
  [0b011111110] = 5,
  [0b100111111] = 1,
  [0b101011111] = 6,
  [0b101101111] = 7,
  [0b101110111] = 6,
  [0b101111011] = 4,
  [0b101111101] = 8,
  [0b101111110] = 4,
  [0b110011111] = 4,
  [0b110101111] = 3,
  [0b110110111] = 1,
  [0b110111011] = 5,
  [0b110111101] = 4,
  [0b110111110] = 2,
  [0b111001111] = 7,
  [0b111010111] = 8,
  [0b111011011] = 1,
  [0b111011101] = 6,
  [0b111011110] = 4,
  [0b111100111] = 7,
  [0b111101011] = 3,
  [0b111101101] = 7,
  [0b111101110] = 3,
  [0b111110011] = 4,
  [0b111110101] = 6,
  [0b111110110] = 1,
  [0b111111001] = 1,
  [0b111111010] = 2,
  [0b111111100] = 1,

  /* eight squares */
  [0b011111111] = 1,
  [0b101111111] = 2,
  [0b110111111] = 1,
  [0b111011111] = 2,
  [0b111101111] = 3,
  [0b111110111] = 2,
  [0b111111011] = 1,
  [0b111111101] = 2,
  [0b111111110] = 1,

  /* nine squares */
  [0b111111111] = 1,
};

int
get_type_index (int i)
{
  return types[i];
}

static int
map_six_to_bremner (int ty)
{
  //map to bremner's numbering of sixes.
  switch (ty)
    {
    case 1: return 5; //our type 6:1 is bremner's type 6.V, etc
    case 2: return 1;
    case 3: return 9;
    case 4: return 8;
    case 5: return 10;
    case 6: return 7;
    case 7: return 11;
    case 8: return 12;
    case 9: return 6;
    case 10: return 4;
    case 11: return 15;
    case 12: return 2;
    case 13: return 13; // we randomly got one the same
    case 14: return 16;
    case 15: return 14;
    case 16: return 3;
    };
  return -1;
}

static int
map_six_from_bremner (int ty)
{
  //map from bremner's numbering of sixes.
  switch (ty)
    {
    case 5: return 1; //bremner's type 6.V is our type 6:1, etc
    case 1: return 2;
    case 9: return 3;
    case 8: return 4;
    case 10: return 5;
    case 7: return 6;
    case 11: return 7;
    case 12: return 8;
    case 6: return 9;
    case 4: return 10;
    case 15: return 11;
    case 2: return 12;
    case 13: return 13;
    case 16: return 14;
    case 14: return 15;
    case 3: return 16;
    };
  return -1;
}

static int
map_seven_to_bremner (int ty)
{
  //map to bremner's numbering of sevens.
  switch (ty)
    {
    case 1: return 3; //our 7:1 is bremner's 7.III, etc
    case 2: return 5;
    case 3: return 8;
    case 4: return 4; //
    case 5: return 6;
    case 6: return 2;
    case 7: return 7; // hey we got two the same, randomly
    case 8: return 1;
    }
}

static int
map_seven_from_bremner (int ty)
{
  //map from bremner's numbering of sevens.
  switch (ty)
    {
    case 3: return 1;
    case 5: return 2;
    case 8: return 3;
    case 4: return 4;
    case 6: return 5;
    case 2: return 6;
    case 7: return 7;
    case 1: return 8;
    }
}

static int
map_eight_to_bremner (int ty)
{
  //map to bremner's numbering of eights.
  switch (ty)
    {
    case 1: return 2; //our 8:1 is bremner's 8.II, etc
    case 2: return 1;
    case 3: return 3;
    }
}

static int
map_eight_from_bremner (int ty)
{
  //map from bremner's numbering of eights.
  switch (ty)
    {
    case 2: return 1;
    case 1: return 2;
    case 3: return 3;
    }
}

int
get_type_from_signature (int signature, int num_squares)
{
  if (num_squares == 6)
    return map_six_to_bremner (types[signature]);
  else if (num_squares == 7)
    return map_seven_to_bremner (types[signature]);
  else if (num_squares == 8)
    return map_eight_to_bremner (types[signature]);
  return types[signature];
}

int
square_get_type (mpz_t (*a)[3][3], int *num_squares)
{
  int signature = 0;
  *num_squares = 0;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      {
        signature <<= 1;
        if (mpz_perfect_square_p ((*a)[i][j]))
          {
            (*num_squares)++;
            signature |=  0x1;
          }
      }
  return get_type_from_signature (signature, *num_squares);
}

int
map_from_bremner (int num_squares, int type)
{
  if (num_squares == 6)
    type = map_six_from_bremner (type);
  else if (num_squares == 7)
    type = map_seven_from_bremner (type);
  else if (num_squares == 8)
    type = map_eight_from_bremner (type);
  return type;
}

int
get_last_three_digits (mpz_t n)
{
  mpz_t r;
  mpz_init (r);
  int ret = mpz_mod_ui (r, n, 1000);
  mpz_clear (r);
  return ret;
}

int
get_last_digit (mpz_t n)
{
  mpz_t r;
  mpz_init (r);
  int ret = mpz_mod_ui (r, n, 10);
  mpz_clear (r);
  return ret;
}

int
mpz_cmp_str (mpz_t i, char *str)
{
  mpz_t n;
  mpz_init (n);
  mpz_set_str (n, str, 10);
  int ret = mpz_cmp (n, i);
  mpz_clear (n);
  return ret;
}

int
read_ull_numbers (FILE *stream, unsigned long long int *a, int size, char **line, size_t *len)
{
  char delim = ',';
  char s[3];
  s[0] = delim;
  s[1] = '\n';
  s[2] = '\0';
  int i;
  ssize_t read = 0;
  for (i = 0; i < size; i++)
    {
      if (i == size - 1)
        read = fv_getline (line, len, stream);
      else
        read = fv_getdelim (line, len, delim, stream);
      if (read == -1)
        break;
      char *end = strpbrk (*line, s);
      if (end)
        *end = '\0';
      end = NULL;
      a[i] = strtoull (*line, &end, 10);
    }
  return read;
}

void
search_61_3sq_small (unsigned long long int *progression, unsigned long long int twodistance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void*, char *fmt, unsigned long long int (*)[3][3]), void *app)
{
  unsigned long long int i, iroot, j, sum, p;
  i = 4;
  iroot = 2;
  while (1)
    {
      if (i > twodistance)
        break;

      j = twodistance + i;
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;
          sum = progression[1] * 3;

          (*s)[2][0] = progression[0];
          (*s)[1][1] = progression[1];
          (*s)[0][2] = progression[2];
          (*s)[1][2] = b[0];
          (*s)[2][1] = b[1];

          p = (*s)[2][1] + (*s)[1][1];
          (*s)[0][1] = sum - p;

          p = (*s)[0][2] + (*s)[1][2];
          (*s)[2][2] = sum - p;

          p = (*s)[1][1] + (*s)[1][2];
          (*s)[1][0] = sum - p;

          p = (*s)[0][1] + (*s)[0][2];
          (*s)[0][0] = sum - p;
          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[0][1]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[2][2]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[1][0]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[0][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if ((*s)[1][1] != (*s)[0][0] && (*s)[2][0] != (*s)[2][1])
                    func (app,
                          "%llu, %lld, %llu, "
                          "%llu, %llu, %llu, "
                          "%llu, %llu, %llu, \n", s);
                }
            }
          else
            func (app,
                  "%llu, %lld, %llu, "
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %llu, \n", s);
        }
      i += (iroot * 2) + 1;
      iroot++;
    }
}

void
search_64_3sq_small (unsigned long long int *progression, unsigned long long int distance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app)
{
  unsigned long long int i, iroot, sum, p;
  long long int j;
  i = progression[2];
  iroot = sqrtl (i);

  i = iroot * iroot;

  iroot--;
  i = i - iroot;
  i = i - iroot;
  i--;

  while (1)
    {
      j = i - distance;
      if (j < 0)
        break;
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;

          sum = b[0] + b[1];
          (*s)[2][0] = b[1] - distance;
          sum = sum + (*s)[2][0];

          (*s)[1][2] = progression[0];
          (*s)[0][0] = progression[1];
          (*s)[2][1] = progression[2];
          (*s)[0][2] = b[0];
          (*s)[1][1] = b[1];

          p = (*s)[0][0] + (*s)[0][2];
          (*s)[0][1] = sum - p;

          p = (*s)[0][2] + (*s)[1][2];
          (*s)[2][2] = sum - p;

          p = (*s)[1][1] + (*s)[1][2];
          (*s)[1][0] = sum - p;

          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[2][0]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[0][1]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[2][2]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[1][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if ((*s)[1][1] != (*s)[1][2])
                    func (app,
                          "%llu, %lld, %llu, "
                          "%lld, %llu, %llu, "
                          "%lld, %llu, %lld, \n", s);
                }
            }
          else
            func (app,
                  "%llu, %lld, %llu, "
                  "%lld, %llu, %llu, "
                  "%lld, %llu, %lld, \n", s);
        }
      iroot--;
      i = i - iroot;
      i = i - iroot;
      i--;
    }
}

void
search_65_3sq_small (unsigned long long int *progression, unsigned long long int distance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app)
{
  unsigned long long int i, iroot, j, limit, sum, p;
  i = 4;
  iroot = 2;
  limit = progression[1] * 2;
  while (1)
    {
      if (i > limit)
        break;

      j = distance + i;
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;
          sum = progression[1] * 3;

          (*s)[0][1] = progression[2];
          (*s)[1][1] = progression[1];
          (*s)[2][1] = progression[0];
          (*s)[0][2] = b[0];
          (*s)[2][2] = b[1];

          p = (*s)[0][2] + (*s)[2][2];
          (*s)[1][2] = sum - p;

          p = (*s)[0][1] + (*s)[0][2];
          (*s)[0][0] = sum - p;

          p = (*s)[1][1] + (*s)[1][2];
          (*s)[1][0] = sum - p;

          p = (*s)[2][1] + (*s)[2][2];
          (*s)[2][0] = sum - p;

          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[1][2]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[0][0]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[1][0]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[2][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if ((*s)[1][1] != (*s)[0][0] &&
                      (*s)[1][1] != (*s)[0][2])
                    func (app,
                          "%llu, %lld, %llu, "
                          "%llu, %llu, %llu, "
                          "%llu, %llu, %llu, \n", s);
                }
            }
          else
            func (app,
                  "%llu, %lld, %llu, "
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %llu, \n", s);
        }
      i += (iroot * 2) + 1;
      iroot++;
    }
  return;
}

void
search_67_3sq_small (unsigned long long int *progression, unsigned long long int distance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app)
{
  unsigned long long int i, iroot, sum, p;
  long long int j;
  i = progression[0];
  iroot = sqrt (i);

  iroot = iroot - 1;
  i = i - iroot;
  i = i - iroot;
  i--;

  while (1)
    {

      j = i - distance;
      if (j < 0)
        break;
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;
          sum = progression[1] * 3;

          (*s)[0][0] = progression[0];
          (*s)[1][1] = progression[1];
          (*s)[2][2] = progression[2];
          (*s)[0][1] = b[0];
          (*s)[2][0] = b[1];

          p = (*s)[0][1] + (*s)[1][1];
          (*s)[2][1] = sum - p;

          p = (*s)[0][0] + (*s)[0][1];
          (*s)[0][2] = sum - p;

          p = (*s)[0][2] + (*s)[2][2];
          (*s)[1][2] = sum - p;

          p = (*s)[0][0] + (*s)[2][0];
          (*s)[1][0] = sum - p;
          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[2][1]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[0][2]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[1][2]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[1][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                func (app,
                      "%llu, %llu, %llu, "
                      "%llu, %llu, %lld, "
                      "%llu, %llu, %llu, \n", s);
            }
          else
            func (app,
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %lld, "
                  "%llu, %llu, %llu, \n", s);
        }
      iroot = iroot - 1;
      i = i - iroot;
      i = i - iroot;
      i--;
    }
}

void
search_68_3sq_small (unsigned long long int *progression, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app)
{
  unsigned long long int i, iroot, diff, j, sum, p;
  i = 4;
  iroot = 2;
  while (1)
    {
      if (i > progression[0])
        break;

      diff = progression[0] - i;
      j = progression[0] + diff;
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;

          sum = progression[1] * 3;

          (*s)[0][2] = progression[0];
          (*s)[1][1] = progression[1];
          (*s)[2][0] = progression[2];
          (*s)[1][0] = b[0];
          (*s)[2][1] = b[1];

          p = (*s)[2][0] + (*s)[2][1];
          (*s)[2][2] = sum - p;

          p = (*s)[1][0] + (*s)[1][1];
          (*s)[1][2] = sum - p;

          p = (*s)[1][1] + (*s)[2][1];
          (*s)[0][1] = sum - p;

          p = (*s)[1][0] + (*s)[2][0];
          (*s)[0][0] = sum - p;
          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[2][2]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[1][2]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[0][1]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[0][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if ((*s)[1][1] != (*s)[0][0] &&
                      (*s)[0][1] != (*s)[0][2])
                    func (app,
                          "%llu, %llu, %llu, "
                          "%llu, %llu, %llu, "
                          "%llu, %llu, %llu, \n", s);
                }
            }
          else
            func (app,
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %llu, \n", s);
        }
      i += (iroot * 2) + 1;
      iroot ++;
    }
}

void
search_69_3sq_small (unsigned long long int *progression, unsigned long long int distance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app)
{
  unsigned long long int i, iroot, j, limit, sum, p;
  i = distance * 8;
  iroot = sqrtl (i);
  i = iroot * iroot;

  limit = distance;

  while (1)
    {
      if (i < limit)
        break;

      j = i - distance;
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;
          (*s)[0][1] = progression[0];
          (*s)[2][2] = progression[1];
          (*s)[1][0] = progression[2];
          (*s)[2][1] = b[0];
          (*s)[0][0] = b[1];

          if ((*s)[0][1] > (*s)[2][1])
            {
              p = (*s)[0][1] - (*s)[2][1];
              p = p / 2;
              (*s)[1][1] = (*s)[2][1] + p;
            }
          else
            {
              p = (*s)[2][1] - (*s)[0][1];
              p = p / 2;
              (*s)[1][1] = (*s)[0][1] + p;
            }

          sum = (*s)[1][1] * 3;

          p = (*s)[0][0] + (*s)[0][1];
          (*s)[0][2] = sum - p;

          p = (*s)[1][0] + (*s)[1][1];
          (*s)[1][2] = sum - p;

          p = (*s)[1][1] + (*s)[0][2];
          (*s)[2][0] = sum - p;

          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[1][1]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[0][2]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[1][2]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[2][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if ((*s)[1][1] != (*s)[0][0] &&
                      (*s)[2][1] != (*s)[2][2])
                    {
                      unsigned long long int col =
                        (*s)[0][0] + (*s)[1][0] + (*s)[2][0];
                      if (col == sum)
                        func (app,
                              "%llu, %llu, %llu, "
                              "%llu, %llu, %lld, "
                              "%llu, %llu, %llu, \n", s);
                    }
                }
            }
          else
            func (app,
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %lld, "
                  "%llu, %llu, %llu, \n", s);
        }

      iroot--;
      i = i - iroot;
      i = i - iroot;
      i--;
    }
}

void
search_610_3sq_small (unsigned long long int *progression, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app)
{
  unsigned long long int i, iroot, j, diff, sum, p;
  i = progression[0];
  iroot = sqrtl (i);

  i += (iroot * 2) + 1;
  iroot++;

  while (1)
    {
      diff = i - progression[0];
      j = diff + i;
      
      //okay how do we check if we're done?
      if (i >= progression[1])
        {
          diff = i - progression[1];
          if (diff > progression[1])
            break;
        }

      //we don't get here
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;
          (*s)[2][1] = progression[0];
          (*s)[1][1] = progression[1];
          (*s)[0][1] = progression[2];
          (*s)[0][2] = b[0];
          (*s)[1][0] = b[1];

          sum = (*s)[1][1] * 3;

          p = (*s)[0][1] + (*s)[0][2];
          (*s)[0][0] = sum - p;

          p = (*s)[1][0] + (*s)[1][1];
          (*s)[1][2] = sum - p;

          p = (*s)[0][2] + (*s)[1][1];
          (*s)[2][0] = sum - p;

          p = (*s)[0][0] + (*s)[1][1];
          (*s)[2][2] = sum - p;

          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[0][0]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[1][2]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[2][0]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[2][2]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if ((*s)[1][1] != (*s)[0][2] &&
                      (*s)[2][0] != (*s)[2][1] &&
                      (*s)[1][1] != (*s)[1][0])
                    func (app,
                          "%llu, %llu, %llu, "
                          "%llu, %llu, %lld, "
                          "%llu, %llu, %llu, \n", s);
                }
            }
          else
            func (app,
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %lld, "
                  "%llu, %llu, %llu, \n", s);
        }
      i += (iroot * 2) + 1;
      iroot++;
    }
}

void
search_612_3sq_small (unsigned long long int *progression, unsigned long long int twodistance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app)
{
  unsigned long long int i, iroot, j, sum, p;
  i = 4;
  iroot = 2;
  while (1)
    {
      if (i > twodistance)
        break;

      j = twodistance + i;
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;
          (*s)[1][0] = progression[0];
          (*s)[0][2] = progression[1];
          (*s)[2][1] = progression[2];
          (*s)[0][1] = b[0];
          (*s)[1][2] = b[1];

          if ((*s)[2][1] > (*s)[0][1])
            {
              p = (*s)[2][1] - (*s)[0][1];
              p = p / 2;
              (*s)[1][1] = (*s)[0][1] + p;
            }
          else
            {
              p = (*s)[0][1] - (*s)[2][1];
              p = p / 2;
              (*s)[1][1] = (*s)[2][1] + p;
            }
          sum = (*s)[1][1] * 3;

          p = (*s)[0][2] + (*s)[1][2];
          (*s)[2][2] = sum - p;

          p = (*s)[2][2] + (*s)[2][1];
          (*s)[2][0] = sum - p;

          p = (*s)[0][1] + (*s)[0][2];
          (*s)[0][0] = sum - p;
          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[1][1]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[2][2]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[2][0]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[0][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if ((*s)[1][1] != (*s)[0][2] &&
                      (*s)[1][1] != (*s)[2][2] &&
                      (*s)[1][1] != (*s)[1][0])
                    {
                      p = (*s)[0][0] + (*s)[1][1] + (*s)[2][2];
                      if (p == sum)
                        func (app,
                              "%llu, %llu, %llu, "
                              "%llu, %llu, %llu, "
                              "%llu, %llu, %llu, \n", s);
                    }
                }
            }
          else
            func (app,
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %llu, \n", s);
        }
      i += (iroot * 2) + 1;
      iroot++;
    }
}

void
search_615_3sq_small (unsigned long long int *progression, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app)
{
  unsigned long long int i, iroot, j, diff, max, sum, p;
  i = progression[0];
  iroot = sqrtl (i);
  i = iroot * iroot;

  //iterate i once
  i += (iroot * 2) + 1;
  iroot++;

  max = progression[2] / 2;

  while (1)
    {
      diff = i - progression[0];
      j = i + diff;
      if (i > max)
        break;
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;
          (*s)[1][0] = progression[0];
          (*s)[2][2] = progression[1];
          (*s)[0][1] = progression[2];
          (*s)[0][2] = b[0];
          (*s)[2][1] = b[1];

          if ((*s)[2][1] > (*s)[0][1])
            {
              p = (*s)[2][1] - (*s)[0][1];
              p = p / 2;
              (*s)[1][1] = (*s)[0][1] + p;
            }
          else
            {
              p = (*s)[0][1] - (*s)[2][1];
              p = p / 2;
              (*s)[1][1] = (*s)[2][1] + p;
            }

          sum = (*s)[1][1] * 3;

          p = (*s)[0][1] + (*s)[0][2];
          (*s)[0][0] = sum - p;

          p = (*s)[1][0] + (*s)[1][1];
          (*s)[1][2] = sum - p;

          p = (*s)[2][1] + (*s)[2][2];
          (*s)[2][0] = sum - p;

          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[1][1]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[0][0]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[1][2]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[2][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if ((*s)[2][1] != (*s)[2][2] &&
                      (*s)[1][1] != (*s)[0][1])
                    func (app,
                          "%llu, %llu, %llu, "
                          "%llu, %llu, %llu, "
                          "%llu, %llu, %llu, \n", s);
                }
            }
          else
            func (app,
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %llu, "
                  "%llu, %llu, %llu, \n", s);
        }
      i += (iroot * 2) + 1;
      iroot++;
    }
}

void
search_616_3sq_small (unsigned long long int *progression, unsigned long long int twodistance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app)
{
  unsigned long long int i, iroot, j, sum, p;
  i = 4;
  iroot = 2;
  while (1)
    {
      if (i > twodistance)
        break;

      j = twodistance + i;
      if (small_is_square (j))
        {
          b[0] = i;
          b[1] = j;
          (*s)[2][1] = progression[0];
          (*s)[0][2] = progression[1];
          (*s)[1][0] = progression[2];
          (*s)[0][0] = b[0];
          (*s)[2][2] = b[1];

          if ((*s)[2][2] > (*s)[0][0])
            {
              p = (*s)[2][2] - (*s)[0][0];
              p = p / 2;
              (*s)[1][1] = (*s)[0][0] + p;
            }
          else
            {
              p = (*s)[0][0] - (*s)[2][2];
              p = p / 2;
              (*s)[1][1] = (*s)[2][2] + p;
            }

          sum = (*s)[1][1] * 3;

          p = (*s)[2][1] + (*s)[1][1];
          (*s)[0][1] = sum - p;

          p = (*s)[1][0] + (*s)[1][1];
          (*s)[1][2] = sum - p;

          p = (*s)[2][1] + (*s)[2][2];
          (*s)[2][0] = sum - p;
          if (check)
            {
              int show = 0;
              if (small_is_square ((*s)[1][1]))
                show = 1;
              else
                {
                  if (small_is_square ((*s)[0][1]))
                    show = 1;
                  else
                    {
                      if (small_is_square ((*s)[1][2]))
                        show = 1;
                      else
                        {
                          if (small_is_square ((*s)[2][0]))
                            show = 1;
                        }
                    }
                }
              if (show)
                {
                  if ((*s)[1][1] != (*s)[0][2] &&
                      (*s)[0][0] != (*s)[0][1])
                    func (app,
                          "%llu, %lld, %llu, "
                          "%llu, %llu, %lld, "
                          "%lld, %llu, %llu, \n", s);
                }
            }
          else
            func (app,
                  "%llu, %lld, %llu, "
                  "%llu, %llu, %lld, "
                  "%lld, %llu, %llu, \n", s);
        }
      i += (iroot * 2) + 1;
      iroot++;
    }
}

void
get_coprime_pairs (mpz_t *a, int allow_negative, struct mpz_pair **pairs, int *num_pairs)
{
  mpz_t gcd;
  mpz_init (gcd);
  for (int i = 0; i < SIZE; i++)
    {
      if (!allow_negative && mpz_sgn (a[i]) == -1)
        continue;
      for (int j = 0; j < SIZE; j++)
        {
          if (i == j)
            continue;
          if (mpz_cmp (a[i], a[j]) >= 0)
            continue;
          if (!allow_negative && mpz_sgn (a[j]) < 0)
            continue;
          mpz_gcd (gcd, a[i], a[j]);
          int ret = mpz_cmp_ui (gcd, 1);
          if (ret == 0)
            {
              int found = 0;
              for (int k = 0; k < *num_pairs; k++)
                {
                  if (mpz_cmp ((*pairs)[k].num[0], a[i]) == 0 &&
                      mpz_cmp ((*pairs)[k].num[1], a[j]) == 0)
                    {
                      found = 1;
                      break;
                    }
                }
              if (!found)
                {
                  *pairs = realloc (*pairs,
                                    sizeof (struct mpz_pair) * ((*num_pairs) + 1));
                  mpz_init_set ((*pairs)[*num_pairs].num[0], a[i]);
                  mpz_init_set ((*pairs)[*num_pairs].num[1], a[j]);
                  (*num_pairs)++;
                }
            }
        }
    }
  mpz_clear (gcd);
}

static int
check_rotation (mpz_t (*s)[3][3], int a, int b, int c, int d, int e, int f, int g, int h, int i)
{
  return
    mpz_perfect_square_p ((*s)[0][0]) == a &&
    mpz_perfect_square_p ((*s)[0][1]) == b &&
    mpz_perfect_square_p ((*s)[0][2]) == c &&
    mpz_perfect_square_p ((*s)[1][0]) == d &&
    mpz_perfect_square_p ((*s)[1][1]) == e &&
    mpz_perfect_square_p ((*s)[1][2]) == f &&
    mpz_perfect_square_p ((*s)[2][0]) == g &&
    mpz_perfect_square_p ((*s)[2][1]) == h &&
    mpz_perfect_square_p ((*s)[2][2]) == i;
}

static void
rotate_to_shape (mpz_t (*s)[3][3], int a, int b, int c, int d, int e, int f, int g, int h, int i)
{
  int j;
  if (check_rotation (s, a, b, c, d, e, f, g, h, i))
    return;
  for (j = 0; j < 4; j++)
    {
      rotate_square (s);
      if (check_rotation (s, a, b, c, d, e, f, g, h, i))
        return;
    }
  flip_square_horizontally (s);
  if (check_rotation (s, a, b, c, d, e, f, g, h, i))
    return;
  for (j = 0; j < 4; j++)
    {
      rotate_square (s);
      if (check_rotation (s, a, b, c, d, e, f, g, h, i))
        return;
    }
  flip_square_vertically (s);
  if (check_rotation (s, a, b, c, d, e, f, g, h, i))
    return;
  for (j = 0; j < 4; j++)
    {
      rotate_square (s);
      if (check_rotation (s, a, b, c, d, e, f, g, h, i))
        return;
    }
}

static void
_rotate_six (mpz_t (*a)[3][3], int type)
{
  switch (type)
    {
    case 1: return rotate_to_shape (a, 0, 0, 1, 0, 1, 1, 1, 1, 1);
    case 2: return rotate_to_shape (a, 0, 1, 1, 1, 0, 1, 1, 1, 0);
    case 3: return rotate_to_shape (a, 1, 0, 1, 1, 0, 1, 1, 0, 1);
    case 4: return rotate_to_shape (a, 0, 1, 0, 1, 1, 1, 1, 0, 1);
    case 5: return rotate_to_shape (a, 0, 0, 0, 1, 1, 1, 1, 1, 1);
    case 6: return rotate_to_shape (a, 0, 1, 0, 1, 1, 1, 0, 1, 1);
    case 7: return rotate_to_shape (a, 0, 0, 1, 1, 1, 1, 1, 0, 1);
    case 8: return rotate_to_shape (a, 0, 0, 1, 1, 1, 0, 1, 1, 1);
    case 9: return rotate_to_shape (a, 0, 0, 1, 1, 0, 1, 1, 1, 1);
    case 10: return rotate_to_shape (a, 0, 0, 1, 1, 1, 1, 0, 1, 1);
    case 11: return rotate_to_shape (a, 0, 0, 1, 1, 1, 1, 1, 1, 0);
    case 12: return rotate_to_shape (a, 0, 1, 0, 1, 0, 1, 1, 1, 1);
    case 13: return rotate_to_shape (a, 0, 1, 1, 1, 1, 0, 1, 0, 1);
    case 14: return rotate_to_shape (a, 1, 0, 1, 0, 1, 1, 1, 0, 1);
    case 15: return rotate_to_shape (a, 1, 0, 1, 1, 0, 1, 1, 1, 0);
    case 16: return rotate_to_shape (a, 1, 1, 1, 1, 0, 0, 1, 0, 1);
    }
}

static void swap_cells (mpz_t (*a)[3][3], int x1, int y1, int x2, int y2)
{
  mpz_t s;
  mpz_init (s);
  mpz_set (s, (*a)[x2][y2]);
  mpz_set ((*a)[x2][y2], (*a)[x1][y1]);
  mpz_set ((*a)[x1][y1], s);
  mpz_clear (s);
}

void
rotate_six (mpz_t (*a)[3][3], int type)
{
  _rotate_six (a, type);
  if (type == 1 && mpz_cmp ((*a)[0][2], (*a)[2][0]) < 0)
    {
      swap_cells (a, 0,2, 2,0);
      swap_cells (a, 1,2, 2,1);
      swap_cells (a, 0,1, 1,0);
    }
  else if (type == 2 && mpz_cmp ((*a)[0][2], (*a)[2][1]) < 0)
    {
      swap_cells (a, 1,0, 2,1);
      swap_cells (a, 0,1, 1,2);
      swap_cells (a, 0,0, 2,2);
    }
  else if (type == 3)
    {
      if (mpz_cmp ((*a)[0][0], (*a)[0][2]) < 0 &&
          mpz_cmp ((*a)[0][0], (*a)[2][2]) < 0 &&
          mpz_cmp ((*a)[0][0], (*a)[2][0]) < 0)
        ;
      else if (mpz_cmp ((*a)[0][2], (*a)[0][0]) < 0 &&
               mpz_cmp ((*a)[0][2], (*a)[2][2]) < 0 &&
               mpz_cmp ((*a)[0][2], (*a)[2][0]) < 0)
        flip_square_horizontally (a);
      else if (mpz_cmp ((*a)[2][2], (*a)[0][0]) < 0 &&
               mpz_cmp ((*a)[2][2], (*a)[0][2]) < 0 &&
               mpz_cmp ((*a)[2][2], (*a)[2][0]) < 0)
        {
          flip_square_horizontally (a);
          flip_square_vertically (a);
        }
      else if (mpz_cmp ((*a)[2][0], (*a)[0][0]) < 0 &&
               mpz_cmp ((*a)[2][0], (*a)[0][2]) < 0 &&
               mpz_cmp ((*a)[2][0], (*a)[2][2]) < 0)
        flip_square_vertically (a);
    }
  else if (type == 4 && mpz_cmp ((*a)[1][0], (*a)[1][2]) > 0)
    flip_square_horizontally (a);
  else if (type == 5 && mpz_cmp ((*a)[1][0], (*a)[1][2]) > 0)
    flip_square_horizontally (a);
  else if (type == 6 && mpz_cmp ((*a)[0][1], (*a)[2][2]) > 0)
    {
      swap_cells (a, 0,2, 2,0);
      swap_cells (a, 1,2, 2,1);
      swap_cells (a, 0,1, 1,0);
    }
  else if (type == 7 || type == 8 || type == 9 || type == 10 || type == 11 ||
           type == 15)
    {
      //nothing to do.
      //there are two types and we can't morph one into the other.
    }
  else if (type == 12)
    {
      if (mpz_cmp ((*a)[1][0], (*a)[1][2]) > 0)
        flip_square_horizontally (a);
    }
  else if (type == 13 && mpz_cmp ((*a)[2][0], (*a)[0][2]) < 0)
    {
      swap_cells (a, 0,2, 2,0);
      swap_cells (a, 0,1, 1,0);
      swap_cells (a, 1,2, 2,1);
    }
  else if (type == 16 && mpz_cmp ((*a)[2][0], (*a)[0][2]) > 0)
    {
      swap_cells (a, 0,2, 2,0);
      swap_cells (a, 0,1, 1,0);
      swap_cells (a, 1,2, 2,1);
    }
  else if (type == 14)
    {
      if (mpz_cmp ((*a)[0][2], (*a)[2][2]) > 0)
        flip_square_vertically (a);
        //{
      //swap_cells (a, 0,2, 2,0);
      //swap_cells (a, 1,2, 2,1);
      //swap_cells (a, 0,1, 1,0);
        //}
    }
}

int
mcompar (const void *left, const void *right)
{
  const mpz_t *l = left;
  const mpz_t *r = right;
  return mpz_cmp (*l, *r);
}

int lookup61[8][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
};
int lookup62[8][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
};
int lookup63[4][9][2] =
{
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{2,1}, {0,0}, {0,2}, {1,2}, {1,1}, {1,0}, {2,0}, {2,2}, {0,1}},
    {{2,1}, {0,0}, {1,2}, {0,2}, {1,1}, {2,0}, {1,0}, {2,2}, {0,1}},
};
int lookup64[8][9][2] =
{
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup65[8][9][2] =
{
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup66[8][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
};
int lookup67[16][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
    {{2,1}, {0,0}, {0,2}, {1,2}, {1,1}, {1,0}, {2,0}, {2,2}, {0,1}},
    {{2,1}, {0,0}, {1,2}, {0,2}, {1,1}, {2,0}, {1,0}, {2,2}, {0,1}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup68[16][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
    {{2,1}, {0,0}, {0,2}, {1,2}, {1,1}, {1,0}, {2,0}, {2,2}, {0,1}},
    {{2,1}, {0,0}, {1,2}, {0,2}, {1,1}, {2,0}, {1,0}, {2,2}, {0,1}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup69[16][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
    {{2,1}, {0,0}, {0,2}, {1,2}, {1,1}, {1,0}, {2,0}, {2,2}, {0,1}},
    {{2,1}, {0,0}, {1,2}, {0,2}, {1,1}, {2,0}, {1,0}, {2,2}, {0,1}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup610[16][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
    {{2,1}, {0,0}, {0,2}, {1,2}, {1,1}, {1,0}, {2,0}, {2,2}, {0,1}},
    {{2,1}, {0,0}, {1,2}, {0,2}, {1,1}, {2,0}, {1,0}, {2,2}, {0,1}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup611[16][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
    {{2,1}, {0,0}, {0,2}, {1,2}, {1,1}, {1,0}, {2,0}, {2,2}, {0,1}},
    {{2,1}, {0,0}, {1,2}, {0,2}, {1,1}, {2,0}, {1,0}, {2,2}, {0,1}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup612[8][9][2] =
{
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup613[8][9][2] =
{
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{2,1}, {0,0}, {0,2}, {1,2}, {1,1}, {1,0}, {2,0}, {2,2}, {0,1}},
    {{2,1}, {0,0}, {1,2}, {0,2}, {1,1}, {2,0}, {1,0}, {2,2}, {0,1}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup614[8][9][2] =
{
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{2,1}, {0,0}, {0,2}, {1,2}, {1,1}, {1,0}, {2,0}, {2,2}, {0,1}},
    {{2,1}, {0,0}, {1,2}, {0,2}, {1,1}, {2,0}, {1,0}, {2,2}, {0,1}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup615[16][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,0}, {0,2}, {2,1}, {2,2}, {1,1}, {0,0}, {0,1}, {2,0}, {1,2}},
    {{1,0}, {0,2}, {2,2}, {2,1}, {1,1}, {0,1}, {0,0}, {2,0}, {1,2}},
    {{1,0}, {2,2}, {0,1}, {0,2}, {1,1}, {2,0}, {2,1}, {0,0}, {1,2}},
    {{1,0}, {2,2}, {0,2}, {0,1}, {1,1}, {2,1}, {2,0}, {0,0}, {1,2}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
    {{2,1}, {0,0}, {0,2}, {1,2}, {1,1}, {1,0}, {2,0}, {2,2}, {0,1}},
    {{2,1}, {0,0}, {1,2}, {0,2}, {1,1}, {2,0}, {1,0}, {2,2}, {0,1}},
    {{2,1}, {0,2}, {0,0}, {1,0}, {1,1}, {1,2}, {2,2}, {2,0}, {0,1}},
    {{2,1}, {0,2}, {1,0}, {0,0}, {1,1}, {2,2}, {1,2}, {2,0}, {0,1}},
};
int lookup616[8][9][2] =
{
    {{0,1}, {2,0}, {1,2}, {2,2}, {1,1}, {0,0}, {1,0}, {0,2}, {2,1}},
    {{0,1}, {2,0}, {2,2}, {1,2}, {1,1}, {1,0}, {0,0}, {0,2}, {2,1}},
    {{0,1}, {2,2}, {1,0}, {2,0}, {1,1}, {0,2}, {1,2}, {0,0}, {2,1}},
    {{0,1}, {2,2}, {2,0}, {1,0}, {1,1}, {1,2}, {0,2}, {0,0}, {2,1}},
    {{1,2}, {0,0}, {2,0}, {2,1}, {1,1}, {0,1}, {0,2}, {2,2}, {1,0}},
    {{1,2}, {0,0}, {2,1}, {2,0}, {1,1}, {0,2}, {0,1}, {2,2}, {1,0}},
    {{1,2}, {2,0}, {0,0}, {0,1}, {1,1}, {2,1}, {2,2}, {0,2}, {1,0}},
    {{1,2}, {2,0}, {0,1}, {0,0}, {1,1}, {2,2}, {2,1}, {0,2}, {1,0}},
};
SubTypeArrayType* get_subtypes (int type, int *num_subtypes)
{
  switch (type)
    {
    case 1: *num_subtypes = sizeof (lookup61) / sizeof (lookup61[0]); return lookup61;
    case 2: *num_subtypes = sizeof (lookup62) / sizeof (lookup62[0]); return lookup62;
    case 3: *num_subtypes = sizeof (lookup63) / sizeof (lookup63[0]); return lookup63;
    case 4: *num_subtypes = sizeof (lookup64) / sizeof (lookup64[0]); return lookup64;
    case 5: *num_subtypes = sizeof (lookup65) / sizeof (lookup65[0]); return lookup65;
    case 6: *num_subtypes = sizeof (lookup66) / sizeof (lookup66[0]); return lookup66;
    case 7: *num_subtypes = sizeof (lookup67) / sizeof (lookup67[0]); return lookup67;
    case 8: *num_subtypes = sizeof (lookup68) / sizeof (lookup68[0]); return lookup68;
    case 9: *num_subtypes = sizeof (lookup69) / sizeof (lookup69[0]); return lookup69;
    case 10: *num_subtypes = sizeof (lookup610) / sizeof (lookup610[0]); return lookup610;
    case 11: *num_subtypes = sizeof (lookup611) / sizeof (lookup611[0]); return lookup611;
    case 12: *num_subtypes = sizeof (lookup612) / sizeof (lookup612[0]); return lookup612;
    case 13: *num_subtypes = sizeof (lookup613) / sizeof (lookup613[0]); return lookup613;
    case 14: *num_subtypes = sizeof (lookup614) / sizeof (lookup614[0]); return lookup614;
    case 15: *num_subtypes = sizeof (lookup615) / sizeof (lookup615[0]); return lookup615;
    case 16: *num_subtypes = sizeof (lookup616) / sizeof (lookup616[0]); return lookup616;
    }
  return NULL;
}

int
square_get_subtype (mpz_t (*r)[3][3], int num_squares, int type)
{
  int num = 0;
  if (num_squares != 6)
    return 0;
  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init_set (s[i][j], (*r)[i][j]);
  rotate_six (&s, type);

  int count = 0;
  mpz_t progression[9], vec[9];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      {
        mpz_init_set (progression[count], s[i][j]);
        mpz_init_set (vec[count], s[i][j]);
        count++;
      }
  qsort (progression, 9, sizeof (mpz_t), mcompar);

  int cfg[9][2];
  count = 0;
  for (int i = 0; i < 9; i++)
    {
      for (int j = 0; j < 9; j++)
        {
          if (mpz_cmp (progression[i], vec[j]) == 0)
            {
              cfg[count][0] = j / 3;
              cfg[count][1] = j % 3;
              count++;
            }
        }
    }

  for (int i = 0; i < 9; i++)
    mpz_clear (progression[i]);
  for (int i = 0; i < 9; i++)
    mpz_clear (vec[i]);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  //now we lookup our sub-configuration by type
          
  SubTypeArrayType *lookup = get_subtypes (type, &num);
  for (int i = 0; i < num; i++)
    if (memcmp (cfg, lookup[i], sizeof (cfg)) == 0)
      return i + 1;
  return 0;
}

double
mpz_divide (mpz_t numerator, mpz_t denominator)
{
  mpf_t n, d, r;
  mpf_inits (n, d, r, NULL);
  mpf_set_z (n, numerator);
  mpf_set_z (d, denominator);
  mpf_div (r, n, d);
  double retval = mpf_get_d (r);
  mpf_clears (n, d, r, NULL);
  return retval;
}

void
mpf_mpz_divide (mpf_t r, mpz_t numerator, mpz_t denominator)
{
  mpf_t n, d;
  mpf_inits (n, d, NULL);
  mpf_set_z (n, numerator);
  mpf_set_z (d, denominator);
  mpf_div (r, n, d);
  mpf_clears (n, d, NULL);
}

void
mpf_mpz_multiply (mpz_t rop, mpz_t op, mpf_t c)
{
  mpf_t r, n, m;
  mpf_inits (r, n, m, NULL);
  mpf_set_z (n, op);
  mpf_mul (r, n, c);
  mpf_ceil (r, r);
  mpz_set_f (rop, r);
  mpf_clears (r, n, m, NULL);
}

int
binary_read_3sq_from_stream (FILE *stream, mpz_t *lo, mpz_t *c, mpz_t *hi, char **line, size_t *len)
{
  ssize_t read = mpz_inp_raw (*lo, stream);
  if (!read)
    return read;
  read = mpz_inp_raw (*c, stream);
  if (!read)
    return read;
  read = mpz_inp_raw (*hi, stream);
  if (!read)
    return read;
  return read;
}

int
read_3sq_from_stream (FILE *stream, mpz_t *lo, mpz_t *c, mpz_t *hi, char **line, size_t *len)
{
  int size = 3;
  char delim = ',';
  //what a hassle
  char s[3];
  s[0] = delim;
  s[1] = '\n';
  s[2] = '\0';
  int i;
  ssize_t read = 0;
  for (i = 0; i < size; i++)
    {
      if (i == size - 1)
        read = fv_getline (line, len, stream);
      else
        read = fv_getdelim (line, len, delim, stream);
      if (read == -1)
        break;
      char *end = strpbrk (*line, s);
      if (end)
        *end = '\0';
      mpz_t *p = NULL;
      switch (i)
       {
       case 0: p = lo; break;
       case 1: p = c; break;
       case 2: p = hi; break;
       }
      int retval = mpz_set_str (*p, *line, 10);
      if (retval < 0)
        i--; /* probably takes us to EOF */
    }
  return read;
}

void
fv_init_log (struct fv_app_log_t *log, char *name)
{
  memset (log, 0, sizeof (struct fv_app_log_t));
  log->max_num_lines = 1000000;
  log->timeout = 30 * 60;
  snprintf (log->fname, sizeof (log->fname), "/tmp/%s.%d",
            name, getpid ());
  FILE *fp = fopen (log->fname, "w");
  if (fp)
    fclose (fp);
}

static int
fv_log_check (struct fv_app_log_t *log)
{
  log->num_lines++;
  int ret = log->num_lines == log->max_num_lines || 
      time (NULL) > log->last_log + log->timeout;
  if (ret)
    {
      log->num_lines = 0;
      log->last_log = time (NULL);
    }
  return ret;
}

void
fv_update_log (struct fv_app_log_t *log, char *line)
{
  if (fv_log_check (log))
    {
      FILE *fp = fopen (log->fname, "a");
      if (fp)
        {
          fprintf (fp, "%lu, %s", log->last_log, line);
          int len = strlen (line);
          if (line[len-1] != '\n')
            fprintf (fp, "\n");
          fclose (fp);
        }
    }
}

void
fv_update_log_ull (struct fv_app_log_t *log, unsigned long long int num)
{
  if (fv_log_check (log))
    {
      FILE *fp = fopen (log->fname, "a");
      if (fp)
        {
          fprintf (fp, "%lu, %llu\n", log->last_log, num);
          fclose (fp);
        }
      log->num_lines = 0;
    }
}

void
fv_update_log_mpz (struct fv_app_log_t *log, mpz_t *num)
{
  if (fv_log_check (log))
    {
      FILE *fp = fopen (log->fname, "a");
      if (fp)
        {
          fprintf (fp, "%lu, ", log->last_log);
          display_textual_number (num, fp);
          fclose (fp);
        }
    }
}
