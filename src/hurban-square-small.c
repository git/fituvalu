/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_hurban_square_t
{
  FILE *infile;
  int from_stdin;
  int num_args;
  unsigned long long p, q, r, s;
  FILE *out;
  int threads;
  int in_binary;
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_hurban (struct fv_app_hurban_square_t *app, unsigned long long p, unsigned long long q, unsigned long long r, unsigned long long s, long long(*a)[3][3])
{
  long long i;
  unsigned long long qr, ps, pr, qs, sum, sum_minus_middle, qr2, ps2, pr2, qs2;

  qr = q * r;
  ps = p * s;
  pr = p * r;
  qs = q * s;

  (*a)[2][0] = qr + ps;
  (*a)[2][0] = (*a)[2][0] * (*a)[2][0];

  (*a)[2][2] = pr + qs;
  (*a)[2][2] = (*a)[2][2] * (*a)[2][2];

  qr2 = qr * qr;
  ps2 = ps * ps;
  pr2 = pr * pr;
  qs2 = qs * qs;

  (*a)[1][1] = qr2 + ps2;
  (*a)[1][1] = (*a)[1][1] + pr2;
  (*a)[1][1] = (*a)[1][1] + qs2;
  (*a)[1][1] = (*a)[1][1] / 2;

  sum_minus_middle = (*a)[1][1] * 2;
  sum = (*a)[1][1] * 3;


  (*a)[2][1] = qr2 + ps2;
  (*a)[2][1] = (*a)[2][1] + pr2;
  (*a)[2][1] = (*a)[2][1] + qs2;
  (*a)[2][1] = (*a)[2][1] / 2;

  i = p * 4;
  i = i * q;
  i = i * r;
  i = i * s;
  (*a)[2][1] = (*a)[2][1] - i;

  (*a)[0][0] = sum_minus_middle - (*a)[2][2];
  (*a)[0][2] = sum_minus_middle - (*a)[2][0];

  (*a)[1][2] = sum - (*a)[0][2];
  (*a)[1][2] = (*a)[1][2] - (*a)[2][2];

  (*a)[1][0] = sum_minus_middle - (*a)[1][2];
  (*a)[0][1] = sum_minus_middle - (*a)[2][1];

  pthread_mutex_lock (&display_lock);
  fprintf (app->out,
           "%lld, %lld, %lld, "
           "%lld, %lld, %lld, "
           "%lld, %lld, %lld, \n",
           (*a)[0][0], (*a)[0][1], (*a)[0][2],
           (*a)[1][0], (*a)[1][1], (*a)[1][2],
           (*a)[2][0], (*a)[2][1], (*a)[2][2]);
  fflush (app->out);
  pthread_mutex_unlock (&display_lock);

}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_hurban_square_t *app =
    (struct fv_app_hurban_square_t *) param->data;

  long long s[3][3];

  unsigned long long a[4];

  while (1)
    {
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          if (fread (a, sizeof (unsigned long long), 4, app->infile) != 4)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          size_t read = read_ull_numbers (app->infile, a, 4, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      generate_hurban (app, a[0], a[1], a[2], a[3], &s);
    }

  if (line)
    free (line);
  return NULL;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_hurban_square_t *app = (struct fv_app_hurban_square_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'i':
      app->in_binary = 1;
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 4)
        argp_error (state, "too many arguments");
      else
        {
          switch (app->num_args)
            {
            case 0:
              app->p = strtoull (arg, &end, 10);
              break;
            case 1:
              app->q = strtoull (arg, &end, 10);
              break;
            case 2:
              app->r = strtoull (arg, &end, 10);
              break;
            case 3:
              app->s = strtoull (arg, &end, 10);
              break;
            }
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args < 4 && app->num_args > 0)
        argp_error (state, "too few arguments");
      else
        app->from_stdin = 1;
      break;
    }
  return 0;
}

int
fituvalu_hurban_square (struct fv_app_hurban_square_t *app)
{
  if (app->from_stdin)
    run_threads (app, app->threads, process_record);
  else
    {
      long long s[3][3];
      generate_hurban (app, app->p, app->q, app->r, app->s, &s);
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[P Q R S]",
  "Generate 3x3 magic squares with at least 4 perfect squares according to Joseph's Hurban's parametric solution.\vWhen P Q R S are not specified they are read from the standard input, separated by commas and terminated by a newline.  This program can find magic squares of types of 6:14 and 6:16.  The corners are guaranteed to be perfect squares.  Suitable input for this program is 1 9 5 8.\n"
"  +---+---+---+\n"
"  | X |   | X |\n"
"  +---+---+---+\n"
"  |   |   |   |\n"
"  +---+---+---+\n"
"  | X |   | X |\n"
"  +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_hurban_square_t app;
  memset (&app, 0, sizeof (app));
  app.infile = stdin;
  app.out = stdout;
  app.threads = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_hurban_square (&app);
}
