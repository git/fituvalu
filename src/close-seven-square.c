/* Copyright (C) 2019, 2020, 2024 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct fv_app_close_seven_t
{
  FILE *infile;
  FILE *out;
  int show_near_square;
  int negative;
  int dist;
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int fraction_histogram;
  int histogram[100];
  int just_negative;
  int histogram_tenths;
  int read_one_number;
  int (*read_number)(FILE *, mpz_t *, char **, size_t *);
  int nocenter;
};

static void
get_list_of_non_squares (struct fv_app_close_seven_t *app, mpz_t (*s)[3][3], mpz_t **list, int *num)
{
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      {
        if (app->nocenter && i == 1 && j == 1)
          continue;
        if (mpz_sgn ((*s)[i][j]) > 0 && app->just_negative)
          continue;
        if (!mpz_perfect_square_p ((*s)[i][j]))
          {
            *list = realloc (*list, sizeof (mpz_t) * ((*num) + 1));
            mpz_init_set ((*list)[*num], (*s)[i][j]);
            (*num)++;
          }
      }
  return;
}

static void
show_dist (FILE *out, mpf_t r, mpz_t n)
{
  mpz_t root, m, diff;
  mpz_inits (root, m, diff, NULL);
  mpz_sqrt (root, n);
  if (mpf_cmp_d (r, 0.5) > 0)
    mpz_add_ui (root, root, 1);
  mpz_mul (m, root, root);
  mpz_sub (diff, m, n);
  mpz_abs (diff, diff);
  display_textual_number_no_newline (diff, out);
  mpz_clears (root, m, diff, NULL);
}

static void
show_closest_to_square (struct fv_app_close_seven_t *app, mpz_t (*s)[3][3])
{
  int num_non_squares = 0;
  mpz_t *non_squares = NULL;
  get_list_of_non_squares (app, s, &non_squares, &num_non_squares);
  if (num_non_squares <= 3)
    {
      int count = 0;
      mpf_t r[3], n;
      mpf_inits (r[0], r[1], r[2], n, NULL);
      for (int i = 0; i < num_non_squares; i++)
        {
          if (app->negative)
            mpz_abs (non_squares[i], non_squares[i]);
          if (mpz_sgn (non_squares[i]) > 0)
            {
              mpf_set_z (n, non_squares[i]);
              mpf_sqrt (r[count], n);
              mpf_floor (n, r[count]);
              mpf_sub (r[count], r[count], n);

              if (app->show_near_square)
                {
                  if (app->dist)
                    {
                      show_dist (app->out, r[count], non_squares[i]);
                      fprintf (app->out, ", ");
                    }
                  else
                    gmp_printf ("%.50Ff, ", r[count], r[count]);
                  display_textual_number (&non_squares[i], app->out);
                }
              else
                {
                  if (app->dist)
                    {
                      show_dist (app->out, r[count], non_squares[i]);
                      fprintf (app->out, "\n");
                    }
                  else
                    gmp_printf ("%.50Ff\n", r[count], r[count]);
                }
              fflush (app->out);
              count++;
            }
        }
      mpf_clears (r[0], r[1], r[2], n, NULL);
    }
  for (int i = 0; i < num_non_squares; i++)
    mpz_clear (non_squares[i]);
  free (non_squares);
}

static void
calculate_histogram (struct fv_app_close_seven_t *app, mpz_t (*s)[3][3])
{
  int num_non_squares = 0;
  mpz_t *non_squares = NULL;
  get_list_of_non_squares (app, s, &non_squares, &num_non_squares);
  if (num_non_squares <= 3)
    {
      int count = 0;
      mpf_t r[3], n;
      mpf_inits (r[0], r[1], r[2], n, NULL);
      for (int i = 0; i < num_non_squares; i++)
        {
          if (app->negative)
            mpz_abs (non_squares[i], non_squares[i]);
          if (mpz_sgn (non_squares[i]) > 0)
            {
              mpf_set_z (n, non_squares[i]);
              mpf_sqrt (r[count], n);
              mpf_floor (n, r[count]);
              mpf_sub (r[count], r[count], n);

              mpf_t dd;
              mpf_init (dd);
              mpf_mul_ui (dd, r[count], 100);
              long idx = mpf_get_si (dd);
              if (idx >= 100)
                idx = 99;
              app->histogram[idx]++;
              mpf_clear (dd);

              count++;
            }
        }
      mpf_clears (r[0], r[1], r[2], n, NULL);
    }
  for (int i = 0; i < num_non_squares; i++)
    mpz_clear (non_squares[i]);
  free (non_squares);
}

static void
show_histogram (struct fv_app_close_seven_t *app)
{
  if (app->histogram_tenths)
    {
      int histogram[10];
      memset (histogram, 0, sizeof (histogram));
      for (int i = 0; i < 100; i++)
        histogram[i/10] += app->histogram[i];
      double p = 0.1;
      for (int i = 0; i < 10; i++, p +=0.1)
        fprintf (app->out, "%2.1f, %d\n", p, histogram[i]);
    }
  else
    {
      double p = 0.01;
      for (int i = 0; i < 100; i++, p +=0.01)
        fprintf (app->out, "%3.2f, %d\n", p, app->histogram[i]);
    }
}

int
fituvalu_close_seven (struct fv_app_close_seven_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3][3];

  int i, j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  while (1)
    {
      if (app->read_one_number)
        {
          for (i = 0; i < 3; i++)
            for (j = 0; j < 3; j++)
              mpz_set_ui (a[i][j], 1);
          read = app->read_number (app->infile, &a[0][0], &line, &len);
          if (read == -1)
            break;
          if (app->fraction_histogram)
            calculate_histogram (app, &a);
          else
            show_closest_to_square (app, &a);
        }
      else
        {
          read = app->read_square (app->infile, &a, &line, &len);
          if (read == -1)
            break;

          if (!is_magic_square (a, 1))
            continue;
          if (app->fraction_histogram)
            calculate_histogram (app, &a);
          else
            show_closest_to_square (app, &a);
        }
    }

  if (app->fraction_histogram)
    show_histogram (app);

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_close_seven_t *app = (struct fv_app_close_seven_t *) state->input;
  switch (key)
    {
    case'e':
      app->nocenter = 1;
      break;
    case '1':
      app->read_one_number = 1;
      break;
    case 'h':
      app->fraction_histogram = 1;
      break;
    case 'H':
      app->fraction_histogram = 1;
      app->histogram_tenths = 1;
      break;
    case 'n':
      app->negative = 1;
      break;
    case 'N':
      app->just_negative = 1;
      app->negative = 1;
      break;
    case 'f':
      app->dist = 0;
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      app->read_number = binary_read_one_number_from_stream;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->nocenter && app->read_one_number)
        argp_error (state, "-1 and -e can't be used together");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "negative", 'n', 0, 0, "Treat negative numbers as positive"},
  { "just-negative", 'N', 0, OPTION_HIDDEN, "Disregard all positive numbers"},
  { "fraction", 'f', 0, 0, "Show the remainder"},
  { "fraction-histogram", 'h', 0, 0, "Show a histogram of the fractions"},
  { "histogram-tenths", 'H', 0, OPTION_HIDDEN, "Show a histogram of the fractions"},
  { "one", '1', 0, 0, "Read in one number instead of a magic square"},
  { "exclude-center", 'e', 0, 0, "Ignore the center value"},
  { 0 },
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares with six perfect squares from the standard input, and display how close each of the other non-squares are to being square.\vThe nine values must be separated by a comma and terminated by a newline.  A result of '9, 265' means the number 265 is 9 away from being square.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_close_seven_t app;
  memset (&app, 0, sizeof (app));
  app.read_square = read_square_from_stream;
  app.display_square = display_square_record;
  app.infile = stdin;
  app.out = stdout;
  app.show_near_square = 1;
  app.negative = 0;
  app.dist = 1;
  app.read_number = read_one_number_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  is_magic_square_init ();
  return fituvalu_close_seven (&app);
}
