/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_62_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  double median;
  double percent;
  double max;
  int mult;
  int threesq;
  int quick_abc;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t i, iroot, j, sum, limit, twoiroot;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_62_type_1 (struct fv_app_search_62_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{

  mpz_mul_ui (q->limit, w->hi, app->mult);

  mpz_sqrt (q->iroot, w->distance);
  mpz_add_ui (q->iroot, q->iroot, 1);
  mpz_mul (q->i, q->iroot, q->iroot);

  mpz_set ((*s)[1][0], w->hi);
  mpz_set ((*s)[2][1], w->lo);

  //  +---+---+---+
  //  |   |   | B |
  //  +---+---+---+
  //  | C |   | E |
  //  +---+---+---+
  //  | D | A |   |
  //  +---+---+---+

  while (1)
    {
      mpz_sub (q->j, q->i, w->distance);

      int odd = 0;
      int dup = 0;

      mpz_set ((*s)[1][2], q->j);
      mpz_set ((*s)[2][0], q->i);

      mpz_add ((*s)[0][1], (*s)[2][0], w->distance);

      if (mpz_cmp ((*s)[2][1], (*s)[0][1]) > 0)
        {
          mpz_sub (q->sum, (*s)[2][1], (*s)[0][1]);
          odd = mpz_odd_p (q->sum);
          if (!odd)
            {
              mpz_cdiv_q_ui (q->sum, q->sum, 2);
              mpz_add ((*s)[1][1], (*s)[0][1], q->sum);
            }
        }
      else
        {
          mpz_sub (q->sum, (*s)[0][1], (*s)[2][1]);
          odd = mpz_odd_p (q->sum);
          if (!odd)
            {
              mpz_cdiv_q_ui (q->sum, q->sum, 2);
              mpz_add ((*s)[1][1], (*s)[2][1], q->sum);
            }
        }

      if (!odd && mpz_cmp ((*s)[1][1], (*s)[0][2]) != 0 &&
          mpz_perfect_square_p ((*s)[1][2]) &&
          mpz_perfect_square_p ((*s)[0][1]))
        {
          mpz_mul_ui (q->sum, (*s)[1][1], 3);

          mpz_sub ((*s)[0][0], q->sum, (*s)[0][1]);
          mpz_sub ((*s)[0][0], (*s)[0][0], (*s)[0][2]);

          mpz_sub ((*s)[2][2], q->sum, (*s)[2][1]);
          mpz_sub ((*s)[2][2], (*s)[2][2], (*s)[2][0]);

          pthread_mutex_lock (&display_lock);
          app->display_square (*s, app->out);
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);
        }

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);

      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
handle_progression (struct fv_app_search_62_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  generate_62_type_1 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_62_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[0][2], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[0][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[0][2], w->distance);
          mpz_add (w->hi, (*s)[0][2], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void
quick_generate_progressions (struct fv_app_search_62_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[0][2], 2);
  mpz_set_ui (w->i, 4);
  mpz_set_ui (w->iroot, 2);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[0][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[0][2], w->distance);
          mpz_add (w->hi, (*s)[0][2], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_62_t *app =
    (struct fv_app_search_62_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.sum, q.limit, q.twoiroot, NULL);

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[0][2], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[0][2], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[0][2], 1) > 0)
        {
          if (app->quick_abc)
            quick_generate_progressions (app, &w, &q, &s);
          else
            generate_progressions (app, &w, &q, &s);
        }
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  mpz_clears (q.i, q.iroot, q.j, q.sum, q.limit, q.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_62_t *app =
    (struct fv_app_search_62_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.sum, q.limit, q.twoiroot, NULL);

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[0][2], w.lo);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  mpz_clears (q.i, q.iroot, q.j, q.sum, q.limit, q.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_62 (struct fv_app_search_62_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_62_t *app = (struct fv_app_search_62_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'Q':
      app->quick_abc = 1;
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'm':
        {
          app->percent = strtod (arg, &end);
          if (end == NULL || *end != '\0' || app->percent <= 0)
            argp_error (state, "invalid argument `%s' to option --multiply", arg);
          app->percent /= 100.0;
        }
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      app->mult = ceil (app->max * app->percent);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "multiply", 'm', "PERC", 0, "Find a starting point for E by multiplying C by a value between 1 and 9755.2353 expressed as a percent (1-100)"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "quick-abc", 'Q', 0, OPTION_HIDDEN, "Do an abridged search"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:2 given a FILE containing top right values.\vWhen FILE is not provided, it is read from the standard input.  The default value for PERC is 0.06958109969935836.  Magic squares of type 6:2 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   | X | X |   |   | F | B |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating E\n"
"  | X |   | X |   | C |   | D |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below E.\n"
"  | X | X |   |   | E | A |   |   F is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_62_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.median = 2.2626;
  app.max = 9755.2353;
  app.percent = (app.median * 3.0) / app.max;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_62 (&app);
}
/*

the point of this is to identify information that can help us get more 6:2s.  we want to be able to predict 'center' values (the B values) for 6:2 magic squares.

  +---+---+---+   +---+---+---+
  |   | X | X |   |   | F | B |
  +---+---+---+   +---+---+---+
  | X |   | X |   | C |   | D |
  +---+---+---+   +---+---+---+
  | X | X |   |   | E | A |   |
  +---+---+---+   +---+---+---+

we identified which B values lead to magic squares with 6 perfect squares in the 6:2 configuration, and then we check for patterns that best fit that data.

6:2 is a special among types in that D values are also B values, but we didn't amalgamate them in this analysis.  so in a way, we only got half of the perfect squares that will shake out a 6:2 magic square.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:2 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 2743 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

36, 0, 48, 290 / 75252 = 0.003854
36, 48, 0, 290 / 75252 = 0.003854
108, 12, 48, 218 / 56439 = 0.003863
36, 84, 48, 223 / 56441 = 0.003951
108, 96, 48, 152 / 37638 = 0.004038
120, 84, 48, 156 / 37637 = 0.004145
112, 113, 111, 124 / 28234 = 0.004392
108, 117, 111, 126 / 28234 = 0.004463
115, 110, 111, 145 / 28236 = 0.005135
120, 105, 111, 147 / 28239 = 0.005206

well there's a 120, so maybe we need a greater breadth.
there seems to be a sum of 336 going on among the last 4 (120+105+111).
a hits of 147 is no slouch.  and we're at half a percent.  that's surprisingly good i'd say.

second run is depth of 2, and breadth of 2000.

465, 1215, 30 / 3779 = 0.007939
1905, 783, 19 / 2368 = 0.008024
1368, 1852, 16 / 1973 = 0.008109
897, 783, 31 / 3784 = 0.008192
1884, 552, 22 / 2613 = 0.008419
1921, 1775, 15 / 1721 = 0.008716
144, 1944, 27 / 3038 = 0.008887
1473, 1887, 17 / 1894 = 0.008976
708, 1392, 28 / 3027 = 0.009250
1368, 1740, 19 / 2050 = 0.009268

hmm the pecentage is nearly 2 times higher.  down from 147 hits to 19 though.
there's a worry that we're just trying to predict somewhat random numbers here, and that any of the top 5 are just as good.

and just for kicks, let's try a depth of 1 and a breadth of 100000.

87329, 2 / 39 = 0.051282
88284, 2 / 38 = 0.052632
88477, 2 / 38 = 0.052632
92856, 2 / 37 = 0.054054
95844, 2 / 35 = 0.057143
96460, 2 / 35 = 0.057143
98301, 2 / 35 = 0.057143
99228, 2 / 34 = 0.058824
99699, 2 / 34 = 0.058824
99705, 2 / 34 = 0.058824

i'm sure we're just in la-la guessing land here.
but maybe 99705 is worth trying.

let's say the minimum count has to be 10 or higher.
1071, 13 / 2958 = 0.004395
1260, 12 / 2517 = 0.004768
1380, 11 / 2297 = 0.004789
1404, 11 / 2261 = 0.004865
420, 39 / 7542 = 0.005171
1752, 10 / 1814 = 0.005513
840, 21 / 3781 = 0.005554
1428, 13 / 2222 = 0.005851
1092, 18 / 2908 = 0.006190
2088, 10 / 1522 = 0.006570

well the 39 hits stands out with 420.  and then hey look there's 840 but it gets like half.  is there a 1260? no

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

1, 1, 1, 2743 / 3159636 = 0.000868
1, 1, 0, 2743 / 3159636 = 0.000868
1, 0, 1, 2743 / 3159636 = 0.000868
1, 0, 0, 2743 / 3159636 = 0.000868
0, 1, 1, 2743 / 3159636 = 0.000868
0, 1, 0, 2743 / 3159636 = 0.000868
0, 0, 1, 2743 / 3159636 = 0.000868
1, 2, 1, 2402 / 2369727 = 0.001014
1, 2, 0, 2355 / 2106424 = 0.001118


stinky.  gotta go up by 1s to get them all.

results:

so looking forward for more sixes, pick one of "99228", "99699", or "99705".  Try "420", and "1368, 1740", and disregard the "120, 105, 111" because it happens to have our max breadth.  next best is "115, 110, 111".

we can't recalculate any quicker and get them all.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 1369 in sq-seq-finder.
0, 0, 84, 163 / 37636 = 0.004331
0, 84, 0, 163 / 37636 = 0.004331
0, 84, 84, 163 / 37636 = 0.004331
84, 0, 0, 163 / 37636 = 0.004331
84, 0, 84, 163 / 37636 = 0.004331
84, 84, 0, 163 / 37636 = 0.004331
84, 84, 84, 163 / 37636 = 0.004331
111, 111, 114, 123 / 28232 = 0.004357
111, 105, 120, 135 / 28235 = 0.004781
111, 112, 113, 145 / 28236 = 0.005135

well we have some zeroes.  84 obviously jumps out.

that's a curious pattern "111, 112, 113".  would a pattern of "111,112,113,114" be any good?  probably just a fluke.

and hey look the last 3 add up to 336 again.

well we didn't do any better with a different starting point.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

1071, 1281, 27 / 2705 = 0.009982
1176, 1932, 21 / 2050 = 0.010244
1444, 1664, 21 / 2050 = 0.010244
1960, 1988, 17 / 1616 = 0.010520
1848, 588, 28 / 2618 = 0.010695
1500, 1608, 22 / 2051 = 0.010726
1924, 1184, 23 / 2052 = 0.011209
1792, 1316, 23 / 2051 = 0.011214
1848, 1260, 25 / 2053 = 0.012177
1332, 1776, 28 / 2055 = 0.013625

well we did much better here, but we only matched 28.  i'd say it's worth a shot to try "1332, 1776".
a sum of 3108 (1332+1776) comes up again and again.
and hey we passed 1 percent by a good margin!

okay let's do depth=1, and breadth is 100000

86179, 3 / 40 = 0.075000
88575, 3 / 39 = 0.076923
89424, 3 / 39 = 0.076923
91271, 3 / 38 = 0.078947
91716, 3 / 38 = 0.078947
92133, 3 / 38 = 0.078947
93765, 3 / 37 = 0.081081
95900, 3 / 36 = 0.083333
96471, 3 / 36 = 0.083333
96824, 3 / 36 = 0.083333

kinda ridiculous here. around 8 percent.  if it pans out, that will really be something.

let's do depth=1, breadth is 100000, min hits of 10

1036, 21 / 3064 = 0.006854
1701, 13 / 1869 = 0.006956
1092, 21 / 2908 = 0.007221
1776, 13 / 1791 = 0.007259
1332, 18 / 2388 = 0.007538
2352, 11 / 1353 = 0.008130
2184, 13 / 1458 = 0.008916
2268, 14 / 1407 = 0.009950
3108, 13 / 1029 = 0.012634
3948, 11 / 812 = 0.013547

well we got to 1 percent. and it's similar in effectiveness to breadth=2000 and depth=2.

so overall, looking forward for more sixes, pick one of "99705", "99699", or "99228", and then one of "95900", "96471", or "96824".  Try "1368, 1740", "115, 110, 111", "1332, 1776", and "111, 112, 113".

okay, let's do the first run again but sort on hits to see how it shakes out.

1, 1, 1, 2743 / 3159601 = 0.000868
1, 1, 0, 2743 / 3159601 = 0.000868
1, 0, 1, 2743 / 3159601 = 0.000868
1, 0, 0, 2743 / 3159601 = 0.000868
0, 1, 1, 2743 / 3159601 = 0.000868
0, 1, 0, 2743 / 3159601 = 0.000868
0, 0, 1, 2743 / 3159601 = 0.000868
1, 2, 1, 2402 / 2369701 = 0.001014
1, 2, 0, 2355 / 2106401 = 0.001118

no better than before. gotta go up by 1 square at a time to get them all.

this concludes our analysis of anticipating B values in 6:2 magic squares.

in order to get high value 6:2s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

also we could consider running this test again but including all D values as B values.

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="99705" 1 10000000000000000000000000000 | ./search-62
sq-seq --pattern="99699" 1 10000000000000000000000000000 | ./search-62
sq-seq --pattern="99228" 1 10000000000000000000000000000 | ./search-62
sq-seq --pattern="1368,1740" 1 10000000000000000000000000000 | ./search-62
sq-seq --pattern="115,110,111" 1 10000000000000000000000000000 | ./search-62
sq-seq --pattern="2088" 1 10000000000000000000000000000 | ./search-62

sq-seq --pattern="95900" 1369 10000000000000000000000000000 | ./search-62
sq-seq --pattern="96471" 1369 10000000000000000000000000000 | ./search-62
sq-seq --pattern="96824" 1369 10000000000000000000000000000 | ./search-62
sq-seq --pattern="1332,1776" 1369 10000000000000000000000000000 | ./search-62
sq-seq --pattern="111,112,113" 1369 10000000000000000000000000000 | ./search-62
sq-seq --pattern="3948" 1369 10000000000000000000000000000 | ./search-62


*/
