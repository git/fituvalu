/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_hurban_square_t
{
  FILE *infile;
  int in_binary;
  int from_stdin;
  int num_args;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  mpz_t p, q, r, s;
  FILE *out;
  int threads;
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_hurban (struct fv_app_hurban_square_t *app, mpz_t p, mpz_t q, mpz_t r, mpz_t s, mpz_t (*a)[3][3])
{
  mpz_t qr, ps, pr, qs, sum, sum_minus_middle, i, qr2, ps2, pr2, qs2;
  mpz_inits (qr, ps, pr, qs, sum, sum_minus_middle, i, qr2, ps2, pr2, qs2, NULL);

  mpz_mul (qr, q, r);
  mpz_mul (ps, p, s);
  mpz_mul (pr, p, r);
  mpz_mul (qs, q, s);

  mpz_add ((*a)[2][0], qr, ps);
  mpz_mul ((*a)[2][0], (*a)[2][0], (*a)[2][0]);

  mpz_add ((*a)[2][2], pr, qs);
  mpz_mul ((*a)[2][2], (*a)[2][2], (*a)[2][2]);

  mpz_mul (qr2, qr, qr);
  mpz_mul (ps2, ps, ps);
  mpz_mul (pr2, pr, pr);
  mpz_mul (qs2, qs, qs);

  mpz_add ((*a)[1][1], qr2, ps2);
  mpz_add ((*a)[1][1], (*a)[1][1], pr2);
  mpz_add ((*a)[1][1], (*a)[1][1], qs2);
  mpz_cdiv_q_ui ((*a)[1][1], (*a)[1][1], 2);

  mpz_mul_ui (sum_minus_middle, (*a)[1][1], 2);
  mpz_mul_ui (sum, (*a)[1][1], 3);


  mpz_add ((*a)[2][1], qr2, ps2);
  mpz_add ((*a)[2][1], (*a)[2][1], pr2);
  mpz_add ((*a)[2][1], (*a)[2][1], qs2);
  mpz_cdiv_q_ui ((*a)[2][1], (*a)[2][1], 2);

  mpz_mul_ui (i, p, 4);
  mpz_mul (i, i, q);
  mpz_mul (i, i, r);
  mpz_mul (i, i, s);
  mpz_sub ((*a)[2][1], (*a)[2][1], i);

  mpz_sub ((*a)[0][0], sum_minus_middle, (*a)[2][2]);
  mpz_sub ((*a)[0][2], sum_minus_middle, (*a)[2][0]);

  mpz_sub ((*a)[1][2], sum, (*a)[0][2]);
  mpz_sub ((*a)[1][2], (*a)[1][2], (*a)[2][2]);

  mpz_sub ((*a)[1][0], sum_minus_middle, (*a)[1][2]);
  mpz_sub ((*a)[0][1], sum_minus_middle, (*a)[2][1]);

  pthread_mutex_lock (&display_lock);
  app->display_square (*a, app->out);
  fflush (app->out);
  pthread_mutex_unlock (&display_lock);

  mpz_clears (qr, ps, pr, qs, sum, sum_minus_middle, i, qr2, ps2, pr2, qs2, NULL);
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_hurban_square_t *app =
    (struct fv_app_hurban_square_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  mpz_t a[4];
  mpz_inits (a[0], a[1], a[2], a[3], NULL);
  while (1)
    {
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_four_numbers_from_stream (app->infile, a, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_four_numbers_from_stream (app->infile, a, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      generate_hurban (app, a[0], a[1], a[2], a[3], &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (a[0], a[1], a[2], a[3], NULL);

  if (line)
    free (line);
  return NULL;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_hurban_square_t *app = (struct fv_app_hurban_square_t *) state->input;
  switch (key)
    {
    case 't':
      app->threads = atoi (arg);
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 4)
        argp_error (state, "too many arguments");
      else
        {
          switch (app->num_args)
            {
            case 0:
              mpz_set_str (app->p, arg, 10);
              break;
            case 1:
              mpz_set_str (app->q, arg, 10);
              break;
            case 2:
              mpz_set_str (app->r, arg, 10);
              break;
            case 3:
              mpz_set_str (app->s, arg, 10);
              break;
            }
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      mpz_inits (app->p, app->q, app->r, app->s, NULL);
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args < 4 && app->num_args > 0)
        argp_error (state, "too few arguments");
      else
        app->from_stdin = 1;
      break;
    }
  return 0;
}

int
fituvalu_hurban_square (struct fv_app_hurban_square_t *app)
{
  if (app->from_stdin)
    run_threads (app, app->threads, process_record);
  else
    {
      mpz_t s[3][3];
      for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
          mpz_init (s[i][j]);
      generate_hurban (app, app->p, app->q, app->r, app->s, &s);
      for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
          mpz_clear (s[i][j]);
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[P Q R S]",
  "Generate 3x3 magic squares with at least 4 perfect squares according to Joseph's Hurban's parametric solution.\vWhen P Q R S are not specified they are read from the standard input, separated by commas and terminated by a newline.  This program can find magic squares of types of 6:14 and 6:16.  The corners are guaranteed to be perfect squares.  Suitable input for this program is 1 9 5 8.\n"
"  +---+---+---+\n"
"  | X |   | X |\n"
"  +---+---+---+\n"
"  |   |   |   |\n"
"  +---+---+---+\n"
"  | X |   | X |\n"
"  +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_hurban_square_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.infile = stdin;
  app.out = stdout;
  app.threads = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_hurban_square (&app);
}
