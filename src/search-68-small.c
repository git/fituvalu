/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_68_t
{
  int exhaustive;
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  double median;
  double percent;
  int percent_set;
  double max;
  int mult;
  int threesq;
  int subtype;
};

struct thread_data_t
{
  void *data;
  unsigned long long int prev_center;
  unsigned long long int *ll;
  unsigned long long int sum;
  unsigned long long int sum_minus_middle;
  int num_ll;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      param[i].prev_center = 0;
      param[i].ll = NULL;
      param[i].num_ll = 0;
      param[i].sum = 0;
      param[i].sum_minus_middle = 0;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_68_type_1 (struct fv_app_search_68_t *app, unsigned long long lo, unsigned long long hi, unsigned long long i, unsigned long long j, unsigned long long distance, long long int (*s)[3][3])
{
  unsigned long long int sum, sum_minus_middle;

  //  +---+---+---+
  //  |   |   | B |
  //  +---+---+---+
  //  | A | D |   |
  //  +---+---+---+
  //  |   | C | E |
  //  +---+---+---+

  (*s)[2][1] = hi;
  (*s)[1][0] = lo;

  (*s)[1][1] = i;

  sum = i * 3;
  sum_minus_middle = i * 2;

  (*s)[0][0] = (*s)[1][1] + distance;

  (*s)[2][2] = j;

  (*s)[2][0] = sum - (*s)[2][1];
  (*s)[2][0] = (*s)[2][0] - (*s)[2][2];

  (*s)[1][2] = sum_minus_middle - (*s)[1][0];
  (*s)[0][1] = sum_minus_middle - (*s)[2][1];

    {
      //check for dups
      int dup = 0;
      if ((*s)[1][1] == (*s)[1][0])
        dup = 1;
      else if ((*s)[1][1] == (*s)[2][0])
        dup = 1;
      if (!dup)
        {
          if (small_is_square ((*s)[2][0]))
            {
              pthread_mutex_lock (&display_lock);
              fprintf (app->out,
                       "%llu, %lld, %llu, "
                       "%llu, %llu, %llu, "
                       "%llu, %llu, %llu, \n",
                       (*s)[0][0], (*s)[0][1], (*s)[0][2],
                       (*s)[1][0], (*s)[1][1], (*s)[1][2],
                       (*s)[2][0], (*s)[2][1], (*s)[2][2]);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
generate_68_type_2 (struct fv_app_search_68_t *app, unsigned long long lo, unsigned long long hi, unsigned long long i, unsigned long long j, unsigned long long distance, long long int (*s)[3][3])
{
  unsigned long long int sum, sum_minus_middle;

  //  +---+---+---+
  //  |   |   | B |
  //  +---+---+---+
  //  | C | E |   |
  //  +---+---+---+
  //  |   | A | D |
  //  +---+---+---+

  (*s)[2][1] = lo;
  (*s)[1][0] = hi;

  (*s)[1][1] = j;

  sum = j * 3;
  sum_minus_middle = j * 2;

  (*s)[0][0] = (*s)[1][1] - distance;

  (*s)[2][2] = i;

  (*s)[2][0] = sum - (*s)[2][1];
  (*s)[2][0] = (*s)[2][0] - (*s)[2][2];

  (*s)[1][2] = sum_minus_middle - (*s)[1][0];
  (*s)[0][1] = sum_minus_middle - (*s)[2][1];

    {
      //check for dups
      int dup = 0;
      if ((*s)[1][1] == (*s)[1][0])
        dup = 1;
      else if ((*s)[1][1] == (*s)[2][0])
        dup = 1;
      if (!dup)
        {
          if (small_is_square ((*s)[2][0]))
            {
              pthread_mutex_lock (&display_lock);
              fprintf (app->out,
                       "%lld, %llu, %llu, "
                       "%llu, %llu, %lld, "
                       "%llu, %llu, %llu, \n",
                       (*s)[0][0], (*s)[0][1], (*s)[0][2],
                       (*s)[1][0], (*s)[1][1], (*s)[1][2],
                       (*s)[2][0], (*s)[2][1], (*s)[2][2]);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
generate_68_type_1_and_2 (struct fv_app_search_68_t *app, unsigned long long lo, unsigned long long hi, unsigned long long int distance, long long (*s)[3][3])
{
  unsigned long long int i, iroot, j, limit, twoiroot;

  //we start at sum counting down, and checking downwards for squares
  limit = hi * app->mult;

  iroot = sqrtl (distance);
  iroot++;
  i = iroot * iroot;

  while (1)
    {

      j = i - distance;
      if (small_is_square (j))
        {
          if (app->subtype <= 1)
            generate_68_type_1 (app, lo, hi, i, j, distance, s);
          if (app->subtype == 2 || app->subtype == 0)
            generate_68_type_2 (app, lo, hi, i, j, distance, s);
        }

      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
      if (i > limit)
        break;
    }
}

static void exhaustive_create_68_square (struct fv_app_search_68_t *app, unsigned long long lo, unsigned long long hi, unsigned long long e, unsigned long long sum, unsigned long long sum_minus_middle, long long (*s)[3][3])
{
  /*
  +---+---+---+
  |   |   | A |
  +---+---+---+
  |   | N |   |
  +---+---+---+
  | C |   | D |
  +---+---+---+
  */
  (*s)[0][2] = lo;
  (*s)[2][0] = hi;
  (*s)[2][2] = e;
  (*s)[2][1] = sum - hi;
  (*s)[2][1] = (*s)[2][1] - e;
  if (small_is_square ((*s)[2][1]))
    {
      (*s)[0][0] = sum_minus_middle - (*s)[2][2];

      (*s)[1][0] = sum - (*s)[0][0];
      (*s)[1][0] = (*s)[1][0] - (*s)[2][0];
      if (small_is_square ((*s)[1][0]))
        {
          (*s)[0][1] = sum_minus_middle - (*s)[2][1];
          (*s)[1][2] = sum_minus_middle - (*s)[1][0];
          int dup = 0;
          if (!dup)
            {
              pthread_mutex_lock (&display_lock);
              fprintf (app->out,
                       "%lld, %lld, %llu, "
                       "%llu, %llu, %lld, "
                       "%llu, %llu, %llu, \n",
                       (*s)[0][0], (*s)[0][1], (*s)[0][2],
                       (*s)[1][0], (*s)[1][1], (*s)[1][2],
                       (*s)[2][0], (*s)[2][1], (*s)[2][2]);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
exhaustive_generate_68_type_1_and_2 (struct fv_app_search_68_t *app, unsigned long long lo, unsigned long long hi, unsigned long long int distance, long long (*s)[3][3], struct thread_data_t *t)
{

  if (t->prev_center != (*s)[1][1])
    {
      if ((*s)[1][1] < t->prev_center || t->prev_center == 0)
        {
          unsigned long long int iroot, i, twoiroot, limit;
          t->prev_center = (*s)[1][1];
          t->sum = (*s)[1][1] * 3;
          t->sum_minus_middle = (*s)[1][1] * 2;
          t->num_ll = 0;
          free (t->ll);
          t->ll = NULL;

          //fill ll
          limit = (*s)[1][1] * 3;
          i = 1;
          iroot = 1;
          while (1)
            {
              if (i >= limit)
                break;

              t->ll = realloc (t->ll, sizeof (unsigned long long int) * (t->num_ll + 1));
              t->ll[t->num_ll] = i;
              t->num_ll++;

              twoiroot = iroot * 2;
              i += twoiroot;
              i++;
              iroot++;
            }
        }
      else
        {
          unsigned long long int iroot, i, twoiroot, limit;
          t->prev_center = (*s)[1][1];
          t->sum = (*s)[1][1] * 3;
          t->sum_minus_middle = (*s)[1][1] * 2;

          //fill ll
          limit = (*s)[1][1] * 3;
          i = t->ll[t->num_ll-1];
          iroot = sqrtl (i);

          twoiroot = iroot * 2;
          i += twoiroot;
          i++;
          iroot++;

          while (1)
            {
              if (i >= limit)
                break;

              t->ll = realloc (t->ll, sizeof (unsigned long long int) * (t->num_ll + 1));
              t->ll[t->num_ll] = i;
              t->num_ll++;

              twoiroot = iroot * 2;
              i += twoiroot;
              i++;
              iroot++;
            }
        }
    }
  unsigned long long int limit = t->sum - lo;
  for (int i = 0; i < t->num_ll; i++)
    {
      if ((*s)[1][1] == t->ll[i])
        continue;
      if (t->ll[i] > limit)
        break;
      exhaustive_create_68_square (app, lo, hi, t->ll[i], t->sum, t->sum_minus_middle, s);
      exhaustive_create_68_square (app, hi, lo, t->ll[i], t->sum, t->sum_minus_middle, s);
    }
}

static void
handle_progression (struct fv_app_search_68_t *app, unsigned long long lo, unsigned long long hi, unsigned long long int distance, long long (*s)[3][3], struct thread_data_t *t)
{
  if (app->exhaustive)
    exhaustive_generate_68_type_1_and_2 (app, lo, hi, distance, s, t);
  else
    generate_68_type_1_and_2 (app, lo, hi, distance, s);
}

static void
generate_progressions (struct fv_app_search_68_t *app, unsigned long long n, long long (*s)[3][3], struct thread_data_t *t)
{
  unsigned long long i, iroot, diff, limit, nn, mn, twomn, lo, hi, twoiroot;

  limit = n / 2;
  i = 1;
  iroot = 1;
  while (1)
    {
      if (i > limit)
        break;

      diff = n - i;

      if (small_is_square (diff))
        {
          nn = sqrtl (diff);
          mn = iroot * nn;
          twomn = mn * 2;
          lo = n - twomn;
          hi = n + twomn;
      
          if (twomn > 0)
            handle_progression (app, lo, hi, twomn, s, t);
        }
      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
  return;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_68_t *app =
    (struct fv_app_search_68_t *) param->data;

  long long int s[3][3];

  unsigned long long int a[3];
  unsigned long long int b[3];

  unsigned long long distance;

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          if (fread (a, sizeof (unsigned long long), 3, app->infile) != 3)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          size_t read = read_ull_numbers (app->infile, a, 3, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      b[0] = a[0];
      b[1] = a[1];
      b[2] = a[2];
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on 3sq
        {
          if (app->exhaustive)
            s[1][1] = b[1];
          else
            s[0][2] = b[1];
          distance = b[1] - b[0];
          if (b[1] > b[0])
            handle_progression (app, b[0], b[2],
                                distance, &s, param);
        }
    }


  if (line)
    free (line);
  return NULL;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_68_t *app =
    (struct fv_app_search_68_t *) param->data;

  long long s[3][3];

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          size_t read = fread (&n, sizeof (unsigned long long), 1, app->infile);
          if (read != 1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          num = strtoull (line, &end, 10);
        }
      n = num;

      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
      if (n > 1)
        {
          if (app->exhaustive)
            s[1][1] = n;
          else
            s[0][2] = n;
          generate_progressions (app, n, &s, param);
        }
    }

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_68 (struct fv_app_search_68_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_68_t *app = (struct fv_app_search_68_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'e':
      app->exhaustive = 1;
      break;
    case 's':
      app->subtype = atoi (arg);
      if (app->subtype <= 0 || app->subtype > 2)
        argp_error (state, "invalid subtype");
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'm':
      app->percent = strtod (arg, &end);
      if (end == NULL || *end != '\0' || app->percent <= 0)
        argp_error (state, "invalid argument `%s' to option --multiply", arg);
      app->percent /= 100.0;
      app->percent_set = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      app->mult = ceil (app->max * app->percent);
      if (app->exhaustive && app->percent_set)
        argp_error (state, "can't use --exhaustive with --multiply");
      if (app->exhaustive && app->subtype)
        argp_error (state, "can't use --exhaustive with --subtype");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "multiply", 'm', "PERC", 0, "Find a starting point for E by multiplying C by a value between 1 and 9755.2353 expressed as a percent (1-100)"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "subtype", 's', "NUM", 0, "Only produce subtype 1 or 2 instead of both"},
  { "exhaustive", 'e', 0, 0, "Use an exhaustive method"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:8 given a FILE containing top right values.\vWhen FILE is not provided, it is read from the standard input.  This program is limited to 64-bit integers.  The default value for PERC is 0.013159466436615425.  Magic squares of type 6:8 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   |   | X |   |   |   | B |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating E\n"
"  | X | X |   |   | A | E |   |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below E.\n"
"  | X | X | X |   | Z | C | D |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n"
"                  +---+---+---+\n"
"                  |   |   | B |\n"
"                  +---+---+---+  (starts iterating F upwards)\n"
"                  | C | E |   |\n"
"                  +---+---+---+\n"
"                  | Z | A | F |\n"
"                  +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_68_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.median = 2.8279;
  app.max = 64468.4193;
  app.percent = (app.median * 3.0) / app.max;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_68 (&app);
}
