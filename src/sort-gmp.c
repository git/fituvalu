/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "magicsquareutil.h"

struct fv_app_sort_gmp_t
{
  int removefile;
  char *output;
  FILE *infile;
  char *infilename;
  void (*dump_record) (mpz_t *, int, FILE *);
  int (*read_record) (FILE *, mpz_t *, int, char **, size_t *);
  int num_columns;
};
struct fv_app_sort_gmp_t *global_app;

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_sort_gmp_t *app = (struct fv_app_sort_gmp_t *) state->input;
  switch (key)
    {
    case 'r':
      app->removefile = 1;
      break;
    case 'O':
      app->output = arg;
      break;
    case 'n':
      app->num_columns = atoi (arg);
      break;
    case 'i':
      app->read_record = binary_read_numbers_from_stream;
      break;
    case 'o':
      app->dump_record = disp_binary_record;
      break;
    case ARGP_KEY_ARG:
      if (app->infile)
        argp_error (state, "too many arguments");
      else
        {
          if (strcmp (arg, "-") == 0)
            app->infile = stdin;
          else
            {
              app->infile = fopen (arg, "r");
              if (!app->infile)
                argp_error (state, "could not open `%s' for reading (%m)", arg);
              app->infilename = arg;
            }
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static void
add_record (mpz_t ***records, int *num_records, mpz_t *rec, int size)
{
  *records = realloc (*records, ((*num_records) + 1) * sizeof (mpz_t*));
  (*records)[(*num_records)] = malloc (size * sizeof (mpz_t));
  for (int i = 0; i < size; i++)
    mpz_init_set ((*records)[(*num_records)][i], rec[i]);
  (*num_records)++;
  return;
}

int
compar (const void *left, const void *right)
{
  mpz_t **l1 = (mpz_t **) left;
  mpz_t **r1 = (mpz_t **) right;
  mpz_t *l = *l1;
  mpz_t *r = *r1;
  for (int i = 0; i < global_app->num_columns; i++)
    {
      int ret = mpz_cmp (l[i], r[i]);
      if (ret != 0)
        return ret;
    }
  return 0;
}

int
fituvalu_sort_gmp (struct fv_app_sort_gmp_t *app, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[app->num_columns];

  mpz_t **records = NULL;
  int num_records = 0;
  for (int i = 0; i < app->num_columns; i++)
    mpz_init (a[i]);

  while (1)
    {
      read = app->read_record (app->infile, a, app->num_columns, &line, &len);
      if (read == -1)
        break;
      add_record (&records, &num_records, a, app->num_columns);
    }

  if (app->infile != stdin)
    fclose (app->infile);
  if (app->removefile && app->infilename)
    remove (app->infilename);
  qsort (records, num_records, sizeof (mpz_t*), compar);
  for (int i = 0; i < num_records; i++)
    app->dump_record (records[i], app->num_columns, out);

  for (int i = 0; i < num_records; i++)
    {
      for (int j = 0; j < app->num_columns; j++)
        mpz_clear (records[i][j]);
      free (records[i]);
    }
  free (records);
  records = NULL;
  num_records = 0;
  for (int i = 0; i < app->num_columns; i++)
    mpz_clear (a[i]);

  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "num-columns", 'n', "NUM", 0, "A record has NUM columns (default 9)"},
  { "output", 'O', "FILE", 0, "Write result to FILE instead of standard output"},
  { "remove-file", 'r', 0, 0, "Remove FILE after it is sorted"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Sort lines in FILE, where it consists of a series of numbers.\vIf FILE is not specified, it is read from the standard input.",
  0
};

struct fv_app_sort_gmp_t app;
int
main (int argc, char **argv)
{
  memset (&app, 0, sizeof (app));
  app.dump_record = disp_record;
  app.read_record = read_numbers_from_stream;
  app.num_columns = 9;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (!app.infile)
    app.infile = stdin;
  global_app = &app;
  if (app.output)
    {
      FILE *fp = fopen (app.output, "w");
      int ret = fituvalu_sort_gmp (&app, fp);
      fclose (fp);
      return ret;
    }
  else
    return fituvalu_sort_gmp (&app, stdout);
}
