/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

int max_recs = 1000;

struct fv_app_search_614_t
{
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  int in_binary;
  int threesq;
  int quick;
};


struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

struct prec
{
  unsigned long long int lo;
  unsigned long long int hi;
};

static void
generate_progressions (struct fv_app_search_614_t *app, unsigned long long int n, struct prec **recs, int *num_recs)
{
  unsigned long long int i, iroot = 1, diff, limit, twoiroot;
  limit = n / 2;
  i = 1;
  while (1)
    {
      if (i > limit)
        break;

      diff = n - i;
      long double diffroot = sqrtl (diff);
      if (ceill (diffroot) == diffroot)
      //if ((long long)diffroot * (long long)diffroot == diff)
        {
          unsigned long long int mn, twomn, mm, nn;
          mm = iroot;
          nn = (long long) diffroot;
          mn = mm * nn;
          twomn = mn * 2;
          if (*num_recs >= max_recs)
            *recs = realloc (*recs, sizeof (struct prec) * ((*num_recs) + 1));
          (*recs)[*num_recs].lo = n - twomn;
          (*recs)[*num_recs].hi = n + twomn;
          (*num_recs)++;
        }
      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
  return;
}

static void
generate_614_square_type_a (struct fv_app_search_614_t *app, unsigned long long center, unsigned long long sum, struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  long long int right = sum - one->lo;
  right = right - two->hi;

  if (small_is_square (right))
    {
      long long int left = sum - center;
      left = left - right;

      long long int tc = sum - two->lo;
      tc = tc - one->lo;

      long long int bc = sum - two->hi;
      bc = bc - one->hi;

      if (app->threads > 1)
        pthread_mutex_lock (&display_lock);
      fprintf (app->out,
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, \n",
               two->lo, tc, one->lo,
               left, center, right,
               one->hi, bc, two->hi);
      fflush (app->out);
      if (app->threads > 1)
        pthread_mutex_unlock (&display_lock);
    }
}

static void
generate_614_square_type_b (struct fv_app_search_614_t *app, unsigned long long center, unsigned long long sum, struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  long long int right = sum - one->lo;
  right = right - two->lo;

  if (small_is_square (right))
    {
      long long int left = sum - center;
      left = left - right;

      long long int tc = sum - two->hi;
      tc = tc - one->lo;

      long long int bc = sum - two->lo;
      bc = bc - one->hi;

      if (app->threads > 1)
        pthread_mutex_lock (&display_lock);
      fprintf (app->out,
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, \n",
               two->hi, tc, one->lo,
               left, center, right,
               one->hi, bc, two->lo);
      fflush (app->out);
      if (app->threads > 1)
        pthread_mutex_unlock (&display_lock);
    }
}

static void
generate_614_square_type_c (struct fv_app_search_614_t *app, unsigned long long center, unsigned long long sum, struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  long long int right = sum - one->hi;
  right = right - two->lo;

  if (small_is_square (right))
    {
      long long int left = sum - center;
      left = left - right;

      long long int tc = sum - two->hi;
      tc = tc - one->hi;

      long long int bc = sum - two->lo;
      bc = bc - one->lo;

      if (app->threads > 1)
        pthread_mutex_lock (&display_lock);
      fprintf (app->out,
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, \n",
               two->hi, tc, one->hi,
               left, center, right,
               one->lo, bc, two->lo);
      fflush (app->out);
      if (app->threads > 1)
        pthread_mutex_unlock (&display_lock);
    }
}

static void
generate_614_square_type_d (struct fv_app_search_614_t *app, unsigned long long center, unsigned long long sum, struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  long long int right = sum - one->hi;
  right = right - two->hi;

  if (small_is_square (right))
    {
      long long int left = sum - center;
      left = left - right;

      long long int tc = sum - two->lo;
      tc = tc - one->hi;

      long long int bc = sum - two->hi;
      bc = bc - one->lo;

      if (app->threads > 1)
        pthread_mutex_lock (&display_lock);
      fprintf (app->out,
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, \n",
               two->lo, tc, one->hi,
               left, center, right,
               one->lo, bc, two->hi);
      fflush (app->out);
      if (app->threads > 1)
        pthread_mutex_unlock (&display_lock);
    }
}

static void
generate_614_square (struct fv_app_search_614_t *app, unsigned long long center, unsigned long long sum, struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  generate_614_square_type_a (app, center, sum, one, two, recs, num_recs);
  generate_614_square_type_b (app, center, sum, one, two, recs, num_recs);
  generate_614_square_type_c (app, center, sum, one, two, recs, num_recs);
  generate_614_square_type_d (app, center, sum, one, two, recs, num_recs);
}

//http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_614_t *app, struct prec *recs, int num_recs, unsigned long long center, unsigned long long sum);

void printCombination (int arr[], int n, int r, struct fv_app_search_614_t *app, struct prec *recs, int num_recs, unsigned long long center, unsigned long long sum)
{
  int data[r];
  combinationUtil (arr, data, 0, n-1, 0, r, app, recs, num_recs, center, sum);
}
 
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_614_t *app, struct prec *recs, int num_recs, unsigned long long center, unsigned long long sum)
{
  if (index == r)
    {
      generate_614_square (app, center, sum, &recs[data[0]], &recs[data[1]], recs, num_recs);
      return;
    }

  for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
      data[index] = arr[i];
      combinationUtil(arr, data, i + 1, end, index + 1, r, app, recs, num_recs, center, sum);
    }
}

static void
handle_progressions (struct fv_app_search_614_t *app, unsigned long long int n, struct prec *recs, int num_recs, int by, int *arr)
{
  //now we try every combination of 2.
  printCombination (arr, num_recs, by, app, recs, num_recs, n, n * 3);
}

static void
quick_handle_progression (struct fv_app_search_614_t *app, unsigned long long lo, unsigned long long hi, unsigned long long dist, long long (*s)[3][3])
{
  (*s)[0][0] = lo;
  (*s)[2][2] = hi;

  unsigned long long int i, iroot, j, twoiroot, limit, sum, sum_minus_middle;

  sum = (*s)[1][1] * 3;
  sum_minus_middle = (*s)[1][1] * 2;

  limit = sum_minus_middle;

  i = (*s)[1][1];
  iroot = sqrtl (i);

  twoiroot = iroot * 2;
  i += twoiroot;
  i++;
  iroot++;

  while (1)
    {
      (*s)[0][2] = i;
      (*s)[1][2] = sum - hi;
      (*s)[1][2] = (*s)[1][2] - i;
      if (small_is_square ((*s)[1][2]) && (*s)[1][1] != i)
        {
          (*s)[2][0] = (*s)[1][2] + dist;
          if (small_is_square ((*s)[2][0]))
            {
              (*s)[0][1] = (*s)[2][0] + dist;

              (*s)[1][0] = sum_minus_middle - (*s)[1][2];
              (*s)[2][1] = sum_minus_middle - (*s)[0][1];
                {
                  if (app->threads > 1)
                    pthread_mutex_lock (&display_lock);
                  fprintf (app->out,
                           "%lld, %lld, %lld, "
                           "%lld, %lld, %lld, "
                           "%lld, %lld, %lld, \n",
                           (*s)[0][0], (*s)[0][1], (*s)[0][2],
                           (*s)[1][0], (*s)[1][1], (*s)[1][2],
                           (*s)[2][0], (*s)[2][1], (*s)[2][2]);
                  fflush (app->out);
                  if (app->threads > 1)
                    pthread_mutex_unlock (&display_lock);
                }
            }
        }
      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
      if (i > limit)
        break;
    }
}

static void
quick_generate_progressions (struct fv_app_search_614_t *app, unsigned long long n, long long (*s)[3][3])
{
  unsigned long long i, iroot, diff, limit, nn, mn, twomn, lo, hi, twoiroot;

  //we want twomn to be between 33% and 50% of n
  limit = n / 100;
  limit *= 93;
  i = n / 100;
  i *= 87;
  iroot = sqrtl (i);
  i = iroot * iroot;
  while (1)
    {
      if (i > limit)
        break;

      diff = n - i;

      if (small_is_square (diff))
        {
          nn = sqrtl (diff);
          mn = iroot * nn;
          twomn = mn * 2;
          lo = n - twomn;
          hi = n + twomn;
      
          if (twomn > 0)
            quick_handle_progression (app, lo, hi, twomn, s);
        }
      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_614_t *app =
    (struct fv_app_search_614_t *) param->data;

  int *arr = malloc (sizeof (int) * max_recs);
  for (int i = 0; i < max_recs; i++)
    arr[i] = i;
  struct prec *recs = malloc (sizeof (struct prec) * max_recs);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          size_t r = fread (&num, sizeof (num), 1, app->infile);
          if (r == 0)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          num = strtoull (line, &end, 10);
        }
      n = num;
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
      if (app->quick)
        {
          long long int s[3][3];
          s[1][1] = n;
          quick_generate_progressions (app, n, &s);
        }
      else
        {
          int num_recs = 0;
          generate_progressions (app, n, &recs, &num_recs);
          if (num_recs > max_recs)
            {
              // we won't be hitting this with max_recs = 1000
              // but hey, no arbitrary limits.
              arr = realloc (arr, sizeof (int) * (num_recs));
              for (int i = max_recs; i < num_recs; i++)
                arr[i] = i;
            }
          if (num_recs >= 2)
            handle_progressions (app, n, recs, num_recs, 2, arr);
        }
    }

  free (arr);
  free (recs);
  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_614_t *app =
    (struct fv_app_search_614_t *) param->data;

  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long a[3], n = 0;

  int *arr = malloc (sizeof (int) * max_recs);
  for (int i = 0; i < max_recs; i++)
    arr[i] = i;
  int num_recs = 0;
  struct prec *recs = malloc (sizeof (struct prec) * max_recs);
  while (1)
    {
      //go get the next set of progressions to work on

      if (app->in_binary)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&read_lock);
          size_t read =  fread (a, sizeof (unsigned long long), 3, app->infile);
          if (app->threads > 1)
            pthread_mutex_unlock (&read_lock);
          if (read != 3)
            break;
        }
      else
        {
          if (app->threads > 1)
            pthread_mutex_lock (&read_lock);
          size_t read = read_ull_numbers (app->infile, a, 3, &line, &len);
          if (app->threads > 1)
            pthread_mutex_unlock (&read_lock);
          if (read == -1)
            break;
        }

      if (n == a[1])
        {
          if (num_recs >= max_recs)
            recs = realloc (recs, sizeof (struct prec) * (num_recs + 1));
          recs[num_recs].lo = a[0];
          recs[num_recs].hi = a[2];
          num_recs++;
          if (num_recs > max_recs)
            {
              // we won't be hitting this with max_recs = 1000
              // but hey, no arbitrary limits.
              arr = realloc (arr, sizeof (int) * (num_recs));
              for (int i = max_recs; i < num_recs; i++)
                arr[i] = i;
              max_recs = num_recs;
            }
        }
      else
        {
          if (app->quick)
            {
              unsigned long long dist;
              long long int s[3][3];
              s[1][1] = n;
              for (int q = 0; q < num_recs; q++)
                {
                  dist = n - recs[q].lo;
                  quick_handle_progression (app, recs[q].lo, recs[q].hi, dist, &s);
                }
            }
          else
            {
              if (num_recs >= 2)
                handle_progressions (app, n, recs, num_recs, 2, arr);
            }
          num_recs = 1;
          n = a[1];
          recs[0].lo = a[0];
          recs[0].hi = a[2];
        }
    }
  if (app->quick)
    {
      unsigned long long dist;
      long long int s[3][3];
      s[1][1] = n;
      for (int q = 0; q < num_recs; q++)
        {
          dist = n - recs[q].lo;
          quick_handle_progression (app, recs[q].lo, recs[q].hi, dist, &s);
        }
    }
  else
    {
      if (num_recs >= 2)
        handle_progressions (app, n, recs, num_recs, 2, arr);
    }

  free (arr);
  free (recs);
  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_614 (struct fv_app_search_614_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_614_t *app = (struct fv_app_search_614_t *) state->input;
  switch (key)
    {
    case 'q':
      app->quick = 1;
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->threesq && !app->quick)
        app->threads = 1;
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in sets of three square progressions instead"},
  { "quick", 'q', 0, OPTION_HIDDEN, "Do an abridged search"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:14 given a FILE containing center values.\vWhen FILE is not provided, it is read from the standard input.  Center values must be perfect squares.  This program is limited to 64-bit integers.  Magic squares of type 6:14 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  | X |   | X |   | C |   | A |\n"
"  +---+---+---+   +---+---+---+\n"
"  |   | X | X |   |   | N | Z |  A,N,B and C,N,D are three square\n"
"  +---+---+---+   +---+---+---+  progressions.  Z is a square that shakes\n"
"  | X |   | X |   | B |   | D |  out.\n"
"  +---+---+---+   +---+---+---+\n"
"                  +---+---+---+\n"
"                  | D |   | A |\n"
"                  +---+---+---+\n"
"                  |   | N | Z |\n"
"                  +---+---+---+\n"
"                  | B |   | C |\n"
"                  +---+---+---+\n"
"This program checks every combination of 2 three square progressions.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_614_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_614 (&app);
}
