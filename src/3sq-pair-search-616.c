/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <unistd.h>
#include "magicsquareutil.h"
#include "linecount.h"
int linecount;

struct fv_app_3sq_pair_search_616_t
{
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
};

struct rec
{
  mpz_t ap[3];
};

static void
create_square (struct fv_app_3sq_pair_search_616_t *app, struct rec *rec, mpz_t *b, FILE *out)
{
  mpz_t s[3][3];
  mpz_t sum, p;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  mpz_inits (sum, p, NULL);

  mpz_set (s[2][1], rec->ap[0]);
  mpz_set (s[0][2], rec->ap[1]);
  mpz_set (s[1][0], rec->ap[2]);
  mpz_set (s[0][0], b[0]);
  mpz_set (s[2][2], b[1]);

  mpz_sub (p, s[2][2], s[0][0]);
  mpz_cdiv_q_ui (sum, p, 2);
  mpz_add (s[1][1], s[0][0], sum);

  mpz_add (sum, s[0][0], s[1][1]);
  mpz_add (sum, sum, s[2][2]);

  mpz_add (p, s[2][1], s[1][1]);
  mpz_sub (s[0][1], sum, p);

  mpz_add (p, s[1][0], s[1][1]);
  mpz_sub (s[1][2], sum, p);

  mpz_add (p, s[2][1], s[2][2]);
  mpz_sub (s[2][0], sum, p);

  app->display_square (s, out);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (sum, p, NULL);
}

static int
search_for_diff (struct fv_app_3sq_pair_search_616_t *app, mpz_t match, mpz_t *b, struct rec *rec, FILE *out)
{
  int found = 0;
  mpz_t i, iroot, j, jroot, diff;
  mpz_inits (i, iroot, j, jroot, diff, NULL);
  mpz_set_ui (i, 4);
  mpz_set_ui (iroot, 2);
  while (1)
    {
      if (mpz_cmp (i, match) > 0)
        break;

      mpz_add (diff, match, i);
      //char *buf = malloc (mpz_sizeinbase (diff, 10) + 2);
      //mpz_get_str (buf, 10, diff);
      //fprintf (stdout, "%s\n", buf);
      //free (buf);
      if (mpz_perfect_square_p (diff))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], diff);
          found = 1;
          create_square (app, rec, b, out);
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, jroot, diff, NULL);
  return found;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_3sq_pair_search_616_t *app = (struct fv_app_3sq_pair_search_616_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->infile)
        argp_error (state, "too many arguments");
      if (strcmp (arg, "-") == 0)
        app->infile = stdin;
      else
        app->infile = fopen (arg, "r");
      if (!app->infile)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      app->infile = stdin;
      break;
    }
  return 0;
}

void dump_square (void *data, mpz_t (*s)[3][3])
{
  struct fv_app_3sq_pair_search_616_t *app = 
    (struct fv_app_3sq_pair_search_616_t *) data;
  app->display_square (*s, app->out);
}

int
fituvalu_3sq_pair_search_616 (struct fv_app_3sq_pair_search_616_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  struct rec rec;
  mpz_t b[2], diff, s[3][3];
  mpz_inits (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], diff, NULL);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  while (1)
    {
      read = app->read_tuple (app->infile, rec.ap, &line, &len);
      if (read == -1)
        break;
      update_linecount (&linecount);
      mpz_sub (diff, rec.ap[2], rec.ap[0]);
      if (mpz_cmp_ui (diff, 0) > 0)
        search_616_3sq (rec.ap, diff, b, &s, 0, dump_square, app);
    }
  if (line)
    free (line);
  mpz_clears (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], diff, NULL);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Try to make a 3x3 magic square with 5 or more perfect squares by giving it a series of 3 square progressions in FILE.  The program iterates over pairs of squares to find two squares that have the correct difference.\vWhen FILE is `-' or is unspecified it is read from the standard input.  Each square is laid out like:\n\
  +------+------+------+\n\
  |  Y1  |      |  X2  |\n\
  +------+------+------+\n\
  |  X3  |      |      |\n\
  +------+------+------+\n\
  |      |  X1  |  Y2  |\n\
  +------+------+------+\nThe 616 in the name comes from the fact we're trying to make a square of the form 6:16, but it can also make 6:15, and 6:13.",
  0
};

int
main (int argc, char **argv)
{
  init_linecount (&linecount);
  struct fv_app_3sq_pair_search_616_t app;
  memset (&app, 0, sizeof (app));

  app.display_square = display_square_record;
  app.read_tuple = read_three_numbers_from_stream;
  app.infile = stdin;
  app.out = stdout;

  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_3sq_pair_search_616 (&app);
}
