/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <unistd.h>
#include "magicsquareutil.h"

struct fv_app_cycle_63_square_t
{
  FILE *infile;
  int numargs;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_square) (FILE *, mpz_t (*a)[3][3], char **, size_t *);
  FILE *out;
};


static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_cycle_63_square_t *app = (struct fv_app_cycle_63_square_t *) state->input;
  switch (key)
    {
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->numargs > 0)
        argp_error (state, "too many arguments");
      app->numargs++;
      app->infile = fopen (arg, "r");
      if (!app->infile)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static void
cycle_63 (struct fv_app_cycle_63_square_t *app, mpz_t (*a)[3][3], mpz_t (*b)[3][3], mpz_t sum)
{
  mpz_set ((*b)[0][0], (*a)[0][1]);
  mpz_set ((*b)[1][1], (*a)[1][1]);
  mpz_set ((*b)[2][2], (*a)[2][1]);

  mpz_set ((*b)[0][2], (*a)[0][2]);
  mpz_set ((*b)[2][0], (*a)[2][0]);

  mpz_sub ((*b)[0][1], sum, (*b)[0][0]);
  mpz_sub ((*b)[0][1], (*b)[0][1], (*b)[0][2]);

  mpz_sub ((*b)[1][0], sum, (*b)[0][0]);
  mpz_sub ((*b)[1][0], (*b)[1][0], (*b)[2][0]);

  mpz_sub ((*b)[1][2], sum, (*b)[0][2]);
  mpz_sub ((*b)[1][2], (*b)[1][2], (*b)[2][2]);

  mpz_sub ((*b)[2][1], sum, (*b)[2][0]);
  mpz_sub ((*b)[2][1], (*b)[2][1], (*b)[2][2]);
  app->display_square (*b, app->out);

  mpz_set ((*b)[0][0], (*a)[2][1]);
  mpz_set ((*b)[1][1], (*a)[1][1]);
  mpz_set ((*b)[2][2], (*a)[0][1]);

  mpz_set ((*b)[0][2], (*a)[2][2]);
  mpz_set ((*b)[2][0], (*a)[0][0]);

  mpz_sub ((*b)[0][1], sum, (*b)[0][0]);
  mpz_sub ((*b)[0][1], (*b)[0][1], (*b)[0][2]);

  mpz_sub ((*b)[1][0], sum, (*b)[0][0]);
  mpz_sub ((*b)[1][0], (*b)[1][0], (*b)[2][0]);

  mpz_sub ((*b)[1][2], sum, (*b)[0][2]);
  mpz_sub ((*b)[1][2], (*b)[1][2], (*b)[2][2]);

  mpz_sub ((*b)[2][1], sum, (*b)[2][0]);
  mpz_sub ((*b)[2][1], (*b)[2][1], (*b)[2][2]);
  app->display_square (*b, app->out);
}

int
fituvalu_cycle_63_square (struct fv_app_cycle_63_square_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3][3];
  mpz_t b[3][3];
  mpz_t sum;
  mpz_init (sum);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_inits (a[i][j], b[i][j], NULL);

  while (1)
    {
      read = app->read_square (app->infile, &a, &line, &len);
      if (read == -1)
        break;
      if (is_magic_square (a, 1))
        {
          if (!mpz_perfect_square_p (a[0][1]))
            rotate_square (&a);
          mpz_add (sum, a[0][0], a[1][1]);
          mpz_add (sum, sum, a[2][2]);

          cycle_63 (app, &a, &b, sum);
          cycle_63 (app, &b, &a, sum);
        }
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clears (a[i][j], b[i][j], NULL);
  mpz_clear (sum);
  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Cycle the center column to the main diagonal to make a new magic square.\vThis program is only suitable for magic squares of type 6:3.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_cycle_63_square_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.read_square =  read_square_from_stream;
  app.infile = stdin;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  is_magic_square_init ();
  return fituvalu_cycle_63_square (&app);
}
