/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <argp.h>
#include <gmp.h>
#include <string.h>
#include "magicsquareutil.h"
struct fv_app_mine_pairs_t
{
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  void (*display_pair) (mpz_t *, mpz_t *, FILE *);
  void (*display_coords_and_pair) (mpz_t *, FILE *);
  int allow_negatives;
  int coords;
  int divide;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_mine_pairs_t *app = (struct fv_app_mine_pairs_t *) state->input;
  int count;
  switch (key)
    {
    case 'd':
      app->divide = 1;
      break;
    case 'c':
      app->coords = 1;
      break;
    case 'a':
      app->allow_negatives = 1;
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_pair = display_binary_two_record;
      app->display_coords_and_pair = display_binary_six_record;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

void
dump_pairs (struct fv_app_mine_pairs_t *app, mpz_t *a, FILE *out)
{
  struct mpz_pair *pairs = NULL;
  int num_pairs = 0;
  for (int i = 0; i < SIZE; i++)
    {
      if (!app->allow_negatives && mpz_sgn (a[i]) == -1)
        continue;
      for (int j = 0; j < SIZE; j++)
        {
          if (i == j)
            continue;
          if (mpz_cmp (a[i], a[j]) > 0)
            continue;
          if (!app->allow_negatives && mpz_sgn (a[j]) < 0)
            continue;
          int found = 0;
          for (int k = 0; k < num_pairs; k++)
            {
              if (mpz_cmp (pairs[k].num[0], a[i]) == 0 &&
                  mpz_cmp (pairs[k].num[1], a[j]) == 0)
                {
                  found = 1;
                  break;
                }
            }
          if (!found)
            {
              pairs = realloc (pairs,
                                sizeof (struct mpz_pair) * ((num_pairs) + 1));
              mpz_init_set (pairs[num_pairs].num[0], a[i]);
              mpz_init_set (pairs[num_pairs].num[1], a[j]);
              num_pairs++;
              if (app->coords)
                {
                  if (app->display_coords_and_pair == display_binary_six_record)
                    {
                      mpz_t b[6];
                      mpz_inits (b[0], b[1], b[2], b[3], b[4], b[5], NULL);
                      mpz_set_ui (b[0], i / (int)sqrt (SIZE));
                      mpz_set_ui (b[1], i % (int)sqrt (SIZE));
                      mpz_set_ui (b[2], j / (int)sqrt (SIZE));
                      mpz_set_ui (b[3], j % (int)sqrt (SIZE));
                      mpz_set (b[4], a[i]);
                      mpz_set (b[5], a[j]);
                      app->display_coords_and_pair (b, out);
                      mpz_clears (b[0], b[1], b[2], b[3], b[4], b[5], NULL);
                    }
                  else
                    {
                      fprintf (out, "%d,%d, %d,%d, ",
                               i / (int)sqrt (SIZE), i % (int)sqrt (SIZE),
                               j / (int)sqrt (SIZE), j % (int)sqrt (SIZE));
                      if (app->divide)
                        {
                          mpf_t n, d, res;
                          mpf_inits (n, d, res, NULL);
                          mpf_set_z (n, a[i]);
                          mpf_set_z (d, a[j]);
                          mpf_div (res, n, d);
                          display_floating_point_number (res, out);
                          fprintf (out, ", ");
                          mpf_clears (n, d, res, NULL);
                        }
                      display_two_record (&a[i], &a[j], out);
                    }
                }
              else
                app->display_pair (&a[i], &a[j], out);
            }
        }
    }

  for (int i = 0; i < num_pairs; i++)
    mpz_clears (pairs[i].num[0], pairs[i].num[1], NULL);
  free (pairs);
}

int
fituvalu_mine_pairs (struct fv_app_mine_pairs_t *app,  FILE *in, FILE *out)
{
  int i, j;
  mpz_t a[3][3];
  ssize_t read;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  mpz_t b[SIZE];
  for (i = 0; i < SIZE; i++)
    mpz_init (b[i]);

  char* line = NULL;
  size_t len = 0;
  while (!feof (in))
    {
      read = app->read_square (in, &a, &line, &len);
      if (read == -1)
        break;

      int count = 0;
      for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
          {
            mpz_set (b[count], a[i][j]);
            count++;
          }

      dump_pairs (app, b, out);

    }
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
  for (i = 0; i < SIZE; i++)
    mpz_clear (b[i]);
  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "allow-negative", 'a', 0, 0, "Allow negative numbers"},
  { "coords", 'c', 0, 0, "Also show the coorindates of the pair"},
  { "divide", 'd', 0, OPTION_HIDDEN, ""},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares from the standard input, and show the pairs of numbers.\vThe nine values must be separated by commas and terminated by a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_mine_pairs_t app;
  memset (&app, 0, sizeof (app));
  app.read_square = read_square_from_stream;
  app.display_coords_and_pair = display_six_record;
  app.display_pair = display_two_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_mine_pairs (&app, stdin, stdout);
}
