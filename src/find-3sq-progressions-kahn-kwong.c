/* Copyright (C) 2017, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

struct fv_app_find_3sq_progressions_kk_t
{
  int num_args;
  int showroot;
  int show_diff;
  mpz_t max;
  int from_stdin;
  int high;
  void (*display_record) (mpz_t *, mpz_t*, FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
  mpz_t n1, n2, n3;
};

//from http://www.fq.math.ca/Papers1/43-2/paper43-2-1.pdf
//  A1 =                   B1 = 3b - 2a          C1 = 4b - 3a
//  A2 = 4b - 3a           B2 = 9b - 6a - 2c     C2 = 12b - 8a - 3c
//  A3 = 12b - 8a - 3c     B3 = 19b - 12a - 6c   C3 = 24b - 15a - 8c
//  A4 = 24b - 15a - 8c    B4 = 33b - 20a - 12c  C4 = 40b - 24a - 15c

static void
generate_progression (struct fv_app_find_3sq_progressions_kk_t *app, mpz_t *progression, int sign1, int sign2, int sign3, int sign4)
{
  mpz_t threea, threeb, twoa, fourb, r1, r2, r3, a, b;
  mpz_inits (threea, threeb, twoa, fourb, a, b, NULL);
  mpz_inits (r1, r2, r3, NULL);
  mpz_sqrt (r1, progression[0]);
  mpz_sqrt (r2, progression[1]);
  mpz_sqrt (r3, progression[2]);
  if (sign1 > 0)
    mpz_abs (b, r2);
  else
    mpz_neg (b, r2);
  mpz_mul_ui (threeb, b, 3);
  if (sign2 > 0)
    mpz_abs (a, r1);
  else
    mpz_neg (a, r1);
  mpz_mul_ui (twoa, a, 2);
  if (sign3 > 0)
    mpz_abs (b, r2);
  else
    mpz_neg (b, r2);
  mpz_mul_ui (fourb, b, 4);
  if (sign4 > 0)
    mpz_abs (a, r1);
  else
    mpz_neg (a, r1);
  mpz_mul_ui (threea, a, 3);
  mpz_set (progression[0], r3);
  mpz_sub (progression[1], threeb, twoa);
  mpz_sub (progression[2], fourb, threea);
  mpz_mul (progression[0], progression[0], progression[0]);
  mpz_mul (progression[1], progression[1], progression[1]);
  mpz_mul (progression[2], progression[2], progression[2]);
  mpz_set (app->n1, progression[0]);
  mpz_set (app->n2, progression[1]);
  mpz_set (app->n3, progression[2]);
  mpz_clears (threea, threeb, twoa, fourb, a, b, NULL);
  mpz_clears (r1, r2, r3, NULL);
}

static void
generate_progression_and_show (struct fv_app_find_3sq_progressions_kk_t *app, mpz_t *progression, FILE *out)
{
  mpz_t root, diff;
  mpz_inits (root, diff, NULL);
  generate_progression_kahn_kwong (progression, app->high);
  if (app->show_diff)
    {
      mpz_sub (diff, progression[1], progression[0]);
      if (app->display_record == display_binary_three_record_with_root)
        mpz_out_raw (out, diff);
      else
        {
          display_textual_number_no_newline (diff, out);
          fprintf (out, ", ");
        }
    }
  if (app->showroot)
    mpz_sqrt (root, progression[2]);
  app->display_record (progression, &root, out);

  mpz_clears (root, diff, NULL);
}

int
fituvalu_find_3sq_progression_kk (struct fv_app_find_3sq_progressions_kk_t *app, FILE *in, FILE *out)
{
  mpz_t progression[3], i;
  mpz_inits (progression[0], progression[1], progression[2], i, NULL);
  if (app->from_stdin)
    {
      char *line = NULL;
      size_t len = 0;
      ssize_t read;

      while (1)
        {
          read = app->read_tuple (in, progression, &line, &len);
          if (read == -1)
            break;
          mpz_set_ui (i, 0);
          for (; mpz_cmp (i, app->max) < 0; mpz_add_ui (i, i, 1))
            generate_progression_and_show (app, progression, out);
        }

      if (line)
        free (line);
    }
  else
    {
      mpz_set (progression[0], app->n1);
      mpz_set (progression[1], app->n2);
      mpz_set (progression[2], app->n3);
          
      mpz_set_ui (i, 0);
      for (; mpz_cmp (i, app->max) < 0; mpz_add_ui (i, i, 1))
        generate_progression_and_show (app, progression, out);
    }
  mpz_clears (progression[0], progression[1], progression[2], i, NULL);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_find_3sq_progressions_kk_t *app =
    (struct fv_app_find_3sq_progressions_kk_t *) state->input;
  switch (key)
    {
    case 'h':
      app->high = 1;
      break;
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'd':
      app->show_diff = 1;
      break;
    case 'n':
      app->showroot = 0;
      break;
    case 'o':
      app->display_record = display_binary_three_record_with_root;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 4)
        argp_error (state, "too many arguments");
      else
        {
          switch (app->num_args)
            {
            case 0:
              mpz_set_str (app->n1, arg, 10);
              break;
            case 1:
              mpz_set_str (app->n2, arg, 10);
              break;
            case 2:
              mpz_set_str (app->n3, arg, 10);
              break;
            case 3:
              mpz_set_str (app->max, arg, 10);
              break;
            }
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      mpz_inits (app->n1, app->n2, app->n3, app->max, NULL);
      mpz_set_ui (app->max, 1);
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args == 1)
        {
          mpz_set (app->max, app->n1);
          app->from_stdin = 1;
        }
      else if (app->num_args == 0)
        app->from_stdin = 1;
      else if (app->num_args < 3)
        argp_error (state, "too few arguments");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "no-root", 'n', 0, 0, "Don't show the root of the fourth number"},
  { "show-diff", 'd', 0, 0, "Also show the diff"},
  { "high", 'h', 0, 0, "Use an alternative higher value sequence"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "N1 N2 N3 [NUM]\n[NUM]",
  "Find an arithmetic progression consisting of three squares.\vN1..N3 is an existing primative arithmetic progression of squares.  Iterate the loop NUM times.  If N1..N3 are not provided, they are read from the standard input.  \"kahn-kwong\" refers to M. A. Khan, and Harris Kwong, and their paper 'Arithmetic Progressions With Square Entries'.  Try arguments 1 25 49.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_find_3sq_progressions_kk_t app;
  memset (&app, 0, sizeof (app));
  app.showroot = 1;
  app.display_record = display_three_record_with_root;
  app.read_tuple = read_three_numbers_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_find_3sq_progression_kk (&app, stdin, stdout);
}
