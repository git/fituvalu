/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "magicsquareutil.h"

struct fv_app_fourn1_t
{
  int invert;
  int num_args;
  FILE *infile;
  FILE *out;
  int (*read_number) (FILE *, mpz_t *, char **, size_t *);
  void (*write_number)(mpz_t *i, FILE *out);
};

int
fituvalu_fourn1 (struct fv_app_fourn1_t *app)
{
  char *line = NULL;
  size_t len = 0;

  mpz_t a;
  mpz_init (a);
  while (1)
    {
      ssize_t read = app->read_number (app->infile, &a, &line, &len);
      if (read == -1)
        break;
      if (app->invert)
        {
          if (mpz_perfect_square_p (a))
            {
              mpz_t r, i;
              mpz_inits (r, i, NULL);
              mpz_sqrt (r, a);
              mpz_sub_ui (i, r, 1);
              int ret = mpz_mod_ui (r, i, 4);
              if (ret == 0)
                {
                  mpz_cdiv_q_ui (r, i, 4);
                  app->write_number (&r, app->out);
                }
              mpz_clears (r, i, NULL);
            }
        }
      else
        {
          mpz_mul_ui (a, a, 4);
          mpz_add_ui (a, a, 1);
          mpz_mul (a, a, a);
          app->write_number (&a, app->out);
        }
    }

  mpz_clear (a);
  free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_fourn1_t *app = (struct fv_app_fourn1_t *) state->input;
  switch (key)
    {
    case 'v':
      app->invert = 1;
      break;
    case 'i':
      app->read_number = binary_read_one_number_from_stream;
      break;
    case 'o':
      app->write_number= display_binary_number;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 0)
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open %s for reading (%m)", arg);
          app->num_args++;
        }
      else
        argp_error (state, "too many arguments");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      break;
    }

  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "invert", 'v', 0, 0, "Show the n in a (4n+1)^2 number"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Read in numbers from FILE and multiply by four, add one, and square it.\vWhen FILE is not specified it is read from the standard input.  This program generates suitable center values for the following types: 6:1, 6:6, 6:7, 6:8, 6:10, 6:11, and 6:14.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_fourn1_t app;
  memset (&app, 0, sizeof (app));
  app.infile = stdin;
  app.out = stdout;
  app.read_number= read_one_number_from_stream;
  app.write_number = display_textual_number;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_fourn1 (&app);
}
