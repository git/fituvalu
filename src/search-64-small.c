/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_64_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  int chance_of_seven;
  int threesq;
  int quick;
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_64_type_1 (struct fv_app_search_64_t *app, unsigned long long lo, unsigned long long hi, unsigned long long distance, unsigned long long sum, unsigned long long sum_minus_middle, long long (*s)[3][3])
{
  unsigned long long i, j, iroot, twoiroot;
  iroot = sqrtl (distance);
  iroot++;
  i = iroot * iroot;

  //iterate from sum downwards, checking downwards for another square
  //we start at sum because we want at most two negative numbers
  //three negatives means we can't find a seven
  //well, we're still hitting negatives, so we do an extra check
  //can we pick a better starting point for i to avoid this?
  while (1)
    {
      j = i - distance;

      if (small_is_square (j))
        {
          (*s)[1][0] = lo;
          (*s)[1][2] = hi;

          (*s)[2][0] = i;
          (*s)[2][2] = j;

          (*s)[2][1] = sum - (*s)[2][0];
          (*s)[2][1] = (*s)[2][1] - (*s)[2][2];

          (*s)[0][1] = sum_minus_middle - (*s)[2][1];
          if (small_is_square ((*s)[0][1]))
            {
              (*s)[0][0] = sum_minus_middle - (*s)[2][2];
              (*s)[0][2] = sum_minus_middle - (*s)[2][0];

              int dup = 0;
              if ((*s)[1][1] == (*s)[0][0] || (*s)[1][1] == (*s)[0][2] ||
                  (app->chance_of_seven &&
                   (*s)[0][0] < 0 && (*s)[0][2] < 0 && (*s)[2][1] < 0))
                dup = 1;

              if (!dup)
                {
                  pthread_mutex_lock (&display_lock);
                  fprintf (app->out,
                           "%llu, %llu, %lld, "
                           "%llu, %llu, %llu, "
                           "%llu, %lld, %llu, \n",
                           (*s)[0][0], (*s)[0][1], (*s)[0][2],
                           (*s)[1][0], (*s)[1][1], (*s)[1][2],
                           (*s)[2][0], (*s)[2][1], (*s)[2][2]);
                  fflush (app->out);
                  pthread_mutex_unlock (&display_lock);
                }
            }
        }

      if (i > sum)
        break;

      twoiroot = iroot * 2;
      i = i + twoiroot;
      i++;
      iroot++;
    }
}

static void
quick_generate_64_type_1 (struct fv_app_search_64_t *app, unsigned long long lo, unsigned long long hi, unsigned long long distance, unsigned long long sum, unsigned long long sum_minus_middle, long long (*s)[3][3])
{
  unsigned long long i, j, iroot, twoiroot, limit;
  iroot = sqrtl (distance);
  iroot++;
  i = iroot * iroot;

  limit = sum / 2;
  //iterate from sum downwards, checking downwards for another square
  //we start at sum because we want at most two negative numbers
  //three negatives means we can't find a seven
  //well, we're still hitting negatives, so we do an extra check
  //can we pick a better starting point for i to avoid this?
  while (1)
    {
      j = i - distance;

      if (small_is_square (j))
        {
          (*s)[1][0] = lo;
          (*s)[1][2] = hi;

          (*s)[2][0] = i;
          (*s)[2][2] = j;

          (*s)[2][1] = sum - (*s)[2][0];
          (*s)[2][1] = (*s)[2][1] - (*s)[2][2];

          (*s)[0][1] = sum_minus_middle - (*s)[2][1];
          if (small_is_square ((*s)[0][1]))
            {
              (*s)[0][0] = sum_minus_middle - (*s)[2][2];
              (*s)[0][2] = sum_minus_middle - (*s)[2][0];

              int dup = 0;
              if ((*s)[1][1] == (*s)[0][0] || (*s)[1][1] == (*s)[0][2] ||
                  (app->chance_of_seven &&
                   (*s)[0][0] < 0 && (*s)[0][2] < 0 && (*s)[2][1] < 0))
                dup = 1;

              if (!dup)
                {
                  pthread_mutex_lock (&display_lock);
                  fprintf (app->out,
                           "%llu, %llu, %lld, "
                           "%llu, %llu, %llu, "
                           "%llu, %lld, %llu, \n",
                           (*s)[0][0], (*s)[0][1], (*s)[0][2],
                           (*s)[1][0], (*s)[1][1], (*s)[1][2],
                           (*s)[2][0], (*s)[2][1], (*s)[2][2]);
                  fflush (app->out);
                  pthread_mutex_unlock (&display_lock);
                }
            }
        }

      if (i > limit)
        break;

      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
}

static void
handle_progression (struct fv_app_search_64_t *app, unsigned long long lo, unsigned long long hi, unsigned long long distance, unsigned long long sum, unsigned long long sum_minus_middle, long long (*s)[3][3])
{
  if (app->quick)
    quick_generate_64_type_1 (app, lo, hi, distance, sum, sum_minus_middle, s);
  else
    generate_64_type_1 (app, lo, hi, distance, sum, sum_minus_middle, s);
}

static void
generate_progressions (struct fv_app_search_64_t *app, unsigned long long n, long long (*s)[3][3])
{
  //int ld = n % 10;
  //if (ld != 1 && ld != 5 && ld != 9)
    //return;

  unsigned long long i, iroot, diff, limit, nn, mn, twomn, lo, hi, sum, sum_minus_middle, twoiroot;

  limit = n / 2;
  i = 1;
  iroot = 1;
  sum = n * 3;
  sum_minus_middle = n * 2;
  while (1)
    {
      if (i > limit)
        break;

      diff = n - i;

      if (small_is_square (diff))
        {
          nn = sqrtl (diff);
          mn = iroot * nn;
          twomn = mn * 2;
          lo = n - twomn;
          hi = n + twomn;
      
          if (twomn > 0)
            handle_progression (app, lo, hi, twomn, sum, sum_minus_middle, s);
        }

      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
  return;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_64_t *app =
    (struct fv_app_search_64_t *) param->data;

  long long int s[3][3];

  unsigned long long int a[3];
  unsigned long long int b[3];

  unsigned long long distance, sum, sum_minus_middle;

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          if (fread (a, sizeof (unsigned long long), 3, app->infile) != 3)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          size_t read = read_ull_numbers (app->infile, a, 3, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      b[0] = a[0];
      b[1] = a[1];
      b[2] = a[2];
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on 3sq
        {
          s[1][1] = b[1];
          distance = b[1] - b[0];
          sum = b[1] * 3;
          sum_minus_middle = b[1] * 2;
          if (b[1] > b[0])
            handle_progression (app, b[0], b[2],
                                distance, sum, sum_minus_middle, &s);
        }
    }


  if (line)
    free (line);
  return NULL;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_64_t *app =
    (struct fv_app_search_64_t *) param->data;

  long long s[3][3];

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          size_t read = fread (&n, sizeof (unsigned long long), 1, app->infile);
          if (read != 1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          num = strtoull (line, &end, 10);
        }
      n = num;

      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
      if (n > 1)
        {
          s[1][1] = n;
          generate_progressions (app, n, &s);
        }
    }

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_64 (struct fv_app_search_64_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *end = NULL;
  struct fv_app_search_64_t *app = (struct fv_app_search_64_t *) state->input;
  switch (key)
    {
    case 'q':
      app->quick = 1;
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'N':
      app->chance_of_seven = 0;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "allow-three-negatives", 'N', 0, OPTION_HIDDEN, "Allow squares with three negative numbers"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "quick", 'q', 0, OPTION_HIDDEN, "Do an abridged search"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:4 given a FILE containing center values.\vWhen FILE is not provided, it is read from the standard input.  Center values must be perfect squares.  This program generates magic squares with at most two numbers being negative.  This program is limited to 64-bit integers.  Magic squares of type 6:4 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   | X |   |   |   | Z |   |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating E\n"
"  | X | X | X |   | A | B | C |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below E.\n"
"  | X |   | X |   | E |   | D |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_64_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.chance_of_seven = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_64 (&app);
}
