/* Copyright (C) 2016, 2017, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

enum mode
{
  MODE_EQUAL,
  MODE_GT,
  MODE_GE,
  MODE_LT,
  MODE_LE,
};

enum {
  OPT_GT = -31,
  OPT_GE,
  OPT_LT,
  OPT_LE,
};

struct filter
{
  mpz_t num;
  enum mode mode;
};

struct histogram_rec_t
{
  int magnitude;
  int count;
};

struct fv_app_display_magic_square_t
{
  int show_histogram;
  struct histogram_rec_t *histogram_recs;
  int num_histogram_recs;
  struct filter *filters;
  int num_filters;
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int invert;
};

static void
calculate_histogram (struct fv_app_display_magic_square_t *app, mpz_t sum)
{
  int magnitude = mpz_sizeinbase (sum, 10);
  int found = 0;
  for (int i = 0; i < app->num_histogram_recs; i++)
    {
      if (app->histogram_recs[i].magnitude == magnitude)
        {
          found = 1;
          app->histogram_recs[i].count++;
          break;
        }
    }
  if (!found)
    {
      app->histogram_recs = realloc (app->histogram_recs, sizeof (struct histogram_rec_t) *
                                     (app->num_histogram_recs + 1));
      app->histogram_recs[app->num_histogram_recs].magnitude = magnitude;
      app->histogram_recs[app->num_histogram_recs].count = 1;
      app->num_histogram_recs++;

    }
}

static void
display_histogram (struct fv_app_display_magic_square_t *app, FILE *out)
{
  int max_magnitude = 0;
  for (int i = 0; i < app->num_histogram_recs; i++)
    if (app->histogram_recs[i].magnitude > max_magnitude)
      max_magnitude = app->histogram_recs[i].magnitude;
  if (max_magnitude == 0)
    return;

  for (int i = 1; i <= max_magnitude; i++)
    {
      int found = 0;
      int idx = 0;
      for (int j = 0; j < app->num_histogram_recs; j++)
        {
          if (app->histogram_recs[j].magnitude == i)
            {
              found = 1;
              idx = j;
            }
        }
      if (found)
        fprintf (out, "10^%d, %d\n", i, app->histogram_recs[idx].count);
      else
        fprintf (out, "10^%d, %d\n", i, 0);
    }
}

int
fituvalu_display_magic_number (struct fv_app_display_magic_square_t *app, FILE *stream, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3][3];
  mpz_t sum;
  mpz_init (sum);

  int i, j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  while (1)
    {
      read = app->read_square (stream, &a, &line, &len);
      if (read == -1)
        break;
      if (app->num_filters)
        {
          int match = 0;
          magic_square_get_magic_number (a, &sum);
          for (i = 0; i < app->num_filters; i++)
            {
              switch (app->filters[i].mode)
                {
                case MODE_EQUAL:
                  if (mpz_cmp (sum, app->filters[i].num) == 0)
                    match++;
                  break;
                case MODE_GT:
                  if (mpz_cmp (sum, app->filters[i].num) > 0)
                    match++;
                  break;
                case MODE_GE:
                  if (mpz_cmp (sum, app->filters[i].num) >= 0)
                    match++;
                  break;
                case MODE_LT:
                  if (mpz_cmp (sum, app->filters[i].num) < 0)
                    match++;
                  break;
                case MODE_LE:
                  if (mpz_cmp (sum, app->filters[i].num) <= 0)
                    match++;
                  break;
                }
            }
          int show = 0;
          if (match > 0)
            show = 1;
          if (app->invert)
            show = !show;
          if (show)
            {
              if (app->show_histogram)
                calculate_histogram (app, sum);
              else
                app->display_square (a, out);
            }
        }
      else
        {
          magic_square_get_magic_number (a, &sum);
          if (app->show_histogram)
            calculate_histogram (app, sum);
          else
            display_textual_number (&sum, out);
        }
    }

  if (app->show_histogram)
    display_histogram (app, out);

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);

  mpz_clear (sum);

  if (line)
    free (line);
  return 0;
}

static void
add_filter (struct fv_app_display_magic_square_t *app, char *arg, enum mode m)
{
  struct filter f;

  app->filters = realloc (app->filters,
                          sizeof (struct filter) * (app->num_filters + 1));
  mpz_init_set_str (app->filters[app->num_filters].num, arg, 10);
  app->filters[app->num_filters].mode = m;
  app->num_filters++;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_display_magic_square_t *app = (struct fv_app_display_magic_square_t *) state->input;
  switch (key)
    {
    case 'h':
      app->show_histogram = 1;
      break;
    case 'v':
      app->invert = 1;
      break;
    case OPT_LT:
      add_filter (app, arg, MODE_LT);
      break;
    case OPT_LE:
      add_filter (app, arg, MODE_LE);
      break;
    case OPT_GT:
      add_filter (app, arg, MODE_GT);
      break;
    case OPT_GE:
      add_filter (app, arg, MODE_GE);
      break;
    case 'f':
      add_filter (app, arg, MODE_EQUAL);
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "invert", 'v', 0, OPTION_HIDDEN, "Invert the filter"},
  { "gt", OPT_GT, "NUM", OPTION_HIDDEN, "Only show squares with magic numbers greater than NUM"},
  { "lt", OPT_LT, "NUM", OPTION_HIDDEN, "Only show squares with magic numbers less than NUM"},
  { "ge", OPT_GE, "NUM", OPTION_HIDDEN, "Only show squares with magic number NUM or more"},
  { "gte", OPT_GE, "NUM", OPTION_HIDDEN | OPTION_ALIAS, "Only show squares with magic number NUM or more"},
  { "le", OPT_LE, "NUM", OPTION_HIDDEN, "Only show squares with magic number NUM or less"},
  { "lte", OPT_LE, "NUM", OPTION_HIDDEN | OPTION_ALIAS, "Only show squares with magic number NUM or less"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "filter", 'f', "NUM", 0, "Only show squares with magic number NUM"},
  { "histogram", 'h', 0, 0, "Show a histogram of magic number magnitudes"},
  { 0 },
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares from the standard input, and display the magic number.\vThe nine values must be separated by a comma and terminated by a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_display_magic_square_t app;
  memset (&app, 0, sizeof (app));
  app.read_square = read_square_from_stream;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_display_magic_number (&app, stdin, stdout);
}
