/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

struct fv_app_mine_near_progressions_t
{
  void (*display_tuple) (mpz_t s[3], FILE *out);
  int (*read_numbers)(FILE *, mpz_t *, int size, char **, size_t *);
  FILE *out;
  int start;
  int middle;
  int end;
};

int
compar (const void *left, const void *right)
{
  const mpz_t *l = left;
  const mpz_t *r = right;
  return mpz_cmp (*l, *r);
}

//http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
void combinationUtil (mpz_t arr[], mpz_t data[], int start, int end,
                      int index, int r, struct fv_app_mine_near_progressions_t *app);

void printCombination (mpz_t arr[], int n, int r, struct fv_app_mine_near_progressions_t *app)
{
  mpz_t data[r];
  for (int i = 0; i < r; i++)
    mpz_init (data[i]);
  combinationUtil (arr, data, 0, n-1, 0, r, app);
  for (int i = 0; i < r; i++)
    mpz_clear (data[i]);
}
 
void combinationUtil (mpz_t arr[], mpz_t data[], int start, int end,
                      int index, int r, struct fv_app_mine_near_progressions_t *app)
{
  if (index == r)
    {
      mpz_t diff1, diff2;
      mpz_inits (diff1, diff2, NULL);
      mpz_sub (diff1, data[1], data[0]);
      mpz_sub (diff2, data[2], data[1]);
      int match = mpz_cmp (diff1, diff2) == 0;
      mpz_clears (diff1, diff2, NULL);
      if (!match)
        return;

      int show = 0;
      if (app->middle &&
          mpz_perfect_square_p (data[0]) &&
          !mpz_perfect_square_p (data[1]) &&
          mpz_perfect_square_p (data[2]))
        show = 1;
      else if (app->start &&
          !mpz_perfect_square_p (data[0]) &&
          mpz_perfect_square_p (data[1]) &&
          mpz_perfect_square_p (data[2]))
        show = 1;
      else if (app->end &&
          mpz_perfect_square_p (data[0]) &&
          mpz_perfect_square_p (data[1]) &&
          !mpz_perfect_square_p (data[2]))
        show = 1;
      if (show)
        {
          for (int j = 0; j < r; j++)
            {
              display_textual_number_no_newline (data[j], app->out);
              fprintf (app->out, ", ");
            }
          fprintf (app->out, "\n");
        }
      return;
    }

  for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
      mpz_set (data[index], arr[i]);
      combinationUtil(arr, data, i + 1, end, index + 1, r, app);
    }
}

static void
mine_progression (mpz_t vec[], int size, struct fv_app_mine_near_progressions_t *app)
{
  int count = 0;
  mpz_t progression[size];

  for (int i = 0; i < size; i++)
    {
      mpz_init_set (progression[count], vec[i]);
      count++;
    }
  if (count < 2)
    {
      for (int i = 0; i < count; i++)
        mpz_clear (progression[i]);
      return;
    }

  qsort (progression, count, sizeof (mpz_t), compar);

  printCombination (progression, count, 3, app);

  for (int i = 0; i < count; i++)
    mpz_clear (progression[i]);
  return;
}
int
fituvalu_mine_near_progressions (struct fv_app_mine_near_progressions_t *app, FILE *in, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  mpz_t vec[SIZE];
  mpz_t orig[SIZE];
  for (int i = 0; i < SIZE; i++)
    mpz_inits (vec[i], orig[i], NULL);
  while (1)
    {
      read = app->read_numbers (in, vec, SIZE, &line, &len);
      if (read == -1)
        break;
      mine_progression (vec, SIZE, app);
    }

  for (int i = 0; i < SIZE; i++)
    mpz_clears (vec[i], orig[i], NULL);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_mine_near_progressions_t *app = (struct fv_app_mine_near_progressions_t *) state->input;
  switch (key)
    {
    case '1':
      app->start = 1;
      break;
    case '2':
      app->middle = 1;
      break;
    case '3':
      app->end = 1;
      break;
    case 'i':
      app->read_numbers = binary_read_numbers_from_stream;
      break;
    case ARGP_KEY_FINI:
      if (!app->start && !app->middle && !app->end)
        {
          app->start = 1;
          app->middle = 1;
          app->end = 1;
        }
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "start", '1', 0, 0, "Show near 3 square progressions with 1st number not square"},
  { "middle", '2', 0, 0, "Show near 3 square progressions with 2nd number not square"},
  { "end", '3', 0, 0, "Show near 3 square progressions with 3rd number not square"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares and output the arithmetic progressions containing two perfect squares.\vThe 3x3 magic squares must be provided in the form of 9 numbers separated by a comma and terminated by a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_mine_near_progressions_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.read_numbers = read_numbers_from_stream;
  app.display_tuple = display_three_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_mine_near_progressions (&app, stdin, stdout);
}
