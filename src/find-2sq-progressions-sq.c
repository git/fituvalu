/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct fv_app_find_2sq_progressions_sq_t
{
  mpz_t sq;
  void (*display_record) (mpz_t *, mpz_t *, FILE *);
  int (*read_number) (FILE *, mpz_t *, char **, size_t *);
};

static void
generate_progression (struct fv_app_find_2sq_progressions_sq_t *app, mpz_t *progression, mpz_t mtwo, mpz_t ntwo)
{
  mpz_t mn, twomn, m, n;
  mpz_inits (mn, twomn, m, n, NULL);
  mpz_sqrt (m, mtwo);
  mpz_sqrt (n, ntwo);
  mpz_mul (mn, m, n);
  mpz_mul_ui (twomn, mn, 2);
  mpz_sub (progression[0], progression[1], twomn);
  mpz_add (progression[2], progression[1], twomn);
  mpz_clears (mn, twomn, m, n, NULL);
}

static int
create_square_pair (struct fv_app_find_2sq_progressions_sq_t *app, mpz_t sq, mpz_t *progression, FILE *out)
{
  int found = 0;
  mpz_t i, iroot, diff, limit, root, gcd;
  mpz_inits (i, iroot, diff, limit, root, gcd, NULL);
  mpz_sqrt (root, sq);
  mpz_mul (sq, root, root);
  mpz_set (progression[1], sq);
  mpz_cdiv_q_ui (limit, progression[1], 2);
  mpz_set_ui (i, 1);
  while (1)
    {
      if (mpz_cmp (i, limit) > 0)
        break;

      mpz_add (diff, progression[1], i);
      if (mpz_perfect_square_p (diff))
        {
          generate_progression (app, progression, i, diff);
          mpz_add (diff, progression[2], i);
          mpz_gcd (gcd, progression[1], progression[2]);
          mpz_gcd (gcd, gcd, diff);
          if (mpz_cmp_ui (gcd, 1) == 0)
            {
              found = 1;
              app->display_record (&progression[1], &progression[2], out);
            }
          mpz_gcd (gcd, progression[0], progression[1]);
          if (mpz_cmp_ui (gcd, 1) == 0)
            {
              found = 1;
              if (mpz_cmp_ui (progression[0], 0) >= 0)
                app->display_record (&progression[0], &progression[1], out);
            }
        }
      if (mpz_cmp_ui (i, 1) == 0)
        {
          mpz_set_ui (i, 4);
          mpz_set_ui (iroot, 2);
          continue;
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, diff, limit, root, gcd, NULL);
  return found;
}

static int
gen_2sq (struct fv_app_find_2sq_progressions_sq_t *app, FILE *in, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t sq, vec[3];
  mpz_inits (vec[0], vec[1], vec[2], sq, NULL);
  while (1)
    {
      read = app->read_number (in, &sq, &line, &len);
      if (read < 0)
        break;
      create_square_pair (app, sq, vec, out);
    }
  mpz_clears (vec[0], vec[1], vec[2], sq, NULL);
  return 0;
}


static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_find_2sq_progressions_sq_t *app = (struct fv_app_find_2sq_progressions_sq_t *) state->input;
  switch (key)
    {
    case 'i':
      app->read_number = binary_read_one_number_from_stream;
      break;
    case 'o':
      app->display_record = display_binary_two_record;
      break;
    case ARGP_KEY_ARG:
      mpz_set_str (app->sq, arg, 10);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      mpz_init (app->sq);
      break;
    }
  return 0;
}

int
fituvalu_find_2sq_progressions_sq (struct fv_app_find_2sq_progressions_sq_t *app, FILE *in, FILE *out)
{
  if (mpz_cmp_ui (app->sq, 0) == 0)
    return gen_2sq (app, in, out);
  else
    {
      mpz_t vec[3];
      mpz_inits (vec[0], vec[1], vec[2], NULL);
      create_square_pair (app, app->sq, vec, out);
      mpz_clears (vec[0], vec[1], vec[2], NULL);
    }
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[SQUARE]",
  "Generate pairs of squares where their greatest common divisor is 1, and one of the squares is SQUARE.  When SQUARE is not specified it is read from the standard input.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_find_2sq_progressions_sq_t app;
  memset (&app, 0, sizeof (app));
  app.display_record = display_two_record;
  app.read_number = read_one_number_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_find_2sq_progressions_sq (&app, stdin, stdout);
}
