/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_find_3sq_progressions_sq_t
{
  int one;
  int num_args;
  int threads;
  int showroot;
  int from_stdin;
  FILE *infile;
  FILE *out;
  int gcd;
  unsigned long long int num;
  int out_binary;
  int in_binary;
};

static unsigned long long int
gcd (unsigned long long int n1, unsigned long long int n2)
{
  while (n1 != n2)
    {
      if(n1 > n2)
        n1 -= n2;
      else
        n2 -= n1;
    }
  return n1;
}

//from https://www.fq.math.ca/Papers1/43-2/paper43-2-1.pdf
static void
generate_progression (struct fv_app_find_3sq_progressions_sq_t *app, unsigned long long int *progression, unsigned long long int mtwo, unsigned long long int ntwo)
{
  unsigned long long int mn, twomn, m, n;
  m = sqrtl (mtwo);
  n = sqrtl (ntwo);
  mn = m * n;
  twomn = mn * 2;
  progression[0] = progression[1] - twomn;
  progression[2] = progression[1] + twomn;
}

static void
generate_progression_and_show (struct fv_app_find_3sq_progressions_sq_t *app, unsigned long long int *progression)
{
  unsigned long long int i, iroot, diff, limit, root;
  limit = progression[1] / 2;
  i = 1;
  while (1)
    {
      if (i > limit)
        break;

      diff = progression[1] - i;
      if (small_is_square (diff))
        {
          int show = 1;
          generate_progression (app, progression, i, diff);
          if (app->gcd)
            {
              unsigned long long int g;
              show = 0;
              g = gcd (progression[0], progression[1]);
              g = gcd (g, progression[2]);
              if (g == 1)
                show = 1;
            }
          if (show)
            {
              if (app->showroot)
                {
                  root = sqrtl (progression[2]);
                  pthread_mutex_lock (&display_lock);
                  if (app->out_binary)
                    {
                      fwrite (progression, sizeof (unsigned long long), 3,
                              app->out);
                      fwrite (&root, sizeof (unsigned long long), 1, app->out);
                    }
                  else
                    fprintf (app->out, "%llu, %llu, %llu, %llu\n",
                             progression[0], progression[1], progression[2],
                             root);
                  fflush (app->out);
                  pthread_mutex_unlock (&display_lock);
                }
              else
                {
                  pthread_mutex_lock (&display_lock);
                  if (app->out_binary)
                    fwrite (progression, sizeof (unsigned long long), 3,
                            app->out);
                  else
                    fprintf (app->out, "%llu, %llu, %llu\n", progression[0],
                             progression[1], progression[2]);
                  fflush (app->out);
                  pthread_mutex_unlock (&display_lock);
                }
              if (app->one)
                break;
            }
        }
      if (i == 1)
        {
          i = 4;
          iroot = 2;
          continue;
        }
      i += iroot;
      i += iroot;
      i++;
      iroot++;
    }
  return;
}

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long progression[3], num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_find_3sq_progressions_sq_t *app =
    (struct fv_app_find_3sq_progressions_sq_t *) param->data;

  while (1)
    {
      //go get the next progression to work on
      pthread_mutex_lock (&read_lock);

        {
          if (app->in_binary)
            {
              size_t read = fread (&num, sizeof (unsigned long long), 1,
                                   app->infile);
              if (read != 1)
                {
                  pthread_mutex_unlock (&read_lock);
                  break;
                }
            }
          else
            {
              ssize_t read = getline (&line, &len, app->infile);
              if (read == -1)
                {
                  pthread_mutex_unlock (&read_lock);
                  break;
                }
              num = strtoull (line, &end, 10);
            }
          progression[1] = num;
        }
      pthread_mutex_unlock (&read_lock);
      generate_progression_and_show (app, progression);
    }
      
  if (line)
    free (line);
  return NULL;
}

int
fituvalu_find_3sq_progression_sq (struct fv_app_find_3sq_progressions_sq_t *app)
{
  unsigned long long int progression[3];
  if (app->from_stdin)
    run_threads (app, app->threads, process_record);
  else
    {
      progression[1] = app->num;
      generate_progression_and_show (app, progression);
    }
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_find_3sq_progressions_sq_t *app = (struct fv_app_find_3sq_progressions_sq_t *) state->input;
  switch (key)
    {
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->out_binary = 1;
      break;
    case '1':
      app->one = 1;
      break;
    case 'n':
      app->showroot = 0;
      break;
    case 'g':
      app->gcd = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          char *end = NULL;
          switch (app->num_args)
            {
            case 0:
              app->num = strtoull (arg, &end, 10);
              break;
            }
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args == 0)
        app->from_stdin = 1;
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "gcd", 'g', 0, 0, "Require greatest common divisor of 1"},
  { "no-root", 'n', 0, 0, "Don't show the root of the fourth number"},
  { "show-diff", 'd', 0, 0, "Also show the diff"},
  { "one", '1', 0, 0, "Find one progression per perfect square."},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "out-binary", 'o', 0, 0, "Write out raw unsigned long longs instead of text"},
  { "in-binary", 'i', 0, 0, "Read in raw unsigned long longs instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[SQUARE]",
  "Find arithmetic progressions consisting of three squares given a middle SQUARE.\vWhen SQUARE is not provided, it is read from the standard input.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_find_3sq_progressions_sq_t app;
  memset (&app, 0, sizeof (app));
  app.showroot = 1;
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_find_3sq_progression_sq (&app);
}
