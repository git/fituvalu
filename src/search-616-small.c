/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

int max_recs = 1000;

struct fv_app_search_616_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  double median;
  double max;
  double percent;
  int mult;
  int threesq;
  int exhaustive;
  int center;
};

struct thread_data_t
{
  void *data;
};

struct prec
{
  unsigned long long int lo;
  unsigned long long int hi;
};


static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
exhaustive_generate_616_type_1 (struct fv_app_search_616_t *app, unsigned long long lo, unsigned long long hi, unsigned long long distance, long long (*s)[3][3])
{
  unsigned long long i, iroot, j, distance2, sum_minus_middle, twoiroot;
  unsigned long long int previ = 0;

  distance2 = distance * 2;

  iroot = sqrtl (distance2);
  iroot++;
  i = iroot * iroot;

  (*s)[1][0] = hi;
  (*s)[0][1] = lo;

  //+---+---+---+
  //|   | A | D |
  //+---+---+---+
  //| C |   |   |
  //+---+---+---+
  //| E |   | B |
  //+---+---+---+

  //the idea here is we keep going until the distance between D and E is less than the
  //distance we're looking for.
  //it is much slower and doesn't find anything we don't find with other starting values.

  while (1)
    {
      (*s)[0][2] = i;

      j = i - distance2;

      if (previ)
        {
          unsigned long long int diff = i - previ;
          if (diff > distance2)
            break;
        }

      if (small_is_square (j))
        {
          (*s)[2][0] = j;
          (*s)[1][1] = (*s)[2][0] + distance;
          sum_minus_middle = (*s)[1][1] * 2;

          (*s)[0][0] = sum_minus_middle - (*s)[2][2];
          (*s)[1][2] = sum_minus_middle - (*s)[1][0];
          (*s)[2][1] = sum_minus_middle - (*s)[0][1];

          //check for dups
          int dup = 0;
          if ((*s)[1][1] == (*s)[0][2] ||
              (*s)[1][1] == (*s)[0][0])
            dup = 1;
          if (!dup)
            {
              if (small_is_square ((*s)[0][0]))
                {
                  pthread_mutex_lock (&display_lock);
                  fprintf (app->out,
                           "%llu, %llu, %llu, "
                           "%llu, %llu, %lld, "
                           "%llu, %llu, %llu, \n",
                           (*s)[0][0], (*s)[0][1], (*s)[0][2],
                           (*s)[1][0], (*s)[1][1], (*s)[1][2],
                           (*s)[2][0], (*s)[2][1], (*s)[2][2]);
                  fflush (app->out);
                  pthread_mutex_unlock (&display_lock);
                }
            }
        }

      previ = i;
      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
}
static void
generate_616_type_1 (struct fv_app_search_616_t *app, unsigned long long lo, unsigned long long hi, unsigned long long distance, long long (*s)[3][3])
{
  unsigned long long i, iroot, j, distance2, sum_minus_middle, twoiroot, limit;

  distance2 = distance * 2;

  limit = hi * app->mult;

  iroot = sqrtl (distance2);
  iroot++;
  i = iroot * iroot;

  (*s)[1][0] = hi;
  (*s)[0][1] = lo;

  //+---+---+---+
  //|   | A | D |
  //+---+---+---+
  //| C |   |   |
  //+---+---+---+
  //| E |   | B |
  //+---+---+---+

  while (1)
    {
      (*s)[0][2] = i;

      j = i - distance2;

      if (small_is_square (j))
        {
          (*s)[2][0] = j;
          (*s)[1][1] = (*s)[2][0] + distance;
          sum_minus_middle = (*s)[1][1] * 2;

          (*s)[0][0] = sum_minus_middle - (*s)[2][2];
          (*s)[1][2] = sum_minus_middle - (*s)[1][0];
          (*s)[2][1] = sum_minus_middle - (*s)[0][1];

          //check for dups
          int dup = 0;
          if ((*s)[1][1] == (*s)[0][2] ||
              (*s)[1][1] == (*s)[0][0])
            dup = 1;
          if (!dup)
            {
              if (small_is_square ((*s)[0][0]))
                {
                  pthread_mutex_lock (&display_lock);
                  fprintf (app->out,
                           "%llu, %llu, %llu, "
                           "%llu, %llu, %lld, "
                           "%llu, %llu, %llu, \n",
                           (*s)[0][0], (*s)[0][1], (*s)[0][2],
                           (*s)[1][0], (*s)[1][1], (*s)[1][2],
                           (*s)[2][0], (*s)[2][1], (*s)[2][2]);
                  fflush (app->out);
                  pthread_mutex_unlock (&display_lock);
                }
            }
        }

      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
      if (i > limit)
        break;
    }
}

static void
handle_progression (struct fv_app_search_616_t *app, unsigned long long lo, unsigned long long hi, unsigned long long distance, long long (*s)[3][3])
{
  if (app->exhaustive)
    exhaustive_generate_616_type_1 (app, lo, hi, distance, s);
  else
    generate_616_type_1 (app, lo, hi, distance, s);
}

static void
generate_progressions (struct fv_app_search_616_t *app, unsigned long long n, long long (*s)[3][3])
{
  unsigned long long i, iroot, diff, limit, mm, nn, mn, twomn, lo, hi, twoiroot;

  limit = n / 2;
  i = 1;
  iroot = 1;
  while (1)
    {
      if (i > limit)
        break;

      diff = n - i;

      if (small_is_square (diff))
        {
          mm = iroot;
          nn = sqrtl (diff);
          mn = mm * nn;
          twomn = mn * 2;
          lo = n - twomn;
          hi = n + twomn;
      
          if (twomn > 0)
            handle_progression (app, lo, hi, twomn, s);
        }

      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
  return;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_616_t *app =
    (struct fv_app_search_616_t *) param->data;

  long long int s[3][3];

  unsigned long long int a[3];
  unsigned long long int b[3];

  unsigned long long distance;

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          if (fread (a, sizeof (unsigned long long), 3, app->infile) != 3)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          size_t read = read_ull_numbers (app->infile, a, 3, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      b[0] = a[0];
      b[1] = a[1];
      b[2] = a[2];
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on 3sq
        {
          s[2][2] = b[1];
          distance = b[1] - b[0];
          if (b[1] > b[0])
            handle_progression (app, b[0], b[2], distance, &s);
        }
    }


  if (line)
    free (line);
  return NULL;
}

static void
center_generate_progressions (struct fv_app_search_616_t *app, unsigned long long int n, struct prec **recs, int *num_recs)
{
  unsigned long long int i, iroot = 1, diff, limit, twoiroot;
  limit = n / 2;
  i = 1;
  while (1)
    {
      if (i > limit)
        break;

      diff = n - i;
      long double diffroot = sqrtl (diff);
      if (ceill (diffroot) == diffroot)
      //if ((long long)diffroot * (long long)diffroot == diff)
        {
          unsigned long long int mn, twomn, mm, nn;
          mm = iroot;
          nn = (long long) diffroot;
          mn = mm * nn;
          twomn = mn * 2;
          if (*num_recs >= max_recs)
            *recs = realloc (*recs, sizeof (struct prec) * ((*num_recs) + 1));
          (*recs)[*num_recs].lo = n - twomn;
          (*recs)[*num_recs].hi = n + twomn;
          (*num_recs)++;
        }
      if (i == 1)
        {
          i = 4;
          iroot = 2;
          continue;
        }
      twoiroot = iroot * 2;
      i += twoiroot;
      i++;
      iroot++;
    }
  return;
}

static void
generate_616_square (struct fv_app_search_616_t *app, unsigned long long center, unsigned long long sum, struct prec *one, struct prec *two, struct prec *recs, int num_recs)
{
  long long int a = sum - one->lo - two->lo;
  long long int b = sum - one->lo - two->hi;
  long long int c = sum - two->lo - one->hi;
  long long int d = sum - two->hi - one->hi;

  int asquare = small_is_square (a);
  int bsquare = small_is_square (b);
  int csquare = small_is_square (c);
  int dsquare = small_is_square (d);
  if ((asquare && bsquare) ||
      (bsquare && dsquare) ||
      (csquare && dsquare) ||
      (csquare && asquare))
    {
      pthread_mutex_lock (&display_lock);
      fprintf (app->out,
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, "
               "%lld, %lld, %lld, \n",
               one->lo, a, two->lo,
               b, center, c,
               two->hi, d, one->hi);
      fflush (app->out);
      pthread_mutex_unlock (&display_lock);
    }
}

//http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_616_t *app, struct prec *recs, int num_recs, unsigned long long center, unsigned long long sum);

void printCombination (int arr[], int n, int r, struct fv_app_search_616_t *app, struct prec *recs, int num_recs, unsigned long long center, unsigned long long sum)
{
  int data[r];
  combinationUtil (arr, data, 0, n-1, 0, r, app, recs, num_recs, center, sum);
}
 
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_search_616_t *app, struct prec *recs, int num_recs, unsigned long long center, unsigned long long sum)
{
  if (index == r)
    {
      generate_616_square (app, center, sum, &recs[data[0]], &recs[data[1]], recs, num_recs);
      return;
    }

  for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
      data[index] = arr[i];
      combinationUtil(arr, data, i + 1, end, index + 1, r, app, recs, num_recs, center, sum);
    }
}

static void
handle_progressions (struct fv_app_search_616_t *app, unsigned long long int n, struct prec *recs, int num_recs, int by, int *arr)
{
  //now we try every combination of 2.
  printCombination (arr, num_recs, by, app, recs, num_recs, n, n * 3);
}
static void*
process_center_record (void *arg)
{
  //we get the progressions given the center value
  //then mix and match them until we get a pair that works.
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_616_t *app =
    (struct fv_app_search_616_t *) param->data;

  int *arr = malloc (sizeof (int) * max_recs);
  for (int i = 0; i < max_recs; i++)
    arr[i] = i;
  struct prec *recs = malloc (sizeof (struct prec) * max_recs);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          size_t r = fread (&num, sizeof (num), 1, app->infile);
          if (r == 0)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          num = strtoull (line, &end, 10);
        }
      n = num;
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
        {
          int num_recs = 0;
          center_generate_progressions (app, n, &recs, &num_recs);
          if (num_recs > max_recs)
            {
              // we won't be hitting this with max_recs = 1000
              // but hey, no arbitrary limits.
              arr = realloc (arr, sizeof (int) * (num_recs));
              for (int i = max_recs; i < num_recs; i++)
                arr[i] = i;
            }
          if (num_recs >= 2)
            handle_progressions (app, n, recs, num_recs, 2, arr);
        }
    }

  free (arr);
  free (recs);
  if (line)
    free (line);
  return NULL;
}
static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_616_t *app =
    (struct fv_app_search_616_t *) param->data;

  long long s[3][3];

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          size_t read = fread (&n, sizeof (unsigned long long), 1, app->infile);
          if (read != 1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          num = strtoull (line, &end, 10);
        }
      n = num;

      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
      if (n > 1)
        {
          s[2][2] = n;
          generate_progressions (app, n, &s);
        }
    }

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_616 (struct fv_app_search_616_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    {
      if (app->center)
        run_threads (app, app->threads, process_center_record);
      else
        run_threads (app, app->threads, process_record);
    }
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *end = NULL;
  struct fv_app_search_616_t *app = (struct fv_app_search_616_t *) state->input;
  switch (key)
    {
    case 'c':
      app->center = 1;
      break;
    case 'e':
      app->exhaustive = 1;
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'm':
      app->percent = strtod (arg, &end);
      if (end == NULL || *end != '\0' || app->percent <= 0)
        argp_error (state, "invalid argument `%s' to option --multiply", arg);
      app->percent /= 100.0;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      app->mult = ceil (app->max * app->percent);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "exhaustive", 'e', 0, 0, "Do an exhaustive search"},
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "multiply", 'm', "PERC", 0, "Find a starting point for F by multiplying C by a value between 1 and 9755.2353 expressed as a percent (1-100)"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "center", 'c', 0, 0, "Use incoming (non-square) numbers as center values"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:16 given a FILE containing bottom right values.\vWhen FILE is not provided, it is read from the standard input.  This program is limited by its use of 64-bit integers.  The default value for PERC is 0.07361202787133798.  Magic squares of type 6:16 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  | X | X | X |   | Z | A | F |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating F\n"
"  | X |   |   |   | C |   |   |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of 2*(B-A) below F.\n"
"  | X |   | X |   | D |   | B |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_616_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.median = 3.5335;
  app.max = 14400.4999;
  app.percent = (app.median * 3.0) / app.max;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_616 (&app);
}
