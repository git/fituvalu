/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <unistd.h>
#include "magicsquareutil.h"
#include "linecount.h"
int linecount;

#define OPT_USEFUL 677

struct fv_app_3sq_pair_search_xy_t
{
  mpf_t max;
  int empty;
  int numargs;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
  int coords[5][2];
  int dist_multiplier;
};

struct rec
{
  mpz_t ap[3];
};

static int
parse_coord (char *s, int *x, int *y)
{
  int retval = sscanf (s, "%d,%d", x, y);
  if (retval != 2)
    return 0;
  if (*x < 0 && *x > 2)
    return 0;
  if (*y < 0 && *y > 2)
    return 0;
  return 1;
}

static void
swap (int (*a)[2], int (*b)[2])
{
  int temp[2];

  temp[0] = (*a)[0];
  temp[1] = (*a)[1];

  (*a)[0] = (*b)[0];
  (*a)[1] = (*b)[1];

  (*b)[0] = temp[0];
  (*b)[1] = temp[1];
}

/*
static void
dump_square (int (*s)[3][3], int ty, FILE *fp)
{
  fprintf (fp, "type 3:%d\n", ty);
  fprintf(fp, "  +---+---+---+\n");
  fprintf(fp, "  | %c | %c | %c |\n",
          (*s)[0][0] ? '@' + (*s)[0][0] : ' ',
          (*s)[0][1] ? '@' + (*s)[0][1] : ' ',
          (*s)[0][2] ? '@' + (*s)[0][2] : ' ');
  fprintf(fp, "  +---+---+---+\n");
  fprintf(fp, "  | %c | %c | %c |\n",
          (*s)[1][0] ? '@' + (*s)[1][0] : ' ',
          (*s)[1][1] ? '@' + (*s)[1][1] : ' ',
          (*s)[1][2] ? '@' + (*s)[1][2] : ' ');
  fprintf(fp, "  +---+---+---+\n");
  fprintf(fp, "  | %c | %c | %c |\n",
          (*s)[2][0] ? '@' + (*s)[2][0] : ' ',
          (*s)[2][1] ? '@' + (*s)[2][1] : ' ',
          (*s)[2][2] ? '@' + (*s)[2][2] : ' ');
  fprintf(fp, "  +---+---+---+\n");
}
*/

static void
permutations(int a[5][2], int l, int r, int ty, FILE *fp)
{
  int i;
  if (l == r)
    {
      for (int m = 0; m < 5; m++)
        fprintf (fp, "%d,%d ", a[m][0], a[m][1]);
      fprintf (fp, "\n");
      /*
      int s[3][3];
      memset (s, 0, sizeof (s));
      for (int m = 0; m < 5; m++)
        s[a[m][0]][a[m][1]] = m + 1;
      dump_square (&s, ty, fp);
      */
    }
  else
    {
      for (i = l; i <= r; i++)
        {
          swap(&a[l], &a[i]);
          permutations(a, l+1, r, ty, fp);
          swap(&a[l], &a[i]);
        }
    }
}
static int
display_coordinates (FILE *fp)
{
  int coords [][5][2] = 
    {
        { {1,1}, {1,2}, {2,0}, {2,1}, {2,2}},
        { {1,0}, {1,2}, {2,0}, {2,1}, {2,2}},
        { {1,0}, {1,1}, {1,2}, {2,1}, {2,2}},
        { {1,0}, {1,1}, {1,2}, {2,0}, {2,2}},
        { {0,2}, {1,2}, {2,0}, {2,1}, {2,2}},
        { {0,2}, {1,1}, {2,0}, {2,1}, {2,2}},
        { {0,2}, {1,1}, {1,2}, {2,0}, {2,1}},
        { {0,2}, {1,0}, {2,0}, {2,1}, {2,2}},
        { {0,2}, {1,0}, {1,2}, {2,1}, {2,2}},
        { {0,2}, {1,0}, {1,2}, {2,0}, {2,2}},
        { {0,2}, {1,0}, {1,2}, {2,0}, {2,1}},
        { {0,2}, {1,0}, {1,1}, {2,1}, {2,2}},
        { {0,2}, {1,0}, {1,1}, {2,0}, {2,2}},
        { {0,2}, {1,0}, {1,1}, {2,0}, {2,1}},
        { {0,2}, {1,0}, {1,1}, {1,2}, {2,2}},
        { {0,2}, {1,0}, {1,1}, {1,2}, {2,1}},
        { {0,2}, {1,0}, {1,1}, {1,2}, {2,0}},
        { {0,1}, {1,0}, {1,2}, {2,1}, {2,2}},
        { {0,1}, {1,0}, {1,2}, {2,0}, {2,2}},
        { {0,1}, {1,0}, {1,1}, {1,2}, {2,1}},
        { {0,1}, {0,2}, {1,0}, {2,0}, {2,2}},
        { {0,0}, {0,2}, {2,0}, {2,1}, {2,2}},
        { {0,0}, {0,2}, {1,1}, {2,0}, {2,2}},
    };
  int r = 5;
  int n = 5;
  for (int i = 0; i < sizeof (coords) /  sizeof (int[5][2]); i++)
    {
      int arr[5][2];
      memcpy (arr, coords[i], sizeof (arr));
      permutations (arr, 0, 4, i + 1, fp);
    }
}

static void
display_useful_coords (FILE *out)
{
  fprintf (out, "-d 1 1,0 1,1 1,2 2,0 2,2\n");
  fprintf (out, "-d 1 1,2 1,1 1,0 2,2 2,0\n");
  fprintf (out, "-d 1 1,0 0,2 2,1 1,2 2,0\n");
  fprintf (out, "-d 1 2,1 0,2 1,0 2,0 1,2\n");
  fprintf (out, "-d 1 1,0 0,2 2,1 1,1 2,2\n");
  fprintf (out, "-d 1 2,1 0,2 1,0 2,2 1,1\n");
  fprintf (out, "-d 1 0,2 1,1 2,0 2,2 1,0\n");
  fprintf (out, "-d 1 2,0 1,1 0,2 1,0 2,2\n");
  fprintf (out, "-d 2 0,2 1,1 2,0 1,2 2,1\n");
  fprintf (out, "-d 2 2,0 1,1 0,2 2,1 1,2\n");
  fprintf (out, "-d 2 0,1 2,2 1,0 2,1 1,2\n");
  fprintf (out, "-d 2 1,0 2,2 0,1 1,2 2,1\n");
  fprintf (out, "-d 2 0,1 2,2 1,0 0,2 2,0\n");
  fprintf (out, "-d 2 1,0 2,2 0,1 2,0 0,2\n");
  //fprintf (out, "-d 3 1,2 1,1 1,0 2,1 2,2\n"); //0289 | no sixes
  //fprintf (out, "-d 5 1,2 1,1 1,0 2,1 0,2\n"); //1880 | no sixes
  //fprintf (out, "-d 5 2,1 0,2 1,0 1,2 1,1\n"); //1919 | no sixes
  //fprintf (out, "-d 5 1,0 2,2 0,1 1,2 2,0\n"); //2208 | only one six
  //fprintf (out, "-d 5 1,2 2,0 0,1 1,0 2,2\n"); //2221 | only one six
  //fprintf (out, "-d 6 1,0 1,1 1,2 2,2 2,1\n"); //0242 | no sixes
  //fprintf (out, "-d 6 1,2 1,1 1,0 2,2 2,1\n"); //0290 | no sixes
  //fprintf (out, "-d 6 1,0 1,1 1,2 2,0 2,2\n"); //0361 | no sixes
  //fprintf (out, "-d 6 1,0 1,1 1,2 2,2 2,0\n"); //0362 | no sixes
  //fprintf (out, "-d 6 1,2 1,1 1,0 2,0 2,2\n"); //0409 | no sixes
  //fprintf (out, "-d 6 1,2 1,1 1,0 2,2 2,0\n"); //0410 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 2,1 2,2\n"); //0601 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 2,2 2,1\n"); //0602 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 2,1 2,2\n"); //0649 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 2,2 2,1\n"); //0650 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 1,2 2,1\n"); //0723 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 2,1 1,2\n"); //0724 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 2,1 0,2 1,2\n"); //0795 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 2,1 1,2 0,2\n"); //0796 | no sixes
  //fprintf (out, "-d 6 1,0 0,2 2,1 2,0 2,2\n"); //0867 | no sixes
  //fprintf (out, "-d 6 2,1 0,2 1,0 2,0 2,2\n"); //0927 | no sixes
  //fprintf (out, "-d 6 1,0 0,2 2,1 1,2 2,2\n"); //0987 | no sixes
  //fprintf (out, "-d 6 2,1 0,2 1,0 1,2 2,2\n"); //1047 | no sixes
  //fprintf (out, "-d 6 1,0 0,2 2,1 2,0 1,2\n"); //1229 | no sixes
  //fprintf (out, "-d 6 1,0 0,2 2,1 1,2 2,0\n"); //1230 | no sixes
  //fprintf (out, "-d 6 2,1 0,2 1,0 2,0 1,2\n"); //1319 | no sixes
  //fprintf (out, "-d 6 2,1 0,2 1,0 1,2 2,0\n"); //1320 | no sixes
  //fprintf (out, "-d 6 1,0 0,2 2,1 1,1 2,2\n"); //1347 | no sixes
  //fprintf (out, "-d 6 2,1 0,2 1,0 1,1 2,2\n"); //1407 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 1,0 2,2\n"); //1449 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 2,2 1,0\n"); //1450 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 1,0 2,2\n"); //1521 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 2,2 1,0\n"); //1522 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 1,0 2,1\n"); //1569 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 2,1 1,0\n"); //1570 | no sixes
  //fprintf (out, "-d 6 1,0 0,2 2,1 2,0 1,1\n"); //1589 | no sixes
  //fprintf (out, "-d 6 1,0 0,2 2,1 1,1 2,0\n"); //1590 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 1,0 2,1\n"); //1641 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 2,1 1,0\n"); //1642 | no sixes
  //fprintf (out, "-d 6 2,1 0,2 1,0 2,0 1,1\n"); //1679 | no sixes
  //fprintf (out, "-d 6 2,1 0,2 1,0 1,1 2,0\n"); //1680 | no sixes
  //fprintf (out, "-d 6 1,0 1,1 1,2 0,2 2,2\n"); //1713 | no sixes
  //fprintf (out, "-d 6 1,0 1,1 1,2 2,2 0,2\n"); //1714 | no sixes
  //fprintf (out, "-d 6 1,2 1,1 1,0 0,2 2,2\n"); //1759 | no sixes
  //fprintf (out, "-d 6 1,2 1,1 1,0 2,2 0,2\n"); //1760 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 1,0 2,0 1,2\n"); //1829 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 1,2 1,0 2,0\n"); //1830 | no sixes
  //fprintf (out, "-d 6 1,0 1,1 1,2 0,2 2,1\n"); //1833 | no sixes
  //fprintf (out, "-d 6 1,2 1,1 1,0 0,2 2,1\n"); //1879 | no sixes
  //fprintf (out, "-d 6 2,1 0,2 1,0 1,2 1,1\n"); //1919 | no sixes
  //fprintf (out, "-d 6 2,1 0,2 1,0 1,1 1,2\n"); //1920 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 1,2 1,0\n"); //1931 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 1,0 1,2\n"); //1932 | no sixes
  //fprintf (out, "-d 6 1,0 1,1 1,2 0,2 2,0\n"); //1953 | no sixes
  //fprintf (out, "-d 6 1,0 1,1 1,2 2,0 0,2\n"); //1954 | no sixes
  //fprintf (out, "-d 6 1,2 1,1 1,0 0,2 2,0\n"); //1999 | no sixes
  //fprintf (out, "-d 6 1,2 1,1 1,0 2,0 0,2\n"); //2000 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 1,2 1,0\n"); //2027 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 1,0 1,2\n"); //2028 | no sixes
  //fprintf (out, "-d 6 0,1 2,2 1,0 2,1 1,2\n"); //2063 | no sixes
  //fprintf (out, "-d 6 0,1 2,2 1,0 1,2 2,1\n"); //2064 | no sixes
  //fprintf (out, "-d 6 1,0 2,2 0,1 2,1 1,2\n"); //2087 | no sixes
  //fprintf (out, "-d 6 1,0 2,2 0,1 1,2 2,1\n"); //2088 | no sixes
  //fprintf (out, "-d 6 0,1 2,0 1,2 1,0 2,2\n"); //2173 | no sixes
  //fprintf (out, "-d 6 0,1 2,2 1,0 1,2 2,0\n"); //2184 | no sixes
  //fprintf (out, "-d 6 1,0 2,2 0,1 1,2 2,0\n"); //2208 | no sixes
  //fprintf (out, "-d 6 1,2 2,0 0,1 1,0 2,2\n"); //2221 | no sixes
  //fprintf (out, "-d 6 0,0 1,1 2,2 2,0 0,2\n"); //2651 | no sixes
  //fprintf (out, "-d 6 0,0 1,1 2,2 0,2 2,0\n"); //2652 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 0,0 2,2\n"); //2673 | no sixes
  //fprintf (out, "-d 6 0,2 1,1 2,0 2,2 0,0\n"); //2674 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 0,0 2,2\n"); //2719 | no sixes
  //fprintf (out, "-d 6 2,0 1,1 0,2 2,2 0,0\n"); //2720 | no sixes
  //fprintf (out, "-d 6 2,2 1,1 0,0 2,0 0,2\n"); //2747 | no sixes
  //fprintf (out, "-d 6 2,2 1,1 0,0 0,2 2,0\n"); //2748 | no sixes
  //fprintf (out, "-d 7 1,0 0,2 2,1 1,2 2,2\n"); //0987 | only one six
  //fprintf (out, "-d 7 2,1 0,2 1,0 1,2 2,2\n"); //1047 | no sixes
  //fprintf (out, "-d 8 1,0 0,2 2,1 2,0 2,2\n"); //0867 | no sixes
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_3sq_pair_search_xy_t *app = (struct fv_app_3sq_pair_search_xy_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'x':
      if (mpf_set_str (app->max, arg, 10))
        argp_error (state, "Invalid value `%s' for --maximum.", arg);
      if (mpf_cmp_ui (app->max, 0) < 0)
        argp_error (state, "Invalid value `%s' for --maximum.", arg);
      mpf_div_ui (app->max, app->max, 100);
      break;
    case OPT_USEFUL:
      display_useful_coords (stdout);
      exit (0);
      break;
    case 'd':
      app->dist_multiplier = atoi (arg);
      if (app->dist_multiplier <= 0)
        argp_error (state, "invalid --dist `%s'", arg);
      break;
    case 'e':
      app->empty = atoi (arg);
      break;
    case 'l':
      display_coordinates (stdout);
      exit (0);
      break;
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
        {
          int x, y;
          if (parse_coord (arg, &x, &y))
            {
              app->coords[app->numargs][0] = x;
              app->coords[app->numargs][1] = y;
            }
          else
            argp_error (state, "could not parse coord `%s'.  "
                        "format is NUM,NUM.", arg);
        }
      app->numargs++;
      if (app->numargs > 5)
        argp_error (state, "too many arguments.");
      break;
    case ARGP_KEY_FINI:
      if (app->numargs != 5)
        argp_error (state, "too few arguments.");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      app->infile = stdin;
      mpf_init (app->max);
      mpf_set_ui (app->max, 1);
      break;
    }
  return 0;
}

void dump_square (void *data, mpz_t (*s)[3][3])
{
  struct fv_app_3sq_pair_search_xy_t *app = 
    (struct fv_app_3sq_pair_search_xy_t *) data;
  app->display_square (*s, app->out);
}

void
search_xy_3sq_down (mpz_t *progression, mpz_t high, mpz_t low, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], void (*func)(void *, mpz_t (*)[3][3]), void *data)
{
  struct fv_app_3sq_pair_search_xy_t *app = 
    (struct fv_app_3sq_pair_search_xy_t *) data;
  for (int k = 0; k < 3; k++)
    for (int l = 0; l < 3; l++)
      mpz_set_ui ((*s)[k][l], app->empty);
  mpz_t i, iroot, j, limit, sum, p;
  mpz_inits (i, iroot, j, limit, sum, p, NULL);
  mpz_set (i, high);
  mpz_sqrt (iroot, i);

  mpz_sub_ui (iroot, iroot, 1);
  mpz_sub (i, i, iroot);
  mpz_sub (i, i, iroot);
  mpz_sub_ui (i, i, 1);

  while (1)
    {
      mpz_sub (j, i, distance);
      if (mpz_cmp (j, low) < 0)
        break;

      if (mpz_perfect_square_p (j))
        {
          mpz_set (b[0], i);
          mpz_set (b[1], j);

          mpz_set ((*s)[app->coords[0][0]][app->coords[0][1]], progression[0]);
          mpz_set ((*s)[app->coords[1][0]][app->coords[1][1]], progression[1]);
          mpz_set ((*s)[app->coords[2][0]][app->coords[2][1]], progression[2]);
          mpz_set ((*s)[app->coords[3][0]][app->coords[3][1]], b[0]);
          mpz_set ((*s)[app->coords[4][0]][app->coords[4][1]], b[1]);


          func (data, s);
        }
      mpz_sub_ui (iroot, iroot, 1);
      mpz_sub (i, i, iroot);
      mpz_sub (i, i, iroot);
      mpz_sub_ui (i, i, 1);
    }
  mpz_clears (i, iroot, j, limit, sum, p, NULL);
  return;
}

int
fituvalu_3sq_pair_search_xy (struct fv_app_3sq_pair_search_xy_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  struct rec rec;
  mpz_t b[2], diff, s[3][3], hi, lo;
  mpz_inits (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], diff, hi, lo, NULL);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  while (1)
    {
      read = app->read_tuple (app->infile, rec.ap, &line, &len);
      if (read == -1)
        break;
      update_linecount (&linecount);
      mpz_sub (diff, rec.ap[2], rec.ap[1]);

      mpz_mul (hi, diff, diff);
      if (mpf_cmp_ui (app->max, 1) != 0)
        {
          mpf_t n, r;
          mpf_inits (n, r, NULL);
          mpf_set_z (n, hi);
          mpf_mul (r, n, app->max);
          mpz_set_f (hi, r);
          mpf_clears (n, r, NULL);
        }
      mpz_set_ui (lo, 0);

      mpz_mul_ui (diff, diff, app->dist_multiplier);

      if (mpz_cmp_ui (diff, 0) > 0)
        search_xy_3sq_down (rec.ap, hi, lo, diff, b, &s, dump_square, app);

    }
  if (line)
    free (line);
  mpz_clears (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], diff, hi, lo, NULL);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear  (s[i][j]);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "list", 'l', 0, 0, "Display all possible coordinates and exit"},
  { "empty", 'e', "NUM", 0, "Values marked as empty are NUM instead of zero"},
  { "dist-multiplier", 'd', "MULT", 0, "COORD4 and COORD5 are this far apart (Default 1)"},
  { "useful-input", OPT_USEFUL, 0, 0, "Display the useful coords and strategies and exit"},
  { "maximum", 'x', "PERC", OPTION_HIDDEN, "Set maximum search bounds (Default 100)"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "COORD1 COORD2 COORD3 COORD4 COORD5",
  "Make an unfilled 3x3 magic square with 5 perfect squares by giving it a series of 3 square progressions on the standard input.   Put the 5 perfect squares in COORD1..5.  The program iterates over pairs of squares to find two squares that have the correct difference.\vMULT can be 1 or 2.  The `fill-square' program uses the results of this program as its input.  COORD4 and COORD5 appear in descending order.",
  0
};

int
main (int argc, char **argv)
{
  init_linecount (&linecount);
  struct fv_app_3sq_pair_search_xy_t app;
  memset (&app, 0, sizeof (app));

  app.display_square = display_square_record;
  app.read_tuple = read_three_numbers_from_stream;
  app.infile = stdin;
  app.out = stdout;
  app.dist_multiplier = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_3sq_pair_search_xy (&app);
}
