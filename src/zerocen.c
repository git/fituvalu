#include <stdio.h>
#include <math.h>
#include <string.h>
#include <pthread.h>
#include <argp.h>
#include <stdlib.h>
pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_zerocen_t
{
  FILE *out;
  int numargs;
  int threads;
  unsigned long long i;
  unsigned long long root;
  int offset;
  int step;
  unsigned long long int max;
};

static int
small_is_square2 (unsigned long long num, long double *root)
{
   *root = sqrtl (num);
   if ((unsigned long long)(*root) * (unsigned long long)(*root) == num)
     return 1;
   return 0;
}

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
process_square ( struct fv_app_zerocen_t *app, unsigned long long int i)
{
  long double diffroot;
  long long int s[3][3];
  memset (s, 0, sizeof (s));
  //s[1][1] = 0;
  s[2][0] = i;
  s[0][2] = -i;
  unsigned long long int j = 4;
  unsigned long long int jroot = 2;
  unsigned long long innermax = app->max;
  while (1)
    {
      if (j > innermax)
        break;
      if (i != j)
        {
          s[2][2] = j;
          s[0][0] = -j;
          s[0][1] = 0 - s[0][0] - s[0][2];
          if (small_is_square2 (s[0][1], &diffroot))
            {
              //okay we're at 4
              s[1][2] = 0 - s[0][2] - s[2][2];
              if (s[1][2] != s[1][1])
                {
                  if (small_is_square2 (s[1][2], &diffroot))
                    {
                      //okay we're at 5
                      pthread_mutex_lock (&display_lock);
                      fprintf (app->out, "%lld, %lld, %lld, %lld, %lld, %lld, %lld, %lld, %lld\n", s[0][0], s[0][1], s[0][2], -s[1][2], s[1][1], s[1][2], s[2][0], -s[0][1], s[2][2]);
                      pthread_mutex_unlock (&display_lock);
                    }
                }
            }
        }
      j += jroot;
      j += jroot;
      j++;
      jroot++;
    }
}

static void*
zerocen (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_zerocen_t *app = (struct fv_app_zerocen_t *) param->data;

  unsigned long long n = app->i;
  while (1)
    {
      if (n > app->max)
        break;
      process_square (app, n);
      pthread_mutex_lock (&read_lock);

      for (int q = 0; q < app->step; q++)
        {
          app->i += app->root;
          app->i += app->root;
          app->i++;
          app->root++;
        }

      n = app->i;
      pthread_mutex_unlock (&read_lock);
    }
}

static struct argp_option
options[] =
{
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "offset", 'O', "NUM", 0, "Begin the outer loop with NUM offset"},
  { "step", 's', "NUM", 0, "Increment the outer loop by NUM squares"},
  { 0 }
};
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_zerocen_t *app =
    (struct fv_app_zerocen_t *) state->input;
  switch (key)
    {
    case 'O':
      app->offset = atoi (arg);
      break;
    case 's':
      app->step = atoi (arg);
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_ARG:
      app->numargs++;
      if (app->numargs > 1)
        argp_error (state, "too many arguments.");
      {
        char *end = NULL;
        app->max = strtoull (arg, &end, 10);
        if (end == NULL || *end != '\0')
          argp_error (state, "invalid MAX value `%s'", arg);
      }
      break;
    }
  return 0;
}

static struct argp
argp =
{
  options, parse_opt, "[MAX]",
  "Find 3x3 magic squares with 5 perfect squares and a magic number of zero.\vOne of these squares has never been found.  If MAX is not specified, a value of 1000000000 is used.  Default values for --offset, --step, and --threads are 0, 1, and 1 respectively",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_zerocen_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.threads = 1;
  app.i = 4;
  app.root = 2;
  int offset = 0;
  int step = 1;
  app.offset = 0;
  app.step = 1;
  app.max = 1000000000;

  argp_parse (&argp, argc, argv, 0, 0, &app);
  for (int q = 0; q < app.offset; q++)
    {
      app.i += app.root;
      app.i += app.root;
      app.i++;
      app.root++;
    }
  run_threads (&app, app.threads, zerocen);
  return 0;
}
