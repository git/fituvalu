/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <pthread.h>
#include <unistd.h>
#include "magicsquareutil.h"
#include "linecount.h"
int linecount;

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;
int max_recs = 1000;
struct fv_app_tuple_search_63_t
{
  FILE *infile;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_record) (FILE *, mpz_t *, char **, size_t *);
  FILE *out;
  int threads;
};

struct prec
{
  mpz_t lo;
  mpz_t hi;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_tuple_search_63_t *app = (struct fv_app_tuple_search_63_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 't':
      app->threads = atoi (arg);
      break;
    case 'i':
      app->read_record = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->infile)
        argp_error (state, "too many arguments");
      app->infile = fopen (arg, "r");
      if (!app->infile)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      app->infile = stdin;
      break;
    }
  return 0;
}

//http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_tuple_search_63_t *app, struct prec *recs, int num_recs, mpz_t center);

void printCombination (int arr[], int n, int r, struct fv_app_tuple_search_63_t *app, struct prec *recs, int num_recs, mpz_t center)
{
  int data[r];
  combinationUtil (arr, data, 0, n-1, 0, r, app, recs, num_recs, center);
}

static void
handle_progressions (struct fv_app_tuple_search_63_t *app, mpz_t n, struct prec *recs, int num_recs, int by, int *arr)
{
  printCombination (arr, num_recs, by, app, recs, num_recs, n);
}

static int
search_square (mpz_t s, struct prec *recs, int num_recs)
{
  for (int i = 0; i < num_recs; i++)
    {
      if (mpz_cmp (s, recs[i].lo) == 0)
        return 1;
      if (mpz_cmp (s, recs[i].hi) == 0)
        return 1;
    }
  return 0;
}

static void
generate_63_square (struct fv_app_tuple_search_63_t *app, mpz_t center, struct prec *one, struct prec *two, struct prec *three)
{
  //try to make a 6:3 magic square from three progressions.
  //one->lo, center, one->hi
  //two->lo, center, two->hi
  //three->lo, center, three->hi

  if (mpz_cmp (one->lo, two->lo) > 0 &&
      mpz_cmp (three->hi, two->lo) > 0)
    {
      mpz_t diag1, row1;
      mpz_inits (diag1, row1, NULL);
      mpz_add (diag1, one->lo, center);
      mpz_add (diag1, diag1, one->hi);
      mpz_add (row1, one->lo, two->lo);
      mpz_add (row1, row1, three->hi);
      if (mpz_cmp (diag1, row1) == 0)
        {
          mpz_t sum, left, right, s[3][3];
          mpz_inits (sum, left, right, NULL);
          mpz_mul_ui (sum, center, 3);

          mpz_sub (left, sum, one->lo);
          mpz_sub (left, left, three->lo);

          mpz_sub (right, sum, three->hi);
          mpz_sub (right, right, one->hi);

          for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
              mpz_init (s[i][j]);
          mpz_set (s[0][0], one->lo);
          mpz_set (s[0][1], two->lo);
          mpz_set (s[0][2], three->hi);
          mpz_set (s[1][0], left);
          mpz_set (s[1][1], center);
          mpz_set (s[1][2], right);
          mpz_set (s[2][0], three->lo);
          mpz_set (s[2][1], two->hi);
          mpz_set (s[2][2], one->hi);
          rotate_square (&s);
          flip_square_vertically (&s);
          pthread_mutex_lock (&display_lock);
          app->display_square (s, app->out);
          fflush (app->out);
          pthread_mutex_unlock (&display_lock);
          for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
              mpz_clear (s[i][j]);
          mpz_clears (sum, left, right, NULL);
        }
      mpz_clears (diag1, row1, NULL);
    }
}

 
void combinationUtil (int arr[], int data[], int start, int end,
                      int index, int r, struct fv_app_tuple_search_63_t *app, struct prec *recs, int num_recs, mpz_t center)
{
  if (index == r)
    {
      generate_63_square (app, center, &recs[data[0]], &recs[data[1]], &recs[data[2]]);
      return;
    }

  for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
      data[index] = arr[i];
      combinationUtil(arr, data, i + 1, end, index + 1, r, app, recs, num_recs, center);
    }
}

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}
static void*
process_records (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_tuple_search_63_t *app =
    (struct fv_app_tuple_search_63_t *) param->data;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t record[3];
  mpz_t center, prev;
  mpz_inits (record[0], record[1], record[2], center, prev, NULL);
  struct prec *recs = NULL;
  size_t num_recs = 0;

  int *arr = malloc (max_recs * sizeof (int));
  for (int j = 0; j < max_recs; j++)
    arr[j] = j;

  if (app->threads > 1)
    pthread_mutex_lock (&read_lock);
  while (1)
    {
      read = app->read_record (app->infile, record, &line, &len);
      if (read == -1)
        break;
      mpz_set (prev, center);
      if (mpz_cmp (prev, record[1]) != 0)
        {
          if (num_recs >= max_recs)
            {
              arr = realloc (arr, sizeof (int) * (num_recs + 1));
              arr[num_recs] = num_recs;
              for (int j = 0; j < num_recs; j++)
                arr[j] = j;
              max_recs = num_recs;
            }
          if (num_recs > 0)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              if (app->threads > 1)
                pthread_mutex_lock (&read_lock);
              handle_progressions (app, center, recs, num_recs, 3, arr);
              for (int i = 0; i < num_recs; i++)
                mpz_clears (recs[i].hi, recs[i].lo, NULL);
              free (recs);
              recs = NULL;
              num_recs = 0;
            }
        }
      recs = realloc (recs, sizeof (struct prec) * (num_recs + 1));
      mpz_inits (recs[num_recs].lo, recs[num_recs].hi, NULL);
      mpz_set (recs[num_recs].lo, record[0]);
      mpz_set (recs[num_recs].hi, record[2]);
      num_recs++;
      mpz_set (center, record[1]);
    }
  if (app->threads > 1)
    pthread_mutex_unlock (&read_lock);
      
  if (num_recs >= max_recs)
    {
      arr = realloc (arr, sizeof (int) * (num_recs + 1));
      arr[num_recs] = num_recs;
      for (int j = 0; j < num_recs; j++)
        arr[j] = j;
      max_recs = num_recs;
    }
  handle_progressions (app, center, recs, num_recs, 3, arr);

  for (int i = 0; i < num_recs; i++)
    mpz_clears (recs[i].hi, recs[i].lo, NULL);
  free (recs);
  recs = NULL;

  free (arr);
  if (line)
    free (line);

  mpz_clears (record[0], record[1], record[2], center, prev, NULL);
  return NULL;
}

int
fituvalu_tuple_search_63 (struct fv_app_tuple_search_63_t *app)
{
  run_threads (app, app->threads, process_records);

  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Try to make a 3x3 magic square with 6 or more perfect squares by giving it a series of tuples where the middle value is a center value (X1 and X2).  The program iterates over pairs of squares to find two squares that have the correct differences.\vEach square is laid out like:\n\
  +------+------+------+\n\
  |  Y1  |  Z1  |  X2  |\n\
  +------+------+------+\n\
  |      |      |      |\n\
  +------+------+------+\n\
  |  X1  |  Y2  |  Z2  |\n\
  +------+------+------+\nThe 63 in the name comes from the fact we're trying to make a square of the form 6:3.  The `search-progression' program provides suitable input.",
  0
};

int
main (int argc, char **argv)
{
  init_linecount (&linecount);
  struct fv_app_tuple_search_63_t app;
  memset (&app, 0, sizeof (app));

  app.display_square = display_square_record;
  app.read_record = read_three_numbers_from_stream;
  app.infile = stdin;
  app.out = stdout;
  app.threads = 1;

  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_tuple_search_63 (&app);
}
//threaded doesn't work well.
