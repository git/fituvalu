/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

struct fv_app_mine_progressions_t
{
  int show_diff;
  void (*display_tuple) (mpz_t s[3], FILE *out);
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  FILE *out;
};

static int
fill_progression (mpz_t s[3][3], int y1, int x1, int y2, int x2, int y3, int x3, mpz_t *p)
{
  mpz_set (p[0], s[y1][x1]);
  mpz_set (p[1], s[y2][x2]);
  mpz_set (p[2], s[y3][x3]);
  if (mpz_cmp (p[0], p[2]) > 0)
    {
      mpz_t swap;
      mpz_init (swap);
      mpz_set (swap, p[0]);
      mpz_set (p[0], p[2]);
      mpz_set (p[2], swap);
      mpz_clear (swap);
    }
  mpz_t diff[2];
  mpz_inits (diff[0], diff[1], NULL);
  mpz_sub (diff[0], p[1], p[0]);
  mpz_sub (diff[1], p[2], p[1]);
  int ret = mpz_cmp (diff[0], diff[1]) == 0;
  mpz_clears (diff[0], diff[1], NULL);
  return ret;
}
static void
dump_progression (struct fv_app_mine_progressions_t *app, mpz_t *p, FILE *out)
{
  if (app->show_diff)
    {
      mpz_t diff;
      mpz_init (diff);
      mpz_sub (diff, p[1], p[0]);
      display_three_record_with_root (p, &diff, out);
      mpz_clear (diff);
    }
  else
    app->display_tuple (p, out);
}

static void
mine_progressions_from_square (struct fv_app_mine_progressions_t *app, mpz_t s[3][3], FILE *out)
{
  mpz_t p[3];
  mpz_inits (p[0], p[1], p[2], NULL);

  if (fill_progression (s, 0,0, 1,1, 2,2, p))
    dump_progression (app, p, out);
  if (fill_progression (s, 0,2, 1,1, 2,0, p))
    dump_progression (app, p, out);
  if (fill_progression (s, 2,1, 0,0, 1,2, p))
    dump_progression (app, p, out);
  if (fill_progression (s, 1,0, 0,2, 2,1, p))
    dump_progression (app, p, out);
  if (fill_progression (s, 0,1, 2,2, 1,0, p))
    dump_progression (app, p, out);
  if (fill_progression (s, 1,2, 2,0, 0,1, p))
    dump_progression (app, p, out);

  mpz_clears (p[0], p[1], p[2], NULL);
  return;
}

int
fituvalu_mine_progressions (struct fv_app_mine_progressions_t *app, FILE *in, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  is_magic_square_init ();
  while (1)
    {
      read = app->read_square (in, &s, &line, &len);
      if (read == -1)
        break;
      if (is_magic_square (s, 1))
        mine_progressions_from_square (app, s, out);
    }
  is_magic_square_fini ();

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_mine_progressions_t *app = (struct fv_app_mine_progressions_t *) state->input;
  switch (key)
    {
    case 'd':
      app->show_diff = 1;
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "show-diff", 'd', 0, 0, "Also show the diff"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares and output the arithmetic progressions.\vThe 3x3 magic squares must be separated by a comma and terminated by a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_mine_progressions_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.read_square = read_square_from_stream;
  app.display_tuple = display_three_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_mine_progressions (&app, stdin, stdout);
}
