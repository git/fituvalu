/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <gmp.h>
#include <pthread.h>
#include "magicsquareutil.h"


struct fv_app_generate_mnpr_t;

struct fv_app_generate_mnpr_t
{
  FILE *out;
  int inmem;
  FILE *infile;
  void (*display_record) (mpz_t *s, FILE *out);
  int in_binary;
  int num_args;
};

void *
per_thread_init()
{
  mpz_t *rec = malloc (4 * sizeof (mpz_t));
  mpz_inits (rec[0], rec[1], rec[2], rec[3], NULL);
  return rec;
}

void
per_thread_destroy (void *p)
{
  mpz_t *rec = (mpz_t *) p;
  mpz_clears (rec[0], rec[1], rec[2], rec[3], NULL);
  free (p);
}

static void
display_mnpr (mpz_t m1, mpz_t n1, mpz_t m2, mpz_t n2, void *data, void *p)
{
  mpz_t *rec = (mpz_t *) p;
  struct fv_app_generate_mnpr_t *app = (struct fv_app_generate_mnpr_t *) data;
  mpz_set (rec[0], m1);
  mpz_set (rec[1], n1);
  mpz_set (rec[2], m2);
  mpz_set (rec[3], n2);
  app->display_record (rec, app->out);
}

int
fituvalu_generate_mnpr (struct fv_app_generate_mnpr_t *app, FILE *in)
{
  if (app->in_binary)
    {
      if (app->inmem)
        morgenstern_search_dual_binary_mem (in, app->infile, display_mnpr,
                                            1, per_thread_init,
                                            per_thread_destroy, app);
      else
        morgenstern_search_dual_binary (in, app->infile, display_mnpr,
                                        1, per_thread_init, per_thread_destroy,
                                        app);
    }
  else
    {
      if (app->inmem)
        morgenstern_search_dual_mem (in, app->infile, display_mnpr,
                                     1, per_thread_init, per_thread_destroy,
                                     app);
      else
        morgenstern_search_dual (in, app->infile, display_mnpr, 1,
                                 per_thread_init, per_thread_destroy, app);
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "mem", 'm', 0, 0, "Load numbers from FILE and stdin into memory"},
  { 0 }
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_generate_mnpr_t *app = (struct fv_app_generate_mnpr_t *) state->input;
  switch (key)
    {
    case 'm':
      app->inmem = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_record = display_binary_four_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 1)
        argp_error (state, "too many arguments");
      else
        {
          if (access (arg, R_OK) == 0)
            {
              app->infile = fopen (arg, "r");
              app->num_args++;
            }
        }
      break;
    case ARGP_KEY_NO_ARGS:
      argp_error (state, "missing argument");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->inmem && !app->infile)
        argp_error (state, "-m must be used with FILE specified as argument");
      break;
    }
  return 0;
}

static struct argp
argp =
{
  options, parse_opt, "FILE",
  "Generate M, N P, R coprime pairs by combining every M,N given in FILE or standard input with every other M, N.\vWhen -m is used, FILE contains one M, N, pair per record and stdin provides record numbers.  There are two environment variables to control the production of pairs: Use the environment variable FV_REC_LOW_LIMIT to ignore records this low. Use the FV_REC_REVERSE environment variable to flip M,N with P, R.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_generate_mnpr_t app;
  memset (&app, 0, sizeof (app));
  app.display_record = display_four_record;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);

  return fituvalu_generate_mnpr (&app, stdin);
}
