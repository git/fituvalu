/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <math.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "magicsquareutil.h"

#define OPT_DUMP -951

struct fv_app_sq_seq_finder_t
{
  int num_args;
  int *args;
  char *start;
  int dump;
  int depth;
  int breadth;

  int cur_arg;

  unsigned long long *numbers;
  int num_numbers;

  unsigned long long hits;
  unsigned long long count;

};

static int
advance_to (struct fv_app_sq_seq_finder_t *app, unsigned long long *num, unsigned long long *root, unsigned long long target)
{
  int match = 0;
  while (1)
    {
      //skip the zeroes
      while (1)
        {
          if (app->args[app->cur_arg] == 0)
            {
              app->cur_arg++;
              if (app->cur_arg >= app->num_args)
                app->cur_arg = 0;
            }
          else
            break;
        }

      for (int j = 0; j < app->args[app->cur_arg]; j++)
        {
          *num = *num + *root;
          *num = *num + *root;
          *num = *num + 1;
          *root = *root + 1;
        }
      app->cur_arg++;
      if (app->cur_arg >= app->num_args)
        app->cur_arg = 0;
      app->count++;
      long long ret = (unsigned long long)(*num) - (unsigned long long)target;
      if (ret == 0)
        {
          app->hits++;
          match = 1;
          break;
        }
      else if (ret > 0)
        break;
    }
  return match;
}

static void
read_in_numbers (struct fv_app_sq_seq_finder_t *app, FILE *in)
{
  char *line = NULL, *end;
  size_t len = 0;
  unsigned long long a;
  while (1)
    {
      ssize_t read = fv_getline (&line, &len, in);
      if (read == -1)
        break;
      app->numbers =
        realloc (app->numbers, sizeof (unsigned long long) * (app->num_numbers + 1));
      end = NULL;
      a = strtoull (line, &end, 10);
      app->numbers[app->num_numbers] = a;
      app->num_numbers++;
    }

  free (line);
  return;
}

int
fituvalu_sq_seq_finder (struct fv_app_sq_seq_finder_t *app, FILE *out)
{
  int start = 0;
  char *end = NULL;
  unsigned long long num, root;

  num = strtoull (app->start, &end, 10);
  root = sqrtl (num);
  if (num == app->numbers[0])
    {
      app->count++;
      app->hits++;
      if (app->dump)
        fprintf (out, "%llu\n", app->numbers[0]);
      start = 1;
    }

  for (int i = start; i < app->num_numbers; i++)
    {
      long long int ret = app->numbers[i] - num;
      if (ret < 0)
        continue;
      else if (ret == 0)
        {
          app->count++;
          app->hits++;
          if (app->dump)
            fprintf (out, "%llu\n", app->numbers[i]);
        }
      else if (advance_to (app, &num, &root, app->numbers[i]))
        {
          if (app->dump)
            fprintf (out, "%llu\n", app->numbers[i]);
        }
    }

  if (!app->dump)
    {
      long double p = (long double)app->hits / (long double)app->count;
      fprintf (stdout, "%llu / %llu = %llf\n", app->hits, app->count, p);
    }
  return 0;
}

int
fituvalu_sq_seq_finder_depth (struct fv_app_sq_seq_finder_t *app, FILE *out)
{
  int pattern[app->depth];
  memset (pattern, 0, sizeof (pattern));
  app->num_args = app->depth;
  while (1)
    {
      int found = 0;
      for (int i = 0; i < app->depth; i++)
        {
          pattern[i]++;
          if (pattern[i] >= app->breadth)
            {
              pattern[i] = 0;
              if (i == app->depth - 1)
                {
                  found = 1;
                  break;
                }
            }
          else
            break;
        }
      if (found)
        break;
      for (int i = 0; i < app->depth; i++)
        fprintf (out, "%d, ", pattern[i]);
      fflush (out);
      app->args = pattern;
      app->hits = 0;
      app->count = 0;
      app->cur_arg = 0;
      fituvalu_sq_seq_finder (app, out);
    }
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_sq_seq_finder_t *app = (struct fv_app_sq_seq_finder_t *) state->input;
  switch (key)
    {
    case OPT_DUMP:
      app->dump = 1;
      break;
    case 's':
      app->start = arg;
      break;
    case 'd':
      app->depth = atoi (arg);
      break;
    case 'b':
      app->breadth = atoi (arg);
      break;
    case ARGP_KEY_ARG:
      app->args = realloc (app->args, (app->num_args  + 1) * sizeof (int));
      app->args[app->num_args] = atoi (arg);
      if (app->args[app->num_args] < 0)
        argp_error (state, "arguments must be zero or greater.");
      app->num_args++;
      break;
    case ARGP_KEY_NO_ARGS:
      if (app->depth == 0)
        argp_error (state, "missing argument.");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->dump && app->depth)
        argp_error (state, "cannot use --dump with --depth");
      if (app->num_args < 1 && app->depth == 0)
        argp_error (state, "missing argument.");
      if (app->depth == 0)
        {
          int num_non_zero = 0;
          for (int i = 0; i < app->num_args; i++)
            {
              if (app->args[i])
                num_non_zero++;
            }
          if (num_non_zero == 0)
            argp_error (state, "need at least one argument over zero.");
        }
      break;
    }

  return 0;
}

static struct argp_option
options[] =
{
  { "depth", 'd', "NUM", OPTION_HIDDEN, "Set the depth of the search to NUM"},
  { "breadth", 'b', "NUM", OPTION_HIDDEN, "Set the breadth of the search to NUM"},
  { "start", 's', "NUM", 0, "Begin at NUM instead of 1"},
  { "dump", OPT_DUMP, 0, 0, "Show the matching sequence instead of percentage"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "NUM...",
  "Try to find a pattern in a sequence of squares.  Read in a sequence of perfect squares from the standard input, and calculate the percentage that match the sequence represented by advancing forward by NUM... squares.\vFor example, arguments 5 1 means advance five squares forward then advance one square forward, then five again, etc.  The squares on the standard input must be in ascending order.  Running `sq-seq 100 | sq-seq-finder 2' hits every other perfect square for a 50% result.",
  0
};


static void
free_numbers (struct fv_app_sq_seq_finder_t *app)
{
  free (app->numbers);
}

int
main (int argc, char **argv)
{
  struct fv_app_sq_seq_finder_t app;
  memset (&app, 0, sizeof (app));
  app.start = "1";
  app.breadth = 10;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  read_in_numbers (&app, stdin);
  int ret = 0;
  if (app.depth)
    ret = fituvalu_sq_seq_finder_depth (&app, stdout);
  else
    ret = fituvalu_sq_seq_finder (&app, stdout);
  free_numbers (&app);
  return ret;
}
