/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"

struct fv_app_msd_magic_square_t
{
  int num_filters;
  int *filters;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
};

static int
mpz_msd (mpz_t ret, mpz_t num)
{
  char *buf = malloc (mpz_sizeinbase (num, 10) + 2);
  mpz_get_str (buf, 10, num);
  char *letter = buf;
  if (*letter == '-')
    letter++;
  char n[2];
  n[0] = *letter;
  n[1] = '\0';
  mpz_set_str (ret, n, 10);
  free (buf);
}

int
fituvalu_msd_magic_square (struct fv_app_msd_magic_square_t *app, FILE *stream)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3][3];

  int i, j;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  while (1)
    {
      read = app->read_square (stream, &a, &line, &len);
      if (read == -1)
        break;

      if (app->num_filters)
        {
          mpz_t num;
          mpz_init (num);
          int count = 0;
          for (int k = 0; k < app->num_filters; k++)
            {
              for (i = 0; i < 3; i++)
                for (j = 0; j < 3; j++)
                  {
                    mpz_msd (num, a[i][j]);
                    if (mpz_cmp_ui (num, app->filters[k]) == 0)
                      count++;
                  }
            }
          mpz_clear (num);
          if (count == 9)
            app->display_square (a, stdout);
        }
      else
        {
          for (i = 0; i < 3; i++)
            for (j = 0; j < 3; j++)
              mpz_msd (a[i][j], a[i][j]);
          app->display_square (a, stdout);
        }
    }

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_msd_magic_square_t *app = (struct fv_app_msd_magic_square_t *) state->input;
  switch (key)
    {
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case 'f':
        {
          char *end = NULL;
          int num = strtoul (arg, &end, 10);
          if (num >= 0)
            {
              app->filters =
                realloc (app->filters , (app->num_filters + 1) * sizeof (int));
              app->filters[app->num_filters] = num;
              app->num_filters++;
            }
          else
            argp_error (state, "%s is an invalid value for -f", arg);
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "filter", 'f', "NUM", 0, "Show squares where all cells have NUM for a most significant digit"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "",
  "Accept 3x3 magic squares from the standard input, and show the most significant digits.\vThe nine values must be separated by a comma and terminated by a newline.  This program helps in checking if our magic squares obey Benford's Law.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_msd_magic_square_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.read_square = read_square_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  is_magic_square_init ();
  return fituvalu_msd_magic_square (&app, stdin);
}
