/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

#define MSIZE 3

struct fv_app_mine_2sq_progressions_t
{
  int (*read_numbers)(FILE *, mpz_t *, int size, char **, size_t *);
  FILE *out;
};

int
compar (const void *left, const void *right)
{
  const mpz_t *l = left;
  const mpz_t *r = right;
  return mpz_cmp (*l, *r);
}

//http://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
void combinationUtil (mpz_t arr[], mpz_t data[], int start, int end,
                      int index, int r, struct fv_app_mine_2sq_progressions_t *app);

void printCombination (mpz_t arr[], int n, int r, struct fv_app_mine_2sq_progressions_t *app)
{
  mpz_t data[r];
  for (int i = 0; i < r; i++)
    mpz_init (data[i]);
  combinationUtil (arr, data, 0, n-1, 0, r, app);
  for (int i = 0; i < r; i++)
    mpz_clear (data[i]);
}
 
void combinationUtil (mpz_t arr[], mpz_t data[], int start, int end,
                      int index, int r, struct fv_app_mine_2sq_progressions_t *app)
{
  if (index == r)
    {
      for (int j = 0; j < r; j++)
        {
          display_textual_number_no_newline (data[j], app->out);
          fprintf (app->out, ", ");
        }
      fprintf (app->out, "\n");
      return;
    }

  for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
      mpz_set (data[index], arr[i]);
      combinationUtil(arr, data, i + 1, end, index + 1, r, app);
    }
}

static void
check (mpz_t one, mpz_t two, mpz_t three, mpz_t diff, struct fv_app_mine_2sq_progressions_t *app)
{
  display_textual_number_no_newline (one, app->out);
  fprintf (app->out, ", ");
  display_textual_number_no_newline (two, app->out);
  fprintf (app->out, ", ");
  display_textual_number_no_newline (three, app->out);
  fprintf (app->out, ", ");
  fprintf (app->out, "\n");
}

void combination2Util (mpz_t arr[], mpz_t data[], int start, int end,
                      int index, int r, struct fv_app_mine_2sq_progressions_t *app);

void print2Combination (mpz_t arr[], int n, int r, struct fv_app_mine_2sq_progressions_t *app)
{
  mpz_t data[r];
  for (int i = 0; i < r; i++)
    mpz_init (data[i]);
  combination2Util (arr, data, 0, n-1, 0, r, app);
  for (int i = 0; i < r; i++)
    mpz_clear (data[i]);
}

void combination2Util (mpz_t arr[], mpz_t data[], int start, int end,
                      int index, int r, struct fv_app_mine_2sq_progressions_t *app)
{
  if (index == r)
    {
      //we have exactly 2 in data.
      //so we check to see if another square exists on either side.
      mpz_t diff, next;
      mpz_inits (diff, next, NULL);
      mpz_sub (diff, data[1], data[0]);
      mpz_abs (diff, diff);
      int match = mpz_cmp_ui (diff, 0) == 0;
      if (match)
        {
          mpz_clears (diff, next, NULL);
          return;
        }
      if (mpz_cmp (data[1], data[0]) > 0)
        {
          mpz_add (next, data[1], diff);
          if (mpz_perfect_square_p (next))
            check (data[0], data[1], next, diff, app);
          mpz_sub (next, data[0], diff);
          if (mpz_perfect_square_p (next))
            check (next, data[0], data[1], diff, app);
        }
      else
        {
          mpz_add (next, data[0], diff);
          if (mpz_perfect_square_p (next))
            check (data[1], data[0], next, diff, app);
          mpz_sub (next, data[1], diff);
          if (mpz_perfect_square_p (next))
            check (next, data[1], data[0], diff, app);
        }

      mpz_clears (diff, next, NULL);

      return;
    }

  for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
      mpz_set (data[index], arr[i]);
      combination2Util(arr, data, i + 1, end, index + 1, r, app);
    }
}

static void
mine_progression (mpz_t vec[], int size, struct fv_app_mine_2sq_progressions_t *app)
{
  int count = 0;
  mpz_t progression[size];

  for (int i = 0; i < size; i++)
    {
      if (mpz_perfect_square_p (vec[i]))
        {
          mpz_init_set (progression[count], vec[i]);
          count++;
        }
    }
  if (count < 2)
    {
      for (int i = 0; i < count; i++)
        mpz_clear (progression[i]);
      return;
    }

  qsort (progression, count, sizeof (mpz_t), compar);

  printCombination (progression, count, 2, app);

  for (int i = 0; i < count; i++)
    mpz_clear (progression[i]);
  return;
}

static int
mine_2sq_progressions (struct fv_app_mine_2sq_progressions_t *app, FILE *in)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  mpz_t vec[MSIZE];
  mpz_t orig[MSIZE];
  for (int i = 0; i < MSIZE; i++)
    mpz_inits (vec[i], orig[i], NULL);
  while (1)
    {
      read = app->read_numbers (in, vec, MSIZE, &line, &len);
      if (read == -1)
        break;
      mine_progression (vec, MSIZE, app);
    }

  for (int i = 0; i < MSIZE; i++)
    mpz_clears (vec[i], orig[i], NULL);

  if (line)
    free (line);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_mine_2sq_progressions_t *app = (struct fv_app_mine_2sq_progressions_t *) state->input;
  switch (key)
    {
    case 'i':
      app->read_numbers = binary_read_numbers_from_stream;
      break;
    }
  return 0;
}

int
fituvalu_mine_2sq_progressions (struct fv_app_mine_2sq_progressions_t *app, FILE *in)
{
  return mine_2sq_progressions (app, in);
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept three square progressions and output every pair of squares.\vThe numbers that comprise the three square progressions must be separated by a comma and terminated by a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_mine_2sq_progressions_t app;
  memset (&app, 0, sizeof (app));
  app.read_numbers = read_numbers_from_stream;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_mine_2sq_progressions (&app, stdin);
}
