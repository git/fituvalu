/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#ifndef MAGIC_SQUARE_UTIL_H
#define MAGIC_SQUARE_UTIL_H
#include <stdio.h>
#include <gmp.h>
#include <stdint.h>
#define SIZE 9
void display_nine_record (mpz_t *progression, FILE *out);
void display_three_record (mpz_t *progression, FILE *out);
void display_three_record_with_root (mpz_t *progression, mpz_t *root, FILE *out);
void display_binary_three_record_with_root (mpz_t *progression, mpz_t *root, FILE *out);
void display_four_record (mpz_t *progression, FILE *out);
void display_binary_four_record (mpz_t *progression, FILE *out);
void display_six_record (mpz_t *progression, FILE *out);
void display_binary_six_record (mpz_t *progression, FILE *out);
void display_binary_nine_record (mpz_t *progression, FILE *out);
void display_binary_two_record (mpz_t *one, mpz_t *two, FILE *out);
void display_two_record (mpz_t *one, mpz_t *two, FILE *out);
void display_square_record (mpz_t s[3][3], FILE *out);
void display_binary_square_record (mpz_t s[3][3], FILE *out);
void is_magic_square_init ();
int is_magic_square (mpz_t a[3][3], int diag);
int is_distinct (mpz_t a[3][3]);
void is_magic_square_fini ();
int count_squares (mpz_t a[3][3]);
int read_square_from_stream (FILE *stream, mpz_t (*a)[3][3], char **line, size_t *len);
int binary_read_square_from_stream (FILE *stream, mpz_t (*a)[3][3], char **s, size_t *len);
int read_numbers_from_stream (FILE *stream, mpz_t *a, int size, char **line, size_t *len);
int read_tabbed_numbers_from_stream (FILE *stream, mpz_t *a, int size, char **line, size_t *len);
int binary_read_numbers_from_stream (FILE *stream, mpz_t *a, int size, char **line, size_t *len);
void small_read_square_and_run (FILE *, void (*)(unsigned long long, unsigned long long, unsigned long long, void (*)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void*), void *), void (*)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), unsigned long long, unsigned long long, void*);
void read_square_and_run (FILE *, void (*)(mpz_t,mpz_t, mpz_t, unsigned long long, void (*)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void*), void (*)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), mpz_t, mpz_t, unsigned long long, void*);
void binary_read_square_and_run (FILE *, void (*iterfunc)(mpz_t, mpz_t, mpz_t, unsigned long long, void (*)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void*), void*), void (*check)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void*), mpz_t, mpz_t, unsigned long long, void*);
void loop_and_run (void (*)(mpz_t, mpz_t, mpz_t, unsigned long long, void (*)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *), void (*)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), mpz_t, mpz_t, unsigned long long, void *);
void small_loop_and_run (void (*func)(unsigned long long, unsigned long long, unsigned long long, void (*)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *), void (*)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), unsigned long long start, unsigned long long finish, void *data);
int small_is_square (long long num);

void fwd_4sq_progression1 (mpz_t i, mpz_t start, mpz_t finish, unsigned long long incr, void (*func)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *);
void fwd_4sq_progression2 (mpz_t i, mpz_t start, mpz_t finish, unsigned long long incr, void (*func)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *);
void fwd_4sq_progression3 (mpz_t i, mpz_t start, mpz_t finish, unsigned long long incr, void (*func)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *);
void fwd_4sq_progression4 (mpz_t i, mpz_t start, mpz_t finish, unsigned long long incr, void (*func)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *);
void fwd_4sq_progression5 (mpz_t i, mpz_t start, mpz_t finish, unsigned long long incr, void (*func)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *out);
void fwd_4sq_progression6 (mpz_t i, mpz_t start, mpz_t finish, unsigned long long incr, void (*func)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *out);
void rev_4sq_progression1 (mpz_t i, mpz_t start, mpz_t finish, unsigned long long incr, void (*func)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *);
void optimized_fwd_4sq_progression1 (mpz_t i, mpz_t start, mpz_t finish, unsigned long long incr, void (*func)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *out);

void small_fwd_4sq_progression1 (unsigned long long i, unsigned long long start, unsigned long long finish, void (*func)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *);
void small_fwd_4sq_progression2 (unsigned long long i, unsigned long long start, unsigned long long finish, void (*func)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *);
void small_fwd_4sq_progression3 (unsigned long long i, unsigned long long start, unsigned long long finish, void (*func)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *);
void small_fwd_4sq_progression4 (unsigned long long i, unsigned long long start, unsigned long long finish, void (*func)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *);
void small_fwd_4sq_progression5 (unsigned long long i, unsigned long long start, unsigned long long finish, void (*func)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *);
void small_fwd_4sq_progression6 (unsigned long long i, unsigned long long start, unsigned long long finish, void (*func)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void *), void *);
void small_rev_4sq_progression1 (unsigned long long i, unsigned long long start, unsigned long long finish, void (*func)(unsigned long long *, unsigned long long, unsigned long long, unsigned long long, unsigned long long, void*), void *);

void four_square_to_nine_number_step_progression1 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_step_progression2 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_step_progression3 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_step_progression4 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_reverse_step_progression1 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_fulcrum_progression1 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_fulcrum_progression2 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_fulcrum_progression3 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_fulcrum_progression4 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_fulcrum_progression5 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);
void four_square_to_nine_number_reverse_fulcrum_progression1 (mpz_t *progression, mpz_t one, mpz_t two, mpz_t three, mpz_t four);

int count_squares_in_step_progression1 (mpz_t *progression);
int count_squares_in_step_progression2 (mpz_t *progression);
int count_squares_in_step_progression3 (mpz_t *progression);
int count_squares_in_step_progression4 (mpz_t *progression);
int count_squares_in_reverse_step_progression1 (mpz_t *progression);
int count_squares_in_fulcrum_progression1 (mpz_t *progression);
int count_squares_in_fulcrum_progression2 (mpz_t *progression);
int count_squares_in_fulcrum_progression3 (mpz_t *progression);
int count_squares_in_fulcrum_progression4 (mpz_t *progression);
int count_squares_in_fulcrum_progression5 (mpz_t *progression);
int count_squares_in_reverse_fulcrum_progression1 (mpz_t *progression);

typedef enum four_square_progression_enum_t {
  FWD_PROGRESSION_1,
  FWD_PROGRESSION_2,
  FWD_PROGRESSION_3,
  FWD_PROGRESSION_4,
  FWD_PROGRESSION_5,
  FWD_PROGRESSION_6,
  REV_PROGRESSION_1,
}four_square_progression_enum_t;

typedef struct four_square_progression_t {
  four_square_progression_enum_t kind;
  char *name;
  void (*func) (mpz_t, mpz_t, mpz_t, unsigned long long, void (*)(mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t, void *), void *);
  char *timeline;
} four_square_progression_t;

char * generate_list_of_four_square_progression_types ();
char * generate_list_of_four_square_progression_timelines ();
four_square_progression_t * lookup_four_square_progression_by_name (char *);
four_square_progression_t * lookup_four_square_progression_by_kind (four_square_progression_enum_t);

typedef struct nine_progression_t {
  char *name;
  four_square_progression_enum_t four_square_progression;
  void (*progfunc) (mpz_t *, mpz_t, mpz_t, mpz_t, mpz_t);
  int (*countfunc)(mpz_t *);
  char *timeline;
} nine_progression_t;

char * generate_list_of_nine_progression_types ();
nine_progression_t * lookup_nine_progression_by_name (char *name);
char * generate_list_of_nine_progression_timelines ();
extern four_square_progression_t four_square_progressions[];
extern nine_progression_t nine_progressions[];

int small_morgenstern_search (unsigned long long max, FILE *in, void (*search) (unsigned long long, unsigned long long, unsigned long long, unsigned long long, void*), void *data);
int reduce_three_square_progression (mpz_t *progression);
int binary_read_two_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
int read_two_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
int binary_read_one_number_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
int read_one_number_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
int binary_read_three_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
int read_three_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
int read_four_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
int binary_read_four_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
int read_nine_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
int binary_read_nine_numbers_from_stream (FILE *stream, mpz_t *a, char **line, size_t *len);
void display_binary_three_record (mpz_t *progression, FILE *out);
void morgenstern_search_dual (FILE *in1, FILE *in2, void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *),int threads, void* (*init)(), void (*destroy)(void*),void *data);
void morgenstern_search_dual_binary (FILE *in1, FILE *in2, void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void*, void *), int threads, void* (*init)(), void (*destroy)(void*),void *data);
void display_textual_number (mpz_t *i, FILE *out);
void display_textual_number_no_newline (mpz_t i, FILE *out);
void display_binary_number (mpz_t *i, FILE *out);
void disp_binary_record (mpz_t *vec, int size, FILE *out);
void disp_record (mpz_t *vec, int size, FILE *out);
int fv_getline (char **line, size_t *len, FILE *stream);
int fv_getdelim (char **line, size_t *len, int delim, FILE *stream);
void reduce_square (mpz_t a[3][3]);
void morgenstern_search_dual_binary_mem (FILE *in1, FILE *in2, void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *), int threads, void * (*init)(), void (*destroy)(void*), void *data);
void morgenstern_search_dual_mem (FILE *in1, FILE *in2, void (*search) (mpz_t, mpz_t, mpz_t, mpz_t, void *, void *), int threads, void* (*init)(), void (*destroy)(void*), void *data);
void morgenstern_search_dual_mem_int (FILE *in1, FILE *in2, void (*search_int) (unsigned long, unsigned long, unsigned long, unsigned long, void *, void *), int num_threads, void* (*init)(), void (*destroy)(void*), void *data);
void morgenstern_search_dual_int (FILE *in1, FILE *in2, void (*search_int) (unsigned long, unsigned long, unsigned long, unsigned long, void *, void *), int threads, void* (*init)(), void (*destroy)(void*),void *data);

struct morgenstern_per_thread_data_t
{
  mpz_t a[3][3];
  mpz_t x1, _y1, z1, m12, n12, x2, y2, z2, m22, n22,
        yx1dif, yx1sum, yx2dif, yx2sum;
  int prev_type;
};
void * morgenstern_per_thread_init();
void morgenstern_per_thread_destroy (void *p);
void morgenstern_precalc (mpz_t m1, mpz_t n1, mpz_t m2, mpz_t n2, struct morgenstern_per_thread_data_t *pt);
void morgenstern_precalc_int (unsigned long m1, unsigned long n1, unsigned long m2, unsigned long n2, struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_1_calc_step_1b (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_1_calc_step_1 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_1_calc_step_2 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_2_calc_step_1 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_2_calc_step_2 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_3_calc_step_1 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_3_calc_step_2 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_4_calc_step_1 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_4_calc_step_2 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_5_calc_step_1 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_5_calc_step_2 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_6_calc_step_1 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_6_calc_step_2 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_7_calc_step_1 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_7_calc_step_2 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_8_calc_step_1 (struct morgenstern_per_thread_data_t *pt);
void morgenstern_type_8_calc_step_2 (struct morgenstern_per_thread_data_t *pt);
int morgenstern_type_1_filter (mpz_t a[3][3], int num_squares);
int morgenstern_type_2_filter (mpz_t a[3][3], int num_squares);
int morgenstern_type_3_filter (mpz_t a[3][3], int num_squares);
int morgenstern_type_4_filter (mpz_t a[3][3], int num_squares);
int morgenstern_type_5_filter (mpz_t a[3][3], int num_squares);
int morgenstern_type_6_filter (mpz_t a[3][3], int num_squares);
int morgenstern_type_7_filter (mpz_t a[3][3], int num_squares);
int morgenstern_type_8_filter (mpz_t a[3][3], int num_squares);
void display_threesq (mpz_t a, mpz_t b, mpz_t c, FILE *out);
void binary_display_threesq (mpz_t a, mpz_t b, mpz_t c, FILE *out);
void generate_progression_kahn_kwong (mpz_t *progression, int high);
void rotate_square (mpz_t (*result)[3][3]);
void flip_square_horizontally (mpz_t (*result)[3][3]);
void flip_square_vertically (mpz_t (*result)[3][3]);
void search_61_3sq (mpz_t *progression, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void*, mpz_t (*)[3][3]), void *data);
void search_64_3sq (mpz_t *progression, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data);
void search_65_3sq (mpz_t *progression, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data);
void search_67_3sq (mpz_t *progression, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data);
void search_68_3sq (mpz_t *progression, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data);
void search_69_3sq (mpz_t *progression, mpz_t distance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data);
void search_610_3sq (mpz_t *progression, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data);
void search_612_3sq (mpz_t *progression, mpz_t twodistance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data);
void search_615_3sq (mpz_t *progression, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data);
void search_616_3sq (mpz_t *progression, mpz_t twodistance, mpz_t *b, mpz_t (*s)[3][3], int check, void (*func)(void *, mpz_t (*)[3][3]), void *data);
void magic_square_get_magic_number (mpz_t a[3][3], mpz_t *m);
void magic_square_get_largest_number (mpz_t a[3][3], mpz_t *m);
void magic_square_get_lowest_number (mpz_t a[3][3], mpz_t *low);
void display_floating_point_number (mpf_t num, FILE *out);
int square_get_type (mpz_t (*a)[3][3], int *num_squares);
int get_type_from_signature (int signature, int num_squares);
int get_type_index (int i);
int map_from_bremner (int num_squares, int type);
int get_last_digit (mpz_t n);
int mpz_cmp_str (mpz_t i, char *str);
int read_ull_numbers (FILE *stream, unsigned long long int *a, int size, char **line, size_t *len);
void search_61_3sq_small (unsigned long long int *progression, unsigned long long int distance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
void search_64_3sq_small (unsigned long long int *progression, unsigned long long int distance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
void search_65_3sq_small (unsigned long long int *progression, unsigned long long int distance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
void search_67_3sq_small (unsigned long long int *progression, unsigned long long int distance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
void search_68_3sq_small (unsigned long long int *progression, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
void search_69_3sq_small (unsigned long long int *progression, unsigned long long int distance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
void search_610_3sq_small (unsigned long long int *progression, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
void search_612_3sq_small (unsigned long long int *progression, unsigned long long int twodistance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
void search_615_3sq_small (unsigned long long int *progression, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
void search_616_3sq_small (unsigned long long int *progression, unsigned long long int twodistance, unsigned long long int *b, unsigned long long int (*s)[3][3], int check, void (*func)(void *, char *, unsigned long long int (*s)[3][3]), void *app);
struct mpz_pair
{
  mpz_t num[2];
};
void get_coprime_pairs (mpz_t *a/* an array of SIZE */, int allow_negative, struct mpz_pair **pairs, int *num_pairs);
int square_get_subtype (mpz_t (*s)[3][3], int num_squares, int type);
void rotate_six (mpz_t (*a)[3][3], int type);
int get_last_three_digits (mpz_t n);
double mpz_divide (mpz_t numerator, mpz_t denominator);
int read_3sq_from_stream (FILE *stream, mpz_t *lo, mpz_t *c, mpz_t *hi, char **line, size_t *len);
int binary_read_3sq_from_stream (FILE *stream, mpz_t *lo, mpz_t *c, mpz_t *hi, char **line, size_t *len);
void mpf_mpz_divide (mpf_t r, mpz_t numerator, mpz_t denominator);
void mpf_mpz_multiply (mpz_t rop, mpz_t op, mpf_t c);

#define mpz_decr(X) (mpz_sub_ui (X, X, 1))
#define mpz_incr(X) (mpz_add_ui (X, X, 1))

struct fv_app_log_t
{
  char fname[256];
  int num_lines;
  int max_num_lines;
  time_t last_log;
  int timeout;
};
void fv_init_log (struct fv_app_log_t *log, char *name);
void fv_update_log (struct fv_app_log_t *log, char *line);
void fv_update_log_ull (struct fv_app_log_t *log, unsigned long long int num);
void fv_update_log_mpz (struct fv_app_log_t *log, mpz_t *num);
typedef int SubTypeArrayType[9][2];
SubTypeArrayType* get_subtypes (int type, int *num_subtypes);
#endif
