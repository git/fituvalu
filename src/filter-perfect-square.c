/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <argp.h>
#include <gmp.h>
#include <string.h>
#include "magicsquareutil.h"
#define OPT_FF -311

struct fv_app_perfect_square_t
{
  int fourn1;
  int invert;
  int (*read_number) (FILE *, mpz_t *, char **, size_t *);
  void (*write_number) (mpz_t *i, FILE *out);
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_perfect_square_t *app = (struct fv_app_perfect_square_t *) state->input;
  switch (key)
    {
    case OPT_FF:
      app->fourn1 = 2;
      break;
    case '4':
      app->fourn1 = 1;
      break;
    case 'v':
      app->invert = 1;
      break;
    case 'o':
      app->write_number = display_binary_number;
      break;
    case 'i':
      app->read_number = binary_read_one_number_from_stream;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static int
check_square (struct fv_app_perfect_square_t *app, mpz_t num)
{
  if (!mpz_perfect_square_p (num))
    return 0;
  if (app->fourn1)
    {
      mpz_t r, i;
      mpz_inits (r, i, NULL);
      mpz_sqrt (r, num);
      mpz_sub_ui (i, r, 1);
      int ret = mpz_mod_ui (r, i, 4);
      mpz_clears (r, i, NULL);
      if (ret == 0 && app->fourn1 == 1)
        return 1;

      if (app->fourn1 > 1)
        {
          mpz_inits (r, i, NULL);
          mpz_sqrt (r, num);
          mpz_sub_ui (i, r, 1);
          mpz_cdiv_q_ui (r, i, 4);
          mpz_sqrt (r, r);
          mpz_sub_ui (i, r, 1);
          ret = mpz_mod_ui (r, i, 4);
          mpz_clears (r, i, NULL);
          return ret == 0;
        }
     return 0;
    }
  return 1;
}

int
fituvalu_perfect_square (struct fv_app_perfect_square_t *app,  FILE *in, FILE *out)
{
  mpz_t num;
  mpz_init (num);
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  while (1)
    {
      read = app->read_number (in, &num, &line, &len);
      if (read == -1)
        break;
      if (app->invert && check_square (app, num) == 0)
        app->write_number (&num, out);
      else if (!app->invert && check_square (app, num))
        app->write_number (&num, out);
    }

  if (line)
    free (line);
  mpz_clear (num);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "invert", 'v', 0, 0, "Show the numbers that aren't perfect squares"},
  { "fourn1", '4', 0, 0, "Only show squares that are (4n+1)^2"},
  { "fourn1-fourn1", OPT_FF, 0, OPTION_HIDDEN, "Only show squares that are (4n+1)^2 and n is also (4n+1)^2"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept numbers from the standard input and display them if they're a perfect square.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_perfect_square_t app;
  memset (&app, 0, sizeof (app));
  argp_parse (&argp, argc, argv, 0, 0, &app);
  app.write_number = display_textual_number;
  app.read_number = read_one_number_from_stream;
  return fituvalu_perfect_square (&app, stdin, stdout);
}
