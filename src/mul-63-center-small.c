/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "magicsquareutil.h"

struct fv_app_mul_63_center_t
{
  int num_args;
  long long int num;
  FILE *infile;
  FILE *out;
  int in_binary;
  int out_binary;
  long double max_perc;
  unsigned long long int min;
  int debug;
  int nearby;
  int no_nearby;
  long long int max;
  long long int first_max;
};

static long long int
top (struct fv_app_mul_63_center_t *app, long long int num)
{
  long double p = app->max_perc / 100.0;
  long long int n = (long long int) LONG_MAX * p / 3 / num;
  return (n);
}

static void
search (struct fv_app_mul_63_center_t *app, unsigned long long num)
{
  unsigned long long min_center = app->min / 3;
  if (app->debug)
    fprintf (stderr, "%lld\n", num);
  unsigned long long i = top (app, num) * num;

  if (app->max > 0)
    i = app->max;
  if (app->first_max > 0)
    {
      i = app->first_max;
      app->first_max = 0;
    }
  if (app->debug)
    fprintf (stderr, "app max is %llu\n", app->max);
  if (app->debug)
    fprintf (stderr, "searching %llu down to %llu\n", i, min_center);

  while (i > min_center)
    {
      unsigned long long int j = i;
      if (!app->no_nearby)
        {
          unsigned long long k;
          switch (j % 10)
            {
            case 0: 
                {
                  k = j + 1;
                  if (app->out_binary)
                    fwrite (&k, sizeof (k), 1, app->out);
                  else
                    fprintf (app->out, "%llu\n", k);
                  fflush (app->out);
                }
              break;
            case 1: break;
            case 2:
                    {
                      k = j - 1;
                      if (app->out_binary)
                        fwrite (&k, sizeof (k), 1, app->out);
                      else
                        fprintf (app->out, "%llu\n", k);
                      fflush (app->out);
                    }
                  break;

            case 3: 
                    {
                      k = j + 2;
                      if (app->out_binary)
                        fwrite (&k, sizeof (k), 1, app->out);
                      else
                        fprintf (app->out, "%llu\n", k);
                      fflush (app->out);
                    }
                  break;
            case 4: 
                    {
                      k = j + 1;
                      if (app->out_binary)
                        fwrite (&k, sizeof (k), 1, app->out);
                      else
                        fprintf (app->out, "%llu\n", k);
                      fflush (app->out);
                    }
                  break;
            case 5: break;
            case 6: 
                    {
                      k = j-1;
                      if (app->out_binary)
                        fwrite (&k, sizeof (k), 1, app->out);
                      else
                        fprintf (app->out, "%llu\n", k);
                      fflush (app->out);
                    }
                  break;
            case 7: 
                    {
                      k = j - 2;
                      if (app->out_binary)
                        fwrite (&k, sizeof (k), 1, app->out);
                      else
                        fprintf (app->out, "%llu\n", k);
                      fflush (app->out);
                    }
                  break;
            case 8: 
                    {
                      k = j + 1;
                      if (app->out_binary)
                        fwrite (&k, sizeof (k), 1, app->out);
                      else
                        fprintf (app->out, "%llu\n", k);
                      fflush (app->out);
                    }
                  break;
            case 9: break;
            }
        }

      if (!app->nearby)
        {
          if (app->out_binary)
            fwrite (&j, sizeof (j), 1, app->out);
          else
            fprintf (app->out, "%llu\n", j);
        }
      fflush (app->out);

      i -= num;
    }
}


int
fituvalu_mul_63_center (struct fv_app_mul_63_center_t *app)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long rec[1];

  while (1)
    {
      if (app->in_binary)
        {
          size_t r = fread (rec, sizeof (unsigned long long), 1, app->infile);
          if (r == 0)
            break;
        }
      else
        {
          ssize_t read = read_ull_numbers (app->infile, rec, 1, &line, &len);
          if (read == -1)
            break;
        }
      search (app, rec[0]);

    }
  if (line)
    free (line);
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_mul_63_center_t *app = (struct fv_app_mul_63_center_t *) state->input;
  switch (key)
    {
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->out_binary = 1;
      break;
    case 'x':
        {
          char *end = NULL;
          app->max  = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0')
            argp_error (state, "invalid max value `%s'", arg);
        }
      break;
    case 'M':
        {
          char *end = NULL;
          app->max_perc  = strtold (arg, &end);
          if (end == NULL || *end != '\0')
            argp_error (state, "invalid max percent value `%s'", arg);
        }
      break;
    case 'd':
      app->debug = 1;
      break;
    case 'N':
      app->no_nearby = 1;
      break;
    case 'n':
      app->nearby = 1;
      break;
    case 'm':
        {
          char *end = NULL;
          app->min  = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0')
            argp_error (state, "invalid min value `%s'", arg);
        }
      break;
    case 'f':
        {
          char *end = NULL;
          app->first_max  = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0')
            argp_error (state, "invalid first max value `%s'", arg);
        }
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw long longs instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw long longs instead of text"},
  { "min", 'm', "NUM", 0, "Search downwards until magic number NUM"},
  { "max", 'M', "PERC", 0, "Start search at 2^63*PERC/100"},
  { "maximum", 'x', "NUM", OPTION_HIDDEN, "Start search at NUM"},
  { "debug", 'd', 0, 0, "Show extra information"},
  { "nearby", 'n', 0, OPTION_HIDDEN, "Only show nearby values"},
  { "no-nearby", 'N', 0, OPTION_HIDDEN, "Do not show nearby values"},
  { "first-max", 'f', "NUM", OPTION_HIDDEN, "Search downwards from magic number NUM but only the first record"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:3 given an existing center value.\n"
    "We multiply it by a square that takes it close 2^63, and then we march "
    "down the squares to find other potential center values.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_mul_63_center_t app;
  memset (&app, 0, sizeof (app));
  app.infile = stdin;
  app.out = stdout;
  app.min = 74000000000000L;
  app.max_perc = 100.0;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_mul_63_center (&app);
}
