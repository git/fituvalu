/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <argp.h>
#include <gmp.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct thread_data_t
{
  void *data;
};

struct fv_app_gen_63_center_t
{
  FILE *infile;
  int in_binary;
  int out_binary;
  int num_columns;
  mpz_t *divisors;
  int num_divisors;
  mpz_t *known;
  int num_known;
  int minimum_matching_divisors;
  int check_known_multiples;
  int quicker;
  int threads;
  FILE *out;
  int all;
  int factor;
  int high_prime;
  int low_prime;
  int fast;
};

static int
is_known_multiple (struct fv_app_gen_63_center_t *app, mpz_t *num)
{
  for (int i = 0; i < app->num_known; i++)
    {
      //precisely matching knowns go through
      if (mpz_cmp (*num, app->known[i]) <= 0)
        return 0;
      if (mpz_divisible_p (*num, app->known[i]))
        return 1;
    }
  return 0;
}

static int
prime_is_fournminusone (mpz_t i)
{
  mpz_t j, r;
  mpz_inits (j, r, NULL);
  mpz_sub_ui (j, i, 1);
  int ret = mpz_mod_ui (r, j, 4);
  mpz_clears (j, r, NULL);
  return ret == 0;
}

void
dump_num_to_stream (mpz_t *i, FILE *out)
{
  char *buf = malloc (mpz_sizeinbase (*i, 10) + 2);
  mpz_get_str (buf, 10, *i);
  fprintf (out, "%s", buf);
  free (buf);
}

static void
liner (struct fv_app_gen_63_center_t *app, FILE *stream, mpz_t *num)
{
  ssize_t fnread, nread;
  char *line = NULL;
  size_t len = 0;
  int count = 0;
  int high_prime = 0;
  int low_prime_exceeded = 0;
  int fast_passed = 0;
  int all_4n1 = 1;
  mpz_t root, p, prev, r;
  if (app->threads > 1)
    pthread_mutex_lock (&read_lock);
  fnread = getdelim (&line, &len, ':', stream);
  if (fnread == -1)
    {
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);
      mpz_set_si (*num, -1);
      return;
    }

  char *colon = strchr (line, ':');
  if (colon)
    *colon = ' ';
  mpz_set_str (*num, line, 10);
  nread = getline (&line, &len, stream);
  if (app->threads > 1)
    pthread_mutex_unlock (&read_lock);
  if (nread == -1)
    {
      mpz_set_si (*num, -1);
      return;
    }

    {
      mpz_inits (root, p, prev, r, NULL);
      if (app->high_prime || app->low_prime)
        mpz_sqrt (root, *num);

      mpz_mod_ui (r, *num, 10);
      int imod10 = mpz_get_ui (r);
      mpz_mod_ui (r, *num, 25);
      int imod25 = mpz_get_ui (r);

      int tw, m24 =0, m3 = 0;
      tw = imod10 == 5 && imod25 != 0;
      if (!tw)
        {
          int crap = mpz_mod_ui (r, *num, 24);
          int imod24 = mpz_get_ui (r);
          m24 = imod24 != 1;
        }
      if (!tw && !m24)
        {
          mpz_mod_ui (r, *num, 3);
          int imod3 = mpz_get_ui (r);
          m3 = imod3 == 0;
        }

      if (!tw && !m24 && !m3)
        {
          char *s1 = NULL;
          size_t s1len = 0;

          FILE *fp = fmemopen (line + 1, strlen (line), "r");
          while ((nread = getdelim (&s1, &s1len, ' ', fp)) != -1) 
            {
              mpz_set_str (p, s1, 10);
              if (mpz_cmp (prev, p) != 0)
                {
                  if (prime_is_fournminusone (p))
                    {
                      if (app->high_prime)
                        {
                          if (mpz_cmp (p, root) > 0 ||
                              mpz_cmp (p, app->divisors[app->num_divisors-1]) > 0)
                            high_prime = 1;
                        }
                      if (app->low_prime)
                        {
                          if (mpz_cmp (p, root) > 0 ||
                              mpz_cmp (p, app->divisors[app->num_divisors-1]) > 0)
                            {
                              low_prime_exceeded = 1;
                            }
                        }
                      count++;
                      if (app->fast)
                        {
                          if (count == app->minimum_matching_divisors)
                            {
                              if (mpz_cmp_ui (p, 1800) < 0)
                                fast_passed = 1;
                            }
                        }
                    }
                  else // a non pythagorean prime, all bets are OFF kimosabe
                    {
                      all_4n1 = 0;
                      count = 0;
                      break;
                    }
                }

              size_t mlen = strlen (s1);
              if (mlen > 0)
                {
                  if (s1[mlen-1] == '\n')
                    break;
                }
              mpz_set (prev, p);
            }
          free (s1);
          fclose (fp);
        }
      mpz_clears (root, p, prev, r, NULL);
    }
  free (line);

  if (app->fast)
    {
      if (count >= app->minimum_matching_divisors && fast_passed && all_4n1)
        ;
      else
        mpz_set_ui (*num, 0);
    }
  else if (app->high_prime)
    {
      if (count >= app->minimum_matching_divisors && high_prime && all_4n1)
        ;
      else
        mpz_set_ui (*num, 0);
    }
  else if (app->low_prime)
    {
      if (count >= app->minimum_matching_divisors && !low_prime_exceeded && all_4n1)
        ;
      else
        mpz_set_ui (*num, 0);
    }
  else
    {
      if (count >= app->minimum_matching_divisors && all_4n1)
        ;
      else
        mpz_set_ui (*num, 0);
    }
}
static void*
process_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_gen_63_center_t *app =
    (struct fv_app_gen_63_center_t *) param->data;
  if (app->factor)
    {
      mpz_t retval;
      mpz_init (retval);
      while (1)
        {
          liner (app, app->infile, &retval);
          if (mpz_cmp_ui (retval, 0) > 0)
            {
              pthread_mutex_lock (&display_lock);

              if (app->out_binary)
                display_binary_number (&retval, app->out);
              else
                {
                  dump_num_to_stream (&retval, app->out);
                  fprintf (app->out, "\n");
                }
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
          else if (mpz_cmp_si (retval, -1) == 0)
            break;
        }
      mpz_clear (retval);
      return NULL;
    }
  ssize_t read;
  char *line = NULL;
  char *orig_line;
  size_t len = 0;
  char *sav;
  mpz_t i, k, limit, r;
  mpz_inits (i, k, limit, r, NULL);
  int count = 0;
  while (1)
    {
      int stop = 0;
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);
      if (app->in_binary)
        {
          read = mpz_inp_raw (i, app->infile);
          if (!read)
            stop = 1;
        }
      else
        {
          read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            stop = 1;
          else
            mpz_set_str (i, line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);
      if (stop)
        break;
      mpz_mod_ui (r, i, 24);
      if (mpz_cmp_ui (r, 1) != 0)
        continue;
      if (mpz_divisible_ui_p (i, 3))
        continue;
      mpz_mod_ui (r, i, 10);
      if (mpz_cmp_ui (r, 3) == 0 || mpz_cmp_ui (r, 7) == 0)
        continue;
      else if (mpz_cmp_ui (r, 5) == 0)
        {
          mpz_mod_ui (r, i, 25);
          if (mpz_cmp_ui (r, 0) != 0)
            continue;
        }
      if (mpz_perfect_square_p (i))
        continue;
      if (app->all)
        mpz_cdiv_q_ui (limit, i, 5);
      else
        mpz_sqrt (limit, i);
      count = 0;
      int next = 0;
      mpz_set (k, i);
      for (int j = 0; j < app->num_divisors; j++)
        {
          // a way to short-circuit some probably useless checks
          if (app->quicker && j > 2247)
            break;
          if (mpz_cmp (app->divisors[j], limit) > 0)
            break;
          if (mpz_divisible_p (k, app->divisors[j]))
            {
              if (!prime_is_fournminusone (app->divisors[j]))
                {
                  next = 1;
                  break;
                }
              mpz_div (k, k, app->divisors[j]);
              count++;
              if (count >= app->minimum_matching_divisors)
                break;
              if (mpz_cmp (k, app->divisors[j]) <= 0)
                break;
            }
        }
      if (next)
        {
          next = 0;
          continue;
        }
      //our static list ran out, let's just use probabilistic primes.
      if (mpz_cmp (app->divisors[app->num_divisors-1], limit) <= 0 &&
          count < app->minimum_matching_divisors)
        {
          mpz_t p;
          mpz_init (p);
          mpz_set (p, app->divisors[app->num_divisors-1]);
          //mpz_set_ui (p, 3);
          //printf ("start is %d\n", time (NULL));

         /* 
              fprintf (stderr,"doing ");
                dump_num_to_stream (&p, stderr);
              fprintf (stderr," to ");
                dump_num_to_stream (&limit, stderr);
              fprintf (stderr,"\n");
              */
          while (mpz_cmp (p, limit) <= 0)
            {
              if (mpz_divisible_p (k, p))
                {
                  if (!prime_is_fournminusone (p))
                    {
                      next = 1;
                      break;
                    }
                  mpz_div (k, k, p);
                  if (prime_is_fournminusone (p))
                    {
                      count++;
                      if (count >= app->minimum_matching_divisors)
                        break;
                    }
                  if (mpz_cmp (k, p) <= 0)
                    break;
                }
              mpz_nextprime (p, p);
            }
          //printf ("end is %d\n", time (NULL));
          mpz_clear (p);
          if (next)
            {
              next = 0;
              continue;
            }
        }
      if (count < app->minimum_matching_divisors)
        continue;

      if (!is_known_multiple (app, &i) || !app->check_known_multiples)
        {
          pthread_mutex_lock (&display_lock);
          if (app->out_binary)
            display_binary_number (&i, app->out);
          else
            {
              if (app->in_binary)
                display_binary_number (&i, app->out);
              else
                fprintf (app->out, "%s", line);
            }
          pthread_mutex_unlock (&display_lock);
        }
    }
  mpz_clears (i, k, limit, r, NULL);
  if (line)
    free (line);
  return NULL;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_gen_63_center_t *app = (struct fv_app_gen_63_center_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'F':
      app->fast = 1;
      app->factor = 1;
      break;
    case 'S':
      app->low_prime = 1;
      app->factor = 1;
      break;
    case 'H':
      app->high_prime = 1;
      app->factor = 1;
      break;
    case 'f':
      app->factor = 1;
      break;
    case 'a':
      app->all = 1;
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case 'q':
      app->quicker = 1;
      break;
    case 'm':
        {
          long long n = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0' || n <= 0)
            argp_error (state, "invalid value `%s' for --match option", arg);
          app->minimum_matching_divisors = n;
        }
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->out_binary = 1;
      break;
    case ARGP_KEY_ARG:
      app->infile = fopen (arg, "r");
      if (app->infile == NULL)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      break;
    }
  return 0;
}

extern char *known_63_centers[];
static void
load_known_centers (struct fv_app_gen_63_center_t *app)
{
  mpz_t num;
  mpz_init (num);
  char **n = known_63_centers;
  while (*n != 0)
    {
      mpz_set_str (num, *n, 10);
      if (mpz_cmp_ui (num, 2) != 0 && mpz_cmp_ui (num, 3) != 0)
        {
          app->known =
            realloc (app->known, (app->num_known + 1) * sizeof (mpz_t));
          mpz_init_set (app->known[app->num_known], num);
          app->num_known++;
        }
      *n++;
    }
  mpz_clear (num);
}

extern char *prime_numbers[];
static void
load_primes (struct fv_app_gen_63_center_t *app)
{
  mpz_t num;
  mpz_init (num);
  char **n = prime_numbers;
  while (*n != 0)
    {
      mpz_set_str (num, *n, 10);
      if (mpz_cmp_ui (num, 2) != 0 && mpz_cmp_ui (num, 3) != 0)
        {
          app->divisors =
            realloc (app->divisors, (app->num_divisors + 1) * sizeof (mpz_t));
          mpz_init_set (app->divisors[app->num_divisors], num);
          app->num_divisors++;
        }
      *n++;
    }
  mpz_clear (num);
}

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}
int
fituvalu_gen_63_center (struct fv_app_gen_63_center_t *app)
{
  load_primes (app);
  load_known_centers (app);
  run_threads (app, app->threads, process_record);
  return 0;
}

static struct argp_option
options[] =
{
  { "match", 'm', "NUM", 0, "Candidate must have NUM divisors (Default 3)"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "quicker", 'q', 0, OPTION_HIDDEN, "Short-circuit a bunch of prime division checks"},
  { "all", 'a', 0, OPTION_HIDDEN, "Search all primes up to i / 2"},
  { "factor", 'f', 0, 0, "Input is from GNU factor"},
  { "factor-high-primes", 'H', 0, OPTION_HIDDEN, "Input is from GNU factor but only show numbers that have a prime over sqrt(n) or 17 mil"},
  { "factor-small-primes", 'S', 0, OPTION_HIDDEN, "Input is from GNU factor but only show numbers that have a prime under sqrt(n) or 17 mil"},
  { "factor-fast", 'F', 0, OPTION_HIDDEN, "the third factor is less than 156k and the rest are 4n+1"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Generate center cells suitable for magic squares of type 6:3.  Odd numbers are passed in on the standard input or FILE and the suitable ones are shown.\vIncoming numbers are generated with `seq 1 2 10000`, etc.  The `search-63' program is suitable for passing this program's output to.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_gen_63_center_t app;
  memset (&app, 0, sizeof (app));
  app.infile = stdin;
  app.minimum_matching_divisors = 3;
  app.threads = 1;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_gen_63_center (&app);
}
