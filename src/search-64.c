/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_64_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  int chance_of_seven;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int threesq;
  int quick_def;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, sum, sum_minus_middle, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t i, j, iroot, twoiroot, limit;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
quick_generate_64_type_1 (struct fv_app_search_64_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
// +---+---+---+
// |   | Z |   |
// +---+---+---+
// | A | B | C |
// +---+---+---+
// | E |   | D |
// +---+---+---+

  mpz_sqrt (q->iroot, w->distance);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  mpz_cdiv_q_ui (q->limit, w->sum, 2);

  while (1)
    {
      mpz_sub (q->j, q->i, w->distance);

      if (mpz_perfect_square_p (q->j))
        {
          mpz_set ((*s)[1][0], w->lo);
          mpz_set ((*s)[1][2], w->hi);

          mpz_set ((*s)[2][0], q->i);
          mpz_set ((*s)[2][2], q->j);

          mpz_sub ((*s)[2][1], w->sum, (*s)[2][0]);
          mpz_sub ((*s)[2][1], (*s)[2][1], (*s)[2][2]);

          mpz_sub ((*s)[0][1], w->sum_minus_middle, (*s)[2][1]);

          if (mpz_perfect_square_p ((*s)[0][1]))
            {
              mpz_sub ((*s)[0][0], w->sum_minus_middle, (*s)[2][2]);
              mpz_sub ((*s)[0][2], w->sum_minus_middle, (*s)[2][0]);

              int dup = 0;
              if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
                  mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0 ||
                  (app->chance_of_seven &&
                   mpz_sgn ((*s)[0][0]) < 0 &&
                   mpz_sgn ((*s)[0][2]) < 0 &&
                   mpz_sgn ((*s)[2][1]) < 0))
                dup = 1;

              if (!dup)
                {
                  pthread_mutex_lock (&display_lock);
                  app->display_square (*s, app->out);
                  fflush (app->out);
                  pthread_mutex_unlock (&display_lock);
                }
            }
        }

      if (mpz_cmp (q->i, q->limit) > 0)
        break;

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
    }

}

static void
generate_64_type_1 (struct fv_app_search_64_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
// +---+---+---+
// |   | Z |   |
// +---+---+---+
// | A | B | C |
// +---+---+---+
// | E |   | D |
// +---+---+---+

  mpz_sqrt (q->iroot, w->distance);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);


  while (1)
    {
      mpz_sub (q->j, q->i, w->distance);

      if (mpz_perfect_square_p (q->j))
        {
          mpz_set ((*s)[1][0], w->lo);
          mpz_set ((*s)[1][2], w->hi);

          mpz_set ((*s)[2][0], q->i);
          mpz_set ((*s)[2][2], q->j);

          mpz_sub ((*s)[2][1], w->sum, (*s)[2][0]);
          mpz_sub ((*s)[2][1], (*s)[2][1], (*s)[2][2]);

          mpz_sub ((*s)[0][1], w->sum_minus_middle, (*s)[2][1]);

          if (mpz_perfect_square_p ((*s)[0][1]))
            {
              mpz_sub ((*s)[0][0], w->sum_minus_middle, (*s)[2][2]);
              mpz_sub ((*s)[0][2], w->sum_minus_middle, (*s)[2][0]);

              int dup = 0;
              if (mpz_cmp ((*s)[1][1], (*s)[0][0]) == 0 ||
                  mpz_cmp ((*s)[1][1], (*s)[0][2]) == 0 ||
                  (app->chance_of_seven &&
                   mpz_sgn ((*s)[0][0]) < 0 &&
                   mpz_sgn ((*s)[0][2]) < 0 &&
                   mpz_sgn ((*s)[2][1]) < 0))
                dup = 1;

              if (!dup)
                {
                  pthread_mutex_lock (&display_lock);
                  app->display_square (*s, app->out);
                  fflush (app->out);
                  pthread_mutex_unlock (&display_lock);
                }
            }
        }

      if (mpz_cmp (q->i, w->sum) > 0)
        break;

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
    }
}

static void
handle_progression (struct fv_app_search_64_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  if (app->quick_def)
    quick_generate_64_type_1 (app, w, q, s);
  else
    generate_64_type_1 (app, w, q, s);

}

static void
generate_progressions (struct fv_app_search_64_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[1][1], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  mpz_mul_ui (w->sum, (*s)[1][1], 3);
  mpz_mul_ui (w->sum_minus_middle, (*s)[1][1], 2);

  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[1][1], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[1][1], w->distance);
          mpz_add (w->hi, (*s)[1][1], w->distance);
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_64_t *app =
    (struct fv_app_search_64_t *) param->data;

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.j, q.iroot, q.twoiroot, q.limit, NULL);

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[1][1], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[1][1], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[1][1], 1) > 0)
        generate_progressions (app, &w, &q, &s);
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  mpz_clears (q.i, q.j, q.iroot, q.twoiroot, q.limit, NULL);

  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_64_t *app =
    (struct fv_app_search_64_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.j, q.iroot, q.twoiroot, q.limit, NULL);

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[1][1], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[1][1], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[1][1], w.lo);
      mpz_mul_ui (w.sum, s[1][1], 3);
      mpz_mul_ui (w.sum_minus_middle, s[1][1], 2);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.sum, w.sum_minus_middle, w.twoiroot, NULL);
  mpz_clears (q.i, q.j, q.iroot, q.twoiroot, q.limit, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_64 (struct fv_app_search_64_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *end = NULL;
  struct fv_app_search_64_t *app = (struct fv_app_search_64_t *) state->input;
  switch (key)
    {
    case 'q':
      app->quick_def = 1;
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'N':
      app->chance_of_seven = 0;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "allow-three-negatives", 'N', 0, OPTION_HIDDEN, "Allow squares with three negative numbers"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "quick-def", 'q', 0, OPTION_HIDDEN, "Do an abridged search"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:4 given a FILE containing center values.\vWhen FILE is not provided, it is read from the standard input.  Center values must be perfect squares.  This program generates magic squares with at most two numbers being negative.  Magic squares of type 6:4 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   | X |   |   |   | Z |   |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating E\n"
"  | X | X | X |   | A | B | C |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below E.\n"
"  | X |   | X |   | E |   | D |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_64_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.chance_of_seven = 1;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_64 (&app);
}
/*

the point of this is to identify information that can help us get more 6:4s.  we want to be able to predict 'center' values (the B values) for 6:4 magic squares.

  +---+---+---+   +---+---+---+
  |   | X |   |   |   | Z |   |
  +---+---+---+   +---+---+---+
  | X | X | X |   | A | B | C |
  +---+---+---+   +---+---+---+
  | X |   | X |   | E |   | D |
  +---+---+---+   +---+---+---+


we identified which B values lead to magic squares with 6 perfect squares in the 6:4 configuration, and then we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:4 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 725 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

4, 120, 56, 79 / 52636 = 0.001501
90, 114, 42, 58 / 38515 = 0.001506
64, 120, 116, 48 / 31583 = 0.001520
16, 80, 108, 71 / 46445 = 0.001529
16, 104, 84, 73 / 46445 = 0.001572
16, 84, 104, 75 / 46448 = 0.001615
16, 72, 116, 76 / 46445 = 0.001636
88, 116, 42, 63 / 38515 = 0.001636
16, 120, 68, 78 / 46447 = 0.001679
16, 68, 120, 82 / 46446 = 0.001765

well there's a 120, so maybe we need a greater breadth.
there seems to be a sum of 204 going on (16+68+120, 16+84+104).
it's nice that the highest number of hits 82 here is the highest percentage.

second run is depth of 2, and breadth of 2000.

964, 1931, 9 / 2187 = 0.004115
1308, 1990, 8 / 1919 = 0.004169
696, 780, 18 / 4286 = 0.004200
1900, 1442, 8 / 1892 = 0.004228
1146, 1830, 9 / 2125 = 0.004235
1720, 1256, 9 / 2125 = 0.004235
154, 1334, 19 / 4247 = 0.004474
964, 966, 15 / 3278 = 0.004576
1024, 1436, 12 / 2570 = 0.004669
1642, 1334, 10 / 2127 = 0.004701

well, no max breadths here.

hmm the pecentage is 2.5 times higher.  down from 82 hits to 10 though.
there's a worry that we're just trying to predict somewhat random numbers here, and that any of the top 10 are just as good.

and just for kicks, let's try a depth of 1 and a breadth of 100000.

75511, 2 / 44 = 0.045455
77367, 2 / 43 = 0.046512
81092, 2 / 41 = 0.048780

i'm sure we're just in la-la guessing land here.
but maybe 75511 is worth trying.

let's say the minimum count has to be 10 or higher.
288, 12 / 10966 = 0.001094
352, 10 / 8974 = 0.001114
132, 28 / 23926 = 0.001170
156, 24 / 20248 = 0.001185
228, 17 / 13853 = 0.001227
392, 10 / 8058 = 0.001241
204, 20 / 15483 = 0.001292
444, 10 / 7114 = 0.001406
372, 12 / 8494 = 0.001413
418, 11 / 7556 = 0.001456


look at that, there's 204.  how about that?
nothing to really see here though.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

4, 2, 0, 725 / 1052653 = 0.000689
4, 1, 1, 725 / 1578979 = 0.000459
4, 0, 2, 725 / 1052653 = 0.000689
3, 1, 2, 725 / 1578980 = 0.000459
2, 2, 2, 725 / 1578980 = 0.000459
2, 2, 0, 725 / 1578980 = 0.000459
2, 1, 1, 725 / 2368470 = 0.000306
2, 0, 2, 725 / 1578980 = 0.000459
2, 0, 0, 725 / 1578980 = 0.000459
1, 3, 2, 725 / 1578980 = 0.000459
1, 2, 0, 725 / 2105307 = 0.000344
1, 1, 2, 725 / 2368470 = 0.000306
1, 1, 1, 725 / 3157960 = 0.000230
1, 1, 0, 725 / 3157960 = 0.000230
1, 0, 2, 725 / 2105307 = 0.000344
1, 0, 1, 725 / 3157960 = 0.000230
1, 0, 0, 725 / 3157960 = 0.000230
0, 4, 2, 725 / 1052653 = 0.000689
0, 2, 2, 725 / 1578980 = 0.000459
0, 2, 0, 725 / 1578980 = 0.000459
0, 1, 2, 725 / 2105307 = 0.000344
0, 1, 1, 725 / 3157960 = 0.000230
0, 1, 0, 725 / 3157960 = 0.000230
0, 0, 2, 725 / 1578980 = 0.000459
0, 0, 1, 725 / 3157960 = 0.000230
2, 2, 4, 619 / 1184235 = 0.000523
0, 0, 4, 466 / 789491 = 0.000590


lots of sums of 6, 3 and 4.

note that iterating over 4 squares, and then 2 squares gets us all of them.

results:

so looking forward for more sixes, pick one of "81092", "75511", or "77367".  Try "418, 11", and disregard the "16, 68, 120" because it happens to have our max breadth.  next best is "88, 116, 42".

in recalculating quicker, we skip forward by "4, 2" squares starting from 1 and still get all of them.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 58081 in sq-seq-finder.
24, 40, 116, 74 / 52631 = 0.001406
34, 30, 116, 74 / 52631 = 0.001406
84, 100, 56, 56 / 39473 = 0.001419
64, 60, 56, 75 / 52632 = 0.001425
90, 120, 36, 55 / 38513 = 0.001428
100, 84, 20, 67 / 46443 = 0.001443
94, 116, 108, 43 / 29792 = 0.001443
106, 104, 36, 56 / 38514 = 0.001454
96, 114, 36, 57 / 38513 = 0.001480
94, 116, 36, 62 / 38513 = 0.001610

well we didn't do any better with a different starting point.
the top 3 happen to have the same sum (94+116+36).
so if i were to do another sq-seq-finder i'd make the breadth that sum, and keep the depth.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

934, 1886, 10 / 2245 = 0.004454
1044, 1526, 11 / 2461 = 0.004470
1194, 856, 14 / 3089 = 0.004532
1884, 1764, 8 / 1736 = 0.004608
784, 1676, 12 / 2571 = 0.004667
456, 1020, 20 / 4284 = 0.004669
1786, 1590, 9 / 1875 = 0.004800
456, 1758, 14 / 2856 = 0.004902
724, 1206, 18 / 3280 = 0.005488
1932, 1020, 16 / 2148 = 0.007449

well we did much better here, but we only matched 16.  i'd say it's worth a shot to try "1932, 1020".

notice that we can't seem to ever get to 1 percent.

okay let's do depth=1, and breadth is 100000

99882, 2 / 34 = 0.058824
71448, 3 / 48 = 0.062500
82374, 3 / 42 = 0.071429
91586, 3 / 38 = 0.078947

kinda ridiculous here. nearly 8 percent.  if it pans out, that will really be something.

let's do depth=1, breadth is 100000, min hits of 10

212, 19 / 14897 = 0.001275
324, 13 / 9748 = 0.001334
318, 14 / 9932 = 0.001410
424, 12 / 7451 = 0.001611
564, 10 / 5600 = 0.001786
636, 10 / 4967 = 0.002013

nobody gets to 1 percent.  notice 636, 424 are multiple of 212, and 318 is there too.  so there's something about skipping 106 squares forward here.

so overall, looking forward for more sixes, pick one of "81092", "75511", or "77367", and then one of "71448", "82374", or "91586".  Try "418, 11", "88, 116, 42", "1932, 1020", and "94, 116, 36".

okay, let's do the first run again but sort on hits to see how it shakes out.

4, 2, 0, 725 / 1052574 = 0.000689
4, 1, 1, 725 / 1578860 = 0.000459
4, 0, 2, 725 / 1052574 = 0.000689
3, 1, 2, 725 / 1578861 = 0.000459
2, 2, 2, 725 / 1578861 = 0.000459
2, 2, 0, 725 / 1578861 = 0.000459
2, 1, 1, 725 / 2368291 = 0.000306
2, 0, 2, 725 / 1578861 = 0.000459
2, 0, 0, 725 / 1578861 = 0.000459
1, 3, 2, 725 / 1578861 = 0.000459
1, 2, 0, 725 / 2105148 = 0.000344
1, 1, 2, 725 / 2368291 = 0.000306
1, 1, 1, 725 / 3157721 = 0.000230
1, 1, 0, 725 / 3157721 = 0.000230
1, 0, 2, 725 / 2105148 = 0.000344
1, 0, 1, 725 / 3157721 = 0.000230
1, 0, 0, 725 / 3157721 = 0.000230
0, 4, 2, 725 / 1052574 = 0.000689
0, 2, 2, 725 / 1578861 = 0.000459
0, 2, 0, 725 / 1578861 = 0.000459
0, 1, 2, 725 / 2105148 = 0.000344
0, 1, 1, 725 / 3157721 = 0.000230
0, 1, 0, 725 / 3157721 = 0.000230
0, 0, 2, 725 / 1578861 = 0.000459
0, 0, 1, 725 / 3157721 = 0.000230
2, 2, 4, 619 / 1184146 = 0.000523

okay 2,2,4 is where it breaks down.  best is 4,2 just like before.

lots of sums of 6 going on (not shown).

this concludes our analysis of anticipating B values in 6:4 magic squares.

in order to get high value 6:4s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="81092" 1 10000000000000000000000000000 | ./search-64
sq-seq --pattern="75511" 1 10000000000000000000000000000 | ./search-64
sq-seq --pattern="77367" 1 10000000000000000000000000000 | ./search-64
sq-seq --pattern="418,11" 1 10000000000000000000000000000 | ./search-64
sq-seq --pattern="88,116,42" 1 10000000000000000000000000000 | ./search-64
sq-seq --pattern="418" 1 10000000000000000000000000000 | ./search-64

sq-seq --pattern="71448" 58081 10000000000000000000000000000 | ./search-64
sq-seq --pattern="82374" 58081 10000000000000000000000000000 | ./search-64
sq-seq --pattern="91586" 58081 10000000000000000000000000000 | ./search-64
sq-seq --pattern="1932,1020" 58081 10000000000000000000000000000 | ./search-64
sq-seq --pattern="94,116,36" 58081 10000000000000000000000000000 | ./search-64
sq-seq --pattern="636" 58081 10000000000000000000000000000 | ./search-64

*/
