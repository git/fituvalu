/* Copyright (C) 2019, 2020, 2023, 2024 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <argp.h>
#include <gmp.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>
#include "magicsquareutil.h"

struct fv_app_gen_61_center_t
{
  int out_binary;
  FILE *out;
  unsigned long long int start;
  unsigned long long int stop;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_gen_61_center_t *app = (struct fv_app_gen_61_center_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 's':
        {
          unsigned long long int num = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0' || num < 5)
            argp_error (state, "invalid value `%s' for --start option", arg);
          app->start = num;
        }
      break;
    case 'S':
        {
          unsigned long long int num = strtoull (arg, &end, 10);
          if (end == NULL || *end != '\0' || num < 5)
            argp_error (state, "invalid value `%s' for --stop option", arg);
          app->stop = num;
        }
      break;
    case 'o':
      app->out_binary = 1;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->stop && app->start > app->stop)
        argp_error (state, "invalid value `%s' for --stop option", arg);
      break;
    }
  return 0;
}

static int
square_is_twelvenminusone (unsigned long long i)
{
  return (i - 1) % 12 == 0;
}

static int
detect (unsigned long long int num)
{
  //we return 1 if the next number in the sequence is 4 squares ahead
  unsigned long long int root = sqrtl(num);
  assert (root % 2 == 1);

  unsigned long long int p1root = root + 2;
  unsigned long long int p2root = root + 4;
  unsigned long long int p1 = p1root * p1root;
  unsigned long long int p2 = p2root * p2root;

  int checkp1 = square_is_twelvenminusone (p1);
  int checkp2 = square_is_twelvenminusone (p2);

  if (checkp1 && !checkp2)
    return 0;
  else if (!checkp1 && checkp2)
    return 1;
  //can't get here
  return 0;
}

int
fituvalu_gen_61_center (struct fv_app_gen_61_center_t *app)
{
  //we either go up by two squares or by four
  //but we don't know which so we have to detect
  
  unsigned long long int start;
  unsigned long long int up_by_four;
  unsigned long long root = 0;
  if (app->start)
    {
      root = sqrtl (app->start);
      if (root % 2 == 0)
        root++;
      start = root * root;
      up_by_four = detect (start);
    }
  else
    {
      start = 1;
      root = 1;
      up_by_four = 1;
    }

  unsigned long long int i = start;
  while (1)
    {

      if (app->out_binary)
        fwrite (&i, sizeof (i), 1, app->out);
      else
        fprintf (app->out, "%llu\n", i);
      fflush (app->out);

      if (up_by_four)
        {
          //advance i by 4 squares
          unsigned long long int eightroot = root * 8;
          i += eightroot;
          i+=16;
          root+=4;
        }
      else
        {
          //advance i by 2 squares
          unsigned long long int fourroot = root * 4;
          i += fourroot;
          i+=4;
          root+=2;
        }

      up_by_four = !up_by_four;
      if (app->stop && i >= app->stop)
        break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "out-binary", 'o', 0, 0, "Output raw unsigned long longs instead of text"},
  { "start", 's', "NUM", 0, "Start at NUM instead of 5"},
  { "stop", 'S', "NUM", 0, "Stop when we pass NUM"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "",
  "Generate center cells suitable for magic squares of type 6:1.  These values are numbers congruent to 1 or 5 mod 6 then squared.\vEach generated number is composed of 4n+1 primes.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_gen_61_center_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_gen_61_center (&app);
}
