/* Copyright (C) 2019 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"
#include "avl.h"

struct fv_app_gen_middles_t
{
  int skip_gcd;
  int start_flipflop;
  mpz_t start;
  int center;
  int flipflop;
  int filter;
  mpz_t stop;
  void (*dump_func) (mpz_t *, FILE *);
  void (*display_tuple) (mpz_t s[3], FILE *out);
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
  int (*read_square) (FILE *stream, mpz_t (*a)[3][3], char **line, size_t *len);
};

static void
dump_binary_num (mpz_t *i, FILE *out)
{
  mpz_out_raw (out, *i);
}

static void
dump_num (mpz_t *i, FILE *out)
{
  char *buf = malloc (mpz_sizeinbase (*i, 10) + 2);
  mpz_get_str (buf, 10, *i);
  fprintf (out, "%s", buf);
  fprintf (out, "\n");
  free (buf);
}

static int
parse_start (struct fv_app_gen_middles_t *app, char *s)
{
  char *num = NULL;
  int flip = 0;
  char *comma = strchr (s, ',');
  if (!comma)
    return 0;
  *comma = '\0';
  comma++;
  flip = atoi (comma);
  mpz_set_str (app->start, s, 10);
  if (mpz_cmp_ui (app->start, 0) <= 0)
    return 0;
  app->start_flipflop = flip;
  return 1;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_gen_middles_t *app =
    (struct fv_app_gen_middles_t *) state->input;
  switch (key)
    {
    case 'k':
      app->skip_gcd = 1;
      break;
    case 'S':
      if (!parse_start (app, arg))
        argp_error (state, "invalid start %s", arg);
      break;
    case 'c':
      app->center = 1;
      break;
    case 'F':
      app->flipflop = 1;
      mpz_set_str (app->stop, arg, 10);
      break;
    case 'f':
      app->filter = 1;
      break;
    case 's':
      mpz_set_str (app->stop, arg, 10);
      break;
    case 'o':
      app->dump_func = dump_binary_num;
      app->display_tuple = display_binary_three_record;
      app->display_square = display_binary_square_record;
      break;
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      app->read_square = binary_read_square_from_stream;
      break;
    case ARGP_KEY_ARG:
      argp_error (state, "too many arguments");
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      app->dump_func = dump_num;
      mpz_inits (app->stop, app->start, NULL);
      break;
    }
  return 0;
}

static int
has_num_gcd (mpz_t num, int match)
{
  if (match == 0)
    return 1;
  int found = 0;
  mpz_t i, iroot, diff, limit, diffroot, mn, twomn, p[3], gcd;
  mpz_inits (i, iroot, diff, limit, diffroot, mn, twomn, p[0], p[1], p[2], gcd, NULL);
  mpz_cdiv_q_ui (limit, num, 2);
  mpz_set_ui (i, 4);
  mpz_set_ui (iroot, 2);
  mpz_set (p[1], num);
  while (1)
    {
      if (mpz_cmp (i, limit) > 0)
        break;

      mpz_sub (diff, num, i);
      if (mpz_perfect_square_p (diff))
        {
          mpz_sqrt (diffroot, diff);
          //okay make the progression and check gcd is 1

          mpz_mul (mn, iroot, diffroot);
          mpz_mul_ui (twomn, mn, 2);
          mpz_sub (p[0], p[1], twomn);
          mpz_gcd (gcd, p[0], p[1]);
            {
              mpz_add (p[2], p[1], twomn);
              mpz_gcd (gcd, gcd, p[2]);
              int ret = mpz_cmp_ui (gcd, 1) == 0;
              if (ret)
                found++;
              if (found >= match)
                break;
            }
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, diff, limit, diffroot, mn, twomn, p[0], p[1], p[2], gcd, NULL);
  return found == match;
}

static int
middle_seq (struct fv_app_gen_middles_t *app, FILE *out)
{
  int incr;
  int flipflop = app->start_flipflop;
  mpz_t i, root;
  mpz_inits (i, root, NULL);

  mpz_set (i, app->start);
  mpz_sqrt (root, i);
  if (mpz_cmp_ui (root, 0) == 0)
    mpz_add_ui (root, root, 1);
  mpz_mul (i, root, root);

  app->dump_func (&i, out);

  while (1)
    {
      if (flipflop)
        incr = app->center ? 20 : 8;
      else
        incr = app->center ? 40 : 4;
      flipflop = !flipflop;
      for (int j = 0; j < incr; j++)
        {
          mpz_add (i, i, root);
          mpz_add (i, i, root);
          mpz_add_ui (i, i, 1);
          mpz_add_ui (root, root, 1);
        }
      if (mpz_cmp_ui (app->stop, 0) > 0 && mpz_cmp (i, app->stop) > 0)
        break;
      if (app->skip_gcd)
        app->dump_func (&i, out);
      else if (has_num_gcd (i, 1))
        app->dump_func (&i, out);
    }
  mpz_clears (i, root, NULL);
  return 0;
}

int
compare_mpzs (const void *lhs, const void *rhs, void *hook)
{
  const mpz_t *l = (const mpz_t *) lhs;
  const mpz_t *r = (const mpz_t *) rhs;
  return mpz_cmp (*l, *r);
}

static void
add_item (struct avl_table *t, char *number)
{
  mpz_t *item = malloc (sizeof (mpz_t));
  mpz_init_set_str (*item, number, 10);
  avl_probe (t, item);
}

extern char *middle_numbers[];
extern char *center_numbers[];

static struct avl_table *
setup_avl_tree (int m)
{
  struct avl_table *tree =
    avl_create (compare_mpzs, NULL, NULL);

  char **n = m ? center_numbers : middle_numbers;
  while (*n != 0)
    {
      add_item (tree, *n);
      *n++;
    }
  return tree;
}

void
delete_mpz (void *item, void *hook)
{
  mpz_t *i = (mpz_t *)item;
  mpz_clear (*i);
  free (i);
}

static void
tear_down_avl_tree (struct avl_table *t)
{
  avl_destroy (t, delete_mpz);
}

static void
assign_max_generated_middle (struct avl_table *t, mpz_t *max)
{
  struct avl_traverser trav;
  mpz_t *last = avl_t_last (&trav, t);
  if (last)
    mpz_set (*max, *last);
}

static int
middle_find (struct fv_app_gen_middles_t *app, mpz_t num, int flip, mpz_t max, FILE *out )
{
  int found = 0;
  int incr;
  int flipflop = flip;
  mpz_t i, root;
  mpz_inits (i, root, NULL);
  mpz_set (i, max);
  mpz_sqrt (root, i);

  while (1)
    {
      if (flipflop)
        incr = app->center ? 20 : 8;
      else
        incr = app->center ? 40 : 4;
      flipflop = !flipflop;
      for (int j = 0; j < incr; j++)
        {
          mpz_add (i, i, root);
          mpz_add (i, i, root);
          mpz_add_ui (i, i, 1);
          mpz_add_ui (root, root, 1);
        }
      int ret = mpz_cmp (i, num);
      if (ret > 0)
        break;
      else if (ret == 0)
        {
          if (has_num_gcd (i, 1))
            found = 1;
          break;
        }
    }
  mpz_clears (i, root, NULL);
  return found;
}

static int
middle_filter (struct fv_app_gen_middles_t *app, FILE *in, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3], s[3][3], max_generated;

  for (int i = 0; i < 3; i++)
    mpz_init (a[i]);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  mpz_init (max_generated);

  struct avl_table *t = setup_avl_tree (app->center);
  assign_max_generated_middle (t, &max_generated);

  while (1)
    {
      if (app->center)
        {
          read = app->read_square (in, &s, &line, &len);
          if (read == -1)
            break;
          //is the middle in our list?
          if (mpz_cmp (s[1][1], max_generated) < 0)
            {
              if (avl_find (t, &s[1][1]))
                app->display_square (s, out);
            }
          else
            {
              //do it the hard way, we start on incrementing squares by 4,
              //so flip is 0.  we figure this out manually.
              if (middle_find (app, s[1][1], 0, max_generated, out))
                app->display_square (s, out);
            }
        }
      else
        {
          read = app->read_tuple (in, a, &line, &len);
          if (read == -1)
            break;
          //is the middle in our list?
          if (mpz_cmp (a[1], max_generated) < 0)
            {
              if (avl_find (t, &a[1]))
                app->display_tuple (a, out);
            }
          else
            {
              //do it the hard way, we start on incrementing squares by 4,
              //so flip is 0.  we figure this out manually.
              if (middle_find (app, a[1], 0, max_generated, out))
                app->display_tuple (a, out);
            }
        }
    }

  tear_down_avl_tree (t);

  for (int i = 0; i < 3; i++)
    mpz_clear (a[i]);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clear (max_generated);

  if (line)
    free (line);
  return 0;
}

static int
middle_flipflop (struct fv_app_gen_middles_t *app, FILE *out)
{
  int incr;
  int flipflop = 0;
  mpz_t i, root;
  mpz_inits (i, root, NULL);
  if (app->center)
    mpz_set_ui (i, 625);
  else
    mpz_set_ui (i, 1);
  mpz_sqrt (root, i);
  if (mpz_cmp_ui (root, 0) == 0)
    mpz_add_ui (root, root, 1);
  mpz_mul (i, root, root);

  while (1)
    {
      if (flipflop)
        incr = app->center ? 20 : 8;
      else
        incr = app->center ? 40 : 4;
      for (int j = 0; j < incr; j++)
        {
          mpz_add (i, i, root);
          mpz_add (i, i, root);
          mpz_add_ui (i, i, 1);
          mpz_add_ui (root, root, 1);
        }
      if (mpz_cmp (i, app->stop) > 0)
        {
          display_textual_number_no_newline (i, out);
          printf (", %d (which was an increment of %d squares)\n", flipflop, incr);
          break;
        }
      flipflop = !flipflop;
    }
  mpz_clears (i, root, NULL);
  return 0;
}

int
fituvalu_gen_middles (struct fv_app_gen_middles_t *app, FILE *in, FILE *out)
{
  if (app->filter)
    return middle_filter (app, in, out);
  else if (app->flipflop)
    return middle_flipflop (app, out);
  else
    return middle_seq (app, out);
}

static struct argp_option
options[] =
{
  { "skip-gcd", 'k', 0, OPTION_HIDDEN, "Don't perform any gcd tests"},
  { "start", 'S', "NUM,FLIP", OPTION_HIDDEN, "Start at NUM instead of 1"},
  { "flip-flop", 'F', "NUM", OPTION_HIDDEN, "Get the flip flop value after NUM"},
  { "filter", 'f', 0, 0, "Accept three square progressions and show records with matching middles, or with -c accept magic squares instead"},
  { "stop", 's', "NUM", 0, "Stop when we exceed NUM"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "center", 'c', 0, 0, "Optimize for center values in a magic square"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "",
  "Compute a sequence of middle squares for three square progressions.\vThis pattern emerged in the middles of the three square progressions that form 3x3 magic squares with 6 perfect squares.  Or put another way, if a number is in this list, the number is probably a center value in a three square progression that is in a 3x3 magic square with 6 perfect squares.  Use find-3sq-progressions-sq to see the possible progressions.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_gen_middles_t app;
  memset (&app, 0, sizeof (app));
  app.display_tuple = display_three_record;
  app.read_tuple = read_three_numbers_from_stream;
  app.read_square = read_square_from_stream;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (mpz_cmp_ui (app.start, 0) == 0)
    {
      if (app.center)
        mpz_set_ui (app.start, 625);
      else
        mpz_set_ui (app.start, 1);
    }
  return fituvalu_gen_middles (&app, stdin, stdout);

}
/*
* if we're not doing centers, and we're starting from 0
* we can skip every 8th beginning at zero
* at least for 6:4s anyway, maybe all?
*/

