/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

int max_recs = 1000;

struct fv_app_sqfree_t
{
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  int in_binary;
};


struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}



static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, r;
  mpz_inits (n, r, NULL);
  unsigned long long num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_sqfree_t *app =
    (struct fv_app_sqfree_t *) param->data;

  while (1)
    {
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (n, app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (n, line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
        {
          int found = 0;
          mpz_sqrt (r, n);
          unsigned long long rmax = mpz_get_d (r);
          for (unsigned long long i = 2; i < rmax; i++)
            {
              unsigned long long s = i * i;
              if (mpz_divisible_ui_p (n, s))
                {
                  found = 1;
                  //fprintf (app->out, "%lld is divisible by %llu", n, s);
                  break;
                }
            }
          if (found == 0)
            {
              char *buf = malloc (mpz_sizeinbase (n, 10) + 2);
              mpz_get_str (buf, 10, n);
              fprintf (app->out, "%s\n", buf);
              free (buf);
            }
        }
    }

  if (line)
    free (line);
  mpz_clears (n, r, NULL);
  return NULL;
}

int
fituvalu_sqfree (struct fv_app_sqfree_t *app)
{
  run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_sqfree_t *app = (struct fv_app_sqfree_t *) state->input;
  switch (key)
    {
    case 'i':
      app->in_binary = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Show numbers that aren't divisble by squares.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_sqfree_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_sqfree (&app);
}
