/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <unistd.h>
#include "magicsquareutil.h"
#include "linecount.h"
int linecount;

struct fv_app_3sq_pair_search_615_t
{
  FILE *infile;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
};

struct rec
{
  mpz_t ap[3];
};

static void
create_square (struct fv_app_3sq_pair_search_615_t *app, struct rec *rec, mpz_t *b, FILE *out)
{
  mpz_t s[3][3];
  mpz_t sum, p;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  mpz_inits (sum, p, NULL);

  mpz_set (s[0][1], rec->ap[0]);
  mpz_set (s[2][2], rec->ap[1]);
  mpz_set (s[1][0], rec->ap[2]);
  mpz_set (s[0][2], b[0]);
  mpz_set (s[2][1], b[1]);

  mpz_sub (p, s[2][1], s[0][1]);
  mpz_cdiv_q_ui (sum, p, 2);
  mpz_add (s[1][1], s[0][1], sum);

  mpz_add (sum, s[0][1], s[1][1]);
  mpz_add (sum, sum, s[2][1]);

  mpz_add (p, s[0][1], s[0][2]);
  mpz_sub (s[0][0], sum, p);

  mpz_add (p, s[1][0], s[1][1]);
  mpz_sub (s[1][2], sum, p);

  mpz_add (p, s[2][1], s[2][2]);
  mpz_sub (s[2][0], sum, p);

  app->display_square (s, out);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (sum, p, NULL);
}

static int
search_for_pairs (struct fv_app_3sq_pair_search_615_t *app, mpz_t *b, struct rec *rec, FILE *out)
{
  //a middle of new 3sq progression needs to match rec->ap[0]
  int found = 0;
  int magic;
  mpz_t i, iroot, j, jroot, diff, diff2, max;
  mpz_inits (i, iroot, j, jroot, diff, diff2, max, NULL);
  mpz_set (i, rec->ap[2]);
  mpz_add (i, i, rec->ap[1]);
  mpz_sqrt (iroot, i);
  mpz_mul (i, iroot, iroot);

  //iterate i once
  mpz_add (i, i, iroot);
  mpz_add (i, i, iroot);
  mpz_add_ui (i, i, 1);
  mpz_add_ui (iroot, iroot, 1);

  mpz_add (max, rec->ap[0], rec->ap[1]);
  mpz_add (max, max, rec->ap[2]);

  mpz_mul_ui (max, max, 4); //it's a pretty random limiter
  while (1)
    {
      mpz_sub (diff2, i, rec->ap[2]);
      mpz_add (diff, i, diff2);
      {
        //do the limiting check.
        //unfortunately we're recalculating some of this in create-square.
        // when [0][0] goes smaller than [1][1]
        mpz_t sum, p, middle, half, q, col;
        mpz_inits (sum, p, middle, half, q, col, NULL);
        mpz_sub (p, diff, rec->ap[0]);
        mpz_cdiv_q_ui (half, p, 2);
        mpz_sub (middle, diff, half); // is [1][1]
        mpz_add (sum, diff, rec->ap[0]);
        mpz_add (sum, sum, middle); 
        int limit = mpz_cmp (sum, max) > 0;

        mpz_sub (p, sum, rec->ap[0]);
        mpz_sub (p, p, i); // is [0][0]

        mpz_sub (q, sum, i);
        mpz_sub (q, q, middle); //is [2][0]

        mpz_add (col, rec->ap[2], p);
        mpz_add (col, col, q);

        magic = mpz_cmp (col, sum) == 0;
        mpz_clears (sum, p, middle, half, q, col, NULL);
        if (limit)
          break;
      }
      if (mpz_perfect_square_p (diff) && magic)
        {
          mpz_set (b[0], i);
          mpz_set (b[1], diff);
          found = 1;
          create_square (app, rec, b, out);
        }
      mpz_add (i, i, iroot);
      mpz_add (i, i, iroot);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (iroot, iroot, 1);
    }
  mpz_clears (i, iroot, j, jroot, diff, diff2, max, NULL);
  return found;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_3sq_pair_search_615_t *app = (struct fv_app_3sq_pair_search_615_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->infile)
        argp_error (state, "too many arguments");
      if (strcmp (arg, "-") == 0)
        app->infile = stdin;
      else
        app->infile = fopen (arg, "r");
      if (!app->infile)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      app->infile = stdin;
      break;
    }
  return 0;
}

int
fituvalu_3sq_pair_search_615 (struct fv_app_3sq_pair_search_615_t *app, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  struct rec rec;
  mpz_t b[2];
  mpz_inits (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], NULL);
  while (1)
    {
      read = app->read_tuple (app->infile, rec.ap, &line, &len);
      if (read == -1)
        break;
      update_linecount (&linecount);
      search_for_pairs (app, b, &rec, out);
    }
  if (line)
    free (line);
  mpz_clears (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], NULL);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Try to make a 3x3 magic square with 5 or more perfect squares by giving it a series of 3 square progressions in FILE.  The program finds a three square iteration from X3 to Y1 to Y2.\vWhen FILE is `-' or isn't specified it is read from the standard input.  Each square is laid out like:\n\
  +------+------+------+\n\
  |      |  X1  |  Y2  |\n\
  +------+------+------+\n\
  |  X3  |      |      |\n\
  +------+------+------+\n\
  |      |  Y1  |  X2  |\n\
  +------+------+------+\nThe 615 in the name comes from the fact we're trying to make a square of the form 6:15.",
  0
};

int
main (int argc, char **argv)
{
  init_linecount (&linecount);
  struct fv_app_3sq_pair_search_615_t app;
  memset (&app, 0, sizeof (app));

  app.display_square = display_square_record;
  app.read_tuple = read_three_numbers_from_stream;
  app.infile = stdin;

  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_3sq_pair_search_615 (&app, stdout);
}
