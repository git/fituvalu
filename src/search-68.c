/* Copyright (C) 2019, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_68_t
{
  int in_binary;
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  double median;
  double max;
  double percent;
  int percent_set;
  int exhaustive;
  int mult;
  int threesq;
  int subtype;
};

struct thread_data_t
{
  void *data;
};

struct mpz_outer_worker_t
{
  mpz_t i, iroot, diff, limit, nn, mn, distance, lo, hi, twoiroot;
};

struct mpz_inner_worker_t
{
  mpz_t i, iroot, j, limit, twoiroot, sum, sum_minus_middle, prev_center;
  mpz_t *ll;
  int num_ll;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_68_type_1 (struct fv_app_search_68_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_mul_ui (q->sum, q->i, 3);
  mpz_mul_ui (q->sum_minus_middle, q->i, 2);

  mpz_set ((*s)[2][1], w->hi);
  mpz_set ((*s)[1][0], w->lo);

  //   +---+---+---+
  //   |   |   | B |
  //   +---+---+---+
  //   | A | D |   |
  //   +---+---+---+
  //   |   | C | E |
  //   +---+---+---+

  mpz_set ((*s)[1][1], q->i);
  mpz_set ((*s)[2][2], q->j);

  mpz_add ((*s)[0][0], (*s)[1][1], w->distance);

  mpz_sub ((*s)[2][0], q->sum, (*s)[2][1]);
  mpz_sub ((*s)[2][0], (*s)[2][0], (*s)[2][2]);

  mpz_sub ((*s)[1][2], q->sum_minus_middle, (*s)[1][0]);

  mpz_sub ((*s)[0][1], q->sum_minus_middle, (*s)[2][1]);
    {
      //check for dups
      int dup = 0;
      if (mpz_cmp ((*s)[1][1], (*s)[1][0]) == 0 ||
          mpz_cmp ((*s)[1][1], (*s)[2][0]) == 0)
        dup = 1;
      if (!dup)
        {
          if (mpz_perfect_square_p ((*s)[2][0]))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
generate_68_type_2 (struct fv_app_search_68_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_mul_ui (q->sum, q->j, 3);
  mpz_sub (q->sum_minus_middle, q->sum, q->j);

  mpz_set ((*s)[2][1], w->lo);
  mpz_set ((*s)[1][0], w->hi);

  //   +---+---+---+
  //   |   |   | B |
  //   +---+---+---+
  //   | C | E |   |
  //   +---+---+---+
  //   |   | A | D |
  //   +---+---+---+

  mpz_set ((*s)[1][1], q->j);
  mpz_set ((*s)[2][2], q->i);

  mpz_sub ((*s)[0][0], (*s)[1][1], w->distance);

  mpz_sub ((*s)[2][0], q->sum, (*s)[2][1]);
  mpz_sub ((*s)[2][0], (*s)[2][0], (*s)[2][2]);

  mpz_sub ((*s)[1][2], q->sum_minus_middle, (*s)[1][0]);

  mpz_sub ((*s)[0][1], q->sum_minus_middle, (*s)[2][1]);
    {
      //check for dups
      int dup = 0;
      if (mpz_cmp ((*s)[1][1], (*s)[1][0]) == 0 ||
          mpz_cmp ((*s)[1][1], (*s)[2][0]) == 0)
        dup = 1;
      if (!dup)
        {
          if (mpz_perfect_square_p ((*s)[2][0]))
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
exhaustive_create_68_square (struct fv_app_search_68_t *app, mpz_t lo, mpz_t hi, mpz_t e, mpz_t sum, mpz_t sum_minus_middle, mpz_t (*s)[3][3])
{
  /*
  +---+---+---+
  |   |   | A |
  +---+---+---+
  |   | N |   |
  +---+---+---+
  | C |   | D |
  +---+---+---+
  */
  mpz_set ((*s)[0][2], lo);
  mpz_set ((*s)[2][0], hi);
  mpz_set ((*s)[2][2], e);
  mpz_sub ((*s)[2][1], sum, hi);
  mpz_sub ((*s)[2][1], (*s)[2][1], e);
  if (mpz_perfect_square_p ((*s)[2][1]))
    {
      mpz_sub ((*s)[0][0], sum_minus_middle, (*s)[2][2]);

      mpz_sub ((*s)[1][0], sum, (*s)[0][0]);
      mpz_sub ((*s)[1][0], (*s)[1][0], (*s)[2][0]);
      if (mpz_perfect_square_p ((*s)[1][0]))
        {
          mpz_sub ((*s)[0][1], sum_minus_middle, (*s)[2][1]);
          mpz_sub ((*s)[1][2], sum_minus_middle, (*s)[1][0]);
          int dup = 0;
          if (!dup)
            {
              pthread_mutex_lock (&display_lock);
              app->display_square (*s, app->out);
              fflush (app->out);
              pthread_mutex_unlock (&display_lock);
            }
        }
    }
}

static void
exhaustive_generate_68_type_1_and_2 (struct fv_app_search_68_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  if (mpz_cmp (q->prev_center, (*s)[1][1]) != 0)
    {
      if (mpz_cmp ((*s)[1][1], q->prev_center) < 0 ||
          mpz_cmp_ui (q->prev_center, 0) == 0)
        {
          mpz_set (q->prev_center, (*s)[1][1]);
          q->num_ll = 0;
          free (q->ll);
          q->ll = NULL;

          mpz_mul_ui (q->limit, (*s)[1][1], 3);
          //fill ll
          mpz_set_ui (q->i, 1);
          mpz_set_ui (q->iroot, 1);
          while (1)
            {
              if (mpz_cmp (q->i, q->limit) >= 0)
                break;

              q->ll = realloc (q->ll, sizeof (mpz_t) * (q->num_ll + 1));
              mpz_init_set (q->ll[q->num_ll], q->i);
              q->num_ll++;

              mpz_mul_ui (q->twoiroot, q->iroot, 2);
              mpz_add (q->i, q->i, q->twoiroot);
              mpz_incr (q->i);
              mpz_incr (q->iroot);
            }
        }
      else
        {
          mpz_set (q->prev_center, (*s)[1][1]);

          mpz_mul_ui (q->limit, (*s)[1][1], 3);
          mpz_set (q->i, q->ll[q->num_ll-1]);
          mpz_sqrt (q->iroot, q->i);

          mpz_mul_ui (q->twoiroot, q->iroot, 2);
          mpz_add (q->i, q->i, q->twoiroot);
          mpz_incr (q->i);
          mpz_incr (q->iroot);
          while (1)
            {
              if (mpz_cmp (q->i, q->limit) >= 0)
                break;

              q->ll = realloc (q->ll, sizeof (mpz_t) * (q->num_ll + 1));
              mpz_init_set (q->ll[q->num_ll], q->i);
              q->num_ll++;

              mpz_mul_ui (q->twoiroot, q->iroot, 2);
              mpz_add (q->i, q->i, q->twoiroot);
              mpz_incr (q->i);
              mpz_incr (q->iroot);
            }
        }
    }
  mpz_sub (q->limit, q->sum, w->lo);
  for (int j = 0; j < q->num_ll; j++)
    {
      if (mpz_cmp ((*s)[1][1], q->ll[j]) == 0)
        continue;
      if (mpz_cmp (q->ll[j], q->limit) > 0)
        break;
      exhaustive_create_68_square (app, w->lo, w->hi, q->ll[j], q->sum, q->sum_minus_middle, s);
      exhaustive_create_68_square (app, w->hi, w->lo, q->ll[j], q->sum, q->sum_minus_middle, s);
    }
}

static void
generate_68_type_1_and_2 (struct fv_app_search_68_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  //we start counting down, and checking downwards for squares
  mpz_mul_ui (q->limit, w->hi, app->mult);

  mpz_sqrt (q->iroot, w->distance);
  mpz_incr (q->iroot);
  mpz_mul (q->i, q->iroot, q->iroot);

  while (1)
    {
      mpz_sub (q->j, q->i, w->distance);
  
      if (mpz_perfect_square_p (q->j))
        {
          if (app->subtype <= 1)
            generate_68_type_1 (app, w, q, s);
          if (app->subtype == 2 || app->subtype == 0)
            generate_68_type_2 (app, w, q, s);
        }

      mpz_mul_ui (q->twoiroot, q->iroot, 2);
      mpz_add (q->i, q->i, q->twoiroot);
      mpz_incr (q->i);
      mpz_incr (q->iroot);
      if (mpz_cmp (q->i, q->limit) > 0)
        break;
    }
}

static void
handle_progression (struct fv_app_search_68_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  if (app->exhaustive)
    {
      mpz_set ((*s)[1][1], (*s)[0][2]);
      mpz_mul_ui (q->sum, (*s)[1][1], 3);
      mpz_mul_ui (q->sum_minus_middle, (*s)[1][1], 2);
      exhaustive_generate_68_type_1_and_2 (app, w, q, s);
    }
  else
    generate_68_type_1_and_2 (app, w, q, s);
}

static void
generate_progressions (struct fv_app_search_68_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[0][2], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[0][2], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[0][2], w->distance);
          mpz_add (w->hi, (*s)[0][2], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            handle_progression (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void
exhaustive_generate_progressions (struct fv_app_search_68_t *app, struct mpz_outer_worker_t *w, struct mpz_inner_worker_t *q, mpz_t (*s)[3][3])
{
  mpz_cdiv_q_ui (w->limit, (*s)[1][1], 2);
  mpz_set_ui (w->i, 1);
  mpz_set_ui (w->iroot, 1);
  while (1)
    {
      if (mpz_cmp (w->i, w->limit) > 0)
        break;

      mpz_sub (w->diff, (*s)[1][1], w->i);

      if (mpz_perfect_square_p (w->diff))
        {
          mpz_sqrt (w->nn, w->diff);
          mpz_mul (w->mn, w->iroot, w->nn);
          mpz_mul_ui (w->distance, w->mn, 2);
          mpz_sub (w->lo, (*s)[1][1], w->distance);
          mpz_add (w->hi, (*s)[1][1], w->distance);
      
          if (mpz_cmp_ui (w->distance, 0) > 0)
            exhaustive_generate_68_type_1_and_2 (app, w, q, s);
        }

      mpz_mul_ui (w->twoiroot, w->iroot, 2);
      mpz_add (w->i, w->i, w->twoiroot);
      mpz_incr (w->i);
      mpz_incr (w->iroot);
    }
  return;
}

static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  mpz_t n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_68_t *app =
    (struct fv_app_search_68_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, q.sum_minus_middle, q.prev_center, NULL);
  q.num_ll = 0;
  q.ll = NULL;
  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read = mpz_inp_raw (s[0][2], app->infile);
          if (!read)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          mpz_set_str (s[0][2], line, 10);
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      if (mpz_cmp_ui (s[0][2], 1) > 0)
        {
          if (app->exhaustive)
            {
              mpz_set (s[1][1], s[0][2]);
              mpz_mul_ui (q.sum, s[1][1], 3);
              mpz_mul_ui (q.sum_minus_middle, s[1][1], 2);
              exhaustive_generate_progressions (app, &w, &q, &s);
            }
          else
            generate_progressions (app, &w, &q, &s);
        }
    }
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clears (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, q.sum_minus_middle, q.prev_center, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

static void*
process_threesq_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_68_t *app =
    (struct fv_app_search_68_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  struct mpz_outer_worker_t w;
  mpz_inits (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);
  struct mpz_inner_worker_t q;
  mpz_inits (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, q.sum_minus_middle, q.prev_center, NULL);
  q.num_ll = 0;
  q.ll = NULL;

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_3sq_from_stream (app->infile, &w.lo, &s[0][2], &w.hi, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      mpz_sub (w.distance, s[0][2], w.lo);
      if (app->exhaustive)
        mpz_set (s[1][1], s[0][2]);
      if (mpz_cmp_ui (w.distance, 0) > 0)
        handle_progression (app, &w, &q, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);

  mpz_clears (q.i, q.iroot, q.j, q.limit, q.twoiroot, q.sum, q.sum_minus_middle, q.prev_center, NULL);
  mpz_clears (w.i, w.iroot, w.diff, w.limit, w.nn, w.mn, w.distance, w.lo, w.hi, w.twoiroot, NULL);

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_68 (struct fv_app_search_68_t *app)
{
  if (app->threesq)
    run_threads (app, app->threads, process_threesq_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_68_t *app = (struct fv_app_search_68_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'e':
      app->exhaustive = 1;
      break;
    case 's':
      app->subtype = atoi (arg);
      if (app->subtype <= 0 || app->subtype > 2)
        argp_error (state, "invalid subtype");
      break;
    case '3':
      app->threesq = 1;
      break;
    case 'm':
      app->percent = strtod (arg, &end);
      if (end == NULL || *end != '\0' || app->percent <= 0)
        argp_error (state, "invalid argument `%s' to option --multiply", arg);
      app->percent /= 100.0;
      app->percent_set = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      app->mult = ceil (app->max * app->percent);
      if (app->exhaustive && app->percent_set)
        argp_error (state, "can't use --exhaustive with --multiply");
      if (app->exhaustive && app->subtype)
        argp_error (state, "can't use --exhaustive with --subtype");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "multiply", 'm', "PERC", 0, "Find a starting point for E by multiplying C by a value between 1 and 9755.2353 expressed as a percent (1-100)"},
  { "3sq", '3', 0, OPTION_HIDDEN, "Read in a three square progression instead"},
  { "subtype", 's', "NUM", 0, "Only produce subtype 1 or 2 instead of both"},
  { "exhaustive", 'e', 0, 0, "Use an exhaustive method"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:8 given a FILE containing top right values.\vWhen FILE is not provided, it is read from the standard input.  The default value for PERC is 0.013159466436615425.  Magic squares of type 6:8 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  |   |   | X |   |   |   | B |   A,B,C is a three square progression where\n"
"  +---+---+---+   +---+---+---+   B comes from FILE.  Start iterating E\n"
"  | X | X |   |   | A | E |   |   upwards, checking for a new square D at a\n"
"  +---+---+---+   +---+---+---+   distance of B-A below E.\n"
"  | X | X | X |   | Z | C | D |   Z is a square that shakes out.\n"
"  +---+---+---+   +---+---+---+\n"
"                  +---+---+---+\n"
"                  |   |   | B |\n"
"                  +---+---+---+  (starts iterating F upwards)\n"
"                  | C | E |   |\n"
"                  +---+---+---+\n"
"                  | Z | A | F |\n"
"                  +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_68_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  app.median = 2.8279;
  app.max = 64468.4193;
  app.percent = (app.median * 3.0) / app.max;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_68 (&app);
}
/*

the point of this is to identify information that can help us get more 6:8s.  we want to be able to predict 'center' values (the B values) for 6:8 magic squares.

  +---+---+---+   +---+---+---+
  |   |   | X |   |   |   | B |
  +---+---+---+   +---+---+---+
  | X | X |   |   | A | E |   |
  +---+---+---+   +---+---+---+
  | X | X | X |   | Z | C | D |
  +---+---+---+   +---+---+---+
                  +---+---+---+
                  |   |   | B |
                  +---+---+---+
                  | C | E |   |
                  +---+---+---+
                  | Z | A | F |
                  +---+---+---+

we identified which B values lead to magic squares with 6 perfect squares in the 6:8 configuration, and then here we check for patterns that best fit that data.

understanding the results:
columns are, n1, n2, n3, hits / count = percentage

n1, n2, n3 are a repeating pattern of advancing n1 squares, n2 squares, n3 squares, n1 squares, and represents the value of B in the 6:8 configuration.

hits is how many sixes we hit with this pattern.
count is how many tries there were, starting counting at 1, and ending at the final magic square in our dataset.

our first run has depth of 3, and breadth is 121 in sq-seq-finder.
our input data is limited to the first 1380 B values.

121 means that 120 is the maximum and 0 is the minimum.  when a pattern contains a value of 0, it means don't advance at all.

84, 120, 0, 83 / 30836 = 0.002692
0, 84, 86, 100 / 37001 = 0.002703
84, 0, 86, 100 / 37001 = 0.002703
84, 86, 0, 100 / 37001 = 0.002703
50, 68, 120, 115 / 39644 = 0.002901
16, 68, 18, 271 / 92494 = 0.002930
84, 34, 86, 136 / 46251 = 0.002940
118, 102, 86, 98 / 30836 = 0.003178
16, 102, 86, 154 / 46250 = 0.003330
16, 68, 120, 174 / 46251 = 0.003762

well, 6:7 shares this tuple 16, 68, 120.  and it's an arithmetic progression.
204 is a popular sum (16+68+120).
the percentage isn't particularly good.

second run is depth of 2, and breadth of 2000.

526, 1582, 22 / 2991 = 0.007355
1444, 596, 24 / 3091 = 0.007764
1844, 1436, 15 / 1922 = 0.007804
204, 1436, 30 / 3842 = 0.007808
1954, 1616, 14 / 1767 = 0.007923
1024, 1436, 21 / 2559 = 0.008206
1240, 1242, 21 / 2542 = 0.008261
696, 698, 38 / 4520 = 0.008407
1920, 1922, 14 / 1643 = 0.008521
1512, 1514, 23 / 2087 = 0.011021

hmm the pecentage is about 3 times higher.  down from 174 hits to 23 though.
there's a worry that we're just trying to predict somewhat random numbers here, and that any of the top 5 are just as good.   there's no frequent sum.

and just for kicks, let's try a depth of 1 and a breadth of 100000.

65760, 2 / 50 = 0.040000
79103, 2 / 42 = 0.047619
82356, 2 / 41 = 0.048780
83808, 2 / 40 = 0.050000
90112, 2 / 37 = 0.054054
93902, 2 / 36 = 0.055556

i'm sure we're just in la-la guessing land here.
but maybe 93902 is worth trying.

let's say the minimum count has to be 10 or higher.
160, 21 / 19656 = 0.001068
224, 15 / 14041 = 0.001068
240, 14 / 13105 = 0.001068
336, 10 / 9360 = 0.001068
84, 40 / 37438 = 0.001068
192, 18 / 16379 = 0.001099
168, 22 / 18720 = 0.001175
420, 10 / 7489 = 0.001335
416, 11 / 7561 = 0.001455
384, 12 / 8190 = 0.001465

384 is the best i guess.   no real standouts, except for maybe 84.

so that covers our best guess on how to look forward for more sixes.
this next run is the same as the first but sorted by hits.

4, 2, 0, 737 / 1048192 = 0.000703
4, 1, 1, 737 / 1572288 = 0.000469
4, 0, 2, 737 / 1048192 = 0.000703
3, 1, 2, 737 / 1572288 = 0.000469
2, 2, 2, 737 / 1572288 = 0.000469
2, 2, 0, 737 / 1572288 = 0.000469
2, 1, 1, 737 / 2358432 = 0.000312
2, 0, 2, 737 / 1572288 = 0.000469
2, 0, 0, 737 / 1572288 = 0.000469
1, 3, 2, 737 / 1572288 = 0.000469
1, 2, 0, 737 / 2096384 = 0.000352
1, 1, 2, 737 / 2358432 = 0.000312
1, 1, 1, 737 / 3144576 = 0.000234
1, 1, 0, 737 / 3144576 = 0.000234
1, 0, 2, 737 / 2096384 = 0.000352
1, 0, 1, 737 / 3144576 = 0.000234
1, 0, 0, 737 / 3144576 = 0.000234
0, 4, 2, 737 / 1048192 = 0.000703
0, 2, 2, 737 / 1572288 = 0.000469
0, 2, 0, 737 / 1572288 = 0.000469
0, 1, 2, 737 / 2096384 = 0.000352
0, 1, 1, 737 / 3144576 = 0.000234
0, 1, 0, 737 / 3144576 = 0.000234
0, 0, 2, 737 / 1572288 = 0.000469
0, 0, 1, 737 / 3144576 = 0.000234
4, 6, 2, 609 / 786144 = 0.000775

the best is up by 4s and 2s.

results:


so looking forward for more sixes, pick one of "93902", "90112", or "83808".  Try "384", and "1512, 1514", and "16, 68, 120".

we can recalculate quicker by using the pattern "4,2" and still get them all.

but wait, there's more:
what if we do all this again but start at the first B value instead of 1.

our first run has depth of 3, and breadth is 121, and a start of 1681 in sq-seq-finder.

86, 78, 82, 134 / 38362 = 0.003493
116, 48, 82, 135 / 38361 = 0.003519
92, 72, 82, 135 / 38361 = 0.003519
84, 80, 82, 136 / 38363 = 0.003545
96, 68, 82, 136 / 38361 = 0.003545
50, 114, 82, 137 / 38364 = 0.003571
54, 110, 82, 137 / 38364 = 0.003571
108, 56, 82, 138 / 38360 = 0.003597
72, 92, 82, 138 / 38365 = 0.003597
110, 54, 82, 139 / 38360 = 0.003624

well it sure is weird how they all end in 82, and have a sum of 246 (110+54+82).
given the tight distribution i think this means they're all as good as each other.

no arithmetic progressions here.

the best of the bunch is "110, 54, 72".

we did a little worse with a different starting point in terms of percentage.

okay, let's go with depth=2 breadth = 2000 and see how that shakes out.

1476, 492, 29 / 3203 = 0.009054
1880, 1962, 15 / 1644 = 0.009124
1968, 984, 20 / 2142 = 0.009337
492, 984, 40 / 4274 = 0.009359
984, 246, 49 / 5125 = 0.009561
656, 738, 44 / 4523 = 0.009728
1804, 1476, 19 / 1924 = 0.009875
164, 1476, 40 / 3846 = 0.010400
1472, 1554, 22 / 2086 = 0.010547
984, 1476, 33 / 2564 = 0.012871

hmm, well we did a tiny bit better. 1476 appears a few times.
i guess "984, 1476" is the best of the bunch.

okay let's do depth=1, and breadth is 100000

99399, 2 / 34 = 0.058824
99510, 2 / 34 = 0.058824
99630, 2 / 34 = 0.058824
99675, 2 / 34 = 0.058824
99876, 2 / 34 = 0.058824
83804, 3 / 41 = 0.073171
89790, 3 / 39 = 0.076923

well nearly 8 percent is high.  does it pan out?  probably not.
and 8 is a lot better than 5 percent from earlier.

let's do depth=1, breadth is 100000, min hits of 10
984, 20 / 3203 = 0.006244
1230, 17 / 2564 = 0.006630
738, 30 / 4270 = 0.007026
1968, 12 / 1605 = 0.007477
492, 50 / 6404 = 0.007808
2214, 12 / 1431 = 0.008386
2706, 10 / 1168 = 0.008562
1476, 19 / 2139 = 0.008883
2460, 12 / 1290 = 0.009302
2952, 10 / 1073 = 0.009320

well i suppose the 492 stands out with 50 hits.
but 2952 is the best.

so overall, looking forward for more sixes, pick one of "93902", "90112", or "83808", and then one of "99876", "83804", or "89790".  Try "1512, 1514", "16, 68, 120", "984, 1476", and "110, 54, 82".  and throw in "384" and "2952".

okay, let's do the first run again but sort on hits to see how it shakes out.

2, 4, 0, 737 / 1048180 = 0.000703
2, 3, 1, 737 / 1572269 = 0.000469
2, 2, 2, 737 / 1572269 = 0.000469
2, 2, 0, 737 / 1572269 = 0.000469
2, 1, 3, 737 / 1572269 = 0.000469
2, 1, 1, 737 / 2358403 = 0.000312
2, 1, 0, 737 / 2096358 = 0.000352
2, 0, 4, 737 / 1048180 = 0.000703
2, 0, 2, 737 / 1572269 = 0.000469
2, 0, 1, 737 / 2096358 = 0.000352
2, 0, 0, 737 / 1572269 = 0.000469
1, 1, 4, 737 / 1572270 = 0.000469
1, 1, 2, 737 / 2358403 = 0.000312
1, 1, 1, 737 / 3144537 = 0.000234
1, 1, 0, 737 / 3144537 = 0.000234
1, 0, 1, 737 / 3144537 = 0.000234
1, 0, 0, 737 / 3144537 = 0.000234
0, 2, 4, 737 / 1048180 = 0.000703
0, 2, 2, 737 / 1572269 = 0.000469
0, 2, 1, 737 / 2096358 = 0.000352
0, 2, 0, 737 / 1572269 = 0.000469
0, 1, 1, 737 / 3144537 = 0.000234
0, 1, 0, 737 / 3144537 = 0.000234
0, 0, 2, 737 / 1572269 = 0.000469
0, 0, 1, 737 / 3144537 = 0.000234
6, 2, 4, 609 / 786135 = 0.000775

no better than before. gotta go up by 2,4 with this starting point to get them all.

this concludes our analysis of anticipating B values in 6:8 magic squares.

in order to get high value 6:8s we can amalgamate these prospective patterns together.
put the lower values in one run, and the higher values in another.

or if i really want to find out which pattern is particularly awesome, run them all separately (and then rehit some similar B values)

or do the work to track which B values we've used and then don't recalculate what we already have but that's a pain but saves a lot of time.

it might be interesting to try the values that are common to all of these patterns (if any.)

also we could consider running this test again but including all D values as B values.

we were hoping to find some grand pattern, and it didn't happen here.

try some of the patterns like so:

sq-seq --pattern="93902" 1 10000000000000000000000000000 | ./search-68
sq-seq --pattern="90112" 1 10000000000000000000000000000 | ./search-68
sq-seq --pattern="83808" 1 10000000000000000000000000000 | ./search-68
sq-seq --pattern="1512,1514" 1 10000000000000000000000000000 | ./search-68
sq-seq --pattern="16,68,120" 1 10000000000000000000000000000 | ./search-68
sq-seq --pattern="384" 1 10000000000000000000000000000 | ./search-68

sq-seq --pattern="99876" 1681 10000000000000000000000000000 | ./search-68
sq-seq --pattern="83804" 1681 10000000000000000000000000000 | ./search-68
sq-seq --pattern="89790" 1681 10000000000000000000000000000 | ./search-68
sq-seq --pattern="984,1476" 1681 10000000000000000000000000000 | ./search-68
sq-seq --pattern="110,54,82" 1681 10000000000000000000000000000 | ./search-68
sq-seq --pattern="2952" 1681 10000000000000000000000000000 | ./search-68
*/
