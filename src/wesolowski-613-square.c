/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_wesolowski_613_square_t
{
  int num_args;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int threads;
  FILE *infile;
  int from_stdin;
  int in_binary;
  mpz_t n;
};

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_613_square (struct fv_app_wesolowski_613_square_t *app, mpz_t n, mpz_t (*s)[3][3])
{
  mpz_t w, x, y, z, n2, twon, fourn, sixn;
  mpz_inits (w, x, y, z, n2, twon, fourn, sixn, NULL);
  mpz_mul (n2, n, n);
  mpz_mul_ui (twon, n, 2);
  mpz_mul_ui (fourn, n, 4);
  mpz_mul_ui (sixn, n, 6);

  // w = 6n^2 + 6n + 2
  // x = 2n + 1
  // y = 3n^2 + 2n
  // z = 3n^2 + 4n + 1

  mpz_mul_ui (w, n2, 6);
  mpz_add (w, w, sixn);
  mpz_add_ui (w, w, 2);

  mpz_add_ui (x, twon, 1);

  mpz_mul_ui (y, n2, 3);
  mpz_add (y, y, twon);

  mpz_mul_ui (z, n2, 3);
  mpz_add (z, z, fourn);
  mpz_add_ui (z, z, 1);

    {
      mpz_t w2, x2, y2, z2, p, q, sum_minus_middle;
      mpz_inits (w2, x2, y2, z2, p, q, sum_minus_middle, NULL);
      mpz_mul (w2, w, w);
      mpz_mul (x2, x, x);
      mpz_mul (y2, y, y);
      mpz_mul (z2, z, z);

      mpz_mul (p, x2, y2);
      mpz_mul (q, w2, z2);
      mpz_add ((*s)[1][1], p, q);
      mpz_mul_ui (sum_minus_middle, (*s)[1][1], 2);

      mpz_mul (p, x2, z2);
      mpz_mul (q, w2, y2);
      mpz_add ((*s)[2][0], p, q);

      mpz_mul (p, w, y);
      mpz_mul (q, x, z);
      mpz_sub ((*s)[0][1], p, q);
      mpz_mul ((*s)[0][1], (*s)[0][1], (*s)[0][1]);

      mpz_mul (p, w, z);
      mpz_mul (q, x, y);
      mpz_sub ((*s)[2][2], p, q);
      mpz_mul ((*s)[2][2], (*s)[2][2], (*s)[2][2]);

      mpz_mul (p, w, y);
      mpz_mul (q, x, z);
      mpz_add ((*s)[1][2], p, q);
      mpz_mul ((*s)[1][2], (*s)[1][2], (*s)[1][2]);


      mpz_sub ((*s)[0][0], sum_minus_middle, (*s)[2][2]);
      mpz_sub ((*s)[1][0], sum_minus_middle, (*s)[1][2]);
      mpz_sub ((*s)[2][1], sum_minus_middle, (*s)[0][1]);
      mpz_sub ((*s)[0][2], sum_minus_middle, (*s)[2][0]);

      pthread_mutex_lock (&display_lock);
      app->display_square (*s, app->out);
      fflush (app->out);
      pthread_mutex_unlock (&display_lock);
      mpz_clears (w2, x2, y2, z2, p, q, sum_minus_middle, NULL);
    }
  mpz_clears (w, x, y, z, n2, twon, fourn, sixn, NULL);
}

static void*
process_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_wesolowski_613_square_t *app =
    (struct fv_app_wesolowski_613_square_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  mpz_t n;
  mpz_init (n);
  while (1)
    {
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      mpz_set (n, app->n);
      mpz_add_ui (app->n, app->n, 1);

      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      generate_613_square (app, n, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  mpz_clear (n);
  return NULL;
}

static void*
process_num_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_wesolowski_613_square_t *app =
    (struct fv_app_wesolowski_613_square_t *) param->data;

  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  while (1)
    {
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          ssize_t read =
            binary_read_one_number_from_stream (app->infile, &app->n, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
            }
        }
      else
        {
          ssize_t read =
            read_one_number_from_stream (app->infile, &app->n, &line, &len);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      generate_613_square (app, app->n, &s);
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);


  if (line)
    free (line);
  return NULL;
}

int
fituvalu_wesolowski_613_square (struct fv_app_wesolowski_613_square_t *app)
{
  if (app->from_stdin)
    run_threads (app, app->threads, process_num_record);
  else
    run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_wesolowski_613_square_t *app = (struct fv_app_wesolowski_613_square_t *) state->input;
  switch (key)
    {
    case 'i':
      app->in_binary = 1;
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          mpz_set_str (app->n, arg, 10);
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      mpz_init (app->n);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args == 0)
        app->from_stdin = 1;
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[NUM]",
  "Generate 3x3 magic squares of the type 6:3 given NUM.  If NUM is not provided it is read from the standard input.\vThis solution was developed by Arkadiusz Wesolowski.  Only one magic square is created per record on the standard input.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_wesolowski_613_square_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.threads = 1;
  app.infile = stdin;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_wesolowski_613_square (&app);
}
