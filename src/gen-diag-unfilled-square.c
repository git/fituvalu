/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

struct fv_app_search_six_two_t
{
  int empty;
  int num_args;
  FILE *infile;
  int (*read_numbers)(FILE *, mpz_t *, int, char **, size_t *);
  void (*display_square) (mpz_t s[3][3], FILE *out);
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_six_two_t *app = (struct fv_app_search_six_two_t *) state->input;
  switch (key)
    {
    case 'e':
      app->empty = atoi (arg);
      break;
    case 'i':
      app->read_numbers = binary_read_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 1)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading (%m)", arg);
        }
      break;
    case ARGP_KEY_NO_ARGS:
      app->infile = stdin;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static void
sq_seq (struct fv_app_search_six_two_t *app, mpz_t start, mpz_t finish, mpz_t s[3][3], FILE *out, int j, int k)
{
  mpz_t i, root, lastroot;
  mpz_inits (i, root, lastroot, NULL);
  mpz_set (i, start);
  mpz_sqrt (root, i);
  if (mpz_cmp_ui (root, 0) == 0)
    mpz_add_ui (root, root, 1);
  mpz_mul (i, root, root);
  mpz_sqrt (lastroot, finish);

  mpz_set (s[j][k], i);
  app->display_square (s, out);

  while (mpz_cmp (root, lastroot) < 0)
    {
      mpz_add (i, i, root);
      mpz_add (i, i, root);
      mpz_add_ui (i, i, 1);
      mpz_add_ui (root, root, 1);
      mpz_set (s[j][k], i);
      app->display_square (s, out);
    }
  mpz_clears (i, root, lastroot, NULL);
}

static void
clear_square (struct fv_app_search_six_two_t *app, mpz_t s[3][3])
{
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_set_si (s[i][j], app->empty);
}

static void
create_square (struct fv_app_search_six_two_t *app, mpz_t a[3], mpz_t s[3][3], FILE *out)
{
  mpz_t one;
  mpz_init (one);
  mpz_set_ui (one, 1);

  mpz_set (s[2][0], a[0]);
  mpz_set (s[1][1], a[1]);
  mpz_set (s[0][2], a[2]);
  sq_seq (app, one, a[2], s, out, 1, 2);
  mpz_set_si (s[1][2], app->empty);

  mpz_set (s[2][0], a[0]);
  mpz_set (s[1][1], a[1]);
  mpz_set (s[0][2], a[2]);
  sq_seq (app, one, a[2], s, out, 1, 0);
  mpz_set_si (s[1][0], app->empty);

  mpz_clear (one);
}

int
fituvalu_search_six_two (struct fv_app_search_six_two_t *app, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3];
  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  clear_square (app, s);
  for (int i = 0; i < 3; i++)
    mpz_init (a[i]);
  while (1)
    {
      read = app->read_numbers (app->infile, a, 3, &line, &len);
      if (read == -1)
        break;
      create_square (app, a, s, out);
    }

  for (int i = 0; i < 3; i++)
    mpz_clear (a[i]);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "empty", 'e', "NUM", 0, "Values marked as empty are NUM instead of zero"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Accept progressions of three squares from FILE, and make them the diagonal in a magic square.  Iterate over other cells in the square to find new magic squares.\vIf FILE is not provided, it is read from the standard input.  This program produces unfilled magic squares that can be filled in with the `fill-square' program.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_six_two_t app;
  memset (&app, 0, sizeof (app));
  app.read_numbers = read_numbers_from_stream;
  app.display_square = display_square_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_six_two (&app, stdout);

}
