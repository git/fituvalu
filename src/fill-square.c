/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include "magicsquareutil.h"


struct transform_rec
{
  int target_x, target_y;
  int indices[2][2];
};
struct fv_app_fill_square_t
{
  FILE *transform_infile;
  struct transform_rec * transforms;
  int num_transforms;

  int display_solution;
  int (*read_square)(FILE *, mpz_t (*s)[3][3], char **, size_t *);
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int empty;
  int magic;
};

static void
get_sum (struct fv_app_fill_square_t *app, mpz_t s[3][3], int a, int b, int c, int d, int e, int f, mpz_t sum)
{
  if (mpz_cmp_si (s[a][b], app->empty) != 0 && mpz_cmp_si (s[c][d], app->empty) != 0 &&
      mpz_cmp_si (s[e][f], app->empty) != 0)
    {
      mpz_set (sum, s[a][b]);
      mpz_add (sum, sum, s[c][d]);
      mpz_add (sum, sum, s[e][f]);
    }
}

//return 1 if there are lines and they all add up to the same number.
//dump the magic number into outsum.
static int
check_lines (struct fv_app_fill_square_t *app, mpz_t s[3][3], mpz_t outsum)
{
  int ret = 0;
  int tb[8][6] = 
    {
        { 0, 0, 0, 1, 0, 2 },
        { 1, 0, 1, 1, 1, 2 },
        { 2, 0, 2, 1, 2, 2 },
        { 0, 0, 1, 0, 2, 0 },
        { 0, 1, 1, 1, 2, 1 },
        { 0, 2, 1, 2, 2, 2 },
        { 0, 0, 1, 1, 2, 2 },
        { 0, 2, 1, 1, 2, 0 },
    };
  mpz_t sum, t;
  mpz_inits (sum, t, NULL);
  for (int i = 0; i < 8; i++)
    {
      get_sum (app, s,
               tb[i][0], tb[i][1], tb[i][2], tb[i][3], tb[i][4], tb[i][5], t);
      if (mpz_cmp_ui (t, 0) != 0)
        {
          if (mpz_cmp_ui (sum, 0) == 0)
            mpz_set (sum, t);
          if (mpz_cmp (sum, t) != 0)
            {
              ret = 0;
              break;
            }
          else
            {
              mpz_set (outsum, sum);
              ret = 1;
            }
        }
    }
  mpz_clears (sum, t, NULL);
  return ret;
}

//fill in the zero at i,j
static int
solve_zero (mpz_t s[3][3], int z[3][3], int i, int j, mpz_t n, int disp, FILE *out)
{
  int count = 0;
  mpz_t sum;
  mpz_init (sum);

  mpz_set_ui (sum, 0);
  count = 0;
  //do the row
  for (int k = 0; k < 3; k++)
    {
      if (j == k)
        continue;
      if (z[i][k])
        continue;
      //do i, k
      mpz_add (sum, sum, s[i][k]);
      count++;
    }
  if (count == 2)
    {
      mpz_sub (s[i][j], n, sum);
      mpz_clear (sum);
      if (disp)
        {
          for (int k = 0; k < 3; k++)
            {
              if (j == k)
                continue;
              if (z[i][k])
                continue;
              fprintf (out, "%d, %d, ", i, k);
            }
          fprintf (out, "%d, %d,\n", i, j);
        }
      return 1;
    }

  //do the column
  mpz_set_ui (sum, 0);
  count = 0;
  for (int k = 0; k < 3; k++)
    {
      if (i == k)
        continue;
      if (z[k][j])
        continue;
      //do k, j
      mpz_add (sum, sum, s[k][j]);
      count++;
    }
  if (count == 2)
    {
      mpz_sub (s[i][j], n, sum);
      mpz_clear (sum);
      if (disp)
        {
          for (int k = 0; k < 3; k++)
            {
              if (i == k)
                continue;
              if (z[k][j])
                continue;
              fprintf (out, "%d, %d, ", k, j);
            }
          fprintf (out, "%d, %d,\n", i, j);
        }
      return 1;
    }
  //do main diagonal
  if ((i == 0 && j == 0) || (i == 2 && j == 2))
    {
      mpz_set_ui (sum, 0);
      count = 0;
      for (int k = 0; k < 3; k++)
        {
          if (k == i && k == j)
            continue;
          if (z[k][k])
            continue;
          mpz_add (sum, sum, s[k][k]);
          count++;
        }
      if (count == 2)
        {
          mpz_sub (s[i][j], n, sum);
          mpz_clear (sum);
          if (disp)
            {
              for (int k = 0; k < 3; k++)
                {
                  if (k == i && k == j)
                    continue;
                  if (z[k][k])
                    continue;
                  fprintf (out, "%d, %d, ", k, k);
                }
              fprintf (out, "%d, %d,\n", i, j);
            }
          return 1;
        }
    }
  //do alternate diagonal
  if ((i == 2 && j == 0) || (i == 0 && j == 2))
    {
      mpz_set_ui (sum, 0);
      count = 0;
      for (int k = 0, l = 2; k < 3; k++, l--)
        {
          if (k == i && l == j)
            continue;
          if (z[k][l])
            continue;
          mpz_add (sum, sum, s[k][l]);
          count++;
        }
      if (count == 2)
        {
          mpz_sub (s[i][j], n, sum);
          mpz_clear (sum);
          if (disp)
            {
              for (int k = 0, l = 2; k < 3; k++, l--)
                {
                  if (k == i && l == j)
                    continue;
                  if (z[k][l])
                    continue;
                  fprintf (out, "%d, %d, ", k, l);
                }
              fprintf (out, "%d, %d,\n", i, j);
            }
          return 1;
        }
    }
  mpz_clear (sum);
  return 0;
}

//fill in a zero, and return 1 if we did.
static int
solve_zeroes (mpz_t s[3][3], int (*z)[3][3], mpz_t n, int disp, FILE *out)
{
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      {
        if ((*z)[i][j])
          {
            if (solve_zero (s, *z, i, j, n, disp, out))
              {
                (*z)[i][j] = 0;
                return 1;
              }
          }
      }
  return 0;
}

static void
get_magic_number (struct fv_app_fill_square_t *app, mpz_t s[3][3], mpz_t num)
{
  //we need the magic number. it is either s[1][1] times 3 or
  //the sum of a row/column
  if (mpz_cmp_si (s[1][1], app->empty) != 0)
    {
      mpz_mul_ui (num, s[1][1], 3);
      return;
    }
  int zeroes[3][3];
  memset (zeroes, 0, sizeof (zeroes));
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      zeroes[i][j] = mpz_cmp_si (s[i][j], app->empty) == 0;
  //top row
  if (zeroes[0][0] && zeroes[0][1] && zeroes[0][2])
    {
      for (int i = 0; i < 3; i++)
        mpz_add (num, num, s[0][i]);
      return;
    }
  //bottom row
  if (zeroes[2][0] && zeroes[2][1] && zeroes[2][2])
    {
      for (int i = 0; i < 3; i++)
        mpz_add (num, num, s[2][i]);
      return;
    }
  //leftmost column
  if (zeroes[0][0] && zeroes[1][0] && zeroes[2][0])
    {
      for (int i = 0; i < 3; i++)
        mpz_add (num, num, s[i][0]);
      return;
    }
  //rightmost column
  if (zeroes[0][2] && zeroes[1][2] && zeroes[2][2])
    {
      for (int i = 0; i < 3; i++)
        mpz_add (num, num, s[i][2]);
      return;
    }
}

static void
transform_square (struct fv_app_fill_square_t *app, mpz_t s[3][3])
{
  mpz_t num;
  mpz_init (num);
  get_magic_number (app, s, num);

  for (int i = 0; i < app->num_transforms; i++)
    {
      struct transform_rec *rec = &app->transforms[i];
      mpz_set (s[rec->target_x][rec->target_y], num);
      for (int j = 0; j < 2; j++)
        mpz_sub (s[rec->target_x][rec->target_y],
                 s[rec->target_x][rec->target_y],
                 s[rec->indices[j][0]][rec->indices[j][1]]);
    }
  mpz_clear (num);
}

static int
fill_center (struct fv_app_fill_square_t *app, mpz_t s[3][3], int z[3][3], mpz_t *n)
{
  // there are four ways to get to the center when we don't have a
  // complete row or column

  mpz_t i, j, diff;
  mpz_inits (i, j, diff, NULL);
  if (!z[0][1] && !z[2][1]) // center column
    {
      mpz_set (i, s[0][1]);
      mpz_set (j, s[2][1]);
    }
  else if (!z[1][0] && !z[1][2]) // center row
    {
      mpz_set (i, s[1][0]);
      mpz_set (j, s[1][2]);
    }
  else if (!z[0][0] && !z[2][2]) // main diagonal
    {
      mpz_set (i, s[0][0]);
      mpz_set (j, s[2][2]);
    }
  else if (!z[0][2] && !z[2][0]) // alternate diagaonal
    {
      mpz_set (i, s[0][2]);
      mpz_set (j, s[2][0]);
    }
  else
    {
      mpz_clears (i, j, diff, NULL);
      return 0;
    }
  if (mpz_cmp (i, j) > 0)
    mpz_sub (diff, i, j);
  else
    mpz_sub (diff, j, i);

  mpz_cdiv_q_ui (diff, diff, 2);

  if (mpz_cmp (i, j) > 0)
    mpz_add (*n, j, diff);
  else
    mpz_add (*n, i, diff);

  mpz_clears (i, j, diff, NULL);
  return 1;
}

static int
fill_square (struct fv_app_fill_square_t *app, mpz_t s[3][3], FILE *out)
{
  if (app->num_transforms)
    {
      transform_square (app, s);
      return app->magic ? is_magic_square (s, 1) : 1;
    }
  int zeroes[3][3];
  memset (zeroes, 0, sizeof (zeroes));
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      zeroes[i][j] = mpz_cmp_si (s[i][j], app->empty) == 0;

  mpz_t n;
  mpz_init (n);
  if (!check_lines (app, s, n))
    {
      if (zeroes[1][1])
        {
          if (!fill_center (app, s, zeroes, &s[1][1]))
            {
              mpz_clear (n);
              return 0;
            }
          else
            zeroes[1][1] = 0;
        }
      //hmm, okay we have a center square
      //so we have the magic number
      mpz_add (n, s[1][1], s[1][1]);
      mpz_add (n, n, s[1][1]);
    }
  //fill or check center square
  else if (zeroes[1][1])
    {
      zeroes[1][1] = 0;
      //center square is magic number / 3.
      mpz_t r;
      mpz_init (r);
      mpz_cdiv_qr_ui (s[1][1], r, n, 3);
      if (mpz_cmp_ui (r, 0) != 0)
        {
          mpz_clear (r);
          return 0;
        }
      mpz_clear (r);
      //check if it's distinct
      int distinct = 1;
      for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
          {
            if (i == 1 && j == 1)
              continue;
            if (zeroes[i][j])
              continue;
            if (mpz_cmp (s[i][j], s[1][1]) == 0)
              {
                distinct = 0;
                break;
              }
          }
      if (!distinct)
        return 0;
    }
  else
    {
      //magic number is supposed to be 3x center square
      mpz_t m;
      mpz_init (m);
      mpz_add (m, s[1][1], s[1][1]);
      mpz_add (m, m, s[1][1]);
      if (mpz_cmp (m, n) != 0)
        {
          mpz_clear (n);
          mpz_clear (m);
          return 0;
        }
      mpz_clear (m);
    }
  while (1)
    {
      if (!solve_zeroes (s, &zeroes, n, app->display_solution, out))
        break;
    }
  mpz_clear (n);
  return app->magic ? is_magic_square (s, 1) : 1;
}

int
fituvalu_fill_square (struct fv_app_fill_square_t *app, FILE *stream, FILE *out)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t s[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);

  while (1)
    {
      read = app->read_square (stream, &s, &line, &len);
      if (read == -1)
        break;
      if (fill_square (app, s, out) && !app->display_solution)
        app->display_square (s, out);
    }

  if (line)
    free (line);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  return 0;
}

static void
read_in_transformations (struct fv_app_fill_square_t *app, FILE *in)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  while (!feof (in))
    {
      app->transforms = realloc (app->transforms,
                                 (app->num_transforms + 1) * sizeof (struct transform_rec));
      for (int i = 0; i < 6; i++)
        {
          if (i == 6 - 1)
            read = fv_getline (&line, &len, in);
          else
            read = fv_getdelim (&line, &len, ',', in);
          if (read == -1)
            break;
          char *end = strpbrk (line, ",\n");
          if (end)
            *end = '\0';
          switch (i)
            {
            case 0:
              app->transforms[app->num_transforms].indices[0][0] = atoi (line);
              break;
            case 1:
              app->transforms[app->num_transforms].indices[0][1] = atoi (line);
              break;
            case 2:
              app->transforms[app->num_transforms].indices[1][0] = atoi (line);
              break;
            case 3:
              app->transforms[app->num_transforms].indices[1][1] = atoi (line);
              break;
            case 4:
              app->transforms[app->num_transforms].target_x = atoi (line);
              break;
            case 5:
              app->transforms[app->num_transforms].target_y = atoi (line);
              break;
            }
        }
      app->num_transforms++;
    }
  app->num_transforms--;
  free (line);
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_fill_square_t *app = (struct fv_app_fill_square_t *) state->input;
  switch (key)
    {
    case 'n':
      app->magic = 0;
      break;
    case 't':
      app->transform_infile = fopen (arg, "r");
      if (!app->transform_infile)
        argp_error (state, "could not open `%s' for reading (%m)", arg);
      read_in_transformations (app, app->transform_infile);
      break;
    case 'd':
      app->display_solution = 1;
      break;
    case 'e':
      app->empty = atoi (arg);
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "display-solution", 'd', 0, 0, "Display order in which cells are solved"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "empty", 'e', "NUM", 0, "Values marked as empty are NUM instead of zero"},
  { "transform", 't', "FILE", 0, "Manually fill the square according to FILE"},
  { "non-magic", 'n', 0, 0, "Show non-magic squares."},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 9 numbers representing a partially-filled magic square from the standard input, and try to fill in the zeroes to make it magic.\vThis program only tries to fill in the magic square if the center square has a value, or there is a line already filled-in.  --display-solution shows the order in which cells (represented by x,y pairs) are summed and then subtracted from the magic number.  This option provides the input data for the --transform option.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_fill_square_t app;
  memset (&app, 0, sizeof (app));
  app.read_square = read_square_from_stream;
  app.display_square = display_square_record;
  app.magic = 1;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  is_magic_square_init ();
  return fituvalu_fill_square (&app, stdin, stdout);
}
