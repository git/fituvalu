/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <error.h>
#include "magicsquareutil.h"

struct fv_app_find_3sq_wesolowski_t
{
  int num_args;
  FILE *out;
  void (*display_tuple) (mpz_t *s, FILE *out);
  int in_binary;
  mpz_t n;
};

struct thread_data_t
{
  void *data;
};

static void
generate_3sq (struct fv_app_find_3sq_wesolowski_t *app, mpz_t n)
{
  mpz_t a[3];
  mpz_inits (a[0], a[1], a[2], NULL);
  mpz_t b[3];
  mpz_inits (b[0], b[1], b[2], NULL);
  mpz_t w, x, y, z, n2, twon, fourn, sixn;
  mpz_inits (w, x, y, z, n2, twon, fourn, sixn, NULL);
  mpz_mul (n2, n, n);
  mpz_mul_ui (twon, n, 2);
  mpz_mul_ui (fourn, n, 4);
  mpz_mul_ui (sixn, n, 6);

  // w = 6n^2 + 6n + 2
  // x = 2n + 1
  // y = 3n^2 + 2n
  // z = 3n^2 + 4n + 1

  mpz_mul_ui (w, n2, 6);
  mpz_add (w, w, sixn);
  mpz_add_ui (w, w, 2);

  mpz_add_ui (x, twon, 1);

  mpz_mul_ui (y, n2, 3);
  mpz_add (y, y, twon);

  mpz_mul_ui (z, n2, 3);
  mpz_add (z, z, fourn);
  mpz_add_ui (z, z, 1);

    {
      mpz_t w2, x2, y2, z2, p, q, sum_minus_middle;
      mpz_inits (w2, x2, y2, z2, p, q, sum_minus_middle, NULL);
      mpz_mul (w2, w, w);
      mpz_mul (x2, x, x);
      mpz_mul (y2, y, y);
      mpz_mul (z2, z, z);

      mpz_mul (p, x2, y2);
      mpz_mul (q, w2, z2);
      mpz_add (a[1], p, q);
      mpz_mul_ui (sum_minus_middle, a[1], 2);

      mpz_mul (p, x2, z2);
      mpz_mul (q, w2, y2);
      mpz_add (b[1], p, q);

      mpz_mul (p, w, y);
      mpz_mul (q, x, z);
      mpz_sub (b[0], p, q);
      mpz_mul (b[0], b[0], b[0]);

      mpz_mul (p, w, z);
      mpz_mul (q, x, y);
      mpz_sub (a[0], p, q);
      mpz_mul (a[0], a[0], a[0]);

      mpz_mul (p, w, y);
      mpz_mul (q, x, z);
      mpz_add (b[2], p, q);
      mpz_mul (b[2], b[2], b[2]);


      mpz_sub (a[2], sum_minus_middle, a[0]);

      app->display_tuple(a, app->out);
      app->display_tuple(b, app->out);
      fflush (app->out);

      mpz_clears (w2, x2, y2, z2, p, q, sum_minus_middle, NULL);
    }
  mpz_clears (w, x, y, z, n2, twon, fourn, sixn, NULL);
  mpz_clears  (a[0], a[1], a[2], NULL);
  mpz_clears  (b[0], b[1], b[2], NULL);
}

int
fituvalu_find_3sq_wesolowski (struct fv_app_find_3sq_wesolowski_t *app)
{
  mpz_t n;
  mpz_init (n);
  while (1)
    {
      mpz_set (n, app->n);
      mpz_add_ui (app->n, app->n, 1);

      generate_3sq (app, n);
    }

  mpz_clear (n);
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_find_3sq_wesolowski_t *app = (struct fv_app_find_3sq_wesolowski_t *) state->input;
  switch (key)
    {
    case 'i':
      app->in_binary = 1;
      break;
    case 'o':
      app->display_tuple = display_binary_three_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          mpz_set_str (app->n, arg, 10);
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      mpz_init (app->n);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args != 1)
        argp_error (state, "too few arguments");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "NUM",
  "Generate three square progressions given NUM.\vThis parametric solution was developed by Arkadiusz Wesolowski.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_find_3sq_wesolowski_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.display_tuple = display_three_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_find_3sq_wesolowski (&app);
}
/*
 * comes from wesolowski's 6:13 generator
 */
