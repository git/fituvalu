/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_progressions_t
{
  int num_args;
  int from_stdin;
  unsigned long long int num;
  int max_segments;
  int segment;
  int debug;
  int threads;
  FILE *out;
};


static void
calculate_segment_length (int max_seg, unsigned long long int total, unsigned long long int *seg)
{
  long double m = 1.0 / max_seg;
  *seg = total * (long double)m;
}

static void
calculate_segment (int seg, int max_seg, unsigned long long int *start, unsigned long long int *end)
{
  unsigned long long int root, start_root, end_root, segment_length;

  // okay we have a total set of ROOT squares
  root = sqrtl (*end);

  // of our total, the segment we'll be working on is SEGMENT LENGTH squares
  calculate_segment_length (max_seg, root, &segment_length);

  // START ROOT is just the i-th chunk
  start_root = segment_length * (seg - 1);

  // END ROOT is just start plus our length
  end_root = start_root + segment_length;

  // finally, translate back to squares
  *start = start_root * start_root;
  *end = end_root * end_root;
}

struct thread_data_t
{
  void *data;
  int seg;
  int max_seg;
  unsigned long long int n;
};

static void*
generate_and_show_for_segment (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_progressions_t *app =
    (struct fv_app_search_progressions_t *) param->data;

  unsigned long long int i = 1, iroot = 1, limit = param->n / 2, diff;
  unsigned long long int twomn;

  if (param->max_seg != 1)
    {
      calculate_segment (param->seg, param->max_seg, &i, &limit);
      iroot = sqrtl (i);
    }

  if (app->debug)
    printf ("going from %llu to %llu\n", i, limit);

  // from https://www.fq.math.ca/Papers1/43-2/paper43-2-1.pdf
  while (i <= limit)
    {
      diff = param->n - i;
      if (small_is_square (diff))
        {
          twomn = iroot * sqrtl (diff) * 2;

          if (app->threads)
            pthread_mutex_lock (&display_lock);
          fprintf (app->out, "%llu, %llu, %llu, \n", param->n - twomn,
                   param->n, param->n + twomn);
          fflush (app->out);
          if (app->threads)
            pthread_mutex_unlock (&display_lock);
        }
      i += (iroot * 2) + 1;
      iroot++;
    }
  return NULL;
}

static void
run_threads (void *data, int num_threads, unsigned long long int n, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      param[i].seg = i + 1;
      param[i].max_seg = num_threads;
      param[i].n = n;

      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static void
generate_progression_and_show (struct fv_app_search_progressions_t *app, unsigned long long int num)
{
  run_threads (app, app->threads, num, generate_and_show_for_segment);
  return;
}

int
fituvalu_search_progression (struct fv_app_search_progressions_t *app, FILE *in)
{
  if (app->from_stdin)
    {
      unsigned long long int num;
      char *line = NULL;
      size_t len = 0;
      ssize_t read;
      char *end = NULL;

      while (1)
        {
          read = fv_getline (&line, &len, in);
          if (read == -1)
            break;
          num = strtoull (line, &end, 10);
          generate_progression_and_show (app, num);
        }

      if (line)
        free (line);
    }
  else
    generate_progression_and_show (app, app->num);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_progressions_t *app = (struct fv_app_search_progressions_t *) state->input;
  switch (key)
    {
    case 't':
      app->threads = atoi (arg);
      break;
    case 'd':
      app->debug = 1;
      break;
    case 's':
        {
          int m, n;
          int retval = sscanf (arg, "%d/%d", &m, &n);
          if (retval != 2 || m <= 0 || n < m || n <= 0)
            argp_error (state, "invalid argument `%s' to option --segment", arg);

          app->segment = m;
          app->max_segments = n;
        }
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          char *end = NULL;
          switch (app->num_args)
            {
            case 0:
              app->num = strtoull (arg, &end, 10);
              break;
            }
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    case ARGP_KEY_FINI:
      if (app->num_args == 0)
        app->from_stdin = 1;
      if (app->max_segments != 1 && app->threads > 1)
        argp_error (state, "--threads and --segment cannot be used together");
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "debug", 'd', 0, OPTION_HIDDEN, "Show extra information while running"},
  { "segment", 's', "N/M", 0, "Only do a portion, do segment N of M" },
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[CENTER]",
  "Find arithmetic progressions consisting of two squares around a given CENTER.\vWhen CENTER is not provided, it is read from the standard input.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_progressions_t app;
  memset (&app, 0, sizeof (app));
  app.segment = 1;
  app.max_segments = 1;
  app.threads = 1;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_search_progression (&app, stdin);
}
