/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <gmp.h>
#include <pthread.h>
#include "magicsquareutil.h"

#define OPT_INT -644

struct fv_app_morgenstern_search_mnpr_12345678_t;
static int
no_filter (mpz_t a[3][3], int num)
{
  return 1;
}

struct fv_app_morgenstern_search_mnpr_12345678_t
{
  FILE *out;
  FILE *infile;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  void (*dump_record) (mpz_t *, FILE *out);
  int (*read_record) (FILE *, mpz_t *, char **, size_t *);
  int filter_num_squares;
  int num_args;
  int mnpr;
  int (*filter_square1) (mpz_t s[3][3], int);
  int (*filter_square2) (mpz_t s[3][3], int);
  int (*filter_square3) (mpz_t s[3][3], int);
  int (*filter_square4) (mpz_t s[3][3], int);
  int (*filter_square5) (mpz_t s[3][3], int);
  int (*filter_square6) (mpz_t s[3][3], int);
  int (*filter_square7) (mpz_t s[3][3], int);
  int (*filter_square8) (mpz_t s[3][3], int);
  int threads;
  pthread_mutex_t display_lock;
  int ul;
};

static int
search_type_12345678_int (unsigned long m1, unsigned long n1, unsigned long m2, unsigned long n2, void *data, void *p)
{
  int ret = 0;
  struct morgenstern_per_thread_data_t *pt = (struct morgenstern_per_thread_data_t *)p;
  struct fv_app_morgenstern_search_mnpr_12345678_t *app = (struct fv_app_morgenstern_search_mnpr_12345678_t *) data;
  morgenstern_precalc_int (m1, n1, m2, n2, pt);

  pt->prev_type = -1;
  morgenstern_type_1_calc_step_1 (pt);
  morgenstern_type_1_calc_step_2 (pt);
  if (app->filter_square1 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 1;
  morgenstern_type_5_calc_step_1 (pt);
  morgenstern_type_5_calc_step_2 (pt);
  if (app->filter_square5 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 5;
  morgenstern_type_6_calc_step_1 (pt);
  morgenstern_type_6_calc_step_2 (pt);
  if (app->filter_square6 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 6;
  morgenstern_type_4_calc_step_1 (pt);
  morgenstern_type_4_calc_step_2 (pt);
  if (app->filter_square4 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 4;
  morgenstern_type_7_calc_step_1 (pt);
  morgenstern_type_7_calc_step_2 (pt);
  if (app->filter_square7 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        app->display_square (pt->a, app->out);
      else
        return ret;
    }

  pt->prev_type = 7;
  morgenstern_type_3_calc_step_1 (pt);
  morgenstern_type_3_calc_step_2 (pt);
  if (app->filter_square3 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 3;
  morgenstern_type_8_calc_step_1 (pt);
  morgenstern_type_8_calc_step_2 (pt);
  if (app->filter_square8 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 8;
  morgenstern_type_2_calc_step_1 (pt);
  morgenstern_type_2_calc_step_2 (pt);
  if (app->filter_square2 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  return ret;
}
static int
search_type_12345678 (mpz_t m1, mpz_t n1, mpz_t m2, mpz_t n2, void *data, void *p)
{
  int ret = 0;
  struct morgenstern_per_thread_data_t *pt = (struct morgenstern_per_thread_data_t *)p;
  struct fv_app_morgenstern_search_mnpr_12345678_t *app = (struct fv_app_morgenstern_search_mnpr_12345678_t *) data;
  morgenstern_precalc (m1, n1, m2, n2, pt);

  pt->prev_type = -1;
  morgenstern_type_1_calc_step_1 (pt);
  morgenstern_type_1_calc_step_2 (pt);
  if (app->filter_square1 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 1;
  morgenstern_type_5_calc_step_1 (pt);
  morgenstern_type_5_calc_step_2 (pt);
  if (app->filter_square5 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 5;
  morgenstern_type_6_calc_step_1 (pt);
  morgenstern_type_6_calc_step_2 (pt);
  if (app->filter_square6 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 6;
  morgenstern_type_4_calc_step_1 (pt);
  morgenstern_type_4_calc_step_2 (pt);
  if (app->filter_square4 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 4;
  morgenstern_type_7_calc_step_1 (pt);
  morgenstern_type_7_calc_step_2 (pt);
  if (app->filter_square7 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        app->display_square (pt->a, app->out);
      else
        return ret;
    }

  pt->prev_type = 7;
  morgenstern_type_3_calc_step_1 (pt);
  morgenstern_type_3_calc_step_2 (pt);
  if (app->filter_square3 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 3;
  morgenstern_type_8_calc_step_1 (pt);
  morgenstern_type_8_calc_step_2 (pt);
  if (app->filter_square8 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  pt->prev_type = 8;
  morgenstern_type_2_calc_step_1 (pt);
  morgenstern_type_2_calc_step_2 (pt);
  if (app->filter_square2 (pt->a, app->filter_num_squares))
    {
      ret = 1;
      if (!app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock (&app->display_lock);
          app->display_square (pt->a, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock (&app->display_lock);
        }
      else
        return ret;
    }

  return ret;
}

struct thread_data_t
{
  FILE *in;
  void *data;
  pthread_mutex_t *read_lock;
  void *pt;
};

static void
run_threads (FILE *in, void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_mutex_t read_lock;
  pthread_mutex_init (&read_lock, NULL);
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].in = in;
      param[i].data = data;
      param[i].read_lock = &read_lock;
      param[i].pt = morgenstern_per_thread_init ();
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
  for (int i = 0; i < num_threads; i++)
    {
      if (param[i].pt)
        morgenstern_per_thread_destroy (param[i].pt);
    }
}

static int
read_ul_numbers_from_stream (FILE *stream, unsigned long *a, int size, char **line, size_t *len)
{
  int i;
  ssize_t read = 0;
  for (i = 0; i < size; i++)
    {
      if (i == size - 1)
        read = getline (line, len, stream);
      else
        read = getdelim (line, len, ',', stream);
      if (read == -1)
        break;
      char *end = strpbrk (*line, ",\n");
      if (end)
        *end = '\0';
      a[i] = strtoul (*line, &end, 10);
    }
  return read;
}
static void*
process_ul_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  FILE *in = param->in;
  struct fv_app_morgenstern_search_mnpr_12345678_t *app = 
    (struct fv_app_morgenstern_search_mnpr_12345678_t *) param->data;
  void *pt = param->pt;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  unsigned long v[4];
  while (1)
    {
      pthread_mutex_lock (param->read_lock);
      read = read_ul_numbers_from_stream (in, v, 4, &line, &len);
      pthread_mutex_unlock (param->read_lock);
      if (read == -1)
        break;
      int disp = search_type_12345678_int (v[0], v[1], v[2], v[3], app, pt);
      if (disp && app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock(&app->display_lock);
          fprintf (app->out, "%lu, %lu, %lu, %lu\n", v[0], v[1], v[2], v[3]);
          if (app->threads > 1)
            pthread_mutex_unlock(&app->display_lock);
        }
    }
  //morgenstern_per_thread_destroy (pt);
  if (line)
    free (line);
  return NULL;
}

static void*
process_record (void *arg)
{
  struct thread_data_t *param = (struct thread_data_t *) arg;
  FILE *in = param->in;
  struct fv_app_morgenstern_search_mnpr_12345678_t *app = 
    (struct fv_app_morgenstern_search_mnpr_12345678_t *) param->data;
  void *pt = param->pt;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t v[4];
  for (int i = 0; i < 4; i++)
    mpz_init (v[i]);
  while (1)
    {
      pthread_mutex_lock (param->read_lock);
      read = app->read_record (in, v, &line, &len);
      pthread_mutex_unlock (param->read_lock);
      if (read == -1)
        break;
      int disp = search_type_12345678 (v[0], v[1], v[2], v[3], app, pt);
      if (disp && app->mnpr)
        {
          if (app->threads > 1)
            pthread_mutex_lock(&app->display_lock);
          app->dump_record (v, app->out);
          if (app->threads > 1)
            pthread_mutex_unlock(&app->display_lock);
        }
    }
  //morgenstern_per_thread_destroy (pt);
  for (int i = 0; i < 4; i++)
    mpz_clear (v[i]);
  if (line)
    free (line);
  return NULL;
}

int
fituvalu_morgenstern_search_mnpr_12345678 (struct fv_app_morgenstern_search_mnpr_12345678_t *app, FILE *in)
{
  if (app->ul)
    run_threads (in, app, app->threads, process_ul_record);
  else
    run_threads (in, app, app->threads, process_record);
  return 0;
}

static struct argp_option
options[] =
{
  { "filter", 'f', "NUM", 0, "Only show magic squares that have at least NUM perfect squares" },
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "mnpr", 'p', 0, 0, "Show the coprime pair instead of the magic square"},
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "int", OPT_INT, 0, 0, "Use unsigned integers instead of GMP"},
  { 0 }
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_morgenstern_search_mnpr_12345678_t *app = (struct fv_app_morgenstern_search_mnpr_12345678_t *) state->input;
  switch (key)
    {
    case OPT_INT:
      app->ul = 1;
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case 'p':
      app->mnpr = 1;
      break;
    case 'i':
      app->read_record = binary_read_four_numbers_from_stream;
      break;
    case 'f':
      app->filter_num_squares = atoi (arg);
      app->filter_square1 = morgenstern_type_1_filter;
      app->filter_square2 = morgenstern_type_2_filter;
      app->filter_square3 = morgenstern_type_3_filter;
      app->filter_square4 = morgenstern_type_4_filter;
      app->filter_square5 = morgenstern_type_5_filter;
      app->filter_square6 = morgenstern_type_6_filter;
      app->filter_square7 = morgenstern_type_7_filter;
      app->filter_square8 = morgenstern_type_8_filter;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      app->dump_record = display_binary_four_record;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 1)
        argp_error (state, "too many arguments");
      else
        {
          if (access (arg, R_OK) == 0)
            {
              app->infile = fopen (arg, "r");
              app->num_args++;
            }
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Generate 3x3 magic squares with 5 perfect squares or more by creating two arithmetic progressions of three perfect squares with the center square or the first square in common.\vEither the standard input or FILE provides the parametric \"M,N, P,R\" values -- four values per record to use in the calculation.  They are pairs of coprimes.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_morgenstern_search_mnpr_12345678_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.filter_square1 = no_filter;
  app.filter_square2 = no_filter;
  app.filter_square3 = no_filter;
  app.filter_square4 = no_filter;
  app.filter_square5 = no_filter;
  app.filter_square6 = no_filter;
  app.filter_square7 = no_filter;
  app.filter_square8 = no_filter;
  app.out = stdout;
  app.threads = 1;
  app.read_record = read_four_numbers_from_stream;
  app.dump_record = display_four_record;
  pthread_mutex_init (&app.display_lock, NULL);
  argp_parse (&argp, argc, argv, 0, 0, &app);

  return fituvalu_morgenstern_search_mnpr_12345678
    (&app, app.infile ? app.infile : stdin);
}
