#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <argp.h>
#include "magicsquareutil.h"
struct fv_app_combine_pairs_t
{
  int num_args;
  FILE *infile;
  FILE *out;
  int out_binary;
  int (*read_pair) (FILE *, mpz_t *, char **, size_t *);
  int ull;
  int ul;
};

#define OPT_INT -644

struct rec
{
  mpz_t one;
  mpz_t two;
};

struct urec
{
  unsigned long long one;
  unsigned long long two;
};

struct irec
{
  unsigned long one;
  unsigned long two;
};

static int
read_ul_numbers_from_stream (FILE *stream, unsigned long *a, int size, char **line, size_t *len)
{
  int i;
  ssize_t read = 0;
  for (i = 0; i < size; i++)
    {
      if (i == size - 1)
        read = getline (line, len, stream);
      else
        read = getdelim (line, len, ',', stream);
      if (read == -1)
        break;
      char *end = strpbrk (*line, ",\n");
      if (end)
        *end = '\0';
      a[i] = strtoul (*line, &end, 10);
    }
  return read;
}

static int
read_ull_numbers_from_stream (FILE *stream, unsigned long long *a, int size, char **line, size_t *len)
{
  int i;
  ssize_t read = 0;
  for (i = 0; i < size; i++)
    {
      if (i == size - 1)
        read = getline (line, len, stream);
      else
        read = getdelim (line, len, ',', stream);
      if (read == -1)
        break;
      char *end = strpbrk (*line, ",\n");
      if (end)
        *end = '\0';
      a[i] = strtoull (*line, &end, 10);
    }
  return read;
}

static int
fituvalu_combine_pairs_ull (struct fv_app_combine_pairs_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;

  unsigned long long a[2];
  struct urec *recs = NULL;
  int num_recs = 0;
  mpz_t num;
  mpz_init (num);
  while (1)
    {
      int nread = read_ull_numbers_from_stream (app->infile, a, 2, &line, &len);
      if (nread == -1)
        break;
      recs = realloc (recs, sizeof (struct urec) * (num_recs + 1));
      recs[num_recs].one = a[0];
      recs[num_recs].two = a[1];
      num_recs++;
    }
  free (line);
  for (int i = 0; i < num_recs; i++)
    {
      for (int j = 0; j < num_recs; j++)
        {
          if (i == j)
            continue;
          if (app->out_binary)
            {
              char buf[256];
              snprintf (buf, sizeof (buf), "%llu", recs[i].one);
              mpz_set_str (num, buf, 10);
              display_binary_number (&num, app->out);
              snprintf (buf, sizeof (buf), "%llu", recs[i].two);
              mpz_set_str (num, buf, 10);
              display_binary_number (&num, app->out);
              snprintf (buf, sizeof (buf), "%llu", recs[j].one);
              mpz_set_str (num, buf, 10);
              display_binary_number (&num, app->out);
              snprintf (buf, sizeof (buf), "%llu", recs[j].two);
              mpz_set_str (num, buf, 10);
              display_binary_number (&num, app->out);
            }
          else
            fprintf (app->out, "%llu, %llu, %llu, %llu\n",
                     recs[i].one, recs[i].two, recs[j].one, recs[j].two);
        }
    }
  free (recs);
  mpz_clear (num);
  return 0;
}

static int
fituvalu_combine_pairs_int (struct fv_app_combine_pairs_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;

  unsigned long a[2];
  struct irec *recs = NULL;
  int num_recs = 0;
  mpz_t num;
  mpz_init (num);
  while (1)
    {
      int nread = read_ul_numbers_from_stream (app->infile, a, 2, &line, &len);
      if (nread == -1)
        break;
      recs = realloc (recs, sizeof (struct irec) * (num_recs + 1));
      recs[num_recs].one = a[0];
      recs[num_recs].two = a[1];
      num_recs++;
    }
  free (line);
  for (int i = 0; i < num_recs; i++)
    {
      for (int j = 0; j < num_recs; j++)
        {
          if (i == j)
            continue;
          if (app->out_binary)
            {
              mpz_set_ui (num, recs[i].one);
              display_binary_number (&num, app->out);
              mpz_set_ui (num, recs[i].two);
              display_binary_number (&num, app->out);
              mpz_set_ui (num, recs[j].one);
              display_binary_number (&num, app->out);
              mpz_set_ui (num, recs[j].two);
              display_binary_number (&num, app->out);
            }
          else
            fprintf (app->out, "%lu, %lu, %lu, %lu\n",
                     recs[i].one, recs[i].two, recs[j].one, recs[j].two);
        }
    }
  mpz_clear (num);
  free (recs);
  return 0;
}

static int
fituvalu_combine_pairs_mpz (struct fv_app_combine_pairs_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;

  mpz_t a[2];
  mpz_inits (a[0], a[1], NULL);
  struct rec *recs = NULL;
  int num_recs = 0;
  while (1)
    {
      int nread = app->read_pair (app->infile, a, &line, &len);
      if (nread == -1)
        break;
      recs = realloc (recs, sizeof (struct rec) * (num_recs + 1));
      mpz_init_set (recs[num_recs].one, a[0]);
      mpz_init_set (recs[num_recs].two, a[1]);
      num_recs++;
    }
  free (line);
  mpz_clears (a[0], a[1], NULL);
  for (int i = 0; i < num_recs; i++)
    {
      for (int j = 0; j < num_recs; j++)
        {
          if (i == j)
            continue;
          if (app->out_binary)
            {
              display_binary_number (&recs[i].one, app->out);
              display_binary_number (&recs[i].two, app->out);
              display_binary_number (&recs[j].one, app->out);
              display_binary_number (&recs[j].two, app->out);
            }
          else
            {
              display_textual_number_no_newline (recs[i].one, app->out);
              fprintf (app->out, ", ");
              display_textual_number_no_newline (recs[i].two, app->out);
              fprintf (app->out, ", ");
              display_textual_number_no_newline (recs[j].one, app->out);
              fprintf (app->out, ", ");
              display_textual_number_no_newline (recs[j].two, app->out);
              fprintf (app->out, "\n");
            }
        }
    }
  for (int i = 0; i < num_recs; i++)
    mpz_clears (recs[i].one, recs[i].two, NULL);
  free (recs);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_combine_pairs_t *app = (struct fv_app_combine_pairs_t *) state->input;
  switch (key)
    {
    case OPT_INT:
      app->ul = 1;
      break;
    case 'u':
      app->ull = 1;
      break;
    case 'i':
      app->read_pair = binary_read_two_numbers_from_stream;
      break;
    case 'o':
      app->out_binary = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args > 1)
        argp_error (state, "too many arguments");
      else
        {
          if (strcmp (arg, "-") == 0)
            app->infile = stdin;
          else
            app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading", arg);
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}
static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "int", OPT_INT, 0, 0, "Use unsigned integers instead of GMP"},
  { "ull", 'u', 0, 0, "Use unsigned long longs instead of GMP"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "For every record in FILE combine it with every record in FILE, excepting repeats.\vIf FILE is not specified or it is `-' read the records from the standard input.  Each record is two numbers per row separated by a comma and terminated by a newline.  The `mn-seq' and `pr-seq' programs provide suitable input for FILE.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_combine_pairs_t app;
  memset (&app, 0, sizeof (app));
  app.infile = stdin;
  app.out = stdout;
  app.read_pair = read_two_numbers_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (app.ull)
    return fituvalu_combine_pairs_ull (&app);
  else if (app.ul)
    return fituvalu_combine_pairs_int (&app);
  else
    return fituvalu_combine_pairs_mpz (&app);
}
