/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>

struct fv_app_find_sq_pairs_t
{
  int num_args;
  mpz_t args[3];
  void (*dump_func) (mpz_t *, mpz_t *, FILE *);
};

static void
dump_binary_pair (mpz_t *i, mpz_t *j, FILE *out)
{
  if (mpz_cmp (*i, *j) > 0)
    {
      mpz_out_raw (out, *j);
      mpz_out_raw (out, *i);
    }
  else
    {
      mpz_out_raw (out, *i);
      mpz_out_raw (out, *j);
    }
}

static void
dump_pair (mpz_t *i, mpz_t *j, FILE *out)
{
  if (mpz_cmp (*i, *j) > 0)
    {
      char *buf = malloc (mpz_sizeinbase (*j, 10) + 2);
      mpz_get_str (buf, 10, *j);
      fprintf (out, "%s, ", buf);
      free (buf);
      buf = malloc (mpz_sizeinbase (*i, 10) + 2);
      mpz_get_str (buf, 10, *i);
      fprintf (out, "%s", buf);
      free (buf);
      fprintf (out, "\n");
    }
  else
    {
      char *buf = malloc (mpz_sizeinbase (*i, 10) + 2);
      mpz_get_str (buf, 10, *i);
      fprintf (out, "%s, ", buf);
      free (buf);
      buf = malloc (mpz_sizeinbase (*j, 10) + 2);
      mpz_get_str (buf, 10, *j);
      fprintf (out, "%s", buf);
      free (buf);
      fprintf (out, "\n");
    }
}

static int
inner (struct fv_app_find_sq_pairs_t *app, mpz_t *i, mpz_t finish, FILE *out)
{
  mpz_t j, jroot;
  mpz_inits (j, jroot, NULL);
  mpz_set_ui (j, 4);
  mpz_set_ui (jroot, 2);
  while (1)
    {
      if (mpz_cmp (j, *i) >= 0)
        break;
      app->dump_func (i, &j, out);
      mpz_add (j, j, jroot);
      mpz_add (j, j, jroot);
      mpz_add_ui (j, j, 1);
      mpz_add_ui (jroot, jroot, 1);
    }
  mpz_clears (j, jroot, NULL);
}

static int
find_sq_pairs (struct fv_app_find_sq_pairs_t *app, mpz_t start, mpz_t finish, mpz_t incr, FILE *out)
{
  mpz_t i, root, lastroot, j, jroot;
  mpz_inits (i, root, lastroot, j, jroot, NULL);
  mpz_set (i, start);
  mpz_sqrt (root, i);
  if (mpz_cmp_ui (root, 0) == 0)
    mpz_add_ui (root, root, 1);
  mpz_mul (i, root, root);
  mpz_sqrt (lastroot, finish);

  inner (app, &i, finish, out);

  while (mpz_cmp (root, lastroot) < 0)
    {
      for (int j = 0; mpz_cmp_ui (incr, j) > 0; j++)
        {
          mpz_add (i, i, root);
          mpz_add (i, i, root);
          mpz_add_ui (i, i, 1);
          mpz_add_ui (root, root, 1);
        }
      inner (app, &i, finish, out);
    }
  mpz_clears (i, root, lastroot, j, jroot, NULL);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_find_sq_pairs_t *app = (struct fv_app_find_sq_pairs_t *) state->input;
  switch (key)
    {
    case 'o':
      app->dump_func = dump_binary_pair;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 3)
        argp_error (state, "too many arguments");
      else
        {
          mpz_set_str (app->args[app->num_args], arg, 10);
          app->num_args++;
        }
      break;
    case ARGP_KEY_INIT:
      app->dump_func = dump_pair;
      for (int i = 0; i < 3; i++)
        mpz_init (app->args[i]);
      break;
    case ARGP_KEY_NO_ARGS:
      argp_error (state, "missing argument.");
      break;
    }
  return 0;
}

int
fituvalu_find_sq_pairs (struct fv_app_find_sq_pairs_t *app, FILE *out)
{
  mpz_t one;
  mpz_init (one);
  mpz_set_ui (one, 1);

  int ret = 0;

  switch (app->num_args)
    {
    case 1:
      ret = find_sq_pairs (app, one, app->args[0], one, out);
      break;
    case 2:
      ret = find_sq_pairs (app, app->args[0], app->args[1], one, out);
      break;
    case 3:
      ret = find_sq_pairs (app, app->args[0], app->args[2], app->args[1], out);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }

  mpz_clear (one);
  return ret;
}

static struct argp_option
options[] =
{
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "FIRST\nFIRST LAST\nFIRST INCREMENT LAST",
  "Generate pairs of squares.\vIf FIRST or INCREMENT is omitted, it defaults to 1.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_find_sq_pairs_t app;
  memset (&app, 0, sizeof (app));
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_find_sq_pairs (&app, stdout);

}
