/* Copyright (C) 2019, 2020, 2024 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <argp.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "magicsquareutil.h"

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t display_lock = PTHREAD_MUTEX_INITIALIZER;

struct fv_app_search_631_t
{
  int num_args;
  int threads;
  FILE *infile;
  FILE *out;
  int in_binary;
  int progress;
};

struct fv_app_log_t lg;

struct thread_data_t
{
  void *data;
};

static void
run_threads (void *data, int num_threads, void* (*func)(void*))
{
  int retval;
  pthread_t threads[num_threads];
  struct thread_data_t param[num_threads];
  for (int i = 0; i < num_threads; i++)
    {
      param[i].data = data;
      //run process_perfect_square
      if ((retval = pthread_create (&threads[i], NULL, func, &param[i])))
        {
          fprintf (stderr, "can't create a thread!\n");
          return;
        }
    }
  for (int i = 0; i < num_threads; i++)
    pthread_join (threads[i], NULL);
}

static int
square_is_twelvenminusone (unsigned long long i)
{
  return (i - 1) % 12 == 0;
}
static int
detect (unsigned long long int num, int *advance)
{
  //we return 1 if the next number in the sequence is 4 squares ahead
  unsigned long long int root = sqrtl(num);

  unsigned long long int p1root = root + 2;
  unsigned long long int p2root = root + 4;
  unsigned long long int p1 = p1root * p1root;
  unsigned long long int p2 = p2root * p2root;

  int checkp1 = square_is_twelvenminusone (p1);
  int checkp2 = square_is_twelvenminusone (p2);

  if (checkp1 && !checkp2)
    return 0;
  else if (!checkp1 && checkp2)
    return 1;
  else if (checkp1 && checkp2)
    *advance = 1;
  return 0;
}

/*
  +---+---+---+	  +---+---+---+
  | X |   | X |	  | 2 | 6 | 7 |
  +---+---+---+	  +---+---+---+
  | X |   | X |	  | 9 | 5 | 1 |
  +---+---+---+	  +---+---+---+
  | X |   | X |	  | 3 | 4 | 8 |
  +---+---+---+	  +---+---+---+
  */
static void
search_631 (struct fv_app_search_631_t *app, unsigned long long int n)
{
  //we get 9 for free, then we find 8 and then 6.
  //then we search all squares between 6 and 8 to find 7.
  unsigned long long int diff;
  unsigned long long int s[3][3];
  memset (s, 0, sizeof (s));

  s[1][0] = n;
  unsigned long long int nroot = sqrtl (n);
  unsigned long long int iroot = sqrtl(n/2);
  unsigned long long int i = iroot * iroot;
  unsigned long long int jroot;
  unsigned long long int j;

  while (!square_is_twelvenminusone (i))
    {
      i += (iroot * 2) + 1;
      iroot++;
    }
      
  int advance = 0;
  int iup_by_four = detect (i, &advance);
  if (advance)
    {
      iroot += 4;
      i = iroot * iroot;
      iup_by_four = detect (i, &advance);
    }

  while (iroot < nroot)
    {
      s[2][2] = i;

      diff = n - i;
      s[0][1] = i - diff;

      j = n / 3;
      j *= 2;
      jroot = sqrtl(j);
      j = jroot * jroot;
      while (!square_is_twelvenminusone (j))
        {
          j += (jroot * 2) + 1;
          jroot++;
        }

      advance = 0;
      int jup_by_four = detect (j, &advance);
      if (advance)
        {
          jroot += 4;
          j = jroot * jroot;
          jup_by_four = detect (j, &advance);
        }
      while (j < s[2][2])
        {
          s[0][2] = j;
          s[1][1] = j - diff;
          s[2][0] = s[1][1] - diff;
          if (small_is_square (s[2][0]))
            {
              unsigned long long int sum = s[1][1] * 3; 
              s[0][0] = sum - s[1][0] - s[2][0];
              if (small_is_square (s[0][0]))
                {
                  s[1][2] = sum - s[0][2] - s[2][2];
                  if (small_is_square (s[1][2]))
                    {
                      s[2][1] = sum - s[2][0] - s[2][2];
                      pthread_mutex_lock (&display_lock);
                      fprintf (app->out,
                               "%lld, %lld, %lld, "
                               "%lld, %lld, %lld, "
                               "%lld, %lld, %lld, \n",
                               s[0][0], s[0][1], s[0][2],
                               s[1][0], s[1][1], s[1][2], 
                               s[2][0], s[2][1], s[2][2]);
                      fflush (app->out);
                      pthread_mutex_unlock (&display_lock);
                    }
                }
            }

          if (jup_by_four)
            {
              //advance i by 4 squares
              unsigned long long int eightroot = jroot * 8;
              j += eightroot;
              j+=16;
              jroot+=4;
            }
          else
            {
              //advance i by 2 squares
              unsigned long long int fourroot = jroot * 4;
              j += fourroot;
              j+=4;
              jroot+=2;
            }
          jup_by_four = !jup_by_four;
        }

      if (iup_by_four)
        {
          //advance i by 4 squares
          unsigned long long int eightroot = iroot * 8;
          i += eightroot;
          i+=16;
          iroot+=4;
        }
      else
        {
          //advance i by 2 squares
          unsigned long long int fourroot = iroot * 4;
          i += fourroot;
          i+=4;
          iroot+=2;
        }

      iup_by_four = !iup_by_four;
    }
}
static void*
process_record (void *arg)
{
  char *line = NULL, *end = NULL;
  size_t len = 0;
  unsigned long long n, num;
  struct thread_data_t *param = (struct thread_data_t *) arg;
  struct fv_app_search_631_t *app =
    (struct fv_app_search_631_t *) param->data;

  while (1)
    {
      //go get the next progression to work on
      if (app->threads > 1)
        pthread_mutex_lock (&read_lock);

      if (app->in_binary)
        {
          size_t r = fread (&num, sizeof (num), 1, app->infile);
          if (r == 0)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
        }
      else
        {
          ssize_t read = fv_getline (&line, &len, app->infile);
          if (read == -1)
            {
              if (app->threads > 1)
                pthread_mutex_unlock (&read_lock);
              break;
            }
          num = strtoull (line, &end, 10);
        }
      n = num;
      if (app->progress)
        fv_update_log_ull (&lg, num);
      if (app->threads > 1)
        pthread_mutex_unlock (&read_lock);

      //now we work on n
      search_631 (app, n);
    }

  if (line)
    free (line);
  return NULL;
}

int
fituvalu_search_631 (struct fv_app_search_631_t *app)
{
  run_threads (app, app->threads, process_record);
  return 0;
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_search_631_t *app = (struct fv_app_search_631_t *) state->input;
  switch (key)
    {
    case 'p':
      app->progress = 1;
      break;
    case 'i':
      app->in_binary = 1;
      break;
    case ARGP_KEY_ARG:
      if (app->num_args == 2)
        argp_error (state, "too many arguments");
      else
        {
          app->infile = fopen (arg, "r");
          if (!app->infile)
            argp_error (state, "could not open `%s' for reading");
          app->num_args++;
        }
      break;
    case 't':
      app->threads = atoi (arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static struct argp_option
options[] =
{
  { "threads", 't', "NUM", 0, "Spread the work across NUM threads"},
  { "in-binary", 'i', 0, 0, "Input raw unsigned long longs instead of text"},
  { "progress", 'p', 0, OPTION_HIDDEN, "Show progress information in /tmp"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Find 3x3 magic squares of the type 6:3:1 given a FILE containing perfect squares for cell 1,0.\vWhen FILE is not provided, it is read from the standard input.  This program is limited to 64-bit integers.  Magic squares of type 6:3:1 have the following layout of squares vs non-squares:\n"
"  +---+---+---+   +---+---+---+\n"
"  | X |   | X |   | 2 |   | 7 |   The lowest square is marked 1 and so on.\n"
"  +---+---+---+   +---+---+---+\n"  
"  | X |   | X |   | 9 |   | 1 |\n"
"  +---+---+---+   +---+---+---+\n"
"  | X |   | X |   | 3 |   | 8 |\n"
"  +---+---+---+   +---+---+---+\n",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_search_631_t app;
  memset (&app, 0, sizeof (app));
  app.threads = 1;
  app.infile = stdin;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  if (app.progress)
    fv_init_log (&lg, "search-631-small");
  return fituvalu_search_631 (&app);
}
