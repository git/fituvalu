/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include "magicsquareutil.h"

struct fv_app_mine_sq_t
{
  int (*read_numbers)(FILE *, mpz_t *, int size, char **, size_t *);
  int out_binary;
  FILE *out;
  int num_columns;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_mine_sq_t *app = (struct fv_app_mine_sq_t *) state->input;
  switch (key)
    {
    case 'n':
      app->num_columns = atoi (arg);
      break;
    case 'o':
      app->out_binary = 1;
      break;
    case 'i':
      app->read_numbers = binary_read_numbers_from_stream;
      break;
    }
  return 0;
}

int
fituvalu_mine_sq (struct fv_app_mine_sq_t *app, FILE *in)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  mpz_t vec[app->num_columns];
  for (int i = 0; i < app->num_columns; i++)
    mpz_inits (vec[i], NULL);
  while (1)
    {
      read = app->read_numbers (in, vec, app->num_columns, &line, &len);
      if (read == -1)
        break;
      for (int i = 0; i < app->num_columns; i++)
        {
          if (mpz_perfect_square_p (vec[i]))
            {
              if (app->out_binary)
                mpz_out_raw (app->out, vec[i]);
              else
                display_textual_number (&vec[i], app->out);
              fflush (app->out);
            }
        }
    }

  for (int i = 0; i < app->num_columns; i++)
    mpz_clears (vec[i], NULL);

  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "num-columns", 'n', "COLS", 0, "Instead of reading in 9 numbers read in this many"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept magic squares and output every square.\vThe numbers that comprise the magic square must be separated by a comma and terminated by a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_mine_sq_t app;
  memset (&app, 0, sizeof (app));
  app.read_numbers = read_numbers_from_stream;
  app.out = stdout;
  app.num_columns = 9;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_mine_sq (&app, stdin);
}
