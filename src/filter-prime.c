/* Copyright (C) 2016, 2017, 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <gmp.h>
#include <argp.h>
#include <string.h>
#include <stdlib.h>
#include <argz.h>
#include "magicsquareutil.h"

struct fv_app_check_prime_t
{
  int invert;
  FILE *out;
  int (*read_number) (FILE *, mpz_t *, char **, size_t *);
  void (*dump_func) (mpz_t *, FILE *);
  int negative;
  int pythag;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_check_prime_t *app = (struct fv_app_check_prime_t *) state->input;
  switch (key)
    {
    case 'p':
      app->pythag = 1;
      break;
    case 'a':
      app->negative = 1;
      break;
    case 'i':
      app->read_number = binary_read_one_number_from_stream;
      break;
    case 'o':
      app->dump_func = display_binary_number;
      break;
    case 'v':
      app->invert = !app->invert;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static int
prime_is_fournminusone (mpz_t i)
{
  mpz_t j, r;
  mpz_inits (j, r, NULL);
  mpz_sub_ui (j, i, 1);
  int ret = mpz_mod_ui (r, j, 4);
  mpz_clears (j, r, NULL);
  return ret == 0;
}

int
fituvalu_check_prime (struct fv_app_check_prime_t *app, FILE *in)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  mpz_t n;
  mpz_init (n);
  while (1)
    {
      read = app->read_number (in, &n, &line, &len);
      if (read == -1)
        break;
      if (!app->negative && mpz_cmp_ui (n, 0) < 0)
        continue;
      int is_prime = mpz_probab_prime_p (n, 50);
      if (app->pythag)
        {
          if (!mpz_odd_p (n) || !prime_is_fournminusone (n))
            is_prime = 0;
        }
      if ((is_prime && !app->invert) ||
          !is_prime && app->invert)
        app->dump_func (&n, app->out);
    }
  mpz_clear (n);

  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "invert", 'v', 0, 0, "Show numbers that aren't prime"},
  { "allow-negative", 'a', 0, 0, "Allow negative primes"},
  { "pythagorean", 'p', 0, 0, "Only allow pythagorean primes"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept numbers from the standard input and display them if they're a prime number.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_check_prime_t app;
  memset (&app, 0, sizeof (app));
  app.out = stdout;
  app.dump_func = display_textual_number;
  app.read_number = read_one_number_from_stream;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_check_prime (&app, stdin);
}
