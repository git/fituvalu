/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <unistd.h>
#include "magicsquareutil.h"

struct fv_app_cycle_62_square_t
{
  FILE *infile;
  int numargs;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_square) (FILE *, mpz_t (*a)[3][3], char **, size_t *);
  FILE *out;
};


static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_cycle_62_square_t *app = (struct fv_app_cycle_62_square_t *) state->input;
  switch (key)
    {
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->numargs > 0)
        argp_error (state, "too many arguments");
      app->numargs++;
      app->infile = fopen (arg, "r");
      if (!app->infile)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

static void
generate_613_squares (struct fv_app_cycle_62_square_t *app, mpz_t (*s)[3][3])
{
  mpz_t a[3][3], sum_minus_middle;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (a[i][j]);
  mpz_init (sum_minus_middle);

  mpz_set (a[1][1], (*s)[2][0]);
  mpz_mul_ui (sum_minus_middle, a[1][1], 2);
  mpz_set (a[0][2], (*s)[1][2]);
  mpz_set (a[2][0], (*s)[0][1]);

  mpz_set (a[0][1], (*s)[1][0]);
  mpz_set (a[2][2], (*s)[0][2]);
  mpz_set (a[1][0], (*s)[2][1]);

  mpz_sub (a[1][2], sum_minus_middle, a[1][0]);
  mpz_sub (a[2][1], sum_minus_middle, a[0][1]);
  mpz_sub (a[0][0], sum_minus_middle, a[2][2]);

  if (is_magic_square (a, 1))
    app->display_square (a, app->out);

  mpz_set (a[1][1], (*s)[0][2]);
  mpz_mul_ui (sum_minus_middle, a[1][1], 2);
  mpz_set (a[0][2], (*s)[2][1]);
  mpz_set (a[2][0], (*s)[1][0]);
  mpz_set (a[0][1], (*s)[0][1]);
  mpz_set (a[2][2], (*s)[2][0]);
  mpz_set (a[1][0], (*s)[1][2]);

  mpz_sub (a[1][2], sum_minus_middle, a[1][0]);
  mpz_sub (a[2][1], sum_minus_middle, a[0][1]);
  mpz_sub (a[0][0], sum_minus_middle, a[2][2]);

  if (is_magic_square (a, 1))
    app->display_square (a, app->out);

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
  mpz_clear (sum_minus_middle);
}

int
fituvalu_cycle_62_square (struct fv_app_cycle_62_square_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  mpz_t a[3][3];
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  while (1)
    {
      read = app->read_square (app->infile, &a, &line, &len);
      if (read == -1)
        break;
      if (is_magic_square (a, 1))
        {
          int num_squares;
          int type = square_get_type (&a, &num_squares);
          if (num_squares == 6 && type == 2)
            {
              rotate_six (&a, 2);
              generate_613_squares (app, &a);
            }
        }
    }

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Read in 3x3 magic squares of type 6:2 and cycle the progressions to make 6:13 squares.  If FILE is not specified the magic squares are read from the standard input.\vThis program is only suitable for magic squares of type 6:2.  The nine values must be separated by a comma and terminated with a newline.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_cycle_62_square_t app;
  memset (&app, 0, sizeof (app));
  app.display_square = display_square_record;
  app.read_square =  read_square_from_stream;
  app.infile = stdin;
  app.out = stdout;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  is_magic_square_init ();
  return fituvalu_cycle_62_square (&app);
}
