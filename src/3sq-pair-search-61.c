/* Copyright (C) 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <string.h>
#include <gmp.h>
#include <unistd.h>
#include "magicsquareutil.h"
#include "linecount.h"
int linecount;

struct fv_app_3sq_pair_search_61_t
{
  FILE *infile;
  FILE *out;
  void (*display_square) (mpz_t s[3][3], FILE *out);
  int (*read_tuple) (FILE *, mpz_t *, char **, size_t *);
};

struct rec
{
  mpz_t ap[3];
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_3sq_pair_search_61_t *app = (struct fv_app_3sq_pair_search_61_t *) state->input;
  char *end = NULL;
  switch (key)
    {
    case 'i':
      app->read_tuple = binary_read_three_numbers_from_stream;
      break;
    case 'o':
      app->display_square = display_binary_square_record;
      break;
    case ARGP_KEY_ARG:
      if (app->infile)
        argp_error (state, "too many arguments");
      if (strcmp (arg, "-") == 0)
        app->infile = stdin;
      else
        app->infile = fopen (arg, "r");
      if (!app->infile)
        argp_error (state, "could not open `%s' for reading", arg);
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      app->infile = stdin;
      break;
    }
  return 0;
}

void
dump_square (void *data, mpz_t (*s)[3][3])
{
  struct fv_app_3sq_pair_search_61_t *app = 
    (struct fv_app_3sq_pair_search_61_t *) data;
  app->display_square (*s, app->out);
}

int
fituvalu_3sq_pair_search_61 (struct fv_app_3sq_pair_search_61_t *app)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  struct rec rec;
  mpz_t b[2], diff, s[3][3];
  mpz_inits (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], diff, NULL);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (s[i][j]);
  while (1)
    {
      read = app->read_tuple (app->infile, rec.ap, &line, &len);
      if (read == -1)
        break;
      update_linecount (&linecount);
      mpz_sub (diff, rec.ap[2], rec.ap[0]);
      if (mpz_cmp_ui (diff, 0) > 0)
        search_61_3sq (rec.ap, diff, b, &s, 0, dump_square, app);
    }
  if (line)
    free (line);
  mpz_clears (rec.ap[0], rec.ap[1], rec.ap[2], b[0], b[1], diff, NULL);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (s[i][j]);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, "[FILE]",
  "Try to make a 3x3 magic square with 5 or more perfect squares by giving it a series of 3 square progressions in FILE.  The program iterates over pairs of squares to find two squares that have the correct difference.\vWhen FILE is `-' or isn't specified it is read from the standard input.  Each square is laid out like:\n\
  +------+------+------+\n\
  |      |      |  X3  |\n\
  +------+------+------+\n\
  |      |  X2  |  Y1  |\n\
  +------+------+------+\n\
  |  X1  |  Y2  |      |\n\
  +------+------+------+\nThe 61 in the name comes from the fact we're trying to make a square of the form 6:1, but it can also make 6:13, and 6:11.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_3sq_pair_search_61_t app;
  memset (&app, 0, sizeof (app));
  init_linecount (&linecount);

  app.display_square = display_square_record;
  app.read_tuple = read_three_numbers_from_stream;
  app.infile = stdin;
  app.out = stdout;

  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_3sq_pair_search_61 (&app);
}
