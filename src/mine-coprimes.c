/* Copyright (C) 2020 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <argp.h>
#include <gmp.h>
#include <string.h>
#include "magicsquareutil.h"
struct fv_app_mine_coprime_pairs_t
{
  int (*read_square) (FILE *, mpz_t (*)[3][3], char **, size_t *);
  void (*display_pair) (mpz_t *, mpz_t *, FILE *);
  int allow_negatives;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct fv_app_mine_coprime_pairs_t *app = (struct fv_app_mine_coprime_pairs_t *) state->input;
  int count;
  switch (key)
    {
    case 'a':
      app->allow_negatives = 1;
      break;
    case 'i':
      app->read_square = binary_read_square_from_stream;
      break;
    case 'o':
      app->display_pair = display_binary_two_record;
      break;
    case ARGP_KEY_INIT:
      setenv ("ARGP_HELP_FMT", "no-dup-args-note", 1);
      break;
    }
  return 0;
}

int
fituvalu_mine_coprime_pairs (struct fv_app_mine_coprime_pairs_t *app,  FILE *in, FILE *out)
{
  int i, j;
  mpz_t a[3][3];
  ssize_t read;
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_init (a[i][j]);

  mpz_t b[9];
  for (i = 0; i < 9; i++)
    mpz_init (b[i]);

  char* line = NULL;
  size_t len = 0;
  while (!feof (in))
    {
      read = app->read_square (in, &a, &line, &len);
      if (read == -1)
        break;

      int count = 0;
      for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
          {
            mpz_set (b[count], a[i][j]);
            count++;
          }

      int num_pairs = 0;
      struct mpz_pair *pairs = NULL;

      get_coprime_pairs (b, app->allow_negatives, &pairs, &num_pairs);

      for (i = 0; i < num_pairs; i++)
        app->display_pair  (&pairs[i].num[0], &pairs[i].num[1], out);

      for (i = 0; i < num_pairs; i++)
        mpz_clears (pairs[i].num[0], pairs[i].num[1], NULL);
      free (pairs);

    }
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mpz_clear (a[i][j]);
  for (i = 0; i < 9; i++)
    mpz_clear (b[i]);
  if (line)
    free (line);
  return 0;
}

static struct argp_option
options[] =
{
  { "in-binary", 'i', 0, 0, "Input raw GMP numbers instead of text"},
  { "out-binary", 'o', 0, 0, "Output raw GMP numbers instead of text"},
  { "allow-negative", 'a', 0, 0, "Allow negative numbers"},
  { 0 }
};

static struct argp
argp =
{
  options, parse_opt, 0,
  "Accept 3x3 magic squares from the standard input, and show the pairs of coprimes.\vThe nine values must be separated by commas and terminated by a newline.  A coprime is when the greatest common divisor between two numbers is 1.",
  0
};

int
main (int argc, char **argv)
{
  struct fv_app_mine_coprime_pairs_t app;
  memset (&app, 0, sizeof (app));
  app.read_square = read_square_from_stream;
  app.display_pair = display_two_record;
  argp_parse (&argp, argc, argv, 0, 0, &app);
  return fituvalu_mine_coprime_pairs (&app, stdin, stdout);
}
