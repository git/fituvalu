/* Copyright (C) 2016, 2017 Ben Asselstine
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include "magicsquareutil.h"

void *
morgenstern_per_thread_init()
{
  struct morgenstern_per_thread_data_t *pt;
  pt = (struct morgenstern_per_thread_data_t*) malloc (sizeof (struct morgenstern_per_thread_data_t));
  memset (pt, 0, sizeof (struct morgenstern_per_thread_data_t));
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_init (pt->a[i][j]);
  mpz_inits (pt->x1, pt->_y1, pt->z1, pt->m12, pt->n12, pt->x2, pt->y2, pt->z2,
             pt->m22, pt->n22, pt->yx1dif, pt->yx1sum, pt->yx2dif, pt->yx2sum,
	     NULL);
  pt->prev_type = -1;
  return pt;
}

void
morgenstern_per_thread_destroy (void *p)
{
  struct morgenstern_per_thread_data_t *pt = (struct morgenstern_per_thread_data_t *)p;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mpz_clear (pt->a[i][j]);
  mpz_clears (pt->x1, pt->_y1, pt->z1, pt->m12, pt->n12, pt->x2, pt->y2,
	      pt->z2, pt->m22, pt->n22, pt->yx1dif, pt->yx1sum, pt->yx2dif,
	      pt->yx2sum, NULL);
}

void
morgenstern_precalc_int (unsigned long m1, unsigned long n1, unsigned long m2, unsigned long n2, struct morgenstern_per_thread_data_t *pt)
{
  //where X1 = 2*m1*n1,  Y1 = m1^2-n1^2,  Z1 = m1^2+n1^2,
  mpz_set_ui (pt->x1, m1);
  mpz_mul_ui (pt->x1, pt->x1, n1);
  mpz_mul_ui (pt->x1, pt->x1, 2);

  mpz_set_ui (pt->m12, m1);
  mpz_mul_ui (pt->m12, pt->m12, m1);

  mpz_set_ui (pt->n12, n1);
  mpz_mul_ui (pt->n12, pt->n12, n1);

  mpz_sub (pt->_y1, pt->m12, pt->n12);

  mpz_add (pt->z1, pt->m12, pt->n12);

  mpz_sub (pt->yx1dif, pt->_y1, pt->x1);

  mpz_add (pt->yx1sum, pt->_y1, pt->x1);

  // where X2 = 2*m2*n2,  Y2 = m2^2-n2^2,  Z2 = m2^2+n2^2,
  mpz_set_ui (pt->x2, m2);
  mpz_mul_ui (pt->x2, pt->x2, n2);
  mpz_mul_ui (pt->x2, pt->x2, 2);

  mpz_set_ui (pt->m22, m2);
  mpz_mul_ui (pt->m22, pt->m22, m2);

  mpz_set_ui (pt->n22, n2);
  mpz_mul_ui (pt->n22, pt->n22, n2);

  mpz_sub (pt->y2, pt->m22, pt->n22);

  mpz_add (pt->z2, pt->m22, pt->n22);

  mpz_sub (pt->yx2dif, pt->y2, pt->x2);

  mpz_add (pt->yx2sum, pt->y2, pt->x2);
}

void
morgenstern_precalc (mpz_t m1, mpz_t n1, mpz_t m2, mpz_t n2, struct morgenstern_per_thread_data_t *pt)
{
  //where X1 = 2*m1*n1,  Y1 = m1^2-n1^2,  Z1 = m1^2+n1^2,
  mpz_set (pt->x1, m1);
  mpz_mul (pt->x1, pt->x1, n1);
  mpz_mul_ui (pt->x1, pt->x1, 2);

  mpz_set (pt->m12, m1);
  mpz_mul (pt->m12, pt->m12, m1);

  mpz_set (pt->n12, n1);
  mpz_mul (pt->n12, pt->n12, n1);

  mpz_sub (pt->_y1, pt->m12, pt->n12);

  mpz_add (pt->z1, pt->m12, pt->n12);

  mpz_sub (pt->yx1dif, pt->_y1, pt->x1);

  mpz_add (pt->yx1sum, pt->_y1, pt->x1);

  // where X2 = 2*m2*n2,  Y2 = m2^2-n2^2,  Z2 = m2^2+n2^2,
  mpz_set (pt->x2, m2);
  mpz_mul (pt->x2, pt->x2, n2);
  mpz_mul_ui (pt->x2, pt->x2, 2);

  mpz_set (pt->m22, m2);
  mpz_mul (pt->m22, pt->m22, m2);

  mpz_set (pt->n22, n2);
  mpz_mul (pt->n22, pt->n22, n2);

  mpz_sub (pt->y2, pt->m22, pt->n22);

  mpz_add (pt->z2, pt->m22, pt->n22);

  mpz_sub (pt->yx2dif, pt->y2, pt->x2);

  mpz_add (pt->yx2sum, pt->y2, pt->x2);
}

void
morgenstern_type_1_calc_step_1b (struct morgenstern_per_thread_data_t *pt)
{
/*
    -----------
    I^2  -  C^2
     -  E^2  - 
    G^2  -  A^2
    -----------
         1     
*/
  //I = Z2*(Y1-X1), E = Z2*Z1, A = Z2*(Y1+X1)
    {
      mpz_mul (pt->a[2][2], pt->z2, pt->yx1dif);
      mpz_mul (pt->a[2][2], pt->a[2][2], pt->a[2][2]);
    }

    {
      mpz_mul (pt->a[1][1], pt->z2, pt->z1);
      mpz_mul (pt->a[1][1], pt->a[1][1], pt->a[1][1]);
    }

    {
      mpz_mul (pt->a[0][0], pt->z2, pt->yx1sum);
      mpz_mul (pt->a[0][0], pt->a[0][0], pt->a[0][0]);
    }


  //G = (Y2-X2)*Z1, E = Z2*Z1, C = (Y2+X2)*Z1.
    {
      mpz_mul (pt->a[0][2], pt->z1, pt->yx2dif);
      mpz_mul (pt->a[0][2], pt->a[0][2], pt->a[0][2]);
    }

    {
      mpz_mul (pt->a[2][0], pt->z1, pt->yx2sum);
      mpz_mul (pt->a[2][0], pt->a[2][0], pt->a[2][0]);
    }

}
void
morgenstern_type_1_calc_step_1 (struct morgenstern_per_thread_data_t *pt)
{
/*
    -----------
    A^2  -  C^2
     -  E^2  - 
    G^2  -  I^2
    -----------
         1     
*/
  //I = Z2*(Y1-X1), E = Z2*Z1, A = Z2*(Y1+X1)
  if (pt->prev_type != 3 && pt->prev_type != 4 && pt->prev_type != 7)
    {
      mpz_mul (pt->a[2][2], pt->z2, pt->yx1dif);
      mpz_mul (pt->a[2][2], pt->a[2][2], pt->a[2][2]);
    }

  if (pt->prev_type != 2 && pt->prev_type != 3 && pt->prev_type != 8)
    {
      mpz_mul (pt->a[1][1], pt->z2, pt->z1);
      mpz_mul (pt->a[1][1], pt->a[1][1], pt->a[1][1]);
    }

  if (pt->prev_type != 3 && pt->prev_type != 6)
    {
      mpz_mul (pt->a[0][0], pt->z2, pt->yx1sum);
      mpz_mul (pt->a[0][0], pt->a[0][0], pt->a[0][0]);
    }


  //G = (Y2-X2)*Z1, E = Z2*Z1, C = (Y2+X2)*Z1.
  if (pt->prev_type != 4 && pt->prev_type != 6 && pt->prev_type != 8)
    {
      mpz_mul (pt->a[2][0], pt->z1, pt->yx2dif);
      mpz_mul (pt->a[2][0], pt->a[2][0], pt->a[2][0]);
    }

  if (pt->prev_type != 7 && pt->prev_type != 8)
    {
      mpz_mul (pt->a[0][2], pt->z1, pt->yx2sum);
      mpz_mul (pt->a[0][2], pt->a[0][2], pt->a[0][2]);
    }

}

void
morgenstern_type_2_calc_step_1 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     -  B^2  - 
     D^2 E^2 F^2
     -  H^2  - 
     -----------
     2     
     */
  //E = Z1 * Z2;
  //H = YX1sum * Z2;
  //B = YX1dif * Z2;
  //F = YX2sum * Z1;
  //D = YX2dif * Z1;
  
  if (pt->prev_type != 1 && pt->prev_type != 3 && pt->prev_type != 8)
    {
      mpz_mul (pt->a[1][1], pt->z1, pt->z2);
      mpz_mul (pt->a[1][1], pt->a[1][1], pt->a[1][1]);
    }

  mpz_mul (pt->a[2][1], pt->yx1sum, pt->z2);
  mpz_mul (pt->a[2][1], pt->a[2][1], pt->a[2][1]);

  mpz_mul (pt->a[0][1], pt->yx1dif, pt->z2);
  mpz_mul (pt->a[0][1], pt->a[0][1], pt->a[0][1]);

  mpz_mul (pt->a[1][2], pt->yx2sum, pt->z1);
  mpz_mul (pt->a[1][2], pt->a[1][2], pt->a[1][2]);

  mpz_mul (pt->a[1][0], pt->yx2dif, pt->z1);
  mpz_mul (pt->a[1][0], pt->a[1][0], pt->a[1][0]);

}

void
morgenstern_type_3_calc_step_1 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     A^2 B^2  - 
     -  E^2  - 
     -  H^2 I^2
     -----------
     3     
     */
  // E = Z1 * Z2;
  // A = YX1sum * Z2;
  // I = YX1dif * Z2;
  // H = YX2sum * Z1;
  // B = YX2dif * Z1;

  if (pt->prev_type != 1 && pt->prev_type != 2 && pt->prev_type != 8)
    {
      mpz_mul (pt->a[1][1], pt->z1, pt->z2);
      mpz_mul (pt->a[1][1], pt->a[1][1], pt->a[1][1]);
    }

  if (pt->prev_type != 1 && pt->prev_type != 6)
    {
      mpz_mul (pt->a[0][0], pt->yx1sum, pt->z2);
      mpz_mul (pt->a[0][0], pt->a[0][0], pt->a[0][0]);
    }

  if (pt->prev_type != 1 && pt->prev_type != 4 && pt->prev_type != 7)
    {
      mpz_mul (pt->a[2][2], pt->yx1dif, pt->z2);
      mpz_mul (pt->a[2][2], pt->a[2][2], pt->a[2][2]);
    }

  mpz_mul (pt->a[2][1], pt->yx2sum, pt->z1);
  mpz_mul (pt->a[2][1], pt->a[2][1], pt->a[2][1]);

  mpz_mul (pt->a[0][1], pt->yx2dif, pt->z1);
  mpz_mul (pt->a[0][1], pt->a[0][1], pt->a[0][1]);

}

void
morgenstern_type_4_calc_step_1 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     -  B^2  - 
     D^2  -  F^2
     G^2  -  I^2
     -----------
     4     
     */
  // G = Z1 * YX2dif;
  // B = YX1dif * YX2dif;
  // F = YX1sum * YX2dif;
  // I = YX1dif * Z2;
  // D = YX1dif * YX2sum;
  if (pt->prev_type != 1 && pt->prev_type != 6 && pt->prev_type != 8)
    {
      mpz_mul (pt->a[2][0], pt->z1, pt->yx2dif);
      mpz_mul (pt->a[2][0], pt->a[2][0], pt->a[2][0]);
    }

  if (pt->prev_type != 6 && pt->prev_type != 7)
    {
      mpz_mul (pt->a[0][1], pt->yx1dif, pt->yx2dif);
      mpz_mul (pt->a[0][1], pt->a[0][1], pt->a[0][1]);
    }

  if (pt->prev_type != 6)
    {
      mpz_mul (pt->a[1][2], pt->yx1sum, pt->yx2dif);
      mpz_mul (pt->a[1][2], pt->a[1][2], pt->a[1][2]);
    }

  if (pt->prev_type != 1 && pt->prev_type != 3 && pt->prev_type != 7)
    {
      mpz_mul (pt->a[2][2], pt->yx1dif, pt->z2);
      mpz_mul (pt->a[2][2], pt->a[2][2], pt->a[2][2]);
    }

  if (pt->prev_type != 7)
    {
      mpz_mul (pt->a[1][0], pt->yx1dif, pt->yx2sum);
      mpz_mul (pt->a[1][0], pt->a[1][0], pt->a[1][0]);
    }

}

void
morgenstern_type_5_calc_step_1 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     A^2  -  C^2
     D^2  -  F^2
     -  H^2  - 
     -----------
     5     
     */
  // A = Z1 * YX2sum;
  // F = YX1dif * YX2sum;
  // H = YX1sum * YX2sum;
  // C = YX1sum * Z2;
  // D = YX1sum * YX2dif;
  mpz_mul (pt->a[0][0], pt->z1, pt->yx2sum);
  mpz_mul (pt->a[0][0], pt->a[0][0], pt->a[0][0]);

  mpz_mul (pt->a[1][2], pt->yx1dif, pt->yx2sum);
  mpz_mul (pt->a[1][2], pt->a[1][2], pt->a[1][2]);

  if (pt->prev_type != 6 && pt->prev_type != 7)
    {
      mpz_mul (pt->a[2][1], pt->yx1sum, pt->yx2sum);
      mpz_mul (pt->a[2][1], pt->a[2][1], pt->a[2][1]);
    }

  mpz_mul (pt->a[0][2], pt->yx1sum, pt->z2);
  mpz_mul (pt->a[0][2], pt->a[0][2], pt->a[0][2]);

  mpz_mul (pt->a[1][0], pt->yx1sum, pt->yx2dif);
  mpz_mul (pt->a[1][0], pt->a[1][0], pt->a[1][0]);

}

void
morgenstern_type_6_calc_step_1 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     A^2 B^2  - 
     -   -  F^2
     G^2 H^2  - 
     -----------
     6     
     */
  // G = Z1 * YX2dif;
  // B = YX1dif * YX2dif;
  // F = YX1sum * YX2dif;
  // A = YX1sum * Z2;
  // H = Yx1sum * YX2sum;
  if (pt->prev_type != 1 && pt->prev_type != 4 && pt->prev_type != 8)
    {
      mpz_mul (pt->a[2][0], pt->z1, pt->yx2dif);
      mpz_mul (pt->a[2][0], pt->a[2][0], pt->a[2][0]);
    }

  if (pt->prev_type != 4 && pt->prev_type != 7)
    {
      mpz_mul (pt->a[0][1], pt->yx1dif, pt->yx2dif);
      mpz_mul (pt->a[0][1], pt->a[0][1], pt->a[0][1]);
    }

  if (pt->prev_type != 4)
    {
      mpz_mul (pt->a[1][2], pt->yx1sum, pt->yx2dif);
      mpz_mul (pt->a[1][2], pt->a[1][2], pt->a[1][2]);
    }

  if (pt->prev_type != 1 && pt->prev_type != 3)
    {
      mpz_mul (pt->a[0][0], pt->yx1sum, pt->z2);
      mpz_mul (pt->a[0][0], pt->a[0][0], pt->a[0][0]);
    }

  if (pt->prev_type != 5 && pt->prev_type != 7)
    {
      mpz_mul (pt->a[2][1], pt->yx1sum, pt->yx2sum);
      mpz_mul (pt->a[2][1], pt->a[2][1], pt->a[2][1]);
    }

}

void
morgenstern_type_7_calc_step_1 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     -  B^2 C^2
     D^2  -   - 
     -  H^2 I^2
     -----------
     7     
     */
  // I = Z2 * YX1dif;
  // B = YX2dif * YX1dif;
  // D = YX2sum * YX1dif;
  // C = YX2sum * Z1;
  // H = YX2sum * YX1sum;
  if (pt->prev_type != 1 && pt->prev_type != 3 && pt->prev_type != 4)
    {
      mpz_mul (pt->a[2][2], pt->z2, pt->yx1dif);
      mpz_mul (pt->a[2][2], pt->a[2][2], pt->a[2][2]);
    }

  if (pt->prev_type != 4 && pt->prev_type != 6)
    {
      mpz_mul (pt->a[0][1], pt->yx2dif, pt->yx1dif);
      mpz_mul (pt->a[0][1], pt->a[0][1], pt->a[0][1]);
    }

  if (pt->prev_type != 4)
    {
      mpz_mul (pt->a[1][0], pt->yx2sum, pt->yx1dif);
      mpz_mul (pt->a[1][0], pt->a[1][0], pt->a[1][0]);
    }

  if (pt->prev_type != 1 && pt->prev_type != 8)
    {
      mpz_mul (pt->a[0][2], pt->yx2sum, pt->z1);
      mpz_mul (pt->a[0][2], pt->a[0][2], pt->a[0][2]);
    }

  if (pt->prev_type != 5 && pt->prev_type != 6)
    {
      mpz_mul (pt->a[2][1], pt->yx2sum, pt->yx1sum);
      mpz_mul (pt->a[2][1], pt->a[2][1], pt->a[2][1]);
    }

}

void
morgenstern_type_8_calc_step_1 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     -  B^2 C^2
     -  E^2  - 
     G^2 H^2  - 
     -----------
     8     
     */
  // E = Z1 * Z2;
  // C = YX2sum * Z1;
  // G = YX2dif * Z1;
  // H = YX1sum * Z2;
  // B = YX1dif * Z2;
  /*
  mpz_mul (pt->a[1][1], pt->z1, pt->z2);
  mpz_mul (pt->a[1][1], pt->a[1][1], pt->a[1][1]);
  mpz_mul (pt->a[0][2], pt->yx2sum, pt->z1);
  mpz_mul (pt->a[0][2], pt->a[0][2], pt->a[0][2]);
  mpz_mul (pt->a[2][0], pt->yx2dif, pt->z1);
  mpz_mul (pt->a[2][0], pt->a[2][0], pt->a[2][0]);
  mpz_mul (pt->a[2][1], pt->yx1sum, pt->z2);
  mpz_mul (pt->a[2][1], pt->a[2][1], pt->a[2][1]);
  mpz_mul (pt->a[0][1], pt->yx1dif, pt->z2);
  mpz_mul (pt->a[0][1], pt->a[0][1], pt->a[0][1]);
  */
  /*
     -----------
     -  B^2 C^2
     -  E^2  - 
     G^2 H^2  - 
     -----------
     8     
     */
  // E = Z1 * Z2;
  // C = YX2sum * Z1;
  // G = YX2dif * Z1;
  // H = YX1dif * Z2;
  // B = YX1sum * Z2;

  if (pt->prev_type != 1 && pt->prev_type != 2 && pt->prev_type != 3)
    {
      mpz_mul (pt->a[1][1], pt->z1, pt->z2);
      mpz_mul (pt->a[1][1], pt->a[1][1], pt->a[1][1]);
    }

  if (pt->prev_type != 1 && pt->prev_type != 7)
    {
      mpz_mul (pt->a[0][2], pt->yx2sum, pt->z1);
      mpz_mul (pt->a[0][2], pt->a[0][2], pt->a[0][2]);
    }

  if (pt->prev_type != 1 && pt->prev_type != 4 && pt->prev_type != 6)
    {
      mpz_mul (pt->a[2][0], pt->yx2dif, pt->z1);
      mpz_mul (pt->a[2][0], pt->a[2][0], pt->a[2][0]);
    }

  mpz_mul (pt->a[2][1], pt->yx1dif, pt->z2);
  mpz_mul (pt->a[2][1], pt->a[2][1], pt->a[2][1]);

  mpz_mul (pt->a[0][1], pt->yx1sum, pt->z2);
  mpz_mul (pt->a[0][1], pt->a[0][1], pt->a[0][1]);
}

void
morgenstern_type_1_calc_step_2 (struct morgenstern_per_thread_data_t *pt)
{
/*
    -----------
    A^2  -  C^2
     -  E^2  - 
    G^2  -  I^2
    -----------
*/

  //B^2 = G^2 + I^2 - E^2;
  //D^2 = C^2 + I^2 - E^2;
  //F^2 = G^2 + A^2 - E^2;
  //H^2 = C^2 + A^2 - E^2;
  mpz_add (pt->a[0][1], pt->a[2][0], pt->a[2][2]);
  mpz_sub (pt->a[0][1], pt->a[0][1], pt->a[1][1]);

  mpz_add (pt->a[1][0], pt->a[0][2], pt->a[2][2]);
  mpz_sub (pt->a[1][0], pt->a[1][0], pt->a[1][1]);

  mpz_add (pt->a[1][2], pt->a[2][0], pt->a[0][0]);
  mpz_sub (pt->a[1][2], pt->a[1][2], pt->a[1][1]);

  mpz_add (pt->a[2][1], pt->a[0][2], pt->a[0][0]);
  mpz_sub (pt->a[2][1], pt->a[2][1], pt->a[1][1]);
}

void
morgenstern_type_2_calc_step_2 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     -  B^2  - 
     D^2 E^2 F^2
     -  H^2  - 
     -----------
     */

  //A^2 = (F^2 + H^2) / 2;
  //C^2 = (D^2 + H^2) / 2;
  //G^2 = (F^2 + B^2) / 2;
  //I^2 = (D^2 + B^2) / 2;
  mpz_add (pt->a[0][0], pt->a[1][2], pt->a[2][1]);
  mpz_cdiv_q_ui (pt->a[0][0], pt->a[0][0], 2);
  mpz_add (pt->a[0][2], pt->a[1][0], pt->a[2][1]);
  mpz_cdiv_q_ui (pt->a[0][2], pt->a[0][2], 2);
  mpz_add (pt->a[2][0], pt->a[1][2], pt->a[0][1]);
  mpz_cdiv_q_ui (pt->a[2][0], pt->a[2][0], 2);
  mpz_add (pt->a[2][2], pt->a[1][0], pt->a[0][1]);
  mpz_cdiv_q_ui (pt->a[2][2], pt->a[2][2], 2);
}

void
morgenstern_type_3_calc_step_2 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     A^2 B^2  - 
     -  E^2  - 
     -  H^2 I^2
     -----------
     */

  // C^2 = H^2 + E^2 - A^2;
  // D^2 = H^2 + I^2 - A^2;
  // F^2 = B^2 + A^2 - I^2;
  // G^2 = B^2 + E^2 - I^2;
  mpz_add (pt->a[0][2], pt->a[2][1], pt->a[1][1]);
  mpz_sub (pt->a[0][2], pt->a[0][2], pt->a[0][0]);
  mpz_add (pt->a[1][0], pt->a[2][1], pt->a[2][2]);
  mpz_sub (pt->a[1][0], pt->a[1][0], pt->a[0][0]);
  mpz_add (pt->a[1][2], pt->a[0][1], pt->a[0][0]);
  mpz_sub (pt->a[1][2], pt->a[1][2], pt->a[2][2]);
  mpz_add (pt->a[2][0], pt->a[0][1], pt->a[1][1]);
  mpz_sub (pt->a[2][0], pt->a[2][0], pt->a[2][2]);
}

void
morgenstern_type_4_calc_step_2 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     -  B^2  - 
     D^2  -  F^2
     G^2  -  I^2
     -----------
     */

  // A^2 = F^2 + I^2 - B^2;
  // C^2 = D^2 + G^2 - B^2;
  // H^2 = D^2 + F^2 - B^2;
  // E^2 = A^2 + C^2 - H^2;
  mpz_add (pt->a[0][0], pt->a[1][2], pt->a[2][2]);
  mpz_sub (pt->a[0][0], pt->a[0][0], pt->a[0][1]);

  mpz_add (pt->a[0][2], pt->a[1][0], pt->a[2][0]);
  mpz_sub (pt->a[0][2], pt->a[0][2], pt->a[0][1]);

  mpz_add (pt->a[2][1], pt->a[1][0], pt->a[1][2]);
  mpz_sub (pt->a[2][1], pt->a[2][1], pt->a[0][1]);

  mpz_add (pt->a[1][1], pt->a[0][0], pt->a[0][2]);
  mpz_sub (pt->a[1][1], pt->a[1][1], pt->a[2][1]);
}

void
morgenstern_type_5_calc_step_2 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     A^2  -  C^2
     D^2  -  F^2
     -  H^2  - 
     -----------
     */

  // B^2 = D^2 + F^2 - H^2;
  // G^2 = C^2 + F^2 - H^2;
  // I^2 = D^2 + A^2 - H^2;
  // E^2 = G^2 + I^2 - B^2;
  mpz_add (pt->a[0][1], pt->a[1][0], pt->a[1][2]);
  mpz_sub (pt->a[0][1], pt->a[0][1], pt->a[2][1]);

  mpz_add (pt->a[2][0], pt->a[0][2], pt->a[1][2]);
  mpz_sub (pt->a[2][0], pt->a[2][0], pt->a[2][1]);

  mpz_add (pt->a[2][2], pt->a[1][0], pt->a[0][0]);
  mpz_sub (pt->a[2][2], pt->a[2][2], pt->a[2][1]);

  mpz_add (pt->a[1][1], pt->a[2][0], pt->a[2][2]);
  mpz_sub (pt->a[1][1], pt->a[1][1], pt->a[0][1]);
}

void
morgenstern_type_6_calc_step_2 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     A^2 B^2  - 
     -   -  F^2
     G^2 H^2  - 
     -----------
     */

  // I^2 = B^2 + A^2 - F^2;
  // D^2 = B^2 + H^2 - F^2;
  // C^2 = H^2 + G^2 - F^2;
  // E^2 = C^2 + I^2 - D^2;
  mpz_add (pt->a[2][2], pt->a[0][1], pt->a[0][0]);
  mpz_sub (pt->a[2][2], pt->a[2][2], pt->a[1][2]);

  mpz_add (pt->a[1][0], pt->a[0][1], pt->a[2][1]);
  mpz_sub (pt->a[1][0], pt->a[1][0], pt->a[1][2]);

  mpz_add (pt->a[0][2], pt->a[2][1], pt->a[2][0]);
  mpz_sub (pt->a[0][2], pt->a[0][2], pt->a[1][2]);

  mpz_add (pt->a[1][1], pt->a[0][2], pt->a[2][2]);
  mpz_sub (pt->a[1][1], pt->a[1][1], pt->a[1][0]);
}

void
morgenstern_type_7_calc_step_2 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     -  B^2 C^2
     D^2  -   - 
     -  H^2 I^2
     -----------
     */

  // G^2 = B^2 + C^2 - D^2;
  // F^2 = B^2 + H^2 - D^2;
  // A^2 = H^2 + I^2 - D^2;
  // E^2 = A^2 + G^2 - F^2;
  mpz_add (pt->a[2][0], pt->a[0][1], pt->a[0][2]);
  mpz_sub (pt->a[2][0], pt->a[2][0], pt->a[1][0]);

  mpz_add (pt->a[1][2], pt->a[0][1], pt->a[2][1]);
  mpz_sub (pt->a[1][2], pt->a[1][2], pt->a[1][0]);

  mpz_add (pt->a[0][0], pt->a[2][1], pt->a[2][2]);
  mpz_sub (pt->a[0][0], pt->a[0][0], pt->a[1][0]);

  mpz_add (pt->a[1][1], pt->a[0][0], pt->a[2][0]);
  mpz_sub (pt->a[1][1], pt->a[1][1], pt->a[1][2]);
}

void
morgenstern_type_8_calc_step_2 (struct morgenstern_per_thread_data_t *pt)
{
  /*
     -----------
     -  B^2 C^2
     -  E^2  - 
     G^2 H^2  - 
     -----------
     8     
     */

  // A^2 = H^2 + E^2 - C^2;
  // F^2 = H^2 + G^2 - C^2;
  // D^2 = B^2 + C^2 - G^2;
  // I^2 = B^2 + E^2 - G^2;
  mpz_add (pt->a[0][0], pt->a[2][1], pt->a[1][1]);
  mpz_sub (pt->a[0][0], pt->a[0][0], pt->a[0][2]);
  mpz_add (pt->a[1][2], pt->a[2][1], pt->a[2][0]);
  mpz_sub (pt->a[1][2], pt->a[1][2], pt->a[0][2]);
  mpz_add (pt->a[1][0], pt->a[0][1], pt->a[0][2]);
  mpz_sub (pt->a[1][0], pt->a[1][0], pt->a[2][0]);
  mpz_add (pt->a[2][2], pt->a[0][1], pt->a[1][1]);
  mpz_sub (pt->a[2][2], pt->a[2][2], pt->a[2][0]);
}

int
morgenstern_type_1_filter (mpz_t a[3][3], int num_squares)
{
  if (!is_distinct (a))
    return 0;
  int count = 5;
  if (count >= num_squares)
    return 1;
  if (mpz_perfect_square_p (a[0][1]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][1]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  return 0;
}

int
morgenstern_type_2_filter (mpz_t a[3][3], int num_squares)
{
  if (!is_distinct (a))
    return 0;
  int count = 5;
  if (count >= num_squares)
    return 1;
  if (mpz_perfect_square_p (a[0][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[0][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  return 0;
}

int
morgenstern_type_3_filter (mpz_t a[3][3], int num_squares)
{
  if (!is_distinct (a))
    return 0;
  int count = 5;
  if (count >= num_squares)
    return 1;
  if (mpz_perfect_square_p (a[0][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  return 0;
}

int
morgenstern_type_4_filter (mpz_t a[3][3], int num_squares)
{
  if (!is_distinct (a))
    return 0;
  int count = 5;
  if (count >= num_squares)
    return 1;
  if (mpz_perfect_square_p (a[0][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[0][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][1]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][1]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  return 0;
}

int
morgenstern_type_5_filter (mpz_t a[3][3], int num_squares)
{
  if (!is_distinct (a))
    return 0;
  int count = 5;
  if (count >= num_squares)
    return 1;
  if (mpz_perfect_square_p (a[0][1]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][1]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  return 0;
}

int
morgenstern_type_6_filter (mpz_t a[3][3], int num_squares)
{
  if (!is_distinct (a))
    return 0;
  int count = 5;
  if (count >= num_squares)
    return 1;
  if (mpz_perfect_square_p (a[0][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][1]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  return 0;
}

int
morgenstern_type_7_filter (mpz_t a[3][3], int num_squares)
{
  if (!is_distinct (a))
    return 0;
  int count = 5;
  if (count >= num_squares)
    return 1;
  if (mpz_perfect_square_p (a[0][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][1]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  return 0;
}

int
morgenstern_type_8_filter (mpz_t a[3][3], int num_squares)
{
  if (!is_distinct (a))
    return 0;
  int count = 5;
  if (count >= num_squares)
    return 1;
  if (mpz_perfect_square_p (a[0][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][0]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[1][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  if (mpz_perfect_square_p (a[2][2]))
    {
      count++;
      if (count >= num_squares)
        return 1;
    }
  return 0;
}

//best types to preceed 1: 8 (3), 3 (3), 4 (2), 7 (2), 6 (2), 2 (1)
//best types to preceed 2: 1 (1), 3 (1), 8 (1)
//best types to preceed 3: 1 (3), 2 (1), 4 (1), 6 (1), 7 (1), 8 (1)
//best types to preceed 4: 6 (3), 7 (3), 1 (2), 3 (1), 8 (1)
//best types to preceed 5: 6 (1), 7 (1)
//best types to preceed 6: 4 (3), 7 (2), 2 (1), 8 (1), 5 (1), 3 (1)
//best types to preceed 7: 4 (3), 6 (2), 1 (2), 3 (1), 8 (1), 5 (1)
//best types to preceed 8: 1 (3), 2 (1), 3 (1), 4 (1), 6 (1), 7 (1)
